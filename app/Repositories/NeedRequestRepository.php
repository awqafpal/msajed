<?php


namespace App\Repositories;
use App\Models\MosqueNeedRequest;
use App\Repositories\Interfaces\NeedRequestRepositoryInterface;
use Illuminate\Support\Arr;

/** 
 * Class NeedRequestRepository
 * @property MosqueNeedRequest $needReq
 * @package App\Repositories 
 */
class NeedRequestRepository implements NeedRequestRepositoryInterface
{
    /**
     * NeedRequestRepository constructor.
     */
    function __construct()
    {
        $this->needReq = new MosqueNeedRequest();
    }
    /**
     * @param $id
     * @return mixed
     */
    public function get($id)
    {
        return  $this->needReq->find($id);
    }
    /**
     * Get's all MosqueNeedRequest
     *
     * @return mixed
     */
    public function all()
    {
        return  $this->needReq->all();
    }

    /**
     * Deletes a MosqueNeedRequest.
     *
     * @param int
     * @return int
     */
    public function delete($id)
    {
        return  $this->needReq->destroy($id);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function store(array $data)
    {
        return  $this->needReq->create($data);
    }

    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function update($id, array $data)
    {
        return  $this->needReq->find($id)->update($data);
    }


    /**
     * @param array $data
     * @return $this|\Illuminate\Database\Eloquent\Builder|mixed
     */
    public function allDataTable(array $data)
    {
        $query =  $this->needReq;
        $skip = 0;
        $take = 25;

        if(Arr::exists($data,"request_type") && !is_null($data['request_type']))
        {
            $query = $query->where('request_type', 'LIKE', '%' . $data['request_type']. '%');
        }
        

        return $query->skip($skip)->take($take);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function countDataTable(array $data)
    {
        $query =  $this->needReq;

        if(Arr::exists($data,"request_type") && !is_null($data['request_type']))
        {
            $query = $query->where('request_type', 'LIKE', '%' . $data['request_type']. '%');
        }
        

        return $query->count('id');
    }

}

