<?php


namespace App\Repositories;
use App\Models\Region;
use App\Repositories\Interfaces\RegionRepositoryInterface;
use Illuminate\Support\Arr;

/**
 * Class RegionRepository
 * @property Region $region
 * @package App\Repositories
 */
class RegionRepository implements RegionRepositoryInterface
{
    /**
     * RegionRepository constructor.
     */
    function __construct()
    {
        $this->region = new Region();
    }
    /**
     * @param $id
     * @return mixed
     */
    public function get($id)
    {
        return $this->region->find($id);
    }
    /**
     * Get's all regions
     *
     * @return mixed
     */
    public function all()
    {
        return $this->region->all();
    }

    /**
     * Deletes a region.
     *
     * @param int
     * @return int
     */
    public function delete($id)
    {
        return $this->region->destroy($id);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function store(array $data)
    {
        return $this->region->create($data);
    }

    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function update($id, array $data)
    {
        return $this->region->find($id)->update($data);
    }

    /**
     * @param $province_id
     * @return mixed
     */
    public function provinceRegionList($province_id)
    {
        return $this->region->where('province_id',$province_id)->get();
    }

    /**
     * @param array $data
     * @return $this|\Illuminate\Database\Eloquent\Builder|mixed
     */
    public function allDataTable(array $data)
    {
        $query = $this->region;
        $skip = 0;
        $take = 25;

        if(Arr::exists($data,"name") && !is_null($data['name']))
        {
            $query = $query->where('name', 'LIKE', '%' . $data['name']. '%');
        }
        if(Arr::exists($data,"province_id") && !is_null($data['province_id']))
        {
            $query = $query->where('province_id', '=', $data['province_id']);
        }


        return $query->skip($skip)->take($take);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function countDataTable(array $data)
    {
        $query = $this->region;

        if(Arr::exists($data,"name") && !is_null($data['name']))
        {
            $query = $query->where('name', 'LIKE', '%' . $data['name']. '%');
        }
        if(Arr::exists($data,"province_id") && !is_null($data['province_id']))
        {
            $query = $query->where('province_id', '=', $data['province_id']);
        }

        return $query->count('id');
    }

}

