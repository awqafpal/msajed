<?php


namespace App\Repositories;
use App\Models\Property;
use App\Repositories\Interfaces\PropertyRepositoryInterface;
use Illuminate\Support\Arr;

/**
 * Class PropertyRepository
 * @property Property $property
 * @package App\Repositories
 */
class PropertyRepository implements PropertyRepositoryInterface
{
    /**
     * PropertyRepository constructor.
     */
    function __construct()
    {
        $this->property = new Property();
    }
    /**
     * @param $id
     * @return mixed
     */
    public function get($id)
    {
        return $this->property->find($id);
    }
    /**
     * Get's all properties
     *
     * @return mixed
     */
    public function all()
    {
        return $this->property->all();
    }

    /**
     * Deletes a property.
     *
     * @param int
     * @return int
     */
    public function delete($id)
    {
        return $this->property->destroy($id);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function store(array $data)
    {
        return $this->property->create($data);
    }

    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function update($id, array $data)
    {
        return $this->property->find($id)->update($data);
    }


    /**
     * @param array $data
     * @return $this|\Illuminate\Database\Eloquent\Builder|mixed
     */
    public function allDataTable(array $data)
    {
        $query = $this->property;
        $skip = 0;
        $take = 25;

        if(Arr::exists($data,"name") && !is_null($data['name']))
        {
            $query = $query->where('name', 'LIKE', '%' . $data['name']. '%');
        }
        

        return $query->skip($skip)->take($take);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function countDataTable(array $data)
    {
        $query = $this->property;

        if(Arr::exists($data,"name") && !is_null($data['name']))
        {
            $query = $query->where('name', 'LIKE', '%' . $data['name']. '%');
        }
        

        return $query->count('id');
    }

}

