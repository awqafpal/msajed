<?php


namespace App\Repositories;
use App\Models\AlternativeEnergy;
use App\Repositories\Interfaces\AlternativeEnergyRepositoryInterface;
use Illuminate\Support\Arr;

/**
 * Class AlternativeEnergyRepository
 * @property AlternativeEnergy $energy
 * @package App\Repositories
 */
class AlternativeEnergyRepository implements AlternativeEnergyRepositoryInterface
{
    /**
     * AlternativeEnergyRepository constructor.
     */
    function __construct()
    {
        $this->energy = new AlternativeEnergy();
    }
    /**
     * @param $id
     * @return mixed
     */
    public function get($id)
    {
        return $this->energy->find($id);
    }
    /**
     * Get's all energy
     *
     * @return mixed
     */
    public function all()
    {
        return $this->energy->all();
    }

    /**
     * Deletes a energy.
     *
     * @param int
     * @return int
     */
    public function delete($id)
    {
        return $this->energy->destroy($id);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function store(array $data)
    {
        return $this->energy->create($data);
    }

    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function update($id, array $data)
    {
        return $this->energy->find($id)->update($data);
    }


    /**
     * @param array $data
     * @return $this|\Illuminate\Database\Eloquent\Builder|mixed
     */
    public function allDataTable(array $data)
    {
        $query = $this->energy;
        $skip = 0;
        $take = 25;

        if(Arr::exists($data,"name") && !is_null($data['name']))
        {
            $query = $query->where('name', 'LIKE', '%' . $data['name']. '%');
        }
        

        return $query->skip($skip)->take($take);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function countDataTable(array $data)
    {
        $query = $this->energy;

        if(Arr::exists($data,"name") && !is_null($data['name']))
        {
            $query = $query->where('name', 'LIKE', '%' . $data['name']. '%');
        }
        

        return $query->count('id');
    }

}

