<?php


namespace App\Repositories;
use App\Models\CleaningComponent;
use App\Repositories\Interfaces\CleaningComponentRepositoryInterface;
use Illuminate\Support\Arr;

/**
 * Class CleaningComponentRepository
 * @property CleaningComponent $CleaningComponent
 * @package App\Repositories
 */
class CleaningComponentRepository implements CleaningComponentRepositoryInterface
{
    /**
     * CleaningComponentRepository constructor.
     */
    function __construct()
    {
        $this->component = new CleaningComponent();
    }
    /**
     * @param $id
     * @return mixed
     */
    public function get($id)
    {
        return $this->component->find($id);
    }
    /**
     * Get's all CleaningComponent
     *
     * @return mixed
     */
    public function all()
    {
        return $this->component->all();
    }

    /**
     * Deletes a CleaningComponent.
     *
     * @param int
     * @return int
     */
    public function delete($id)
    {
        return $this->component->destroy($id);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function store(array $data)
    {
        return $this->component->create($data);
    }

    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function update($id, array $data)
    {
        return $this->component->find($id)->update($data);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function changeStatus($id)
    {
        return $this->component->find($id)->toggleStatus();
    }



    /**
     * @param array $data
     * @return $this|\Illuminate\Database\Eloquent\Builder|mixed
     */
    public function allDataTable(array $data)
    {
        $query = $this->component;
        $skip = 0;
        $take = 25;

        if(Arr::exists($data,"name") && !is_null($data['name']))
        {
            $query = $query->where('name', 'LIKE', '%' . $data['name']. '%');
        }
        

        return $query->skip($skip)->take($take);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function countDataTable(array $data)
    {
        $query = $this->component;

        if(Arr::exists($data,"name") && !is_null($data['name']))
        {
            $query = $query->where('name', 'LIKE', '%' . $data['name']. '%');
        }
        

        return $query->count('id');
    }

}

