<?php


namespace App\Repositories;
use App\Models\Mosque;
use App\Models\Employee;
use App\Models\MosqueAttachment;
use App\Models\MosqueAssignee;
use App\Models\MosqueAllocationData;
use App\Models\MosqueSupervisorAuthority;
use App\Models\MosqueBuilding;
use App\Models\MosqueRebuilding;
use App\Models\MosqueBoardMember;
use App\Models\MosqueBuildingBoardMember;
use App\Models\MosqueRebuildingBoardMember;
use App\Repositories\Interfaces\MosqueRepositoryInterface;
use Illuminate\Support\Arr;

/**
 * Class MosqueRepository
 * @property Mosque $mosque
 * @package App\Repositories
 */
class MosqueRepository implements MosqueRepositoryInterface
{
    /**
     * MosqueRepository constructor.
     */
    function __construct()
    {
        $this->mosque = new Mosque();
    }
    /**
     * @param $id
     * @return mixed
     */
    public function get($id)
    {
        return $this->mosque->find($id);
    }
    /**
     * Get's all mosques
     *
     * @return mixed
     */
    public function all()
    {
        return $this->mosque->all();
    }

    /**
     * Get's mosques by user_id
     *
     * @return mixed
     */
    public function getByUserId($id)
    {
        return $this->mosque->where('user_id', $id)->get();
    }

    /**
     * Deletes a Mosque.
     *
     * @param int
     * @return int
     */
    public function delete($id)
    {
        return $this->mosque->destroy($id);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function store(array $data)
    {
        return $this->mosque->create($data);
    }

    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function update($id, array $data)
    {
        return $this->mosque->find($id)->update($data);
    }

    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function syncEmployees($id, array $data)
    {
        /** Remove Old Employees List */
        $oldEmployees = Employee::where('mosque_id',$id)->update(['mosque_id'=>NULL]);
        /** Add New Employees List */
        $newEmployees = Employee::whereIn('id',$data)->update(['mosque_id'=>$id]);
        return $newEmployees;
    }

    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function uploadAttachments($mosque_id,$type,$img)
    {
        return MosqueAttachment::create([
            'mosque_id'=> $mosque_id,
            'attachment_type_id'=> $type,
            'file'=> $img,
        ]);
    }

    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function syncAssigneeData($mosque_id,$data)
    {
       // dd('syncAssigneeData',$mosque_id,$data);
        return MosqueAssignee::updateOrCreate(['mosque_id'=>$mosque_id],$data);
    }

    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function syncAllocationData($mosque_id,$data)
    {
        //dd('syncAllocationData',$mosque_id,$data);
        return MosqueAllocationData::updateOrCreate(['mosque_id'=> $mosque_id],$data);
    }

    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function syncSupervisorData($mosque_id,$data)
    {
        //dd('syncSupervisorData',$mosque_id,$data);
        return MosqueSupervisorAuthority::updateOrCreate(['mosque_id'=> $mosque_id],$data);
    }

    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function syncBuildingData($mosque_id,$data)
    {
        //dd('syncBuildingData',$mosque_id,$data);
        return MosqueBuilding::updateOrCreate(['mosque_id'=> $mosque_id],$data);
    }

    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function syncRebuildingData($mosque_id,$data)
    {
        //dd('syncRebuildingData',$mosque_id,$data);
        return MosqueRebuilding::updateOrCreate(['mosque_id'=> $mosque_id],$data);
    }

    /** ==================================================================================== */

    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function syncBoardData($mosque_id,$data)
    {
        //dd('syncBoardData',$mosque_id,$data);
        $items = json_decode($data['administrativeBoard'],true);
        $names = [];
        foreach($items as $item){
            array_push($names,$item['full_name']);
            $members[] = MosqueBoardMember::updateOrCreate(
                [
                    'mosque_id'=> $mosque_id,
                    'full_name'=> $item['full_name'],
                ],
                [
                    'identity_number'=> $item['identity_number'],
                    'phone_number'=> $item['phone_number'],
                    'address'=> $item['address'],
                    'job'=> $item['job'],
                ]);
        }
        $delete = MosqueBoardMember::where('mosque_id',$mosque_id)->whereNotIn('full_name',$names)->delete();
        return $members ;
    }

    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function syncBuildingBoardData($mosque_id,$data)
    {
        //dd('syncBuildingBoardData',$mosque_id,$data);
        $items = json_decode($data['buildingBoard'],true);
        $names = [];
        foreach($items as $item){
            array_push($names,$item['full_name']);
            $members[] = MosqueBuildingBoardMember::updateOrCreate(
                [
                    'mosque_id'=> $mosque_id,
                    'full_name'=> $item['full_name'],
                ],
                [
                    'identity_number'=> $item['identity_number'],
                    'phone_number'=> $item['phone_number'],
                    'address'=> $item['address'],
                    'job'=> $item['job'],
                ]);
        }
        $delete = MosqueBuildingBoardMember::where('mosque_id',$mosque_id)->whereNotIn('full_name',$names)->delete();
        return $members ;
    }

    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function syncRebuildingBoardData($mosque_id,$data)
    {
        //dd('syncRebuildingBoardData',$mosque_id,$data);
        $items = json_decode($data['rebuildingBoard'],true);
        $names = [];
        foreach($items as $item){
            array_push($names,$item['full_name']);
            $members[] = MosqueRebuildingBoardMember::updateOrCreate(
                [
                    'mosque_id'=> $mosque_id,
                    'full_name'=> $item['full_name'],
                ],
                [
                    'identity_number'=> $item['identity_number'],
                    'phone_number'=> $item['phone_number'],
                    'address'=> $item['address'],
                    'job'=> $item['job'],
                ]);
        }
        $delete = MosqueRebuildingBoardMember::where('mosque_id',$mosque_id)->whereNotIn('full_name',$names)->delete();
        return $members ;
    }
    /** ==================================================================================== */

    /*
    * @param int $user
    * @param array $mosques
    * @param bool $deleteOld
    * @return $this|\Illuminate\Database\Eloquent\Builder|mixed
    */
    public function updateMosqueInspector(int $user, array $mosques, bool $deleteOld = false){
        if($deleteOld){
            $this->mosque->where('user_id',$user)->update(['user_id'=> NULL]);
        }
        return $this->mosque->whereIn('id', $mosques)->update(['user_id'=> $user]);
    }

    /** ==================================================================================== */


    /**
     * @param array $data
     * @return $this|\Illuminate\Database\Eloquent\Builder|mixed
     */
    public function allDataTable(array $data)
    {
        $query = $this->mosque;
        $skip = 0;
        $take = 25;

        if(Arr::exists($data,"name") && !is_null($data['name']))
        {
            $query = $query->where('name', 'LIKE', '%' . $data['name']. '%');
        }


        return $query->skip($skip)->take($take);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function countDataTable(array $data)
    {
        $query = $this->mosque;

        if(Arr::exists($data,"name") && !is_null($data['name']))
        {
            $query = $query->where('name', 'LIKE', '%' . $data['name']. '%');
        }


        return $query->count('id');
    }

    /** ==================================================================================== */

    /**
     * @param int $user_id
     * @param array $data
     * @return $this|\Illuminate\Database\Eloquent\Builder|mixed
     */
    public function getByUserDataTable(int $user_id, array $data)
    {
        $query = $this->getByUserId($user_id);
        $skip = 0;
        $take = 25;

        if(Arr::exists($data,"name") && !is_null($data['name']))
        {
            $query = $query->where('name', 'LIKE', '%' . $data['name']. '%');
        }


        return $query->skip($skip)->take($take);
    }

    /**
     * @param int $user_id
     * @param array $data
     * @return mixed
     */
    public function countByUserDataTable(int $user_id, array $data)
    {
        $query = $this->getByUserId($user_id);

        if(Arr::exists($data,"name") && !is_null($data['name']))
        {
            $query = $query->where('name', 'LIKE', '%' . $data['name']. '%');
        }


        return $query->count('id');
    }

}

