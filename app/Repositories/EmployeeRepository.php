<?php


namespace App\Repositories;
use App\Models\Employee;
use App\Repositories\Interfaces\EmployeeRepositoryInterface;
use Illuminate\Support\Arr;

/**
 * Class EmployeeRepository
 * @property Employee $employee
 * @package App\Repositories
 */
class EmployeeRepository implements EmployeeRepositoryInterface
{
    /**
     * EmployeeRepository constructor.
     */
    function __construct()
    {
        $this->employee = new Employee();
    }
    /**
     * @param $id
     * @return mixed
     */
    public function get($id)
    {
        return $this->employee->find($id);
    }
    /**
     * Get's all Employee
     *
     * @return mixed
     */
    public function all()
    {
        return $this->employee->all();
    }

    /**
     * Deletes a Employee.
     *
     * @param int
     * @return int
     */
    public function delete($id)
    {
        return $this->employee->destroy($id);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function store(array $data)
    {
        return $this->employee->create($data);
    }

    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function update($id, array $data)
    {
        return $this->employee->find($id)->update($data);
    }


    /**
     * @param array $data
     * @return $this|\Illuminate\Database\Eloquent\Builder|mixed
     */
    public function allDataTable(array $data)
    {
        $query = $this->employee;
        $skip = 0;
        $take = 25;

        if(Arr::exists($data,"name") && !is_null($data['name']))
        {
            $query = $query->where('name', 'LIKE', '%' . $data['name']. '%');
        }
        if(Arr::exists($data,"identity_number") && !is_null($data['identity_number']))
        {
            $query = $query->where('identity_number', $data['identity_number']);
        }
        if(Arr::exists($data,"phone_number") && !is_null($data['phone_number']))
        {
            $query = $query->where('phone_number', $data['phone_number']);
        }

        return $query->skip($skip)->take($take);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function countDataTable(array $data)
    {
        $query = $this->employee;

        if(Arr::exists($data,"name") && !is_null($data['name']))
        {
            $query = $query->where('name', 'LIKE', '%' . $data['name']. '%');
        }
        if(Arr::exists($data,"identity_number") && !is_null($data['identity_number']))
        {
            $query = $query->where('identity_number', $data['identity_number']);
        }
        if(Arr::exists($data,"phone_number") && !is_null($data['phone_number']))
        {
            $query = $query->where('phone_number', $data['phone_number']);
        }

        return $query->count('id');
    }

}

