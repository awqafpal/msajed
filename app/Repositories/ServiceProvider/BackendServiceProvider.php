<?php
namespace App\Repositories\ServiceProvider;

use Illuminate\Support\ServiceProvider;

class BackendServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(
            'App\Repositories\Interfaces\UserRepositoryInterface',
            'App\Repositories\UserRepository'
        );
        $this->app->bind(
            'App\Repositories\Interfaces\RoleRepositoryInterface',
            'App\Repositories\RoleRepository'
        );
        $this->app->bind(
            'App\Repositories\Interfaces\ProvinceRepositoryInterface',
            'App\Repositories\ProvinceRepository'
        );
        $this->app->bind(
            'App\Repositories\Interfaces\RegionRepositoryInterface',
            'App\Repositories\RegionRepository'
        );


        $this->app->bind(
            'App\Repositories\Interfaces\UserStructureRepositoryInterface',
            'App\Repositories\UserStructureRepository'
        );
        $this->app->bind(
            'App\Repositories\Interfaces\UserTypeRepositoryInterface',
            'App\Repositories\UserTypeRepository'
        );
        $this->app->bind(
            'App\Repositories\Interfaces\PropertyRepositoryInterface',
            'App\Repositories\PropertyRepository'
        );
        $this->app->bind(
            'App\Repositories\Interfaces\FacilityRepositoryInterface',
            'App\Repositories\FacilityRepository'
        );
        $this->app->bind(
            'App\Repositories\Interfaces\ElectricityTypeRepositoryInterface',
            'App\Repositories\ElectricityTypeRepository'
        );
        $this->app->bind(
            'App\Repositories\Interfaces\AlternativeEnergyRepositoryInterface',
            'App\Repositories\AlternativeEnergyRepository'
        );
        $this->app->bind(
            'App\Repositories\Interfaces\WaterSourceRepositoryInterface',
            'App\Repositories\WaterSourceRepository'
        );
        $this->app->bind(
            'App\Repositories\Interfaces\JobRepositoryInterface',
            'App\Repositories\JobRepository'
        );
        $this->app->bind(
            'App\Repositories\Interfaces\AttachmentTypeRepositoryInterface',
            'App\Repositories\AttachmentTypeRepository'
        );
        $this->app->bind(
            'App\Repositories\Interfaces\AdvertisingRepositoryInterface',
            'App\Repositories\AdvertisingRepository'
        );
        $this->app->bind(
            'App\Repositories\Interfaces\NewsRepositoryInterface',
            'App\Repositories\NewsRepository'
        );
        $this->app->bind(
            'App\Repositories\Interfaces\GeneralizationRepositoryInterface',
            'App\Repositories\GeneralizationRepository'
        );
        $this->app->bind(
            'App\Repositories\Interfaces\CleaningComponentRepositoryInterface',
            'App\Repositories\CleaningComponentRepository'
        );
        $this->app->bind(
            'App\Repositories\Interfaces\EmployeeRepositoryInterface',
            'App\Repositories\EmployeeRepository'
        );
        $this->app->bind(
            'App\Repositories\Interfaces\MosqueRepositoryInterface',
            'App\Repositories\MosqueRepository'
        );

        $this->app->bind(
            'App\Repositories\Interfaces\NeedRequestRepositoryInterface',
            'App\Repositories\NeedRequestRepository'
        );

        $this->app->bind(
            'App\Repositories\Interfaces\InspectionScheduleRepositoryInterface',
            'App\Repositories\InspectionScheduleRepository'
        );

        $this->app->bind(
            'App\Repositories\Interfaces\MosqueVisitRepositoryInterface',
            'App\Repositories\MosqueVisitRepository'
        );

        $this->app->bind(
            'App\Repositories\Interfaces\VisitChangeOrderRepositoryInterface',
            'App\Repositories\VisitChangeOrderRepository'
        );

    }
}
