<?php


namespace App\Repositories;
use App\Models\UserStructure;
use App\Repositories\Interfaces\UserStructureRepositoryInterface;
use Illuminate\Support\Arr;

/**
 * Class UserStructureRepository
 * @property UserStructure $structure
 * @package App\Repositories
 */
class UserStructureRepository implements UserStructureRepositoryInterface
{
    /**
     * UserStructureRepository constructor.
     */
    function __construct()
    {
        $this->structure = new UserStructure();
    }
    /**
     * @param $id
     * @return mixed
     */
    public function get($id)
    {
        return $this->structure->find($id);
    }
    /**
     * Get's all user structures
     *
     * @return mixed
     */
    public function all()
    {
        return $this->structure->all();
    }

    /**
     * Deletes a structure.
     *
     * @param int
     * @return int
     */
    public function delete($id)
    {
        return $this->structure->destroy($id);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function store(array $data)
    {
        return $this->structure->create($data);
    }

    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function update($id, array $data)
    {
        return $this->structure->find($id)->update($data);
    }

    /**
     * @param array $data
     * @return $this|\Illuminate\Database\Eloquent\Builder|mixed
     */
    public function allDataTable(array $data)
    {
        $query = $this->structure;
        $skip = 0;
        $take = 25;

        if(Arr::exists($data,"name") && !is_null($data['name']))
        {
            $query = $query->where('name', 'LIKE', '%' . $data['name']. '%');
        }

        return $query->skip($skip)->take($take);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function countDataTable(array $data)
    {
        $query = $this->structure;

        if(Arr::exists($data,"name") && !is_null($data['name']))
        {
            $query = $query->where('name', 'LIKE', '%' . $data['name']. '%');
        }
        return $query->count('id');
    }

}

