<?php


namespace App\Repositories;
use App\Models\Job;
use App\Repositories\Interfaces\JobRepositoryInterface;
use Illuminate\Support\Arr;

/**
 * Class JobRepository
 * @property Job $job
 * @package App\Repositories
 */
class JobRepository implements JobRepositoryInterface
{
    /**
     * JobRepository constructor.
     */
    function __construct()
    {
        $this->job = new Job();
    }
    /**
     * @param $id
     * @return mixed
     */
    public function get($id)
    {
        return $this->job->find($id);
    }
    /**
     * Get's all job
     *
     * @return mixed
     */
    public function all()
    {
        return $this->job->all();
    }

    /**
     * Deletes a job.
     *
     * @param int
     * @return int
     */
    public function delete($id)
    {
        return $this->job->destroy($id);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function store(array $data)
    {
        return $this->job->create($data);
    }

    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function update($id, array $data)
    {
        return $this->job->find($id)->update($data);
    }


    /**
     * @param array $data
     * @return $this|\Illuminate\Database\Eloquent\Builder|mixed
     */
    public function allDataTable(array $data)
    {
        $query = $this->job;
        $skip = 0;
        $take = 25;

        if(Arr::exists($data,"name") && !is_null($data['name']))
        {
            $query = $query->where('name', 'LIKE', '%' . $data['name']. '%');
        }
        

        return $query->skip($skip)->take($take);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function countDataTable(array $data)
    {
        $query = $this->job;

        if(Arr::exists($data,"name") && !is_null($data['name']))
        {
            $query = $query->where('name', 'LIKE', '%' . $data['name']. '%');
        }
        

        return $query->count('id');
    }

}

