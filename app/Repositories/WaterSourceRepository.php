<?php


namespace App\Repositories;
use App\Models\WaterSource;
use App\Repositories\Interfaces\WaterSourceRepositoryInterface;
use Illuminate\Support\Arr;

/**
 * Class WaterSourceRepository
 * @property WaterSourceRepository $source
 * @package App\Repositories
 */
class WaterSourceRepository implements WaterSourceRepositoryInterface
{
    /**
     * WaterSourceRepository constructor.
     */
    function __construct()
    {
        $this->source = new WaterSource();
    }
    /**
     * @param $id
     * @return mixed
     */
    public function get($id)
    {
        return $this->source->find($id);
    }
    /**
     * Get's all source
     *
     * @return mixed
     */
    public function all()
    {
        return $this->source->all();
    }

    /**
     * Deletes a source.
     *
     * @param int
     * @return int
     */
    public function delete($id)
    {
        return $this->source->destroy($id);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function store(array $data)
    {
        return $this->source->create($data);
    }

    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function update($id, array $data)
    {
        return $this->source->find($id)->update($data);
    }


    /**
     * @param array $data
     * @return $this|\Illuminate\Database\Eloquent\Builder|mixed
     */
    public function allDataTable(array $data)
    {
        $query = $this->source;
        $skip = 0;
        $take = 25;

        if(Arr::exists($data,"name") && !is_null($data['name']))
        {
            $query = $query->where('name', 'LIKE', '%' . $data['name']. '%');
        }
        

        return $query->skip($skip)->take($take);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function countDataTable(array $data)
    {
        $query = $this->source;

        if(Arr::exists($data,"name") && !is_null($data['name']))
        {
            $query = $query->where('name', 'LIKE', '%' . $data['name']. '%');
        }
        

        return $query->count('id');
    }

}

