<?php


namespace App\Repositories;
use App\Models\Mosque;
use App\Models\InspectionSchedule;
use App\Models\InspectionScheduleTime;
use App\Repositories\Interfaces\InspectionScheduleRepositoryInterface;
use Illuminate\Support\Arr;
use Carbon\Carbon;
use Carbon\CarbonPeriod;

/**
 * Class InspectionScheduleRepository
 * @property InspectionSchedule $InspectionSchedule
 * @package App\Repositories
 */
class InspectionScheduleRepository implements InspectionScheduleRepositoryInterface
{
    /**
     * InspectionScheduleRepository constructor.
     */
    function __construct()
    {
        $this->inspectionSchedule = new InspectionSchedule();
        $this->inspectionScheduleTime = new InspectionScheduleTime();
    }
    /**
     * @param $id
     * @return mixed
     */
    public function get($id)
    {
        return $this->inspectionSchedule->find($id);
    }
    /**
     * Get's all InspectionSchedule
     *
     * @return mixed
     */
    public function all()
    {
        return $this->inspectionSchedule->all();
    }

    /**
     * Get a specific schedule
     *
     * @return mixed
     */
    public function hasSchedule($userId, $month)
    {
        return $this->inspectionSchedule
                    ->where('user_id', $userId)
                    ->where('month_year', $month)
                    ->first();
    }


    /**
     * @param array $data
     * @return mixed
     */
    public function store(array $data)
    {
        return $this->inspectionSchedule->create($data);
    }


    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function update($id, array $data)
    {
        return $this->inspectionSchedule->find($id)->update($data);
    }

    /**
     * @param date $schedule
     * @return mixed
     */
    public function getTimes($month_year){
        $firstDayofMonth = $month_year . '-01';
        $lastDayofMonth = Carbon::parse($firstDayofMonth)->endOfMonth()->toDateString();
        $period = CarbonPeriod::create($firstDayofMonth, $lastDayofMonth);
        $dates = $period->toArray();
        return $dates ;
    }

    /**
     * @param object $schedule
     * @return mixed
     */
    public function storeTimes($schedule){
        $firstDayofMonth = $schedule->month_year . '-01';
        $lastDayofMonth = Carbon::parse($firstDayofMonth)->endOfMonth()->toDateString();
        $period = CarbonPeriod::create($firstDayofMonth, $lastDayofMonth);
        $dates = $period->toArray();

        $times = [];
        // Iterate over the period
        foreach ($dates as $date) {
            array_push($times, [
                'inspection_schedule_id' => $schedule->id,
                'date' => $date->format('Y-m-d'),
            ]);
        }
        $chunks = array_chunk($times, 200);
        foreach ($chunks as $chunk) {
            $data = $this->inspectionScheduleTime->insert($chunk);
        }

        return $data ;
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function syncTime(array $data)
    {
        $prayer = $data['prayer'];
        $id =(int) $data['inspection_schedule_id'];
        $date = date('Y-m-d', strtotime($data['date']));
        if(! is_null($data['mosque_id'])){
            $mosque = Mosque::find($data['mosque_id']);
            $details = [
                'id' => $mosque->id,
                'name' => $mosque->name,
                'status' => 'new',
                'color' => '',
                'reason'=>''
            ];
            $details =json_encode($details);
        }else{
            $details = null;
        }
        return $this->inspectionScheduleTime->updateOrCreate([
            'inspection_schedule_id'=> $id,
            'date'=> $date
        ],
        [
            $prayer => $details
        ]);
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function approval($id){
        $auth = auth()->user();
        switch ($auth->user_type_id){
            case '1':
                // مدير عام المديريات
                $data = ['province_manager_approval'=>1 , 'department_head_approval'=>1 , 'inspector_approval'=>1] ;
                break;
            case '3':
                // مدير المساجد
                $data = ['province_manager_approval'=>1 , 'department_head_approval'=>1 , 'inspector_approval'=>1] ;
                break;
            case '13':
                // مدير النظام
                $data = ['province_manager_approval'=>1 , 'department_head_approval'=>1 , 'inspector_approval'=>1] ;
                break;
            case '4':
                $data = ['province_manager_approval'=>1] ;
                break;
            case '8':
                $data = ['department_head_approval'=>1] ;
                break;
            case '9':
                $data = ['department_head_approval'=>1] ;
                break;
            default:
            $data = ['inspector_approval'=>1] ;
        }
        return $this->inspectionSchedule->find($id)->update($data);
    }

    /**
     * Deletes a InspectionSchedule.
     *
     * @param int
     * @return int
     */
    public function delete($id)
    {
        return $this->inspectionSchedule->destroy($id);
    }


    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function timesBySchedule($id)
    {
        return $this->inspectionScheduleTime->where('inspection_schedule_id', $id)->orderBy('id')->get();
    }

    /**
     * @param intger $id
     * @param date $month_year
     * @return mixed
     */
    public function getByUserAndMonth($user, $month_year){
        return $this->inspectionSchedule->where('month_year', $month_year)
                    ->where('user_id', $user)
                    ->where('department_head_approval', 1)
                    ->where('province_manager_approval', 1)
                    ->first();
    }

    /**
     * @param intger $schedule_id
     * @param date $date
     * @return mixed
     */
    public function getPrayersByCustomDate($schedule_id, $date){
        return $this->inspectionScheduleTime
                    ->where('inspection_schedule_id', $schedule_id)
                    ->where('date', $date)
                    // ->where(function ($query) {
                    //     $query->orwhereNotNull('fajr')
                    //     ->orwhereNotNull('dhuhr')
                    //     ->orwhereNotNull('asr')
                    //     ->orwhereNotNull('maghrib')
                    //     ->orwhereNotNull('isha');
                    // })
                    ->first();
    }

    /**
     * @param array $data
     * @return $this|\Illuminate\Database\Eloquent\Builder|mixed
     */
    public function allDataTable(array $data)
    {
        $query = $this->inspectionSchedule;
        $skip = 0;
        $take = 25;

        if(Arr::exists($data,"title") && !is_null($data['title']))
        {
            $query = $query->where('title', 'LIKE', '%' . $data['title']. '%');
        }


        return $query->skip($skip)->take($take);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function countDataTable(array $data)
    {
        $query = $this->inspectionSchedule;

        if(Arr::exists($data,"title") && !is_null($data['title']))
        {
            $query = $query->where('title', 'LIKE', '%' . $data['title']. '%');
        }


        return $query->count('id');
    }


    /**
     * @param array $data
     * @return $this|\Illuminate\Database\Eloquent\Builder|mixed
     */
    public function getScheduleDataTable($id)
    {
        $query = $this->timesBySchedule($id);
        $skip = 0;
        $take = 35;
        return $query->skip($skip)->take($take);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function countScheduleDataTable($id)
    {
        $query = $this->timesBySchedule($id);
        return $query->count('id');
    }
}

