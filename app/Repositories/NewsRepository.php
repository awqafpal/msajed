<?php


namespace App\Repositories;
use App\Models\News;
use App\Repositories\Interfaces\NewsRepositoryInterface;
use Illuminate\Support\Arr;

/**
 * Class NewsRepository
 * @property News $News
 * @package App\Repositories
 */
class NewsRepository implements NewsRepositoryInterface
{
    /**
     * NewsRepository constructor.
     */
    function __construct()
    {
        $this->news = new News();
    }
    /**
     * @param $id
     * @return mixed
     */
    public function get($id)
    {
        return $this->news->find($id);
    }
    /**
     * Get's all News
     *
     * @return mixed
     */
    public function all()
    {
        return $this->news->all();
    }

    /**
     * Deletes a News.
     *
     * @param int
     * @return int
     */
    public function delete($id)
    {
        return $this->news->destroy($id);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function store(array $data)
    {
        return $this->news->create($data);
    }

    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function update($id, array $data)
    {
        return $this->news->find($id)->update($data);
    }


    /**
     * @param array $data
     * @return $this|\Illuminate\Database\Eloquent\Builder|mixed
     */
    public function allDataTable(array $data)
    {
        $query = $this->news;
        $skip = 0;
        $take = 25;

        if(Arr::exists($data,"title") && !is_null($data['title']))
        {
            $query = $query->where('title', 'LIKE', '%' . $data['title']. '%');
        }
        

        return $query->skip($skip)->take($take);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function countDataTable(array $data)
    {
        $query = $this->news;

        if(Arr::exists($data,"title") && !is_null($data['title']))
        {
            $query = $query->where('title', 'LIKE', '%' . $data['title']. '%');
        }
        

        return $query->count('id');
    }

}

