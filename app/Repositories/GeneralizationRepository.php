<?php


namespace App\Repositories;
use App\Models\Generalization;
use App\Repositories\Interfaces\GeneralizationRepositoryInterface;
use Illuminate\Support\Arr;

/**
 * Class GeneralizationRepository
 * @property Generalization $Generalization
 * @package App\Repositories
 */
class GeneralizationRepository implements GeneralizationRepositoryInterface
{
    /**
     * GeneralizationRepository constructor.
     */
    function __construct()
    {
        $this->generalization = new Generalization();
    }
    /**
     * @param $id
     * @return mixed
     */
    public function get($id)
    {
        return $this->generalization->find($id);
    }
    /**
     * Get's all Generalization
     *
     * @return mixed
     */
    public function all()
    {
        return $this->generalization->all();
    }

    /**
     * Deletes a Generalization.
     *
     * @param int
     * @return int
     */
    public function delete($id)
    {
        return $this->generalization->destroy($id);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function store(array $data)
    {
        return $this->generalization->create($data);
    }

    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function update($id, array $data)
    {
        return $this->generalization->find($id)->update($data);
    }


    /**
     * @param array $data
     * @return $this|\Illuminate\Database\Eloquent\Builder|mixed
     */
    public function allDataTable(array $data)
    {
        $query = $this->generalization;
        $skip = 0;
        $take = 25;

        if(Arr::exists($data,"title") && !is_null($data['title']))
        {
            $query = $query->where('title', 'LIKE', '%' . $data['title']. '%');
        }
        

        return $query->skip($skip)->take($take);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function countDataTable(array $data)
    {
        $query = $this->generalization;

        if(Arr::exists($data,"title") && !is_null($data['title']))
        {
            $query = $query->where('title', 'LIKE', '%' . $data['title']. '%');
        }
        

        return $query->count('id');
    }

}

