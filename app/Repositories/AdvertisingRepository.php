<?php


namespace App\Repositories;
use App\Models\Advertising;
use App\Repositories\Interfaces\AdvertisingRepositoryInterface;
use Illuminate\Support\Arr;

/**
 * Class AdvertisingRepository
 * @property Advertising $advertising
 * @package App\Repositories
 */
class AdvertisingRepository implements AdvertisingRepositoryInterface
{
    /**
     * AdvertisingRepository constructor.
     */
    function __construct()
    {
        $this->advertising = new Advertising();
    }
    /**
     * @param $id
     * @return mixed
     */
    public function get($id)
    {
        return $this->advertising->find($id);
    }
    /**
     * Get's all advertising
     *
     * @return mixed
     */
    public function all()
    {
        return $this->advertising->all();
    }

    /**
     * Deletes a advertising.
     *
     * @param int
     * @return int
     */
    public function delete($id)
    {
        return $this->advertising->destroy($id);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function store(array $data)
    {
        return $this->advertising->create($data);
    }

    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function update($id, array $data)
    {
        return $this->advertising->find($id)->update($data);
    }


    /**
     * @param array $data
     * @return $this|\Illuminate\Database\Eloquent\Builder|mixed
     */
    public function allDataTable(array $data)
    {
        $query = $this->advertising;
        $skip = 0;
        $take = 25;

        if(Arr::exists($data,"title") && !is_null($data['title']))
        {
            $query = $query->where('title', 'LIKE', '%' . $data['title']. '%');
        }
        

        return $query->skip($skip)->take($take);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function countDataTable(array $data)
    {
        $query = $this->advertising;

        if(Arr::exists($data,"title") && !is_null($data['title']))
        {
            $query = $query->where('title', 'LIKE', '%' . $data['title']. '%');
        }
        

        return $query->count('id');
    }

}

