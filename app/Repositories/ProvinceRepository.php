<?php


namespace App\Repositories;
use App\Models\Province;
use App\Repositories\Interfaces\ProvinceRepositoryInterface;
use Illuminate\Support\Arr;

/**
 * Class ProvinceRepository
 * @property Province $province
 * @package App\Repositories
 */
class ProvinceRepository implements ProvinceRepositoryInterface
{
    /**
     * ProvinceRepository constructor.
     */
    function __construct()
    {
        $this->province = new Province();
    }
    /**
     * @param $id
     * @return mixed
     */
    public function get($id)
    {
        return $this->province->find($id);
    }
    /**
     * Get's all provinces
     *
     * @return mixed
     */
    public function all()
    {
        return $this->province->all();
    }

    /**
     * Deletes a province.
     *
     * @param int
     * @return int
     */
    public function delete($id)
    {
        return $this->province->destroy($id);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function store(array $data)
    {
        return $this->province->create($data);
    }

    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function update($id, array $data)
    {
        return $this->province->find($id)->update($data);
    }

    /**
     * @param array $data
     * @return $this|\Illuminate\Database\Eloquent\Builder|mixed
     */
    public function allDataTable(array $data)
    {
        $query = $this->province;
        $skip = 0;
        $take = 25;

        if(Arr::exists($data,"name") && !is_null($data['name']))
        {
            $query = $query->where('name', 'LIKE', '%' . $data['name']. '%');
        }

        return $query->skip($skip)->take($take);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function countDataTable(array $data)
    {
        $query = $this->province;

        if(Arr::exists($data,"name") && !is_null($data['name']))
        {
            $query = $query->where('name', 'LIKE', '%' . $data['name']. '%');
        }
        return $query->count('id');
    }

}

