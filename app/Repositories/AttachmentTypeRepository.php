<?php


namespace App\Repositories;
use App\Models\AttachmentType;
use App\Repositories\Interfaces\AttachmentTypeRepositoryInterface;
use Illuminate\Support\Arr;

/**
 * Class AttachmentTypeRepository
 * @property AttachmentType $type
 * @package App\Repositories
 */
class AttachmentTypeRepository implements AttachmentTypeRepositoryInterface
{
    /**
     * AttachmentTypeRepository constructor.
     */
    function __construct()
    {
        $this->type = new AttachmentType();
    }
    /**
     * @param $id
     * @return mixed
     */
    public function get($id)
    {
        return $this->type->find($id);
    }
    /**
     * Get's all type
     *
     * @return mixed
     */
    public function all()
    {
        return $this->type->all();
    }

    /**
     * Deletes a type.
     *
     * @param int
     * @return int
     */
    public function delete($id)
    {
        return $this->type->destroy($id);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function store(array $data)
    {
        return $this->type->create($data);
    }

    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function update($id, array $data)
    {
        return $this->type->find($id)->update($data);
    }


    /**
     * @param array $data
     * @return $this|\Illuminate\Database\Eloquent\Builder|mixed
     */
    public function allDataTable(array $data)
    {
        $query = $this->type;
        $skip = 0;
        $take = 25;

        if(Arr::exists($data,"name") && !is_null($data['name']))
        {
            $query = $query->where('name', 'LIKE', '%' . $data['name']. '%');
        }
        

        return $query->skip($skip)->take($take);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function countDataTable(array $data)
    {
        $query = $this->type;

        if(Arr::exists($data,"name") && !is_null($data['name']))
        {
            $query = $query->where('name', 'LIKE', '%' . $data['name']. '%');
        }
        

        return $query->count('id');
    }

}

