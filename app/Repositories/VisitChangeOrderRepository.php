<?php


namespace App\Repositories;
use App\Models\VisitChangeOrder;
use App\Repositories\Interfaces\VisitChangeOrderRepositoryInterface;
use Illuminate\Support\Arr;

/**
 * Class VisitChangeOrderRepository
 * @property VisitChangeOrder $order
 * @package App\Repositories
 */
class VisitChangeOrderRepository implements VisitChangeOrderRepositoryInterface
{
    /**
     * VisitChangeOrderRepository constructor.
     */
    function __construct()
    {
        $this->order = new VisitChangeOrder();
    }
    /**
     * @param $id
     * @return mixed
     */
    public function get($id)
    {
        return $this->order->find($id);
    }
    /**
     * Get's all VisitChangeOrders
     *
     * @return mixed
     */
    public function all()
    {
        return $this->order->all();
    }

    /**
     * Deletes a VisitChangeOrder.
     *
     * @param int
     * @return int
     */
    public function delete($id)
    {
        return $this->order->destroy($id);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function store(array $data)
    {
        return $this->order->create($data);
    }

    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function update($id, array $data)
    {
        return $this->order->find($id)->update($data);
    }


    /**
     * @param array $data
     * @return $this|\Illuminate\Database\Eloquent\Builder|mixed
     */
    public function allDataTable(array $data)
    {
        $query = $this->order;
        $skip = 0;
        $take = 25;
        /*
        if(Arr::exists($data,"name") && !is_null($data['name']))
        {
            $query = $query->where('name', 'LIKE', '%' . $data['name']. '%');
        }
        if(Arr::exists($data,"identity_number") && !is_null($data['identity_number']))
        {
            $query = $query->where('identity_number', $data['identity_number']);
        }
        if(Arr::exists($data,"phone_number") && !is_null($data['phone_number']))
        {
            $query = $query->where('phone_number', $data['phone_number']);
        }
        *////////
        return $query->skip($skip)->take($take);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function countDataTable(array $data)
    {
        $query = $this->order;
        /*
        if(Arr::exists($data,"name") && !is_null($data['name']))
        {
            $query = $query->where('name', 'LIKE', '%' . $data['name']. '%');
        }
        if(Arr::exists($data,"identity_number") && !is_null($data['identity_number']))
        {
            $query = $query->where('identity_number', $data['identity_number']);
        }
        if(Arr::exists($data,"phone_number") && !is_null($data['phone_number']))
        {
            $query = $query->where('phone_number', $data['phone_number']);
        }
        */

        return $query->count('id');
    }

}

