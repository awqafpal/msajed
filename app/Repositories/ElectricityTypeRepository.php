<?php


namespace App\Repositories;
use App\Models\ElectricityType;
use App\Repositories\Interfaces\ElectricityTypeRepositoryInterface;
use Illuminate\Support\Arr;

/**
 * Class ElectricityTypeRepository
 * @property ElectricityType $electricity
 * @package App\Repositories
 */
class ElectricityTypeRepository implements ElectricityTypeRepositoryInterface
{
    /**
     * ElectricityTypeRepository constructor.
     */
    function __construct()
    {
        $this->electricity = new ElectricityType();
    }
    /**
     * @param $id
     * @return mixed
     */
    public function get($id)
    {
        return $this->electricity->find($id);
    }
    /**
     * Get's all electricity types
     *
     * @return mixed
     */
    public function all()
    {
        return $this->electricity->all();
    }

    /**
     * Deletes a electricity.
     *
     * @param int
     * @return int
     */
    public function delete($id)
    {
        return $this->electricity->destroy($id);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function store(array $data)
    {
        return $this->electricity->create($data);
    }

    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function update($id, array $data)
    {
        return $this->electricity->find($id)->update($data);
    }


    /**
     * @param array $data
     * @return $this|\Illuminate\Database\Eloquent\Builder|mixed
     */
    public function allDataTable(array $data)
    {
        $query = $this->electricity;
        $skip = 0;
        $take = 25;

        if(Arr::exists($data,"name") && !is_null($data['name']))
        {
            $query = $query->where('name', 'LIKE', '%' . $data['name']. '%');
        }
        

        return $query->skip($skip)->take($take);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function countDataTable(array $data)
    {
        $query = $this->electricity;

        if(Arr::exists($data,"name") && !is_null($data['name']))
        {
            $query = $query->where('name', 'LIKE', '%' . $data['name']. '%');
        }
        

        return $query->count('id');
    }

}

