<?php


namespace App\Repositories;
use App\Models\Facility;
use App\Repositories\Interfaces\FacilityRepositoryInterface;
use Illuminate\Support\Arr;

/**
 * Class FacilityRepository
 * @Facility Facility $facility
 * @package App\Repositories
 */
class FacilityRepository implements FacilityRepositoryInterface
{
    /**
     * FacilityRepository constructor.
     */
    function __construct()
    {
        $this->facility = new Facility();
    }
    /**
     * @param $id
     * @return mixed
     */
    public function get($id)
    {
        return $this->facility->find($id);
    }
    /**
     * Get's all facilities
     *
     * @return mixed
     */
    public function all()
    {
        return $this->facility->all();
    }

    /**
     * Deletes a facility.
     *
     * @param int
     * @return int
     */
    public function delete($id)
    {
        return $this->facility->destroy($id);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function store(array $data)
    {
        return $this->facility->create($data);
    }

    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function update($id, array $data)
    {
        return $this->facility->find($id)->update($data);
    }


    /**
     * @param array $data
     * @return $this|\Illuminate\Database\Eloquent\Builder|mixed
     */
    public function allDataTable(array $data)
    {
        $query = $this->facility;
        $skip = 0;
        $take = 25;

        if(Arr::exists($data,"name") && !is_null($data['name']))
        {
            $query = $query->where('name', 'LIKE', '%' . $data['name']. '%');
        }
        

        return $query->skip($skip)->take($take);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function countDataTable(array $data)
    {
        $query = $this->facility;

        if(Arr::exists($data,"name") && !is_null($data['name']))
        {
            $query = $query->where('name', 'LIKE', '%' . $data['name']. '%');
        }
        

        return $query->count('id');
    }

}

