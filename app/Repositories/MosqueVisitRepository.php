<?php


namespace App\Repositories;
use App\Models\MosqueVisit;
use Illuminate\Support\Arr;
use App\Models\EmployeeCondition;
use App\Models\CleaningComponentVisit;
use App\Repositories\Interfaces\MosqueVisitRepositoryInterface;

/**
 * Class MosqueVisitRepository
 * @property MosqueVisit $employee
 * @package App\Repositories
 */
class MosqueVisitRepository implements MosqueVisitRepositoryInterface
{
    /**
     * MosqueVisitRepository constructor.
     */
    function __construct()
    {
        $this->visit = new MosqueVisit();
    }
    /**
     * @param $id
     * @return mixed
     */
    public function get($id)
    {
        return $this->visit->find($id);
    }
    /**
     * Get's all MosqueVisit
     *
     * @return mixed
     */
    public function all()
    {
        return $this->visit->all();
    }

    /**
     * Deletes a MosqueVisit.
     *
     * @param int
     * @return int
     */
    public function delete($id)
    {
        return $this->visit->destroy($id);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function store(array $data)
    {
        return $this->visit->create($data);
    }

    /**
     * @param integer $id
     * @param array $data
     * @return mixed
     */
    public function syncComponents(int $id, array $data)
    {
        foreach ($data as $component_id => $status) {
            $cleaning_evaluation[] = CleaningComponentVisit::updateOrCreate(
                [
                'visit_id' => $id,
                'cleaning_component_id' => $component_id
                ],
                [
                    'status_id' => $status
                ]
            );
        }
        return $cleaning_evaluation ;
    }

    /**
     * @param integer $id
     * @param array $data
     * @return mixed
     */
    public function syncEmployees(int $id, array $data)
    {
        foreach ($data as $employee_id => $condition) {
            $employees_conditions[] = EmployeeCondition::updateOrCreate(
                [
                    'visit_id' => $id,
                    'employee_id' => $employee_id
                ],
                [
                    'attendance' => @$condition['attendance'] ?? Null,
                    'commitment' => @$condition['commitment'] ?? Null,
                    'notes' => @$condition['notes'] ?? Null,
                ]
            );
        }
        return $employees_conditions;
    }


    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function update($id, array $data)
    {
        return $this->visit->find($id)->update($data);
    }

    /**
     * @param integer $id
     * @param array $data
     * @return mixed
     */
    public function approval(int $id , array $data){
        $data['is_submitted'] = 1 ;
        return $this->visit->find($id)->update($data);
    }

    /**
     * @param array $data
     * @return $this|\Illuminate\Database\Eloquent\Builder|mixed
     */
    public function allDataTable(array $data)
    {
        $query = $this->visit->with('mosque');
        $skip = 0;
        $take = 25;

        return $query->skip($skip)->take($take);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function countDataTable(array $data)
    {
        $query = $this->visit;
        return $query->count('id');
    }

}

