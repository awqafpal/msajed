<?php
namespace App\Repositories\Interfaces;

/**
 * Interface UserTypeRepositoryInterface
 * @package App\Repositories\Interfaces
 */
interface UserTypeRepositoryInterface
{
    /**
     * Get's a userType by it's ID
     *
     * @param int
     */
    public function get($id);

    /**
     * Get's all userTypes.
     *
     * @return mixed
     */
    public function all();

    /**
     * Deletes a userType.
     *
     * @param int
     */
    public function delete($id);

    /**
     * @param array $data
     * @return mixed
     */
    public function store(array $data);


    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function update($id, array $data);

    /**
     * @param array $data
     * @return mixed
     */
    public function allDataTable(array $data);

    /**
     * @param array $data
     * @return mixed
     */
    public function countDataTable(array $data);
}
