<?php
namespace App\Repositories\Interfaces;

/**
 * Interface InspectionScheduleRepositoryInterface
 * @package App\Repositories\Interfaces
 */
interface InspectionScheduleRepositoryInterface
{
    /**
     * Get's an InspectionSchedule by it's ID
     *
     * @param int
     */
    public function get($id);

    /**
     * Get's all InspectionSchedule.
     *
     * @return mixed
     */
    public function all();

    /**
     * Deletes an InspectionSchedule.
     *
     * @param int
     */
    public function delete($id);

    /**
     * @param array $data
     * @return mixed
     */
    public function store(array $data);


    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function update($id, array $data);

    /**
     * @param array $data
     * @return mixed
     */
    public function allDataTable(array $data);

    /**
     * @param array $data
     * @return mixed
     */
    public function countDataTable(array $data);


    /**
     * @param int $userId
     * @param date $month
     * @return mixed
     */
    public function hasSchedule($userId, $month);

     /**
     * @param date $schedule
     * @return mixed
     */
    public function getTimes($month_year);

    /**
     * @param array $data
     * @return mixed
     */
    public function syncTime(array $data);

    /**
     * @param object $schedule
     * @return mixed
     */
    public function storeTimes($schedule);

    /**
     * @param intger $id
     * @return mixed
     */
    public function getScheduleDataTable($id);

    /**
     * @param intger $id
     * @param date $month_year
     * @return mixed
     */
    public function getByUserAndMonth($user, $month_year);

    /**
     * @param intger $schedule_id
     * @param date $date
     * @return mixed
     */
    public function getPrayersByCustomDate($schedule_id, $date);

    /**
     * @param intger $id
     * @return mixed
     */
    public function countScheduleDataTable($id);


    /**
     * @param integer $id
     * @return mixed
     */
    public function approval($id);
}
