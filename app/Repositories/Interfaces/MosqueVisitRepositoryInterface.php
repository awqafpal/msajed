<?php
namespace App\Repositories\Interfaces;

/**
 * Interface MosqueVisitRepositoryInterface
 * @package App\Repositories\Interfaces
 */
interface MosqueVisitRepositoryInterface
{
    /**
     * Get's an Visit by it's ID
     *
     * @param int
     */
    public function get($id);

    /**
     * Get's all Visit.
     *
     * @return mixed
     */
    public function all();

    /**
     * Deletes an Visit.
     *
     * @param int
     */
    public function delete($id);

    /**
     * @param array $data
     * @return mixed
     */
    public function store(array $data);


    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function update($id, array $data);

     /**
     * @param integer $id
     * @param array $data
     * @return mixed
     */
    public function syncComponents(int $id, array $data);

    /**
     * @param integer $id
     * @param array $data
     * @return mixed
     */
    public function syncEmployees(int $id, array $data);

    /**
     * @param integer $id
     * @param array $data
     * @return mixed
     */
    public function approval(int $id, array $data);

    /**
     * @param array $data
     * @return mixed
     */
    public function allDataTable(array $data);

    /**
     * @param array $data
     * @return mixed
     */
    public function countDataTable(array $data);
}
