<?php
namespace App\Repositories\Interfaces;

/**
 * Interface AdvertisingRepositoryInterface
 * @package App\Repositories\Interfaces
 */
interface AdvertisingRepositoryInterface
{
    /**
     * Get's an Advertising by it's ID
     *
     * @param int
     */
    public function get($id);

    /**
     * Get's all Advertising.
     *
     * @return mixed
     */
    public function all();

    /**
     * Deletes an Advertising.
     *
     * @param int
     */
    public function delete($id);

    /**
     * @param array $data
     * @return mixed
     */
    public function store(array $data);


    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function update($id, array $data);

    /**
     * @param array $data
     * @return mixed
     */
    public function allDataTable(array $data);

    /**
     * @param array $data
     * @return mixed
     */
    public function countDataTable(array $data);
}
