<?php
namespace App\Repositories\Interfaces;

/**
 * Interface CleaningComponentRepositoryInterface
 * @package App\Repositories\Interfaces
 */
interface CleaningComponentRepositoryInterface
{
    /**
     * Get's a component by it's ID
     *
     * @param int
     */
    public function get($id);

    /**
     * Get's all components.
     *
     * @return mixed
     */
    public function all();

    /**
     * Deletes an component.
     *
     * @param int
     */
    public function delete($id);

    /**
     * @param array $data
     * @return mixed
     */
    public function store(array $data);


    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function update($id, array $data);

    /**
     * @param $id
     * @return mixed
     */
    public function changeStatus($id);

    /**
     * @param array $data
     * @return mixed
     */
    public function allDataTable(array $data);

    /**
     * @param array $data
     * @return mixed
     */
    public function countDataTable(array $data);
}
