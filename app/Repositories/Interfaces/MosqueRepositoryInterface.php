<?php
namespace App\Repositories\Interfaces;

/**
 * Interface MosqueRepositoryInterface
 * @package App\Repositories\Interfaces
 */
interface MosqueRepositoryInterface
{
    /**
     * Get's an Mosque by it's ID
     *
     * @param int
     */
    public function get($id);

    /**
     * Get's all Mosques.
     *
     * @return mixed
     */
    public function all();

    /**
     * Deletes an Mosque.
     *
     * @param int
     */
    public function delete($id);

    /**
     * @param array $data
     * @return mixed
     */
    public function store(array $data);


    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function update($id, array $data);

    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function syncEmployees($id, array $data);

    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function syncAssigneeData($id, array $data);

    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function syncAllocationData($id, array $data);

    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function syncSupervisorData($id, array $data);


    /**
     * @param array $data
     * @return mixed
     */
    public function allDataTable(array $data);

    /**
     * @param array $data
     * @return mixed
     */
    public function countDataTable(array $data);

    /** ==================================================================================== */

    /**
     * @param int $user_id
     * @param array $data
     * @return mixed
     */
    public function getByUserDataTable(int $user_id, array $data);

    /**
     * @param int $user_id
     * @param array $data
     * @return mixed
     */
    public function countByUserDataTable(int $user_id, array $data);

    /*
    * @param int $user
    * @param array $mosques
    * @param bool $deleteOld
    * @return $this|\Illuminate\Database\Eloquent\Builder|mixed
    */
    public function updateMosqueInspector(int $user, array $mosques, bool $deleteOld = false);

}
