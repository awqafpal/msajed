<?php
namespace App\Repositories\Interfaces;

/**
 * Interface UserStructureRepositoryInterface
 * @package App\Repositories\Interfaces
 */
interface UserStructureRepositoryInterface
{
    /**
     * Get's a structure by it's ID
     *
     * @param int
     */
    public function get($id);

    /**
     * Get's all structures.
     *
     * @return mixed
     */
    public function all();

    /**
     * Deletes a structure.
     *
     * @param int
     */
    public function delete($id);

    /**
     * @param array $data
     * @return mixed
     */
    public function store(array $data);


    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function update($id, array $data);

    /**
     * @param array $data
     * @return mixed
     */
    public function allDataTable(array $data);

    /**
     * @param array $data
     * @return mixed
     */
    public function countDataTable(array $data);
}
