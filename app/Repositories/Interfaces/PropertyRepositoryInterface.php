<?php
namespace App\Repositories\Interfaces;

/**
 * Interface PropertyRepositoryInterface
 * @package App\Repositories\Interfaces
 */
interface PropertyRepositoryInterface
{
    /**
     * Get's a property by it's ID
     *
     * @param int
     */
    public function get($id);

    /**
     * Get's all propertys.
     *
     * @return mixed
     */
    public function all();
    
    /**
     * Deletes a property.
     *
     * @param int
     */
    public function delete($id);

    /**
     * @param array $data
     * @return mixed
     */
    public function store(array $data);


    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function update($id, array $data);

    /**
     * @param array $data
     * @return mixed
     */
    public function allDataTable(array $data);

    /**
     * @param array $data
     * @return mixed
     */
    public function countDataTable(array $data);
}
