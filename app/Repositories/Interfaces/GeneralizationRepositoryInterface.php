<?php
namespace App\Repositories\Interfaces;

/**
 * Interface GeneralizationRepositoryInterface
 * @package App\Repositories\Interfaces
 */
interface GeneralizationRepositoryInterface
{
    /**
     * Get's a Generalization by it's ID
     *
     * @param int
     */
    public function get($id);

    /**
     * Get's all Generalizations.
     *
     * @return mixed
     */
    public function all();

    /**
     * Deletes an Generalization.
     *
     * @param int
     */
    public function delete($id);

    /**
     * @param array $data
     * @return mixed
     */
    public function store(array $data);


    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function update($id, array $data);

    /**
     * @param array $data
     * @return mixed
     */
    public function allDataTable(array $data);

    /**
     * @param array $data
     * @return mixed
     */
    public function countDataTable(array $data);
}
