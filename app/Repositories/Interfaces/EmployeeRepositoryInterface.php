<?php
namespace App\Repositories\Interfaces;

/**
 * Interface EmployeeRepositoryInterface
 * @package App\Repositories\Interfaces
 */
interface EmployeeRepositoryInterface
{
    /**
     * Get's an Employee by it's ID
     *
     * @param int
     */
    public function get($id);

    /**
     * Get's all Employee.
     *
     * @return mixed
     */
    public function all();

    /**
     * Deletes an Employee.
     *
     * @param int
     */
    public function delete($id);

    /**
     * @param array $data
     * @return mixed
     */
    public function store(array $data);


    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function update($id, array $data);

    /**
     * @param array $data
     * @return mixed
     */
    public function allDataTable(array $data);

    /**
     * @param array $data
     * @return mixed
     */
    public function countDataTable(array $data);
}
