<?php
namespace App\Repositories\Interfaces;

/**
 * Interface WaterSourceRepositoryInterface
 * @package App\Repositories\Interfaces
 */
interface WaterSourceRepositoryInterface
{
    /**
     * Get's an WaterSource by it's ID
     *
     * @param int
     */
    public function get($id);

    /**
     * Get's all WaterSource.
     *
     * @return mixed
     */
    public function all();

    /**
     * Deletes an WaterSource.
     *
     * @param int
     */
    public function delete($id);

    /**
     * @param array $data
     * @return mixed
     */
    public function store(array $data);


    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function update($id, array $data);

    /**
     * @param array $data
     * @return mixed
     */
    public function allDataTable(array $data);

    /**
     * @param array $data
     * @return mixed
     */
    public function countDataTable(array $data);
}
