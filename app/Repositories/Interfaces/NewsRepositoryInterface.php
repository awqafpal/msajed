<?php
namespace App\Repositories\Interfaces;

/**
 * Interface NewsRepositoryInterface
 * @package App\Repositories\Interfaces
 */
interface NewsRepositoryInterface
{
    /**
     * Get's a news by it's ID
     *
     * @param int
     */
    public function get($id);

    /**
     * Get's all news.
     *
     * @return mixed
     */
    public function all();

    /**
     * Deletes a news.
     *
     * @param int
     */
    public function delete($id);

    /**
     * @param array $data
     * @return mixed
     */
    public function store(array $data);


    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function update($id, array $data);

    /**
     * @param array $data
     * @return mixed
     */
    public function allDataTable(array $data);

    /**
     * @param array $data
     * @return mixed
     */
    public function countDataTable(array $data);
}
