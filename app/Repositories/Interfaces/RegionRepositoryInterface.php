<?php
namespace App\Repositories\Interfaces;

/**
 * Interface RegionRepositoryInterface
 * @package App\Repositories\Interfaces
 */
interface RegionRepositoryInterface
{
    /**
     * Get's a region by it's ID
     *
     * @param int
     */
    public function get($id);

    /**
     * Get's all regions.
     *
     * @return mixed
     */
    public function all();


    /**
     * Get's all regions by province_id.
     *
     * @return mixed
     */
    public function provinceRegionList($province_id);

    /**
     * Deletes a region.
     *
     * @param int
     */
    public function delete($id);

    /**
     * @param array $data
     * @return mixed
     */
    public function store(array $data);


    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function update($id, array $data);

    /**
     * @param array $data
     * @return mixed
     */
    public function allDataTable(array $data);

    /**
     * @param array $data
     * @return mixed
     */
    public function countDataTable(array $data);
}
