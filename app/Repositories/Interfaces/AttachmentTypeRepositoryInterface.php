<?php
namespace App\Repositories\Interfaces;

/**
 * Interface AttachmentTypeRepositoryInterface
 * @package App\Repositories\Interfaces
 */
interface AttachmentTypeRepositoryInterface
{
    /**
     * Get's an AttachmentType by it's ID
     *
     * @param int
     */
    public function get($id);

    /**
     * Get's all AttachmentType.
     *
     * @return mixed
     */
    public function all();

    /**
     * Deletes an AttachmentType.
     *
     * @param int
     */
    public function delete($id);

    /**
     * @param array $data
     * @return mixed
     */
    public function store(array $data);


    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function update($id, array $data);

    /**
     * @param array $data
     * @return mixed
     */
    public function allDataTable(array $data);

    /**
     * @param array $data
     * @return mixed
     */
    public function countDataTable(array $data);
}
