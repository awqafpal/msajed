<?php
namespace App\Repositories\Interfaces;

/**
 * Interface VisitChangeOrderRepositoryInterface
 * @package App\Repositories\Interfaces
 */
interface VisitChangeOrderRepositoryInterface
{
    /**
     * Get's an VisitChangeOrder by it's ID
     *
     * @param int
     */
    public function get($id);

    /**
     * Get's all VisitChangeOrder.
     *
     * @return mixed
     */
    public function all();

    /**
     * Deletes an VisitChangeOrder.
     *
     * @param int
     */
    public function delete($id);

    /**
     * @param array $data
     * @return mixed
     */
    public function store(array $data);


    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function update($id, array $data);

    /**
     * @param array $data
     * @return mixed
     */
    public function allDataTable(array $data);

    /**
     * @param array $data
     * @return mixed
     */
    public function countDataTable(array $data);
}
