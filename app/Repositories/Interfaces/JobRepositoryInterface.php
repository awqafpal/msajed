<?php
namespace App\Repositories\Interfaces;

/**
 * Interface JobRepositoryInterface
 * @package App\Repositories\Interfaces
 */
interface JobRepositoryInterface
{
    /**
     * Get's an job by it's ID
     *
     * @param int
     */
    public function get($id);

    /**
     * Get's all job.
     *
     * @return mixed
     */
    public function all();

    /**
     * Deletes an job.
     *
     * @param int
     */
    public function delete($id);

    /**
     * @param array $data
     * @return mixed
     */
    public function store(array $data);


    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function update($id, array $data);

    /**
     * @param array $data
     * @return mixed
     */
    public function allDataTable(array $data);

    /**
     * @param array $data
     * @return mixed
     */
    public function countDataTable(array $data);
}
