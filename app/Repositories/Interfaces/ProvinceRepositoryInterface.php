<?php
namespace App\Repositories\Interfaces;

/**
 * Interface ProvinceRepositoryInterface
 * @package App\Repositories\Interfaces
 */
interface ProvinceRepositoryInterface
{
    /**
     * Get's a province by it's ID
     *
     * @param int
     */
    public function get($id);

    /**
     * Get's all provinces.
     *
     * @return mixed
     */
    public function all();


    /**
     * Deletes a province.
     *
     * @param int
     */
    public function delete($id);

    /**
     * @param array $data
     * @return mixed
     */
    public function store(array $data);


    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function update($id, array $data);

    /**
     * @param array $data
     * @return mixed
     */
    public function allDataTable(array $data);

    /**
     * @param array $data
     * @return mixed
     */
    public function countDataTable(array $data);
}
