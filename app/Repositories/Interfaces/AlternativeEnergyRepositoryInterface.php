<?php
namespace App\Repositories\Interfaces;

/**
 * Interface AlternativeEnergyRepositoryInterface
 * @package App\Repositories\Interfaces
 */
interface AlternativeEnergyRepositoryInterface
{
    /**
     * Get's an AlternativeEnergy by it's ID
     *
     * @param int
     */
    public function get($id);

    /**
     * Get's all AlternativeEnergies.
     *
     * @return mixed
     */
    public function all();

    /**
     * Deletes an AlternativeEnergy.
     *
     * @param int
     */
    public function delete($id);

    /**
     * @param array $data
     * @return mixed
     */
    public function store(array $data);


    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function update($id, array $data);

    /**
     * @param array $data
     * @return mixed
     */
    public function allDataTable(array $data);

    /**
     * @param array $data
     * @return mixed
     */
    public function countDataTable(array $data);
}
