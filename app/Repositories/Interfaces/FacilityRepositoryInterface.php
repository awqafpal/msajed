<?php
namespace App\Repositories\Interfaces;

/**
 * Interface FacilityRepositoryInterface
 * @package App\Repositories\Interfaces
 */
interface FacilityRepositoryInterface
{
    /**
     * Get's a facility by it's ID
     *
     * @param int
     */
    public function get($id);

    /**
     * Get's all facilities.
     *
     * @return mixed
     */
    public function all();
    
    /**
     * Deletes a facility.
     *
     * @param int
     */
    public function delete($id);

    /**
     * @param array $data
     * @return mixed
     */
    public function store(array $data);


    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function update($id, array $data);

    /**
     * @param array $data
     * @return mixed
     */
    public function allDataTable(array $data);

    /**
     * @param array $data
     * @return mixed
     */
    public function countDataTable(array $data);
}
