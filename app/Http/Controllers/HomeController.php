<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex()
    {
        return redirect()->route('admin.dashboard.view');;
    }

    public function systemClear(){
        \Illuminate\Support\Facades\Artisan::call('cache:clear');
        \Illuminate\Support\Facades\Artisan::call('view:clear');
        \Illuminate\Support\Facades\Artisan::call('config:clear');
        \Cache::forget('spatie.permission.cache');
        return 'cleared';
    }


    public function dictionary(){
        return response()->view('admin.common.general')->header('content-type', 'text/javascript; charset=utf-8');
    }
}
