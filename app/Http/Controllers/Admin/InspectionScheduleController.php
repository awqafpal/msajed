<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\InspectionScheduleRepositoryInterface;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Mosque;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;
use App\Http\Requests\InspectionScheduleRequest;
use Illuminate\Support\Facades\DB;
/**
 * Class InspectionScheduleController
 * @property InspectionScheduleRepositoryInterface $schedule_repo
 * @package App\Http\Controllers\Admin
 */
class InspectionScheduleController extends AdminController
{
    /**
     * InspectionScheduleController constructor.
     * @param InspectionScheduleRepositoryInterface $schedule_repo
     */
    public function __construct(InspectionScheduleRepositoryInterface $schedule_repo)
    {
        parent::__construct();
        $this->schedule_repo = $schedule_repo;
        parent::$data['active_menu'] = 'inspection-schedules';
    }
    /////////////////////////////////////////
    public function getIndex()
    {
        return view('admin.inspection-schedules.view', parent::$data);
    }
    /////////////////////////////////////////
    public function getList(Request $request)
    {
        $info = $this->schedule_repo->allDataTable($request->all());
        $count =  $this->schedule_repo->countDataTable($request->all());

        $datatable = Datatables::of($info)->setTotalRecords($count);
        $datatable->editColumn('month_year', function ($row) {
            return  Carbon::parse($row->month_year)->translatedFormat('M-Y');
        });
        $datatable->addColumn('user', function ($row) {
            return  $row->user->name;
        });
        $datatable->editColumn('inspector_approval', function ($row) {
            $data['approval'] = $row->inspector_approval;
            //$data['approved_by'] = $row->approved_by;
            return view('admin.inspection-schedules.parts.approval', $data)->render();
        });
        $datatable->editColumn('department_head_approval', function ($row) {
            $data['approval'] = $row->department_head_approval;
            //$data['approved_by'] = $row->approved_by;
            return view('admin.inspection-schedules.parts.approval', $data)->render();
        });
        $datatable->editColumn('province_manager_approval', function ($row) {
            $data['approval'] = $row->province_manager_approval;
            //$data['approved_by'] = $row->approved_by;
            return view('admin.inspection-schedules.parts.approval', $data)->render();
        });

        $datatable->addColumn('actions', function ($row)
        {
            $data['id'] = $row->id;
            $data['btn_class'] = parent::$data['btn_class'];
            $data['btn_red'] = parent::$data['btn_red'];
            $data['btn_green'] = parent::$data['btn_green'];
            return view('admin.inspection-schedules.parts.actions', $data)->render();
        });
        $datatable->escapeColumns(['*']);
        return $datatable->make(true);
    }
    /////////////////////////////////////////
    public function getAdd()
    {
        parent::$data['users'] = User::where('user_type_id',12)->get();
        return view('admin.inspection-schedules.add', parent::$data);
    }
    /////////////////////////////////////////
    public function postAdd(InspectionScheduleRequest $request)
    {
        $hasSchedule = $this->schedule_repo->hasSchedule($request->user_id , $request->month_year);

        if ($hasSchedule) {
            return $this->generalResponse('false',409, trans('title.warning'), trans('messages.duplicated'),$hasSchedule);
        }
        DB::beginTransaction();
        try {
            // Some database actions
            $add = $this->schedule_repo->store($request->all());
            //$times = $this->schedule_repo->storeTimes($add);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            // Handle errors
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }

        //////////////////////////////////////////
        return $this->generalResponse('true',200, trans('title.success'), trans('messages.added'),$add);
    }
    /////////////////////////////////////////
    public function getShow(Request $request, $id)
    {
        $info = $this->schedule_repo->get($id);
        if (!$info)
        {
            $request->session()->flash('data', ['title' => trans('title.info'),  'code' => 300, 'message' => trans('messages.not_found')]);
            return redirect(route('admin.inspection-schedules.view'));
        }

        parent::$data['info'] = $info->load('times');
        parent::$data['mosques'] = Mosque::where('user_id', $info->user_id)->get(['id','name']);
        parent::$data['times'] = $this->schedule_repo->getTimes($info->month_year);
        parent::$data['firstDayofMonth'] = $firstDayofMonth = $info->month_year . '-01';
        parent::$data['lastDayofMonth'] = Carbon::parse($firstDayofMonth)->endOfMonth()->toDateString();
        return view('admin.inspection-schedules.show', parent::$data);
    }
     /////////////////////////////////////////
     public function getPrint(Request $request, $id)
     {
        $info = $this->schedule_repo->get($id);
        if (!$info)
        {
            $request->session()->flash('data', ['title' => trans('title.info'),  'code' => 300, 'message' => trans('messages.not_found')]);
            return redirect(route('admin.inspection-schedules.view'));
        }

        parent::$data['info'] = $info->load('times');
        parent::$data['mosques'] = Mosque::where('user_id', $info->user_id)->get(['id','name']);
        parent::$data['times'] = $this->schedule_repo->getTimes($info->month_year);
        parent::$data['firstDayofMonth'] = $firstDayofMonth = $info->month_year . '-01';
        parent::$data['lastDayofMonth'] = Carbon::parse($firstDayofMonth)->endOfMonth()->toDateString();
        return view('admin.inspection-schedules.print', parent::$data);
     }
    /////////////////////////////////////////
    public function getEdit(Request $request, $id)
    {
        $info = $this->schedule_repo->get($id);
        if (!$info)
        {
            $request->session()->flash('data', ['title' => trans('title.info'),  'code' => 300, 'message' => trans('messages.not_found')]);
            return redirect(route('admin.inspection-schedules.view'));
        }

        parent::$data['info'] = $info->load('times');
        parent::$data['mosques'] = Mosque::where('user_id', $info->user_id)->get(['id','name']);
        parent::$data['times'] = $this->schedule_repo->getTimes($info->month_year);
        //dd(parent::$data['times'] );
        //dd(parent::$data['info']);
        parent::$data['firstDayofMonth'] = $firstDayofMonth = $info->month_year . '-01';
        parent::$data['lastDayofMonth'] = Carbon::parse($firstDayofMonth)->endOfMonth()->toDateString();
        return view('admin.inspection-schedules.edit', parent::$data);
    }
    /////////////////////////////////////////
    public function postEdit(InspectionScheduleRequest $request, $id)
    {
        $info = $this->schedule_repo->get($id);
        if (!$info)
        {
            $request->session()->flash('data', ['title' => trans('title.info'),  'code' => 300, 'message' => trans('messages.not_found')]);
            return redirect(route('admin.inspection-schedules.view'));
        }
        if ($request->hasFile('poster')) {
            $request['image'] = $this->uploadImage($request->poster, 'inspection-schedules');
        }
        $request['slug'] = str_replace(' ', '-', $request->title);
        $update = $this->schedule_repo->update($id , $request->all());

        if (! $update )
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }
        //////////////////////////////////////////
        return $this->generalResponse('true',200, trans('title.success'), trans('messages.updated'),$update);
    }
    /////////////////////////////////////////
    public function getDelete(Request $request, $id)
    {
        $info = $this->schedule_repo->get($id);
        if (!$info)
        {
            return $this->generalResponse('false',300, trans('title.info'), trans('messages.not_found'),null);
        }
        $delete = $this->schedule_repo->delete($id);
        if (!$delete)
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }

        return $this->generalResponse('true',200, trans('title.success'), trans('messages.deleted'),null);

    }


    ////////////////////////////////////////////////////////////////
    public function getScheduleTimes($id)
    {

        $info = $this->schedule_repo->getScheduleDataTable($id);
        $count =  $this->schedule_repo->countScheduleDataTable($id);

        $datatable = Datatables::of($info)->setTotalRecords($count);
        $datatable->addColumn('day', function ($row) {
            return Carbon::parse($row->date)->translatedFormat('l');
        });
        $datatable->editColumn('date', function ($row) {
            return $row->date->format('Y-m-d');
        });
        $datatable->editColumn('dhuhr', function ($row) {
           return  $row->dhuhrMsq->name ;
        });
        $datatable->editColumn('asr', function ($row) {
            return  $row->asrMsq->name ;
        });
        $datatable->editColumn('maghrib', function ($row) {
            return  $row->maghribMsq->name ;
        });
        $datatable->editColumn('isha', function ($row) {
            return  $row->ishaMsq->name ;
        });
        $datatable->addColumn('dhuhr-edit', function ($row) {
            $data['prayer'] = 'dhuhr';
            $data['info'] = $row->dhuhr;
            $data['mosques'] = Mosque::get(['id','name']);
            return view('admin.inspection-schedules.parts.time', $data)->render();
        });



        $datatable->escapeColumns(['*']);
        return $datatable->make(true);

    }

    /////////////////////////////////////////
    public function postAddTime(Request $request)
    {
        $add = $this->schedule_repo->syncTime($request->all());
        if (!$add)
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }
        $add->prayer = $request->prayer;
        //////////////////////////////////////////
        return $this->generalResponse('true',200, trans('title.success'), trans('messages.added'),$add);
    }

    /////////////////////////////////////////////
    public function postApproval($id, Request $request)
    {
        $info = $this->schedule_repo->get($id);
        if (!$info)
        {
            request()->session()->flash('data', ['title' => trans('title.info'),  'code' => 300, 'message' => trans('messages.not_found')]);
            return redirect(route('admin.inspection-schedules.view'));
        }
        $approval = $this->schedule_repo->approval($id);

        if (! $approval )
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }
        //////////////////////////////////////////
        return $this->generalResponse('true',200, trans('title.success'), trans('messages.approval'),$approval);
    }
    ////////////////////////////////////////////////////////////////
    // JSON >> get all times within specified schedule
    public function getScheduleTimesAjax($id){
        $info = $this->schedule_repo->get($id);
        if (!$info)
        {
            return response()->json([
                'status' => 'false' ,
                'message'=>'schedule not found'
            ],404);
        }
        else{
            $info->load('times');
            //dd($info->times);
            return response()->json([
                'status' => 'true' ,
                'message'=>'تم ارجاع البيانات بنجاح',
                'data' => $info->times
            ],200);
        }
    }

    ///////////////////////////////////////////////////////////////
    // JSON >> get all required visits in custom date
    public function getRequiredVisitsByDate(Request $request){
        $month_year =  date('Y-m', strtotime($request->date));
        $date = $request->date;
        $user = $request->user;

        $schedule = $this->schedule_repo->getByUserAndMonth($request->user, $month_year);
        if (!$schedule)
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.not-found-schedule'),null);
        }

        $prayers =  $this->schedule_repo->getPrayersByCustomDate($schedule->id, $date);
        if (!$prayers){
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.not-found-schedule-mosques'),null);
        }

        //dd($prayers);
        $mosques_ids = [];
        if($prayers->fajr){
            $fajr = json_decode($prayers->fajr);
            array_push($mosques_ids,$fajr->id);
        }
        if($prayers->dhuhr){
            $dhuhr = json_decode($prayers->dhuhr);
            array_push($mosques_ids,$dhuhr->id);
        }
        if($prayers->asr){
            $asr = json_decode($prayers->asr);
            array_push($mosques_ids,$asr->id);
        }
        if($prayers->maghrib){
            $maghrib = json_decode($prayers->maghrib);
            array_push($mosques_ids,$maghrib->id);
        }
        if($prayers->isha){
            $isha = json_decode($prayers->isha);
            array_push($mosques_ids,$isha->id);
        }

        $mosques = Mosque::whereIn('id', $mosques_ids)
            ->whereDoesntHave('visits', function ($query) use ($date, $user) {
                $query->where('visited_by', $user);
                $query->where('date', $date);
            })->get(['id', 'name']);
        return $this->generalResponse('true',200, trans('title.success'), trans('messages.fetched'), $mosques);
    }
}
