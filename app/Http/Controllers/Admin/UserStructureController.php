<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\UserStructure; 
use App\Repositories\Interfaces\UserStructureRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;
use App\Http\Requests\UserStructureRequest;

class UserStructureController extends AdminController
{
    /**
     * UserStructureController constructor.
     * @param UserStructureRepositoryInterface $structure_repo
     */
    public function __construct(UserStructureRepositoryInterface $structure_repo)
    { 
        parent::__construct();
        $this->structure_repo = $structure_repo;
        parent::$data['active_menu'] = 'user-structures';
    }
    /////////////////////////////////////////
    public function getIndex()
    {
        return view('admin.user-structures.view', parent::$data);
    }
    /////////////////////////////////////////
    public function getList(Request $request)
    {
        $info = $this->structure_repo->allDataTable($request->all());
        $count =  $this->structure_repo->countDataTable($request->all());

        $datatable = Datatables::of($info)->setTotalRecords($count);
        $datatable->addColumn('actions', function ($row)
        {
            $data['id'] = $row->id;
            $data['btn_class'] = parent::$data['btn_class'];
            $data['btn_red'] = parent::$data['btn_red'];
            $data['btn_green'] = parent::$data['btn_green'];
            return view('admin.user-structures.parts.actions', $data)->render();
        });

        $datatable->escapeColumns(['*']);
        return $datatable->make(true);
    }
    /////////////////////////////////////////
    public function getAdd()
    {
        return view('admin.user-structures.add', parent::$data);
    }
    /////////////////////////////////////////
    public function postAdd(UserStructureRequest $request)
    {

        $add = $this->structure_repo->store($request->all());

        if (!$add)
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }

        //////////////////////////////////////////
        return $this->generalResponse('true',200, trans('title.success'), trans('messages.added'),$add);
    }
    /////////////////////////////////////////
    public function getEdit(Request $request, $id)
    {
        $info = $this->structure_repo->get($id);
        if (!$info)
        {
            $request->session()->flash('data', ['title' => trans('title.info'),  'code' => 300, 'message' => trans('messages.not_found')]);
            return redirect(route('admin.userStructures.view'));
        }

        parent::$data['info'] = $info;
        return view('admin.user-structures.edit', parent::$data);
    }
    /////////////////////////////////////////
    public function postEdit(UserStructureRequest $request, $id)
    {
        $info = $this->structure_repo->get($id);
        if (!$info)
        {
            $request->session()->flash('data', ['title' => trans('title.info'),  'code' => 300, 'message' => trans('messages.not_found')]);
            return redirect(route('admin.userStructures.view'));
        }

        $update = $this->structure_repo->update($id , $request->all());

        if (! $update )
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }

        //////////////////////////////////////////
        return $this->generalResponse('true',200, trans('title.success'), trans('messages.updated'),$update);
    }
    /////////////////////////////////////////
    public function getDelete(Request $request, $id)
    {
        $info = $this->structure_repo->get($id);
        if (!$info)
        {
            return $this->generalResponse('false',300, trans('title.info'), trans('messages.not_found'),null);
        }
        $delete = $this->structure_repo->delete($id);
        if (!$delete)
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }

        return $this->generalResponse('true',200, trans('title.success'), trans('messages.deleted'),null);

    }
}
 