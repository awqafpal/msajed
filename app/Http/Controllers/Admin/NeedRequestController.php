<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller; 
use App\Repositories\Interfaces\NeedRequestRepositoryInterface;
use Illuminate\Http\Request;
use App\Models\Mosque;
use App\Http\Requests\MosqueNeedRequest;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables; 

class NeedRequestController extends AdminController
{
    /**
     * NeedRequestController constructor.
     * @param NeedRequestRepositoryInterface $needReq_repo
     */
    public function __construct(NeedRequestRepositoryInterface $needReq_repo)
    {
        parent::__construct();
        $this->needReq_repo = $needReq_repo;
        parent::$data['active_menu'] = 'need-requests';
    }
    /////////////////////////////////////////
    public function getIndex()
    {
        return view('admin.need-requests.view', parent::$data);
    }
    /////////////////////////////////////////
    public function getList(Request $request)
    {
        $info = $this->needReq_repo->allDataTable($request->all());
        $count =  $this->needReq_repo->countDataTable($request->all());

        $datatable = Datatables::of($info)->setTotalRecords($count);
        $datatable->addColumn('mosque-name', function ($row)
        {
            return view('admin.need-requests.parts.mosque-name', ['row'=>$row])->render();
        });
        $datatable->addColumn('province-name', function ($row)
        {
            return view('admin.need-requests.parts.province-name', ['row'=>$row])->render();
        });
        $datatable->addColumn('actions', function ($row)
        {
            $data['id'] = $row->id;
            $data['btn_class'] = parent::$data['btn_class'];
            $data['btn_red'] = parent::$data['btn_red'];
            $data['btn_green'] = parent::$data['btn_green'];
            return view('admin.need-requests.parts.actions', $data)->render();
        });
        $datatable->escapeColumns(['*']);
        return $datatable->make(true);
    }
    /////////////////////////////////////////
    public function getAdd()
    {
        parent::$data['mosques'] = Mosque::get(['id','name']);
        return view('admin.need-requests.add', parent::$data);
    }
    /////////////////////////////////////////
    public function postAdd(MosqueNeedRequest $request)
    {
        $request['status'] = $request->get('req_status', 0);
        $add = $this->needReq_repo->store($request->all());
        if (!$add)
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }
        //////////////////////////////////////////
        return $this->generalResponse('true',200, trans('title.success'), trans('messages.added'),$add);
    }
    /////////////////////////////////////////
    public function getEdit(Request $request, $id)
    {
        $info = $this->needReq_repo->get($id);
        if (!$info)
        {
            $request->session()->flash('data', ['title' => trans('title.info'),  'code' => 300, 'message' => trans('messages.not_found')]);
            return redirect(route('admin.need-requests.view'));
        }
        parent::$data['mosques'] = Mosque::get(['id','name']);
        parent::$data['info'] = $info;
        return view('admin.need-requests.edit', parent::$data);
    }
    /////////////////////////////////////////
    public function postEdit(MosqueNeedRequest $request, $id)
    {
        $info = $this->needReq_repo->get($id);
        if (!$info)
        {
            $request->session()->flash('data', ['title' => trans('title.info'),  'code' => 300, 'message' => trans('messages.not_found')]);
            return redirect(route('admin.need-requests.view'));
        }
        $request['status'] = $request->get('req_status', 0);
        $update = $this->needReq_repo->update($id , $request->all());

        if (! $update )
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }
        //////////////////////////////////////////
        return $this->generalResponse('true',200, trans('title.success'), trans('messages.updated'),$update);
    }
    /////////////////////////////////////////
    public function getDelete(Request $request, $id)
    {
        $info = $this->needReq_repo->get($id);
        if (!$info)
        {
            return $this->generalResponse('false',300, trans('title.info'), trans('messages.not_found'),null);
        }
        $delete = $this->needReq_repo->delete($id);
        if (!$delete)
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }

        return $this->generalResponse('true',200, trans('title.success'), trans('messages.deleted'),null);
    }        
}
 