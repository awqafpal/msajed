<?php

namespace App\Http\Controllers\Admin;
 
use App\Http\Controllers\Controller;
use App\Models\Province;
use App\Repositories\Interfaces\RegionRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;
use App\Http\Requests\RegionRequest;

/**
 * Class RegionController
 * @property RegionRepositoryInterface $region_repo
 * @package App\Http\Controllers\Admin
 */
class RegionController extends AdminController
{
    /**
     * RegionController constructor.
     * @param RegionRepositoryInterface $region_repo
     */
    public function __construct(RegionRepositoryInterface $region_repo)
    {
        parent::__construct();
        $this->region_repo = $region_repo;
        parent::$data['active_menu'] = 'regions';
    }
    /////////////////////////////////////////
    public function getIndex()
    {
        return view('admin.regions.view', parent::$data);
    }
    /////////////////////////////////////////
    public function getList(Request $request)
    {
        $info = $this->region_repo->allDataTable($request->all());
        $count =  $this->region_repo->countDataTable($request->all());

        $datatable = Datatables::of($info)->setTotalRecords($count);
        $datatable->addColumn('actions', function ($row)
        {
            $data['id'] = $row->id;
            $data['btn_class'] = parent::$data['btn_class'];
            $data['btn_red'] = parent::$data['btn_red'];
            $data['btn_green'] = parent::$data['btn_green'];
            return view('admin.regions.parts.actions', $data)->render();
        });

        $datatable->escapeColumns(['*']);
        return $datatable->make(true);
    }
    /////////////////////////////////////////
    public function getAdd()
    {
        parent::$data['provinces'] = Province::all();
        return view('admin.regions.add', parent::$data);
    }
    /////////////////////////////////////////
    public function postAdd(RegionRequest $request)
    {

        $add = $this->region_repo->store($request->all());

        if (!$add)
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }

        //////////////////////////////////////////
        return $this->generalResponse('true',200, trans('title.success'), trans('messages.added'),$add);
    }
    /////////////////////////////////////////
    public function getEdit(Request $request, $id)
    {
        $info = $this->region_repo->get($id);
        if (!$info)
        {
            $request->session()->flash('data', ['title' => trans('title.info'),  'code' => 300, 'message' => trans('messages.not_found')]);
            return redirect(route('admin.regions.view'));
        }

        parent::$data['info'] = $info;
        parent::$data['provinces'] = Province::all();
        return view('admin.regions.edit', parent::$data);
    }
    /////////////////////////////////////////
    public function postEdit(RegionRequest $request, $id)
    {
        $info = $this->region_repo->get($id);
        if (!$info)
        {
            $request->session()->flash('data', ['title' => trans('title.info'),  'code' => 300, 'message' => trans('messages.not_found')]);
            return redirect(route('admin.regions.view'));
        }

        $update = $this->region_repo->update($id , $request->all());

        if (! $update )
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }

        //////////////////////////////////////////
        return $this->generalResponse('true',200, trans('title.success'), trans('messages.updated'),$update);
    }
    /////////////////////////////////////////
    public function getDelete(Request $request, $id)
    {
        $info = $this->region_repo->get($id);
        if (!$info)
        {
            return $this->generalResponse('false',300, trans('title.info'), trans('messages.not_found'),null);
        }
        $delete = $this->region_repo->delete($id);
        if (!$delete)
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }

        return $this->generalResponse('true',200, trans('title.success'), trans('messages.deleted'),null);

    }

    /////////////////////////////////////////
    // JSON >> get all mosques in a specific province
    public function getMosquesAJAX($id){
        $info = $this->region_repo->get($id);
        if(! $info){
            return null;
        }else{
            $mosques = $info->mosques;
            $output = '';
            foreach ($mosques as $row) {
                $output .= '<option class="prov-mosque" value="' . $row->id . '">' . $row->name . '</option>';
            }
            return  $output;
        }
    }

     /////////////////////////////////////////
    // JSON >> get all employees in a specific region
    public function getEmployeesAJAX($id){
        $info = $this->region_repo->get($id);
        if(! $info){
            return null;
        }else{
            $employees = $info->employees;
            $output = '';
            foreach ($employees as $row) {
                $output .= '<option class="region-emp" value="' . $row->id . '">' . $row->name . '</option>';
            }
            return  $output;
        }
    }
}
