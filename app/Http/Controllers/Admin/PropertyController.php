<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Province;
use App\Repositories\Interfaces\PropertyRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;
use App\Http\Requests\PropertyRequest;

/**
 * Class PropertyController
 * @property PropertyRepositoryInterface $property_repo
 * @package App\Http\Controllers\Admin
 */

class PropertyController extends AdminController
{
    /**
     * PropertyController constructor.
     * @param PropertyRepositoryInterface $property_repo
     */
    public function __construct(PropertyRepositoryInterface $property_repo)
    {
        parent::__construct();
        $this->property_repo = $property_repo;
        parent::$data['active_menu'] = 'properties';
    }
    /////////////////////////////////////////
    public function getIndex()
    {
        return view('admin.properties.view', parent::$data);
    }
    /////////////////////////////////////////
    public function getList(Request $request)
    {
        $info = $this->property_repo->allDataTable($request->all());
        $count =  $this->property_repo->countDataTable($request->all());

        $datatable = Datatables::of($info)->setTotalRecords($count);
        $datatable->addColumn('actions', function ($row)
        {
            $data['id'] = $row->id;
            $data['btn_class'] = parent::$data['btn_class'];
            $data['btn_red'] = parent::$data['btn_red'];
            $data['btn_green'] = parent::$data['btn_green'];
            return view('admin.properties.parts.actions', $data)->render();
        });

        $datatable->escapeColumns(['*']);
        return $datatable->make(true);
    }
    /////////////////////////////////////////
    public function getAdd()
    {
        parent::$data['provinces'] = Province::all();
        return view('admin.properties.add', parent::$data);
    }
    /////////////////////////////////////////
    public function postAdd(PropertyRequest $request)
    {

        $add = $this->property_repo->store($request->all());

        if (!$add)
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }

        //////////////////////////////////////////
        return $this->generalResponse('true',200, trans('title.success'), trans('messages.added'),$add);
    }
    /////////////////////////////////////////
    public function getEdit(Request $request, $id)
    {
        $info = $this->property_repo->get($id);
        if (!$info)
        {
            $request->session()->flash('data', ['title' => trans('title.info'),  'code' => 300, 'message' => trans('messages.not_found')]);
            return redirect(route('admin.properties.view'));
        }

        parent::$data['info'] = $info;
        parent::$data['provinces'] = Province::all();
        return view('admin.properties.edit', parent::$data);
    }
    /////////////////////////////////////////
    public function postEdit(PropertyRequest $request, $id)
    {
        $info = $this->property_repo->get($id);
        if (!$info)
        {
            $request->session()->flash('data', ['title' => trans('title.info'),  'code' => 300, 'message' => trans('messages.not_found')]);
            return redirect(route('admin.properties.view'));
        }

        $update = $this->property_repo->update($id , $request->all());

        if (! $update )
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }

        //////////////////////////////////////////
        return $this->generalResponse('true',200, trans('title.success'), trans('messages.updated'),$update);
    }
    /////////////////////////////////////////
    public function getDelete(Request $request, $id)
    {
        $info = $this->property_repo->get($id);
        if (!$info)
        {
            return $this->generalResponse('false',300, trans('title.info'), trans('messages.not_found'),null);
        }
        $delete = $this->property_repo->delete($id);
        if (!$delete)
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }

        return $this->generalResponse('true',200, trans('title.success'), trans('messages.deleted'),null);

    }
}
