<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\VisitChangeOrderRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;
use App\Http\Requests\VisitChangeRequest;
use App\Models\Job;
use App\Models\Province;
use App\Models\Constant;
use App\Models\Region;
use App\Models\Mosque;


/**
 * Class VisitChangeOrderController
 * @property VisitChangeOrderRepositoryInterface $order_repo
 * @package App\Http\Controllers\Admin
 */
class VisitChangeOrderController extends AdminController
{
    /**
     * VisitChangeOrderController constructor.
     * @param VisitChangeOrderRepositoryInterface $order_repo
     */
    public function __construct(VisitChangeOrderRepositoryInterface $order_repo)
    {
        parent::__construct();
        $this->order_repo = $order_repo;
        parent::$data['active_menu'] = 'visit-change-orders';
    }
    /////////////////////////////////////////
    public function getIndex()
    {
        return view('admin.visit-change-orders.view', parent::$data);
    }
    /////////////////////////////////////////
    public function getList(Request $request)
    {
        $info = $this->order_repo->allDataTable($request->all());
        $count =  $this->order_repo->countDataTable($request->all());

        $datatable = Datatables::of($info)->setTotalRecords($count);
        $datatable->editColumn('image', function ($row)
        {
            $data['image'] = $row->image;
            return view('admin.visit-change-orders.parts.image', $data)->render();
        });
        $datatable->addColumn('province-name', function ($row)
        {
            return view('admin.visit-change-orders.parts.province-name', ['row'=>$row])->render();
        });
        $datatable->addColumn('job-name', function ($row)
        {
            return view('admin.visit-change-orders.parts.job-name', ['row'=>$row])->render();
        });
        $datatable->addColumn('mosque-name', function ($row)
        {
            return view('admin.visit-change-orders.parts.mosque-name', ['row'=>$row])->render();
        });
        $datatable->addColumn('actions', function ($row)
        {
            $data['id'] = $row->id;
            $data['btn_class'] = parent::$data['btn_class'];
            $data['btn_red'] = parent::$data['btn_red'];
            $data['btn_green'] = parent::$data['btn_green'];
            return view('admin.visit-change-orders.parts.actions', $data)->render();
        });
        $datatable->escapeColumns(['*']);
        return $datatable->make(true);
    }
    /////////////////////////////////////////
    public function getAdd()
    {
        parent::$data['job_types'] = Constant::where('parent_id',16)->get();
        parent::$data['hiring_types'] = Constant::where('parent_id',10)->get();
        parent::$data['jobs'] = Job::all();
        parent::$data['provinces'] = Province::all();
        return view('admin.visit-change-orders.add', parent::$data);
    }
    /////////////////////////////////////////
    public function postAdd(VisitChangeRequest $request)
    {
        if ($request->hasFile('avatar')) {
            $request['image'] = $this->uploadImage($request->avatar, 'visit-change-orders');
        }
        $add = $this->order_repo->store($request->all());
        if (!$add)
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }
        //////////////////////////////////////////
        return $this->generalResponse('true',200, trans('title.success'), trans('messages.added'),$add);
    }
    /////////////////////////////////////////
    public function getEdit(Request $request, $id)
    {
        $info = $this->order_repo->get($id);
        if (!$info)
        {
            $request->session()->flash('data', ['title' => trans('title.info'),  'code' => 300, 'message' => trans('messages.not_found')]);
            return redirect(route('admin.visit-change-orders.view'));
        }

        parent::$data['info'] = $info;
        parent::$data['job_types'] = Constant::where('parent_id',16)->get();
        parent::$data['hiring_types'] = Constant::where('parent_id',10)->get();
        parent::$data['jobs'] = Job::all();
        parent::$data['provinces'] = Province::all();
        parent::$data['regions'] = Region::where('province_id',$info->region->province_id)->get(['id','name']);
        parent::$data['mosques'] = Mosque::where('region_id',$info->region_id)->get(['id','name']);

        return view('admin.visit-change-orders.edit', parent::$data);
    }
    /////////////////////////////////////////
    public function postEdit(VisitChangeRequest $request, $id)
    {
        $info = $this->order_repo->get($id);
        if (!$info)
        {
            $request->session()->flash('data', ['title' => trans('title.info'),  'code' => 300, 'message' => trans('messages.not_found')]);
            return redirect(route('admin.visit-change-orders.view'));
        }
        if ($request->hasFile('avatar')) {
            $request['image'] = $this->uploadImage($request->avatar, 'visit-change-orders');
        }
        $update = $this->order_repo->update($id , $request->all());

        if (! $update )
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }
        //////////////////////////////////////////
        return $this->generalResponse('true',200, trans('title.success'), trans('messages.updated'),$update);
    }
    /////////////////////////////////////////
    public function getDelete(Request $request, $id)
    {
        $info = $this->order_repo->get($id);
        if (!$info)
        {
            return $this->generalResponse('false',300, trans('title.info'), trans('messages.not_found'),null);
        }
        $delete = $this->order_repo->delete($id);
        if (!$delete)
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }

        return $this->generalResponse('true',200, trans('title.success'), trans('messages.deleted'),null);

    }
    /////////////////////////////////////////

}
