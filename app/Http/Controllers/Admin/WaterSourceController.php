<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\WaterSourceRepositoryInterface;
use Illuminate\Http\Request; 
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables; 
use App\Http\Requests\WaterSourceRequest;

/** 
 * Class WaterSourceController
 * @property WaterSourceRepositoryInterface $source
 * @package App\Http\Controllers\Admin
 */ 
class WaterSourceController extends AdminController
{
    /**
     * WaterSourceController constructor.
     * @param WaterSourceRepositoryInterface $source
     */
    public function __construct(WaterSourceRepositoryInterface $source)
    {
        parent::__construct();
        $this->source = $source;
        parent::$data['active_menu'] = 'water-sources';
    }
    /////////////////////////////////////////
    public function getIndex()
    {
        return view('admin.water-sources.view', parent::$data);
    }
    /////////////////////////////////////////
    public function getList(Request $request)
    {
        $info = $this->source->allDataTable($request->all());
        $count =  $this->source->countDataTable($request->all());

        $datatable = Datatables::of($info)->setTotalRecords($count);
        $datatable->addColumn('actions', function ($row)
        {
            $data['id'] = $row->id;
            $data['btn_class'] = parent::$data['btn_class'];
            $data['btn_red'] = parent::$data['btn_red'];
            $data['btn_green'] = parent::$data['btn_green'];
            return view('admin.water-sources.parts.actions', $data)->render();
        });
        $datatable->escapeColumns(['*']);
        return $datatable->make(true);
    }
    /////////////////////////////////////////
    public function getAdd()
    {
        return view('admin.water-sources.add', parent::$data);
    }
    /////////////////////////////////////////
    public function postAdd(WaterSourceRequest $request)
    {
        $add = $this->source->store($request->all());
        if (!$add)
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }
        //////////////////////////////////////////
        return $this->generalResponse('true',200, trans('title.success'), trans('messages.added'),$add);
    }
    /////////////////////////////////////////
    public function getEdit(Request $request, $id)
    {
        $info = $this->source->get($id);
        if (!$info)
        {
            $request->session()->flash('data', ['title' => trans('title.info'),  'code' => 300, 'message' => trans('messages.not_found')]);
            return redirect(route('admin.WaterSources.view'));
        }

        parent::$data['info'] = $info;
        return view('admin.water-sources.edit', parent::$data);
    }
    /////////////////////////////////////////
    public function postEdit(WaterSourceRequest $request, $id)
    {
        $info = $this->source->get($id);
        if (!$info)
        {
            $request->session()->flash('data', ['title' => trans('title.info'),  'code' => 300, 'message' => trans('messages.not_found')]);
            return redirect(route('admin.WaterSources.view'));
        }
        $update = $this->source->update($id , $request->all());

        if (! $update )
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }
        //////////////////////////////////////////
        return $this->generalResponse('true',200, trans('title.success'), trans('messages.updated'),$update);
    }
    /////////////////////////////////////////
    public function getDelete(Request $request, $id)
    {
        $info = $this->source->get($id);
        if (!$info)
        {
            return $this->generalResponse('false',300, trans('title.info'), trans('messages.not_found'),null);
        }
        $delete = $this->source->delete($id);
        if (!$delete)
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }

        return $this->generalResponse('true',200, trans('title.success'), trans('messages.deleted'),null);

    }
}
