<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\EmployeeRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;
use App\Http\Requests\EmployeeRequest;
use App\Models\Job;
use App\Models\Province;
use App\Models\Constant;
use App\Models\Region;
use App\Models\Mosque;
use App\Exports\EmployeesExport;
use Maatwebsite\Excel\Facades\Excel;

/**
 * Class EmployeeController
 * @property EmployeeRepositoryInterface $employee_repo
 * @package App\Http\Controllers\Admin
 */
class EmployeeController extends AdminController
{
    /**
     * EmployeeController constructor.
     * @param EmployeeRepositoryInterface $employee_repo
     */
    public function __construct(EmployeeRepositoryInterface $employee_repo)
    {
        parent::__construct();
        $this->employee_repo = $employee_repo;
        parent::$data['active_menu'] = 'employees';
    }
    /////////////////////////////////////////
    public function getIndex()
    {
        return view('admin.employees.view', parent::$data);
    }
    /////////////////////////////////////////
    public function getList(Request $request)
    {
        $info = $this->employee_repo->allDataTable($request->all());
        $count =  $this->employee_repo->countDataTable($request->all());

        $datatable = Datatables::of($info)->setTotalRecords($count);
        $datatable->editColumn('image', function ($row)
        {
            $data['image'] = $row->image;
            return view('admin.employees.parts.image', $data)->render();
        });
        $datatable->addColumn('province-name', function ($row)
        {
            return view('admin.employees.parts.province-name', ['row'=>$row])->render();
        });
        $datatable->addColumn('job-name', function ($row)
        {
            return view('admin.employees.parts.job-name', ['row'=>$row])->render();
        });
        $datatable->addColumn('mosque-name', function ($row)
        {
            return view('admin.employees.parts.mosque-name', ['row'=>$row])->render();
        });
        $datatable->addColumn('actions', function ($row)
        {
            $data['id'] = $row->id;
            $data['btn_class'] = parent::$data['btn_class'];
            $data['btn_red'] = parent::$data['btn_red'];
            $data['btn_green'] = parent::$data['btn_green'];
            return view('admin.employees.parts.actions', $data)->render();
        });
        $datatable->escapeColumns(['*']);
        return $datatable->make(true);
    }
    /////////////////////////////////////////
    public function getAdd()
    {
        parent::$data['job_types'] = Constant::where('parent_id',16)->get();
        parent::$data['hiring_types'] = Constant::where('parent_id',10)->get();
        parent::$data['jobs'] = Job::all();
        parent::$data['provinces'] = Province::all();
        return view('admin.employees.add', parent::$data);
    }
    /////////////////////////////////////////
    public function postAdd(EmployeeRequest $request)
    {
        if ($request->hasFile('avatar')) {
            $request['image'] = $this->uploadImage($request->avatar, 'employees');
        }
        $add = $this->employee_repo->store($request->all());
        if (!$add)
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }
        //////////////////////////////////////////
        return $this->generalResponse('true',200, trans('title.success'), trans('messages.added'),$add);
    }
    /////////////////////////////////////////
    public function getEdit(Request $request, $id)
    {
        $info = $this->employee_repo->get($id);
        if (!$info)
        {
            $request->session()->flash('data', ['title' => trans('title.info'),  'code' => 300, 'message' => trans('messages.not_found')]);
            return redirect(route('admin.employees.view'));
        }

        parent::$data['info'] = $info;
        parent::$data['job_types'] = Constant::where('parent_id',16)->get();
        parent::$data['hiring_types'] = Constant::where('parent_id',10)->get();
        parent::$data['jobs'] = Job::all();
        parent::$data['provinces'] = Province::all();
        parent::$data['regions'] = Region::where('province_id',$info->region->province_id)->get(['id','name']);
        parent::$data['mosques'] = Mosque::where('region_id',$info->region_id)->get(['id','name']);

        return view('admin.employees.edit', parent::$data);
    }
    /////////////////////////////////////////
    public function postEdit(EmployeeRequest $request, $id)
    {
        $info = $this->employee_repo->get($id);
        if (!$info)
        {
            $request->session()->flash('data', ['title' => trans('title.info'),  'code' => 300, 'message' => trans('messages.not_found')]);
            return redirect(route('admin.employees.view'));
        }
        if ($request->hasFile('avatar')) {
            $request['image'] = $this->uploadImage($request->avatar, 'employees');
        }
        $update = $this->employee_repo->update($id , $request->all());

        if (! $update )
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }
        //////////////////////////////////////////
        return $this->generalResponse('true',200, trans('title.success'), trans('messages.updated'),$update);
    }
    /////////////////////////////////////////
    public function getDelete(Request $request, $id)
    {
        $info = $this->employee_repo->get($id);
        if (!$info)
        {
            return $this->generalResponse('false',300, trans('title.info'), trans('messages.not_found'),null);
        }
        $delete = $this->employee_repo->delete($id);
        if (!$delete)
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }

        return $this->generalResponse('true',200, trans('title.success'), trans('messages.deleted'),null);

    }
    /////////////////////////////////////////
    public function getExport()
    {
        return Excel::download(new EmployeesExport, 'employees.xlsx');
    }
}
