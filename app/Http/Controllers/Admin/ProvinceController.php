<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\ProvinceRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables; 
use App\Http\Requests\ProvinceRequest;

/** 
 * Class ProvinceController
 * @property ProvinceRepositoryInterface $province_repo
 * @package App\Http\Controllers\Admin
 */
class ProvinceController extends AdminController
{
    /**
     * ProvinceController constructor.
     * @param ProvinceRepositoryInterface $province_repo
     */
    public function __construct(ProvinceRepositoryInterface $province_repo)
    {
        parent::__construct();
        $this->province_repo = $province_repo;
        parent::$data['active_menu'] = 'provinces';
    }
    /////////////////////////////////////////
    public function getIndex()
    {
        return view('admin.provinces.view', parent::$data);
    }
    /////////////////////////////////////////
    public function getList(Request $request)
    {
        $info = $this->province_repo->allDataTable($request->all());
        $count =  $this->province_repo->countDataTable($request->all());

        $datatable = Datatables::of($info)->setTotalRecords($count);
        $datatable->addColumn('regions_count', function ($row)
        {
            return $row->regions->count();
        });
        $datatable->addColumn('actions', function ($row)
        {
            $data['id'] = $row->id;
            $data['btn_class'] = parent::$data['btn_class'];
            $data['btn_red'] = parent::$data['btn_red'];
            $data['btn_green'] = parent::$data['btn_green'];
            return view('admin.provinces.parts.actions', $data)->render();
        });
        $datatable->escapeColumns(['*']);
        return $datatable->make(true);
    }
    /////////////////////////////////////////
    public function getAdd()
    {
        return view('admin.provinces.add', parent::$data);
    }
    /////////////////////////////////////////
    public function postAdd(ProvinceRequest $request)
    {
        $add = $this->province_repo->store($request->all());
        if (!$add)
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }
        //////////////////////////////////////////
        return $this->generalResponse('true',200, trans('title.success'), trans('messages.added'),$add);
    }
    /////////////////////////////////////////
    public function getEdit(Request $request, $id)
    {
        $info = $this->province_repo->get($id);
        if (!$info)
        {
            $request->session()->flash('data', ['title' => trans('title.info'),  'code' => 300, 'message' => trans('messages.not_found')]);
            return redirect(route('admin.provinces.view'));
        }

        parent::$data['info'] = $info;
        return view('admin.provinces.edit', parent::$data);
    }
    /////////////////////////////////////////
    public function postEdit(ProvinceRequest $request, $id)
    {
        $info = $this->province_repo->get($id);
        if (!$info)
        {
            $request->session()->flash('data', ['title' => trans('title.info'),  'code' => 300, 'message' => trans('messages.not_found')]);
            return redirect(route('admin.provinces.view'));
        }
        $update = $this->province_repo->update($id , $request->all());

        if (! $update )
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }
        //////////////////////////////////////////
        return $this->generalResponse('true',200, trans('title.success'), trans('messages.updated'),$update);
    }
    /////////////////////////////////////////
    public function getDelete(Request $request, $id)
    {
        $info = $this->province_repo->get($id);
        if (!$info)
        {
            return $this->generalResponse('false',300, trans('title.info'), trans('messages.not_found'),null);
        }
        $delete = $this->province_repo->delete($id);
        if (!$delete)
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }

        return $this->generalResponse('true',200, trans('title.success'), trans('messages.deleted'),null);
    }
    
    /////////////////////////////////////////
    // JSON >> get all regions in a specific province
    public function getRegionsAJAX($id){
        $province = $this->province_repo->get($id);
        if(! $province){
            return null;
        }else{
            $regions = $province->regions;
            $output = '';
            foreach ($regions as $row) {
                $output .= '<option class="prov-region" value="' . $row->id . '">' . $row->name . '</option>';
            }
            return  $output;
        }
    }

}
