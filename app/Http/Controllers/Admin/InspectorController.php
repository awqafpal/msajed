<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use App\Repositories\Interfaces\UserRepositoryInterface;
use App\Repositories\Interfaces\MosqueRepositoryInterface;
use App\Models\Region;
use App\Models\Mosque;

/**
 * Class UsersController
 * @property UserRepositoryInterface $user_repo
 * @property MosqueRepositoryInterface $mosque_repo

 * @package App\Http\Controllers\Admin
 */

class InspectorController extends AdminController
{
    /**
     * @var UserRepositoryInterface
     * @var MosqueRepositoryInterface
     */
    protected $user_repo;
    protected $mosque_repo;
    /**
     * UsersController constructor.
     * @param UserRepositoryInterface $user_repo
     * @param MosqueRepositoryInterface $mosque_repo
     */
    public function __construct(UserRepositoryInterface $user_repo, MosqueRepositoryInterface $mosque_repo)
    {
        parent::__construct();
        $this->user_repo = $user_repo;
        $this->mosque_repo = $mosque_repo;
        parent::$data['active_menu'] = 'inspectors';
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex()
    {
        return view('admin.inspectors.view', parent::$data);
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws \Exception
     */
    public function getList(Request $request)
    {
        // inspectors type id = 12
        $info = $this->user_repo->typeGetDataTable(12,$request->all());
        $count =  $this->user_repo->typeCountDataTable(12,$request->all());
        $dataTable = Datatables::of($info)->setTotalRecords($count);
        $dataTable->editColumn('image', function ($row)
        {
            $data['image'] = $row->image;
            return view('admin.inspectors.parts.image', $data)->render();
        });
        $dataTable->addColumn('province-name', function ($row)
        {
            return view('admin.inspectors.parts.province-name', ['row'=>$row])->render();
        });
        $dataTable->addColumn('mosques', function ($row)
        {
            $data['mosques'] = $row->mosques->count();
            return view('admin.inspectors.parts.mosques', $data)->render();
        });
        $dataTable->editColumn('status', function ($row)
        {
            $data['id_hash'] = $row->id_hash;
            $data['status'] = $row->status;

            return view('admin.inspectors.parts.status', $data)->render();
        });

        $dataTable->addColumn('actions', function ($row)
        {
            $data['id_hash'] = $row->id_hash;

            return view('admin.inspectors.parts.actions', $data)->render();
        });
        $dataTable->escapeColumns(['*']);
        return $dataTable->make(true);
    }

    /////////////////////////////////////////
    public function getMosques(Request $request, $id)
    {
        $info = $this->user_repo->get($id);
        if (!$info)
        {
            $request->session()->flash('data', ['title' => trans('title.info'),  'code' => 300, 'message' => trans('messages.not_found')]);
            return redirect(route('admin.inspectors.view'));
        }
        parent::$data['id'] = $id;
        parent::$data['info'] = $info;
        return view('admin.inspectors.mosques-list', parent::$data);
    }


    public function getMosquesList(Request $request, $id)
    {
        $info = $this->mosque_repo->getByUserDataTable($id,$request->all());
        $count =  $this->mosque_repo->countByUserDataTable($id,$request->all());
        $datatable = Datatables::of($info)->setTotalRecords($count);
        $datatable->editColumn('image', function ($row)
        {
            $data['image'] = $row->image;
            return view('admin.mosques.parts.image', $data)->render();
        });
        $datatable->addColumn('province-name', function ($row)
        {
            return view('admin.mosques.parts.province-name', ['row'=>$row])->render();
        });
        $datatable->editColumn('archaeological_status', function ($row)
        {
            $data['archaeological_status'] = $row->archaeological_status;
            return view('admin.mosques.parts.archaeological-status', $data)->render();
        });
        $datatable->editColumn('central_status', function ($row)
        {
            $data['central_status'] = $row->central_status;
            return view('admin.mosques.parts.central-status', $data)->render();
        });
        $datatable->addColumn('actions', function ($row)
        {
            $data['id'] = $row->id;
            $data['btn_class'] = parent::$data['btn_class'];
            $data['btn_red'] = parent::$data['btn_red'];
            $data['btn_green'] = parent::$data['btn_green'];
            return view('admin.inspectors.parts.mosque-actions', $data)->render();
        });
        $datatable->escapeColumns(['*']);
        return $datatable->make(true);
    }
    /////////////////////////////////////////
    public function getEdit(Request $request, $id)
    {
        $info = $this->user_repo->get($id);
        if (!$info)
        {
            $request->session()->flash('data', ['title' => trans('title.info'),  'code' => 300, 'message' => trans('messages.not_found')]);
            return redirect(route('admin.inspectors.view'));
        }

        // get all region in inspector province
        $regions = Region::where('province_id',$info->region->province_id)->where('id','!=',$info->region_id)->get();

        /* $inspector_region_mosques = Mosque::where('region_id',$inspector->region_id)->get();  //region where inspector live */

        $inspector_region_mosques = Mosque::where('region_id',$info->region_id)
            ->where(function($query) use ($id){
                $query->whereNull('user_id')
                ->orWhere('user_id',$id);
            })
            ->get(); // region where inspector live

        $other_regions_mosques = [] ;
        foreach ($regions as $region){
            $mosques = Mosque::where('region_id',$region->id)
                        ->where(function($query) use ($id){
                            $query->whereNull('user_id')
                            ->orWhere('user_id',$id);
                        })
                        ->get();
            (count($mosques) > 0 ) ? $other_regions_mosques[$region->name] =$mosques : $other_regions_mosques[$region->name] = [];
        }

        parent::$data['inspector_region_mosques'] = $inspector_region_mosques;
        parent::$data['other_regions_mosques'] = $other_regions_mosques;
        parent::$data['info'] = $info;
        return view('admin.inspectors.edit-mosques', parent::$data);
    }
     /////////////////////////////////////////
    public function postEdit(Request $request, $id)
    {
        $info = $this->user_repo->get($id);
        if (!$info)
        {
            $request->session()->flash('data', ['title' => trans('title.info'),  'code' => 300, 'message' => trans('messages.not_found')]);
            return redirect(route('admin.inspectors.view'));
        }
        $update = false;
        if($request->has('mosques') && !is_null($request->mosques)){
            $update = $this->mosque_repo->updateMosqueInspector($id , $request->mosques , true);
        }

        if (! $update )
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }
        //////////////////////////////////////////
        return $this->generalResponse('true',200, trans('title.success'), trans('messages.updated'),$update);
    }
    /////////////////////////////////////////
    public function getDelete(Request $request, $id)
    {
        $info = $this->mosque_repo->get($id);
        if (!$info)
        {
            return $this->generalResponse('false',300, trans('title.info'), trans('messages.not_found'),null);
        }
        $request['user_id'] = NULL ;
        $update = $this->mosque_repo->update($id , $request->all());

        if (! $update )
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }
        //////////////////////////////////////////
        return $this->generalResponse('true',200, trans('title.success'), trans('messages.deleted'),$update);
    }


}
