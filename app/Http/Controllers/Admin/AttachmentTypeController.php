<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\AttachmentTypeRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables; 
use App\Http\Requests\AttachmentTypeRequest;

/** 
 * Class AttachmentTypeController
 * @property AttachmentTypeRepositoryInterface $type_repo
 * @package App\Http\Controllers\Admin
 */
class AttachmentTypeController extends AdminController
{
    /**
     * AttachmentTypeController constructor.
     * @param AttachmentTypeRepositoryInterface $type_repo
     */
    public function __construct(AttachmentTypeRepositoryInterface $type_repo)
    {
        parent::__construct();
        $this->type_repo = $type_repo;
        parent::$data['active_menu'] = 'attachment-types';
    }
    /////////////////////////////////////////
    public function getIndex()
    {
        return view('admin.attachment-types.view', parent::$data);
    }
    /////////////////////////////////////////
    public function getList(Request $request) 
    {
        $info = $this->type_repo->allDataTable($request->all());
        $count =  $this->type_repo->countDataTable($request->all());

        $datatable = Datatables::of($info)->setTotalRecords($count);
        $datatable->addColumn('actions', function ($row)
        {
            $data['id'] = $row->id;
            $data['btn_class'] = parent::$data['btn_class'];
            $data['btn_red'] = parent::$data['btn_red'];
            $data['btn_green'] = parent::$data['btn_green'];
            return view('admin.attachment-types.parts.actions', $data)->render();
        });
        $datatable->escapeColumns(['*']);
        return $datatable->make(true);
    }
    /////////////////////////////////////////
    public function getAdd()
    {
        return view('admin.attachment-types.add', parent::$data);
    }
    /////////////////////////////////////////
    public function postAdd(AttachmentTypeRequest $request)
    {
        $add = $this->type_repo->store($request->all());
        if (!$add)
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }
        //////////////////////////////////////////
        return $this->generalResponse('true',200, trans('title.success'), trans('messages.added'),$add);
    }
    /////////////////////////////////////////
    public function getEdit(Request $request, $id)
    {
        $info = $this->type_repo->get($id);
        if (!$info)
        {
            $request->session()->flash('data', ['title' => trans('title.info'),  'code' => 300, 'message' => trans('messages.not_found')]);
            return redirect(route('admin.attachmentTypes.view'));
        }

        parent::$data['info'] = $info;
        return view('admin.attachment-types.edit', parent::$data);
    }
    /////////////////////////////////////////
    public function postEdit(AttachmentTypeRequest $request, $id)
    {
        $info = $this->type_repo->get($id);
        if (!$info)
        {
            $request->session()->flash('data', ['title' => trans('title.info'),  'code' => 300, 'message' => trans('messages.not_found')]);
            return redirect(route('admin.attachmentTypes.view'));
        }
        $update = $this->type_repo->update($id , $request->all());

        if (! $update )
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }
        //////////////////////////////////////////
        return $this->generalResponse('true',200, trans('title.success'), trans('messages.updated'),$update);
    }
    /////////////////////////////////////////
    public function getDelete(Request $request, $id)
    {
        $info = $this->type_repo->get($id);
        if (!$info)
        {
            return $this->generalResponse('false',300, trans('title.info'), trans('messages.not_found'),null);
        }
        $delete = $this->type_repo->delete($id);
        if (!$delete)
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }

        return $this->generalResponse('true',200, trans('title.success'), trans('messages.deleted'),null);

    }
}
