<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\CleaningComponentRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables; 
use App\Http\Requests\CleaningComponentRequest;

/** 
 * Class CleaningComponentController
 * @property CleaningComponentRepositoryInterface $component_repo
 * @package App\Http\Controllers\Admin
 */
class CleaningComponentController extends AdminController
{
    /**
     * CleaningComponentController constructor.
     * @param CleaningComponentRepositoryInterface $component_repo
     */
    public function __construct(CleaningComponentRepositoryInterface $component_repo)
    {
        parent::__construct();
        $this->component_repo = $component_repo;
        parent::$data['active_menu'] = 'cleaning-components';
    }
    /////////////////////////////////////////
    public function getIndex()
    {
        return view('admin.cleaning-components.view', parent::$data);
    }
    /////////////////////////////////////////
    public function getList(Request $request)
    {
        $info = $this->component_repo->allDataTable($request->all());
        $count =  $this->component_repo->countDataTable($request->all());

        $datatable = Datatables::of($info)->setTotalRecords($count);
        $datatable->addColumn('status', function ($row)
        {
            $data['status'] = $row->is_used;
            return view('admin.cleaning-components.parts.status', $data)->render();
        });
        $datatable->addColumn('actions', function ($row)
        {
            $data['id'] = $row->id;
            $data['status'] = $row->is_used;
            $data['btn_class'] = parent::$data['btn_class'];
            $data['btn_red'] = parent::$data['btn_red'];
            $data['btn_green'] = parent::$data['btn_green'];
            return view('admin.cleaning-components.parts.actions', $data)->render();
        });
        $datatable->escapeColumns(['*']);
        return $datatable->make(true);
    }
    /////////////////////////////////////////
    public function getAdd()
    {
        return view('admin.cleaning-components.add', parent::$data);
    }
    /////////////////////////////////////////
    public function postAdd(CleaningComponentRequest $request)
    {
        $add = $this->component_repo->store($request->all());
        if (!$add)
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }
        //////////////////////////////////////////
        return $this->generalResponse('true',200, trans('title.success'), trans('messages.added'),$add);
    }
    /////////////////////////////////////////
    public function getEdit(Request $request, $id)
    {
        $info = $this->component_repo->get($id);
        if (!$info)
        {
            $request->session()->flash('data', ['title' => trans('title.info'),  'code' => 300, 'message' => trans('messages.not_found')]);
            return redirect(route('admin.CleaningComponents.view'));
        }

        parent::$data['info'] = $info;
        return view('admin.cleaning-components.edit', parent::$data);
    }
    /////////////////////////////////////////
    public function postEdit(CleaningComponentRequest $request, $id)
    {
        $info = $this->component_repo->get($id);
        if (!$info)
        {
            $request->session()->flash('data', ['title' => trans('title.info'),  'code' => 300, 'message' => trans('messages.not_found')]);
            return redirect(route('admin.CleaningComponents.view'));
        }
        $update = $this->component_repo->update($id , $request->all());

        if (! $update )
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }
        //////////////////////////////////////////
        return $this->generalResponse('true',200, trans('title.success'), trans('messages.updated'),$update);
    }
    /////////////////////////////////////////
    public function getDelete(Request $request, $id)
    {
        $info = $this->component_repo->get($id);
        if (!$info)
        {
            return $this->generalResponse('false',300, trans('title.info'), trans('messages.not_found'),null);
        }
        $delete = $this->component_repo->delete($id);
        if (!$delete)
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }

        return $this->generalResponse('true',200, trans('title.success'), trans('messages.deleted'),null);

    }

    /////////////////////////////////////////
    public function changeStatus(Request $request, $id)
    {
        $info = $this->component_repo->get($id);
        if (!$info)
        {
            return $this->generalResponse('false',300, trans('title.info'), trans('messages.not_found'),null);
        }
        $status = $this->component_repo->changeStatus($id);
        if (!$status)
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }

        return $this->generalResponse('true',200, trans('title.success'), trans('messages.change_status'),null);

    }
}
