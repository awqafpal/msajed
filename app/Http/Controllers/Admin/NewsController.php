<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\NewsRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables; 
use App\Http\Requests\NewsRequest;

/** 
 * Class NewsController
 * @property NewsRepositoryInterface $news_repo
 * @package App\Http\Controllers\Admin
 */
class NewsController extends AdminController
{
    /**
     * NewsController constructor.
     * @param NewsRepositoryInterface $news_repo
     */
    public function __construct(NewsRepositoryInterface $news_repo)
    {
        parent::__construct();
        $this->news_repo = $news_repo;
        parent::$data['active_menu'] = 'news';
    }
    /////////////////////////////////////////
    public function getIndex()
    {
        return view('admin.news.view', parent::$data);
    }
    /////////////////////////////////////////
    public function getList(Request $request)
    {
        $info = $this->news_repo->allDataTable($request->all());
        $count =  $this->news_repo->countDataTable($request->all());

        $datatable = Datatables::of($info)->setTotalRecords($count);
        $datatable->editColumn('image', function ($row)
        {
            $data['image'] = $row->image;
            return view('admin.news.parts.image', $data)->render();
        });
        $datatable->editColumn('views', function ($row)
        {
            $data['views'] = $row->views;
            return view('admin.news.parts.views', $data)->render();
        });
        $datatable->editColumn('status', function ($row)
        {
            $data['status'] = $row->status;
            return view('admin.news.parts.status', $data)->render();
        });
        $datatable->editColumn('slider', function ($row)
        {
            $data['slider'] = $row->status;
            return view('admin.news.parts.slider', $data)->render();
        });
        $datatable->addColumn('actions', function ($row)
        {
            $data['id'] = $row->id;
            $data['btn_class'] = parent::$data['btn_class'];
            $data['btn_red'] = parent::$data['btn_red'];
            $data['btn_green'] = parent::$data['btn_green'];
            return view('admin.news.parts.actions', $data)->render();
        });
        $datatable->escapeColumns(['*']);
        return $datatable->make(true);
    }
    /////////////////////////////////////////
    public function getAdd()
    {
        return view('admin.news.add', parent::$data);
    }
    /////////////////////////////////////////
    public function postAdd(NewsRequest $request)
    {
        if ($request->hasFile('poster')) {
            $request['image'] = $this->uploadImage($request->poster, 'news');
        }
        $request['slug'] = str_replace(' ', '-', $request->title);
        $add = $this->news_repo->store($request->all());
        if (!$add)
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }
        //////////////////////////////////////////
        return $this->generalResponse('true',200, trans('title.success'), trans('messages.added'),$add);
    }
    /////////////////////////////////////////
    public function getEdit(Request $request, $id)
    {
        $info = $this->news_repo->get($id);
        if (!$info)
        {
            $request->session()->flash('data', ['title' => trans('title.info'),  'code' => 300, 'message' => trans('messages.not_found')]);
            return redirect(route('admin.news.view'));
        }

        parent::$data['info'] = $info;
        return view('admin.news.edit', parent::$data);
    }
    /////////////////////////////////////////
    public function postEdit(NewsRequest $request, $id)
    {
        $info = $this->news_repo->get($id);
        if (!$info)
        {
            $request->session()->flash('data', ['title' => trans('title.info'),  'code' => 300, 'message' => trans('messages.not_found')]);
            return redirect(route('admin.news.view'));
        }
        if ($request->hasFile('poster')) {
            $request['image'] = $this->uploadImage($request->poster, 'news');
        }
        $request['slug'] = str_replace(' ', '-', $request->title);
        $update = $this->news_repo->update($id , $request->all()); 

        if (! $update )
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }
        //////////////////////////////////////////
        return $this->generalResponse('true',200, trans('title.success'), trans('messages.updated'),$update);
    }
    /////////////////////////////////////////
    public function getDelete(Request $request, $id)
    {
        $info = $this->news_repo->get($id);
        if (!$info)
        {
            return $this->generalResponse('false',300, trans('title.info'), trans('messages.not_found'),null);
        }
        $delete = $this->news_repo->delete($id);
        if (!$delete)
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }

        return $this->generalResponse('true',200, trans('title.success'), trans('messages.deleted'),null);

    }
}
