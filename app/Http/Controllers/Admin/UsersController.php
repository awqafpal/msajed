<?php
/**
 * Created by PhpStorm.
 * User: mohammed
 * Date: 3/9/19
 * Time: 1:42 PM
 */

namespace App\Http\Controllers\Admin;

use App\Models\ModelHasPermission;
use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use App\Models\Province;
use App\Models\Region;
use App\Models\UserType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;
use App\Repositories\Interfaces\UserRepositoryInterface;
use App\Http\Requests\UserRequest;
/**
 * Class UsersController
 * @property UserRepositoryInterface $user_repo
 * @package App\Http\Controllers\Admin
 */
class UsersController extends AdminController
{
    /**
     * @var UserRepositoryInterface
     */
    protected $user_repo;

    /**
     * UsersController constructor.
     * @param UserRepositoryInterface $user_repo
     */
    public function __construct(UserRepositoryInterface $user_repo)
    {
        parent::__construct();
        $this->user_repo = $user_repo;
        parent::$data['active_menu'] = 'users';
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex()
    {
        return view('admin.users.view', parent::$data);
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws \Exception
     */
    public function getList(Request $request)
    {
        $info = $this->user_repo->allDataTable($request->all());
        $count =  $this->user_repo->countDataTable($request->all());
        $dataTable = Datatables::of($info)->setTotalRecords($count);
        $dataTable->editColumn('image', function ($row)
        {
            $data['image'] = $row->image;
            return view('admin.users.parts.image', $data)->render();
        });
        $dataTable->addColumn('province-name', function ($row)
        {
            return view('admin.users.parts.province-name', ['row'=>$row])->render();
        });
        $dataTable->editColumn('user-type', function ($row)
        {
            $data['type'] = $row->type;
            return view('admin.users.parts.user-type', $data)->render();
        });
        $dataTable->editColumn('role_id', function ($row)
        {
            $roles = $row->roles->pluck('name')->toArray();
            return implode(',', $roles);
        });
        $dataTable->editColumn('status', function ($row)
        {
            $data['id_hash'] = $row->id_hash;
            $data['status'] = $row->status; 

            return view('admin.users.parts.status', $data)->render();
        });

        $dataTable->addColumn('actions', function ($row)
        {
            $data['id_hash'] = $row->id_hash;

            return view('admin.users.parts.actions', $data)->render();
        });
        $dataTable->escapeColumns(['*']);
        return $dataTable->make(true);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getAdd()
    {
        parent::$data['roles'] = Role::all();
        parent::$data['provinces'] = Province::all();
        parent::$data['types'] = UserType::all();
        return view('admin.users.add', parent::$data);
    }

    /**
     * @param UserRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postAdd(UserRequest $request)
    {
        if ($request->hasFile('avatar')) {
            $request['image'] = $this->uploadImage($request->avatar, 'users');
        }
        $add = $this->user_repo->store($request->all());
        if (!$add)
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }
        $add->syncRoles($request->get('role'));
        //////////////////////////////////////////
        return $this->generalResponse('true',200, trans('title.success'), trans('messages.added'),$add);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function getEdit(Request $request, $id)
    {
        $id = $this->decrypt($id);

        $info = $this->user_repo->get($id);
        if (!$info || $id == 1)
        {
            $request->session()->flash('data', ['title' => trans('title.info'),  'code' => 300, 'message' => trans('messages.not_found')]);
            return redirect(route('admin.users.view'));
        }
        parent::$data['info'] = $info;
        parent::$data['roles'] = Role::all();
        parent::$data['provinces'] = Province::all();
        parent::$data['regions'] = Region::where('province_id',$info->region->province_id)->get(['id','name']);
        parent::$data['types'] = UserType::all();
        return view('admin.users.edit', parent::$data);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postEdit(UserRequest $request, $id)
    {
        $id = $this->decrypt($id);

        $info = $this->user_repo->get($id);
        if (!$info || $id == 1)
        {
            $request->session()->flash('data', ['title' => trans('title.info'),  'code' => 300, 'message' => trans('messages.not_found')]);
            return redirect(route('admin.users.view'));
        }

        if ($request->hasFile('avatar')) {
            $request['image'] = $this->uploadImage($request->avatar, 'users');
        }
        $update = $this->user_repo->update($id, $request->all());
        
        if (!$update)
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }
        $info->syncRoles($request->get('role'));
        Cache::forget('spatie.permission.cache');
        //////////////////////////////////////////////
        return $this->generalResponse('true',200, trans('title.success'), trans('messages.updated'),$update);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function getPassword(Request $request, $id)
    {
        $id = $this->decrypt($id);

        $info = $this->user_repo->get($id);
        if (!$info || $id == 1)
        {
            $request->session()->flash('data', ['title' => trans('title.info'),  'code' => 300, 'message' => trans('messages.not_found')]);
            return redirect(route('admin.users.view'));
        }
        parent::$data['info'] = $info;
        return view('admin.users.password', parent::$data);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postPassword(Request $request, $id)
    {
        $id = $this->decrypt($id);

        $info = $this->user_repo->get($id);
        if (!$info || $id == 1)
        {
            $request->session()->flash('data', ['title' => trans('title.info'),  'code' => 300, 'message' => trans('messages.not_found')]);
            return redirect(route('admin.users.view'));
        }

        $data = [
            'password' => $request->get('password')
        ];

        $validator = Validator::make($request->all(), [
            'password' => 'required|between:6,16|confirmed',
            'password_confirmation' => 'required|between:6,16'
        ]);
        ////////////////////////
        if ($validator->fails())
        {
            return $this->generalResponse('false',400, trans('title.warning'), $validator->messages()->first(),null);
        }

        $update = $this->user_repo->update($id, $data);
        if (!$update)
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }
        return $this->generalResponse('true',200, trans('title.success'), trans('messages.changed'),$update);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDelete($id)
    {
        $id = $this->decrypt($id);
        ///////////////////
        $info = $this->user_repo->get($id);
        if (!$info || $id == 1)
        {
            return $this->generalResponse('false',300, trans('title.info'), trans('messages.not_found'),null);
        }
        $delete = $this->user_repo->delete($id);
        if (!$delete)
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }
        return $this->generalResponse('true',200, trans('title.success'), trans('messages.deleted'),null);
    }
    /////////////////////////////////////////
    public function getPermissions(Request $request, $id)
    {
        if ($id == 1)
        {
            $request->session()->flash('danger', trans('messages.not_found'));
            return redirect(route('admin.users.view'));
        }
        /////////////////////////////////////////
        $info = User::find($id);
        if(!$info)
        {
            $request->session()->flash('danger', trans('messages.not_found'));
            return redirect(route('admin.users.view'));
        }
        parent::$data['info'] = $info;
        parent::$data['permissions'] = Permission::with('children')->where('parent_id', 0)->get();
        parent::$data['role_permissions'] = ModelHasPermission::where('model_id', $id)->get()->toArray();
        return view('admin.users.permissions', parent::$data);
    }
    public function postPermissions(Request $request, $id)
    {
        $user = User::find($id);
        $permissions = $request->get('permissions');
        if(is_array($permissions) && sizeof($permissions) > 0)
        {
            ModelHasPermission::where('model_id', $id)->delete();

            foreach ($permissions as $permission)
            {
                $user->givePermissionTo($permission);

            }
            Cache::forget('spatie.permission.cache');
            /////////////////////////////////////////
            return $this->generalResponse('true',200, trans('title.success'), trans('messages.updated'),null);
        }
        else
        {
            ModelHasPermission::where('model_id', $id)->delete();
            /////////////////////////////////////////
            Cache::forget('spatie.permission.cache');
            /////////////////////////////////////////
            return $this->generalResponse('true',200, trans('title.success'), trans('messages.updated'),null);
        }

    }
}
