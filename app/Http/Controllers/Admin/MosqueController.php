<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\MosqueRepositoryInterface;
use Illuminate\Http\Request;
use App\Models\Province;
use App\Models\Region;
use App\Models\Constant;
use App\Models\Property;
use App\Models\User;
use App\Models\Employee;
use App\Models\Mosque;
use App\Models\AttachmentType;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;
use App\Http\Requests\MosqueRequest;
use App\Http\Requests\MosqueEmployeesRequest;
use App\Http\Requests\MosqueAttachmentsRequest;
use App\Http\Requests\MosqueDetailsRequest;
use App\Http\Requests\MosqueBuildingRequest;
use App\Http\Requests\MosqueRebuildingRequest;
use Illuminate\Support\Facades\DB;
use App\Exports\MosquesExport;
use Maatwebsite\Excel\Facades\Excel;

/**
 * Class MosqueController
 * @property MosqueRepositoryInterface $mosque_repo
 * @package App\Http\Controllers\Admin
 */
class MosqueController extends AdminController
{
    /**
     * MosqueController constructor.
     * @param MosqueRepositoryInterface $mosque_repo
     */
    public function __construct(MosqueRepositoryInterface $mosque_repo)
    {
        parent::__construct();
        $this->mosque_repo = $mosque_repo;
        parent::$data['active_menu'] = 'mosques';
    }
    /////////////////////////////////////////
    public function getIndex()
    {
        return view('admin.mosques.view', parent::$data);
    }
    /////////////////////////////////////////
    public function getList(Request $request)
    {
        $info = $this->mosque_repo->allDataTable($request->all());
        $count =  $this->mosque_repo->countDataTable($request->all());

        $datatable = Datatables::of($info)->setTotalRecords($count);
        $datatable->editColumn('image', function ($row)
        {
            $data['image'] = $row->image;
            return view('admin.mosques.parts.image', $data)->render();
        });
        $datatable->addColumn('province-name', function ($row)
        {
            return view('admin.mosques.parts.province-name', ['row'=>$row])->render();
        });
        $datatable->editColumn('archaeological_status', function ($row)
        {
            $data['archaeological_status'] = $row->archaeological_status;
            return view('admin.mosques.parts.archaeological-status', $data)->render();
        });
        $datatable->editColumn('central_status', function ($row)
        {
            $data['central_status'] = $row->central_status;
            return view('admin.mosques.parts.central-status', $data)->render();
        });
        $datatable->addColumn('actions', function ($row)
        {
            $data['id'] = $row->id;
            $data['btn_class'] = parent::$data['btn_class'];
            $data['btn_red'] = parent::$data['btn_red'];
            $data['btn_green'] = parent::$data['btn_green'];
            return view('admin.mosques.parts.actions', $data)->render();
        });
        $datatable->escapeColumns(['*']);
        return $datatable->make(true);
    }
    /////////////////////////////////////////
    public function getAdd()
    {
        parent::$data['provinces'] = Province::all();
        parent::$data['regions'] = Region::all();
        parent::$data['properties'] = Property::all();
        parent::$data['users'] = User::where('user_type_id',12)->get();
        parent::$data['types'] = Constant::where('parent_id',1)->get();
        parent::$data['building_types'] = Constant::where('parent_id',5)->get();
        parent::$data['employees'] = Employee::whereNull('mosque_id')->get(['id','name']);
        parent::$data['attachment_types'] = AttachmentType::all();
        parent::$data['supervisor_authority_types'] = Constant::where('parent_id',19)->get();
        parent::$data['creation_reasons'] = Constant::where('parent_id',24)->get();
        parent::$data['rebuild_reasons'] = Constant::where('parent_id',31)->get();
        parent::$data['locations'] = Constant::where('parent_id',28)->get();

        $info = new Mosque();
        $mosque = $info->load('region','user','assignees','boardMembers','attachments',
        'employees','supervisor','allocationData','mosqueRebuilds');
        parent::$data['info'] = $mosque;
        parent::$data['action'] = route('admin.mosques.store');
        //dd($mosque);
        return view('admin.mosques.add', parent::$data);
    }
    /////////////////////////////////////////
    public function postAdd(MosqueRequest $request)
    {
        if ($request->hasFile('mosque_image')) {
            $request['image'] = $this->uploadImage($request->mosque_image, 'mosques');
        }
        $request['slug'] = str_replace(' ', '-', $request->name);
        if($request->has('mosque_id') && $request->mosque_id){
            $info = $this->mosque_repo->update($request->mosque_id , $request->all());
        }else{
            $info = $this->mosque_repo->store($request->all());
        }

        if (!$info)
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }
        //////////////////////////////////////////
        return $this->generalResponse('true',200, trans('title.success'), trans('messages.added'),$info);
    }
    /////////////////////////////////////////
    public function getEdit(Request $request, $id)
    {
        $info = $this->mosque_repo->get($id);
        if (!$info)
        {
            $request->session()->flash('data', ['title' => trans('title.info'),  'code' => 300, 'message' => trans('messages.not_found')]);
            return redirect(route('admin.mosques.view'));
        }

        parent::$data['provinces'] = Province::all();
        parent::$data['regions'] = Region::all();
        parent::$data['properties'] = Property::all();
        parent::$data['users'] = User::where('user_type_id',12)->get();
        parent::$data['types'] = Constant::where('parent_id',1)->get();
        parent::$data['building_types'] = Constant::where('parent_id',5)->get();
        parent::$data['employees'] = Employee::whereNull('mosque_id')->get(['id','name']);
        parent::$data['attachment_types'] = AttachmentType::all();
        parent::$data['supervisor_authority_types'] = Constant::where('parent_id',19)->get();
        parent::$data['creation_reasons'] = Constant::where('parent_id',24)->get();
        parent::$data['rebuild_reasons'] = Constant::where('parent_id',31)->get();
        parent::$data['locations'] = Constant::where('parent_id',28)->get();

        parent::$data['action'] = route('admin.mosques.update', $info->id);
        parent::$data['info'] = $info->load('region','user','assignees','boardMembers','attachments',
        'employees','supervisor','allocationData','mosqueRebuilds');
        //dd(parent::$data['info']);
        return view('admin.mosques.edit', parent::$data);
    }
    /////////////////////////////////////////
    public function postEdit(MosqueRequest $request, $id)
    {
        //dd($request->all());
        $info = $this->mosque_repo->get($id);
        if (!$info)
        {
            $request->session()->flash('data', ['title' => trans('title.info'),  'code' => 300, 'message' => trans('messages.not_found')]);
            return redirect(route('admin.mosques.view'));
        }
        if ($request->hasFile('mosque_image')) {
            $request['image'] = $this->uploadImage($request->mosque_image, 'mosques');
        }
        $request['slug'] = str_replace(' ', '-', $request->name);
        $update = $this->mosque_repo->update($id , $request->all());

        if (! $update )
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }
        //////////////////////////////////////////
        return $this->generalResponse('true',200, trans('title.success'), trans('messages.updated'),$update);
    }
    /////////////////////////////////////////
    public function getDelete(Request $request, $id)
    {
        $info = $this->mosque_repo->get($id);
        if (!$info)
        {
            return $this->generalResponse('false',300, trans('title.info'), trans('messages.not_found'),null);
        }
        $delete = $this->mosque_repo->delete($id);
        if (!$delete)
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }

        return $this->generalResponse('true',200, trans('title.success'), trans('messages.deleted'),null);

    }
    /////////////////////////////////////////
    public function getExport()
    {
        return Excel::download(new MosquesExport, 'mosques.xlsx');
    }

    /////////////////////////////////////////
    public function syncEmployees(MosqueEmployeesRequest $request)
    {
        $info = $this->mosque_repo->get($request->mosque_id);
        if (!$info)
        {
            $request->session()->flash('data', ['title' => trans('title.info'),  'code' => 300, 'message' => trans('messages.not_found')]);
        }
        $update = $this->mosque_repo->syncEmployees($request->mosque_id , $request->employees);

        if (! $update )
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }
        //////////////////////////////////////////
        return $this->generalResponse('true',200, trans('title.success'), trans('messages.updated'),$update);
    }
    /////////////////////////////////////////
    public function uploadAttachments(MosqueAttachmentsRequest $request)
    {
        //dd($request->all());
        $info = $this->mosque_repo->get($request->mosque_id);
        if (!$info)
        {
            $request->session()->flash('data', ['title' => trans('title.info'),  'code' => 300, 'message' => trans('messages.not_found')]);
        }
        if ($request->hasFile('file')) {
            foreach($request->file('file') as $image)
            {
                $img = $this->uploadImage($image, 'attachments/mosque_'. $request->mosque_id);
                $update = $this->mosque_repo->uploadAttachments($request->mosque_id , $request->attachment_type_id, $img);
            }
        }else{
            $update = false ;
        }

        if (! $update )
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }
        //////////////////////////////////////////
        return $this->generalResponse('true',200, trans('title.success'), trans('messages.updated'),$update);
    }
    /////////////////////////////////////////
    public function syncDetails(Request $request)
    {
        //dd($request->all());
        $mosqueId = $request->mosque_id ;
        $assigneeData = [
            //'mosque_id'=>  $request->mosque_id,
            'assignee_user_name'=> $request->assignee_user_name,
            'assignee_identity'=> $request->assignee_identity,
            'assignee_mobile'=> $request->assignee_mobile,
            'assignee_date'=> $request->assignee_date,
            'assignee_land_space'=> $request->assignee_land_space,
            'assignee_notes'=> $request->assignee_notes,
        ];
        $allocationData = [
            //'mosque_id'=>  $request->mosque_id,
            'allocation_date'=> $request->allocation_date,
            'allocation_land_space'=> $request->allocation_land_space,
            'allocation_notes'=> $request->allocation_notes,
        ];
        $supervisorData = [
            //'mosque_id'=>  $request->mosque_id,
            'type'=> $request->supervisor_authority_type,
            'supervisor_authority_name'=> $request->supervisor_authority_name,
        ];
        $boardData = [
            'administrativeBoard'=> $request->administrativeBoard,
        ];

        DB::beginTransaction();
        try {
            // Some database actions
            $this->mosque_repo->syncAssigneeData($mosqueId, $assigneeData);
            $this->mosque_repo->syncAllocationData($mosqueId, $allocationData);
            $this->mosque_repo->syncSupervisorData($mosqueId, $supervisorData);
            $this->mosque_repo->syncBoardData($mosqueId, $boardData);
            DB::commit();
            $info = $this->mosque_repo->get($mosqueId);
        } catch (\Exception $e) {
            DB::rollback();
            // Handle errors
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }
        // Return success message
        return $this->generalResponse('true',200, trans('title.success'), trans('messages.added'),$info);
    }
    /////////////////////////////////////////
    public function syncBuilding(MosqueBuildingRequest $request)
    {
        //dd($request->all());
        $mosqueId = $request->mosque_id ;
        $buildingData = [
            'date_bulid_ad'=> $request->date_bulid_ad,
            'date_bulid_hijri'=> $request->date_bulid_hijri,
            'creation_reason'=> $request->creation_reason,
            'building_cost'=> $request->building_cost,
            'location'=> $request->location,
            'country_name'=> $request->country_name,
            'user_name'=> $request->user_name,
            'date_of_formation'=> $request->date_of_formation,
            'mosque_building_notes'=> $request->mosque_building_notes,
            'engineer_full_name'=> $request->engineer_full_name,
            'engineer_identity_number'=> $request->engineer_identity_number,
            'engineer_phone_number'=> $request->engineer_phone_number,
            'engineer_address'=> $request->engineer_address,
            'engineer_license_number'=> $request->engineer_license_number
        ];
        $buildingBoard = [
            'buildingBoard'=> $request->buildingBoard,
        ];
        DB::beginTransaction();
        try {
            // Some database actions
            $this->mosque_repo->syncBuildingData($mosqueId, $buildingData);
            $this->mosque_repo->syncBuildingBoardData($mosqueId, $buildingBoard);
            DB::commit();
            $info = $this->mosque_repo->get($mosqueId);
        } catch (\Exception $e) {
            DB::rollback();
            // Handle errors
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }
        // Return success message
        return $this->generalResponse('true',200, trans('title.success'), trans('messages.added'),$info);
    }

    /////////////////////////////////////////
    public function syncRebuilding(MosqueRebuildingRequest $request)
    {
        //dd($request->all());
        $mosqueId = $request->mosque_id ;
        $rebuildingData = [
            'date_rebuilding_ad'=> $request->date_rebuilding_ad,
            'date_rebuilding_hijri'=> $request->date_rebuilding_hijri,
            'rebuilding_reason'=> $request->rebuilding_reason,
            'rebuilding_cost'=> $request->rebuilding_cost,
            'location'=> $request->location,
            'country_name'=> $request->country_name,
            'user_name'=> $request->user_name,
            'date_of_formation'=> $request->date_of_formation,
            'mosque_rebuilding_notes'=> $request->mosque_rebuilding_notes,
            'engineer_full_name'=> $request->engineer_full_name,
            'engineer_identity_number'=> $request->engineer_identity_number,
            'engineer_phone_number'=> $request->engineer_phone_number,
            'engineer_address'=> $request->engineer_address,
            'engineer_license_number'=> $request->engineer_license_number,
            'due_date'=> $request->due_date,
            'grant'=> $request->grant,
            'space'=> $request->space,
            'year_of_damage'=> $request->year_of_damage
        ];
        $rebuildingBoard = [
            'rebuildingBoard'=> $request->rebuildingBoard,
        ];
        DB::beginTransaction();
        try {
            // Some database actions
            $this->mosque_repo->syncRebuildingData($mosqueId, $rebuildingData);
            $this->mosque_repo->syncRebuildingBoardData($mosqueId, $rebuildingBoard);
            DB::commit();
            $info = $this->mosque_repo->get($mosqueId);
        } catch (\Exception $e) {
            DB::rollback();
            // Handle errors
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }
        // Return success message
        return $this->generalResponse('true',200, trans('title.success'), trans('messages.added'),$info);
    }
}
