<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\ElectricityTypeRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;
use App\Http\Requests\ElectricityTypeRequest;

/**
 * Class ElectricityTypeController
 * @property ElectricityTypeRepositoryInterface $electricity_repo
 * @package App\Http\Controllers\Admin
 */

class ElectricityTypeController extends AdminController
{
    /**
     * ElectricityTypeController constructor.
     * @param ElectricityTypeRepositoryInterface $electricity_repo
     */
    public function __construct(ElectricityTypeRepositoryInterface $electricity_repo)
    {
        parent::__construct();
        $this->electricity_repo = $electricity_repo;
        parent::$data['active_menu'] = 'electricity-types';
    }
    /////////////////////////////////////////
    public function getIndex()
    {
        return view('admin.electricity-types.view', parent::$data);
    }
    /////////////////////////////////////////
    public function getList(Request $request)
    { 
        $info = $this->electricity_repo->allDataTable($request->all());
        $count =  $this->electricity_repo->countDataTable($request->all());

        $datatable = Datatables::of($info)->setTotalRecords($count);
        $datatable->addColumn('actions', function ($row)
        {
            $data['id'] = $row->id;
            $data['btn_class'] = parent::$data['btn_class'];
            $data['btn_red'] = parent::$data['btn_red'];
            $data['btn_green'] = parent::$data['btn_green'];
            return view('admin.electricity-types.parts.actions', $data)->render();
        });

        $datatable->escapeColumns(['*']);
        return $datatable->make(true);
    }
    /////////////////////////////////////////
    public function getAdd()
    {
        return view('admin.electricity-types.add', parent::$data);
    }
    /////////////////////////////////////////
    public function postAdd(ElectricityTypeRequest $request)
    {

        $add = $this->electricity_repo->store($request->all());

        if (!$add)
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }

        //////////////////////////////////////////
        return $this->generalResponse('true',200, trans('title.success'), trans('messages.added'),$add);
    }
    /////////////////////////////////////////
    public function getEdit(Request $request, $id)
    {
        $info = $this->electricity_repo->get($id);
        if (!$info)
        {
            $request->session()->flash('data', ['title' => trans('title.info'),  'code' => 300, 'message' => trans('messages.not_found')]);
            return redirect(route('admin.electricityTypes.view'));
        }

        parent::$data['info'] = $info;
        return view('admin.electricity-types.edit', parent::$data);
    } 
    /////////////////////////////////////////
    public function postEdit(ElectricityTypeRequest $request, $id)
    {
        $info = $this->electricity_repo->get($id);
        if (!$info)
        {
            $request->session()->flash('data', ['title' => trans('title.info'),  'code' => 300, 'message' => trans('messages.not_found')]);
            return redirect(route('admin.electricityTypes.view'));
        }

        $update = $this->electricity_repo->update($id , $request->all());

        if (! $update )
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }

        //////////////////////////////////////////
        return $this->generalResponse('true',200, trans('title.success'), trans('messages.updated'),$update);
    }
    /////////////////////////////////////////
    public function getDelete(Request $request, $id)
    {
        $info = $this->electricity_repo->get($id);
        if (!$info)
        {
            return $this->generalResponse('false',300, trans('title.info'), trans('messages.not_found'),null);
        }
        $delete = $this->electricity_repo->delete($id);
        if (!$delete)
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }

        return $this->generalResponse('true',200, trans('title.success'), trans('messages.deleted'),null);

    }
}
