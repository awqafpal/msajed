<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Models\Mosque;
use App\Models\Constant;
use Illuminate\Http\Request;
use App\Models\CleaningComponent;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use App\Http\Requests\MosqueVisitRequest;
use Illuminate\Support\Facades\Validator;
use App\Repositories\Interfaces\MosqueVisitRepositoryInterface;

/**
 * Class MosqueVisitController
 * @property MosqueVisitRepositoryInterface $visit_repo
 * @package App\Http\Controllers\Admin
 */
class MosqueVisitController extends AdminController
{
    /**
     * EmployeeController constructor.
     * @param MosqueVisitRepositoryInterface $visit_repo
     */
    public function __construct(MosqueVisitRepositoryInterface $visit_repo)
    {
        parent::__construct();
        $this->visit_repo = $visit_repo;
        parent::$data['active_menu'] = 'visits';
    }
    /////////////////////////////////////////
    public function getIndex()
    {
        return view('admin.visits.view', parent::$data);
    }
    /////////////////////////////////////////
    public function getList(Request $request)
    {
        $info = $this->visit_repo->allDataTable($request->all());
        $count =  $this->visit_repo->countDataTable($request->all());

        $datatable = Datatables::of($info)->setTotalRecords($count);
        $datatable->editColumn('date', function ($row)
        {
            return $row->date->format('Y-m-d');
        });
        $datatable->addColumn('mosque-name', function ($row)
        {
            return view('admin.visits.parts.mosque-name', ['row'=>$row])->render();
        });
        $datatable->addColumn('visitor-name', function ($row)
        {
            return view('admin.visits.parts.visitor-name', ['row'=>$row])->render();
        });
        $datatable->addColumn('province-name', function ($row)
        {
            return view('admin.visits.parts.province-name', ['row'=>$row])->render();
        });
        $datatable->addColumn('status', function ($row)
        {
            return view('admin.visits.parts.approval-status', ['status'=>$row->status])->render();
        });
        $datatable->addColumn('actions', function ($row)
        {
            $data['id'] = $row->id;
            $data['btn_class'] = parent::$data['btn_class'];
            $data['btn_red'] = parent::$data['btn_red'];
            $data['btn_green'] = parent::$data['btn_green'];
            return view('admin.visits.parts.actions', $data)->render();
        });
        $datatable->escapeColumns(['*']);
        return $datatable->make(true);
    }
    /////////////////////////////////////////
    public function getAdd()
    {
        parent::$data['users'] = User::related()->where('user_type_id', 12)->get();
        parent::$data['hiring_types'] = Constant::where('parent_id',10)->get();
        return view('admin.visits.add', parent::$data);
    }
    /////////////////////////////////////////
    public function getSetup($mosque_id)
    {
        parent::$data['statuses'] = Constant::where('parent_id',35)->get();
        parent::$data['mosque'] = $mosque = Mosque::with('employees')->find($mosque_id);
        parent::$data['employees'] = $mosque->employees;
        parent::$data['components'] = CleaningComponent::all();
        //dd($employees);
        return view('admin.visits.setup', parent::$data);
    }
    /////////////////////////////////////////
    public function postAdd(MosqueVisitRequest $request)
    {
        DB::beginTransaction();
        try {
            // Some database actions
            $visit = $this->visit_repo->store($request->all());
            if($visit && $request->has('components')){
                $components = $this->visit_repo->syncComponents($visit->id,$request->components);
            }
            if($visit && $request->has('employees')){
                $employees = $this->visit_repo->syncEmployees($visit->id,$request->employees);
            }
            DB::commit();
            return $this->generalResponse('true',200, trans('title.success'), trans('messages.added'),$visit);

        } catch (\Exception $e) {
            DB::rollback();
            // Handle errors
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }
    }
    /////////////////////////////////////////
    public function getShow(Request $request, $id)
    {
        $info = $this->visit_repo->get($id);
        if (!$info)
        {
            $request->session()->flash('data', ['title' => trans('title.info'),  'code' => 300, 'message' => trans('messages.not_found')]);
            return redirect(route('admin.visits.view'));
        }

        parent::$data['info'] = $info;
        return view('admin.visits.show', parent::$data);
    }
    /////////////////////////////////////////
    public function getEdit(Request $request, $id)
    {
        $info = $this->visit_repo->get($id);
        if (!$info)
        {
            $request->session()->flash('data', ['title' => trans('title.info'),  'code' => 300, 'message' => trans('messages.not_found')]);
            return redirect(route('admin.visits.view'));
        }

        if ($info->is_submitted == 1 )
        {
            $request->session()->flash('data', ['title' => trans('title.info'),  'code' => 300, 'message' => trans('messages.approved-not-editable')]);
            return redirect(route('admin.visits.view'));
        }
        parent::$data['info'] = $info;
        //parent::$data['users'] = User::related()->where('user_type_id', 12)->get();
        parent::$data['mosque'] = $mosque = Mosque::with('employees')->find($info->mosque_id);
        parent::$data['employees'] = $mosque->employees;
        parent::$data['components'] = CleaningComponent::all();

        return view('admin.visits.edit', parent::$data);
    }
    /////////////////////////////////////////
    public function postEdit(MosqueVisitRequest $request, $id)
    {
        $info = $this->visit_repo->get($id);
        if (!$info)
        {
            $request->session()->flash('data', ['title' => trans('title.info'),  'code' => 300, 'message' => trans('messages.not_found')]);
            return redirect(route('admin.visits.view'));
        }

        DB::beginTransaction();
        try {
            // Some database actions
            $visit = $this->visit_repo->update($id , $request->all());
            if($visit && $request->has('components')){
                $components = $this->visit_repo->syncComponents($id,$request->components);
            }
            if($visit && $request->has('employees')){
                $employees = $this->visit_repo->syncEmployees($id,$request->employees);
            }
            DB::commit();
            return $this->generalResponse('true',200, trans('title.success'), trans('messages.updated'),$visit);

        } catch (\Exception $e) {
            DB::rollback();
            // Handle errors
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }
    }
    /////////////////////////////////////////////
    public function postApproval($id,Request $request)
    {
        $info = $this->visit_repo->get($id);
        if (!$info)
        {
            request()->session()->flash('data', ['title' => trans('title.info'),  'code' => 300, 'message' => trans('messages.not_found')]);
            return redirect(route('admin.visits.view'));
        }
        $approval = $this->visit_repo->approval($id,$request->all());

        if (! $approval )
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }
        //////////////////////////////////////////
        return $this->generalResponse('true',200, trans('title.success'), trans('messages.approval'),$approval);
    }
    /////////////////////////////////////////
    public function getDelete(Request $request, $id)
    {
        $info = $this->visit_repo->get($id);
        if (!$info)
        {
            return $this->generalResponse('false',300, trans('title.info'), trans('messages.not_found'),null);
        }
        $delete = $this->visit_repo->delete($id);
        if (!$delete)
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }

        return $this->generalResponse('true',200, trans('title.success'), trans('messages.deleted'),null);

    }

}
