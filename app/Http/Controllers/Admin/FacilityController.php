<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\FacilityRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;
use App\Http\Requests\FacilityRequest;

/**
 * Class FacilityController
 * @property FacilityRepositoryInterface $facility_repo
 * @package App\Http\Controllers\Admin
 */

class FacilityController extends AdminController
{
    /**
     * FacilityController constructor.
     * @param FacilityRepositoryInterface $facility_repo
     */
    public function __construct(FacilityRepositoryInterface $facility_repo)
    {
        parent::__construct();
        $this->facility_repo = $facility_repo;
        parent::$data['active_menu'] = 'properties';
    }
    /////////////////////////////////////////
    public function getIndex()
    {
        return view('admin.facilities.view', parent::$data);
    }
    /////////////////////////////////////////
    public function getList(Request $request)
    {
        $info = $this->facility_repo->allDataTable($request->all());
        $count =  $this->facility_repo->countDataTable($request->all());

        $datatable = Datatables::of($info)->setTotalRecords($count);
        $datatable->addColumn('actions', function ($row)
        {
            $data['id'] = $row->id;
            $data['btn_class'] = parent::$data['btn_class'];
            $data['btn_red'] = parent::$data['btn_red'];
            $data['btn_green'] = parent::$data['btn_green'];
            return view('admin.facilities.parts.actions', $data)->render();
        });

        $datatable->escapeColumns(['*']);
        return $datatable->make(true);
    }
    /////////////////////////////////////////
    public function getAdd()
    {
        return view('admin.facilities.add', parent::$data);
    }
    /////////////////////////////////////////
    public function postAdd(FacilityRequest $request)
    {

        $add = $this->facility_repo->store($request->all());

        if (!$add)
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }

        //////////////////////////////////////////
        return $this->generalResponse('true',200, trans('title.success'), trans('messages.added'),$add);
    }
    /////////////////////////////////////////
    public function getEdit(Request $request, $id)
    {
        $info = $this->facility_repo->get($id);
        if (!$info)
        {
            $request->session()->flash('data', ['title' => trans('title.info'),  'code' => 300, 'message' => trans('messages.not_found')]);
            return redirect(route('admin.facilities.view'));
        }

        parent::$data['info'] = $info;
        return view('admin.facilities.edit', parent::$data);
    }
    /////////////////////////////////////////
    public function postEdit(FacilityRequest $request, $id)
    {
        $info = $this->facility_repo->get($id);
        if (!$info)
        {
            $request->session()->flash('data', ['title' => trans('title.info'),  'code' => 300, 'message' => trans('messages.not_found')]);
            return redirect(route('admin.facilities.view'));
        }

        $update = $this->facility_repo->update($id , $request->all());

        if (! $update )
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }

        //////////////////////////////////////////
        return $this->generalResponse('true',200, trans('title.success'), trans('messages.updated'),$update);
    }
    /////////////////////////////////////////
    public function getDelete(Request $request, $id)
    {
        $info = $this->facility_repo->get($id);
        if (!$info)
        {
            return $this->generalResponse('false',300, trans('title.info'), trans('messages.not_found'),null);
        }
        $delete = $this->facility_repo->delete($id);
        if (!$delete)
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }

        return $this->generalResponse('true',200, trans('title.success'), trans('messages.deleted'),null);

    }
}
