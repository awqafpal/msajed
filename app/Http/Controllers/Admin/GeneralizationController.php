<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\GeneralizationRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables; 
use App\Http\Requests\GeneralizationRequest;

/** 
 * Class GeneralizationController
 * @property GeneralizationRepositoryInterface $generalization_repo
 * @package App\Http\Controllers\Admin
 */
class GeneralizationController extends AdminController
{
    /**
     * GeneralizationController constructor.
     * @param GeneralizationRepositoryInterface $generalization_repo
     */
    public function __construct(GeneralizationRepositoryInterface $generalization_repo)
    {
        parent::__construct();
        $this->generalization_repo = $generalization_repo;
        parent::$data['active_menu'] = 'generalizations';
    }
    /////////////////////////////////////////
    public function getIndex()
    {
        return view('admin.generalizations.view', parent::$data);
    }
    /////////////////////////////////////////
    public function getList(Request $request)
    {
        $info = $this->generalization_repo->allDataTable($request->all());
        $count =  $this->generalization_repo->countDataTable($request->all());

        $datatable = Datatables::of($info)->setTotalRecords($count);
        $datatable->editColumn('image', function ($row)
        {
            $data['image'] = $row->image;
            return view('admin.generalizations.parts.image', $data)->render();
        });
        $datatable->editColumn('views', function ($row)
        {
            $data['views'] = $row->views;
            return view('admin.generalizations.parts.views', $data)->render();
        });
        $datatable->editColumn('status', function ($row)
        {
            $data['status'] = $row->status;
            return view('admin.generalizations.parts.status', $data)->render();
        });
        $datatable->addColumn('actions', function ($row)
        {
            $data['id'] = $row->id;
            $data['btn_class'] = parent::$data['btn_class'];
            $data['btn_red'] = parent::$data['btn_red'];
            $data['btn_green'] = parent::$data['btn_green'];
            return view('admin.generalizations.parts.actions', $data)->render();
        });
        $datatable->escapeColumns(['*']);
        return $datatable->make(true);
    }
    /////////////////////////////////////////
    public function getAdd() 
    {
        return view('admin.generalizations.add', parent::$data);
    }
    /////////////////////////////////////////
    public function postAdd(GeneralizationRequest $request)
    {
        if ($request->hasFile('poster')) {
            $request['image'] = $this->uploadImage($request->poster, 'generalizations');
        }
        $request['slug'] = str_replace(' ', '-', $request->title);
        $add = $this->generalization_repo->store($request->all());
        if (!$add)
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }
        //////////////////////////////////////////
        return $this->generalResponse('true',200, trans('title.success'), trans('messages.added'),$add);
    }
    /////////////////////////////////////////
    public function getEdit(Request $request, $id)
    {
        $info = $this->generalization_repo->get($id);
        if (!$info)
        {
            $request->session()->flash('data', ['title' => trans('title.info'),  'code' => 300, 'message' => trans('messages.not_found')]);
            return redirect(route('admin.generalizations.view'));
        }

        parent::$data['info'] = $info;
        return view('admin.generalizations.edit', parent::$data);
    }
    /////////////////////////////////////////
    public function postEdit(GeneralizationRequest $request, $id)
    {
        $info = $this->generalization_repo->get($id);
        if (!$info)
        {
            $request->session()->flash('data', ['title' => trans('title.info'),  'code' => 300, 'message' => trans('messages.not_found')]);
            return redirect(route('admin.generalizations.view'));
        }
        if ($request->hasFile('poster')) {
            $request['image'] = $this->uploadImage($request->poster, 'generalizations');
        }
        $request['slug'] = str_replace(' ', '-', $request->title);
        $update = $this->generalization_repo->update($id , $request->all());

        if (! $update )
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }
        //////////////////////////////////////////
        return $this->generalResponse('true',200, trans('title.success'), trans('messages.updated'),$update);
    }
    /////////////////////////////////////////
    public function getDelete(Request $request, $id)
    {
        $info = $this->generalization_repo->get($id);
        if (!$info)
        {
            return $this->generalResponse('false',300, trans('title.info'), trans('messages.not_found'),null);
        }
        $delete = $this->generalization_repo->delete($id);
        if (!$delete)
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }

        return $this->generalResponse('true',200, trans('title.success'), trans('messages.deleted'),null);

    }
}
