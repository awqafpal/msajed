<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\AlternativeEnergyRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables; 
use App\Http\Requests\AlternativeEnergyRequest;

/** 
 * Class AlternativeEnergyController
 * @property AlternativeEnergyRepositoryInterface $energy_repo
 * @package App\Http\Controllers\Admin
 */
class AlternativeEnergyController extends AdminController
{
    /**
     * AlternativeEnergyController constructor.
     * @param AlternativeEnergyRepositoryInterface $energy_repo
     */
    public function __construct(AlternativeEnergyRepositoryInterface $energy_repo)
    {
        parent::__construct();
        $this->energy_repo = $energy_repo;
        parent::$data['active_menu'] = 'alternative-energies';
    }
    /////////////////////////////////////////
    public function getIndex()
    {
        return view('admin.alternative-energies.view', parent::$data);
    }
    /////////////////////////////////////////
    public function getList(Request $request)
    {
        $info = $this->energy_repo->allDataTable($request->all());
        $count =  $this->energy_repo->countDataTable($request->all());

        $datatable = Datatables::of($info)->setTotalRecords($count);
        $datatable->addColumn('actions', function ($row)
        {
            $data['id'] = $row->id;
            $data['btn_class'] = parent::$data['btn_class'];
            $data['btn_red'] = parent::$data['btn_red'];
            $data['btn_green'] = parent::$data['btn_green'];
            return view('admin.alternative-energies.parts.actions', $data)->render();
        });
        $datatable->escapeColumns(['*']);
        return $datatable->make(true);
    }
    /////////////////////////////////////////
    public function getAdd()
    {
        return view('admin.alternative-energies.add', parent::$data);
    }
    /////////////////////////////////////////
    public function postAdd(AlternativeEnergyRequest $request)
    {
        $add = $this->energy_repo->store($request->all());
        if (!$add)
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }
        //////////////////////////////////////////
        return $this->generalResponse('true',200, trans('title.success'), trans('messages.added'),$add);
    }
    /////////////////////////////////////////
    public function getEdit(Request $request, $id)
    {
        $info = $this->energy_repo->get($id);
        if (!$info)
        {
            $request->session()->flash('data', ['title' => trans('title.info'),  'code' => 300, 'message' => trans('messages.not_found')]);
            return redirect(route('admin.alternativeEnergies.view'));
        }

        parent::$data['info'] = $info;
        return view('admin.alternative-energies.edit', parent::$data);
    }
    /////////////////////////////////////////
    public function postEdit(AlternativeEnergyRequest $request, $id)
    {
        $info = $this->energy_repo->get($id);
        if (!$info)
        {
            $request->session()->flash('data', ['title' => trans('title.info'),  'code' => 300, 'message' => trans('messages.not_found')]);
            return redirect(route('admin.alternativeEnergies.view'));
        }
        $update = $this->energy_repo->update($id , $request->all());

        if (! $update )
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }
        //////////////////////////////////////////
        return $this->generalResponse('true',200, trans('title.success'), trans('messages.updated'),$update);
    }
    /////////////////////////////////////////
    public function getDelete(Request $request, $id)
    {
        $info = $this->energy_repo->get($id);
        if (!$info)
        {
            return $this->generalResponse('false',300, trans('title.info'), trans('messages.not_found'),null);
        }
        $delete = $this->energy_repo->delete($id);
        if (!$delete)
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }

        return $this->generalResponse('true',200, trans('title.success'), trans('messages.deleted'),null);

    }
}
