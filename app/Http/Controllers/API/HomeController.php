<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    
    public function AuthRouteAPI(Request $request){
        return $request->user();
    }
    
}
