<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Foundation\Http\FormRequest;
 
class EmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('id');
        return [
            'avatar'=>'nullable|image',
            'name' => 'required|string|max:100',
            'identity_number' => 'required|digits:9|unique:employees,identity_number,' . $id . ',id,deleted_at,NULL',
            'phone_number'=>'nullable|numeric',
            'job_no'=>'nullable|numeric',
            'job_type' => 'required|exists:constants,id',
            'job_id'=>'required|exists:jobs,id',
            'hiring_type' => 'required|exists:constants,id',
            'hiring_date'=>'nullable|date',
            'qualification'=>'nullable|string|max:100',
            'region_id' => 'nullable|exists:regions,id',
            'mosque_id' => 'nullable|exists:mosques,id',
            'address'=>'nullable|string'
        ];
    }

    public function attributes()
    {
        return [
            'avatar'=>'الصورة الشخصية',
            'name' => 'اسم الموظف',
            'identity_number' => 'رقم هوية الموظف',
            'phone_number'=>'رقم الموبايل',
            'job_no'=>'الرقم الوظيفي',
            'job_type' => 'التصنيف الوظيفي',
            'job_id'=>'الوظيفة',
            'hiring_type' => 'بند التعيين',
            'hiring_date'=>'تاريخ التعيين',
            'qualification'=>'المؤهل العلمي',
            'region_id' => 'المنطقة',
            'mosque_id' => 'المسجد',
            'address'=>'عنوان الموظف'
        ];
    }

    public function messages()
    {
        return [
            //
        ];
    }

    public function failedValidation(Validator $validator) {
        //write your bussiness logic here otherwise it will give same old JSON response
       throw new HttpResponseException(
            response()->json([
               'status' => 'false',
               'code' => 422,
               'title' => trans('title.warning'),
               'message' => $validator->messages()->first(),
               'data' => null
            ])
        );
    }
}
