<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Foundation\Http\FormRequest;

class MosqueVisitRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('id');
        return [
            'date' => 'required|date',
            'mosque_id' => 'required|exists:mosques,id',
            'visited_by'=>'required|exists:users,id',
            'visit_time' => 'required',
            'time_to_leave'=>'required',
            'inspector_recommendations'=>'nullable',
        ];
    }

    public function attributes()
    {
        return [
            'date'=>'التاريخ',
            'mosque_id' => 'المسجد',
            'visited_by' => 'المفتش',
            'visit_time'=>'وقت الزيارة',
            'time_to_leave'=>'وقت المغادرة',
            'inspector_recommendations' =>'توصيات المفتش',
        ];
    }

    public function messages()
    {
        return [
            //
        ];
    }

    public function failedValidation(Validator $validator) {
        //write your bussiness logic here otherwise it will give same old JSON response
       throw new HttpResponseException(
            response()->json([
               'status' => 'false',
               'code' => 422,
               'title' => trans('title.warning'),
               'message' => $validator->messages()->first(),
               'data' => null
            ])
        );
    }
}
