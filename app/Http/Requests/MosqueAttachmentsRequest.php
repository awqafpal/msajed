<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Foundation\Http\FormRequest;

class MosqueAttachmentsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */ 
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('id');
        return [
            'mosque_id' => 'required|exists:mosques,id',
        ];
    }

    public function attributes()
    {
        return [
            'mosque_id' => 'المسجد',
        ];
    }

    public function messages()
    {
        return [
            'mosque_id.required' => 'لا يمكن رفع مرفقات على مسجد غير مدخل',
        ];
    }

    public function failedValidation(Validator $validator) {
        //write your bussiness logic here otherwise it will give same old JSON response
       throw new HttpResponseException(
            response()->json([
               'status' => 'false',
               'code' => 422,
               'title' => trans('title.warning'),
               'message' => $validator->messages()->first(),
               'data' => null
            ])
        );
    }
}
