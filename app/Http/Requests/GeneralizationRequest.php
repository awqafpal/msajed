<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Foundation\Http\FormRequest;

class GeneralizationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('id');
        return [
            'title' => 'required|max:60|unique:generalizations,title,' . $id,
            //'slug' => 'required|max:60|unique:generalizations,slug,' . $id,
            'poster'=> $this->id == null ? 'required|mimes:jpeg,jpg,png,gif,svg|max:8000' : 'nullable|mimes:jpeg,jpg,png,gif,svg|max:8000',
            'description'=> 'required',
            'status'=> 'required',
            //'views'=> 'required',
        ];
    }

    public function attributes()
    {
        return [
            'title' => 'عنوان التعميم',
            'image' => 'صورة التعميم',
            'description' => 'وصف التعميم',
            'status' => 'حالة التعميم',
            'views' => 'مشاهدات التعميم',
        ];
    }

    public function messages()
    {
        return [
            //
        ];
    }

    public function failedValidation(Validator $validator) {
        //write your bussiness logic here otherwise it will give same old JSON response
       throw new HttpResponseException(
            response()->json([
               'status' => 'false',
               'code' => 422,
               'title' => trans('title.warning'),
               'message' => $validator->messages()->first(),
               'data' => null
            ])
        );
    }
}
