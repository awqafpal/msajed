<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Foundation\Http\FormRequest;

class MosqueNeedRequest extends FormRequest
{
     /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    { 
        $id = $this->route('id');
        return [
            'mosque_id' => 'required|exists:mosques,id',
            'request_type'=> 'required',
            'request_submit_side'=> 'required',
            'phone_number'=> 'required',
            'address'=> 'required',
            'request_content'=> 'required',
            'request_quantity'=> 'required',
            'estimated_value'=> 'required',
            'status'=> 'nullable',
        ];
    }

    public function attributes()
    {
        return [
            'mosque_id' => 'المسجد',
            'request_type'=> 'نوع الطلب',
            'request_submit_side'=> 'جهة تقديم الطلب',
            'phone_number'=> 'رقم الجوال',
            'address'=> 'العنوان',
            'request_content'=> 'وصف طلب الاحتياج',
            'request_quantity'=> 'الكمية المطلوبة',
            'estimated_value'=> 'القيمة التقديرية',
            'status'=> 'اعتماد جهة الاختصاص',
        ];
    }

    public function messages()
    {
        return [
            //
        ];
    }

    public function failedValidation(Validator $validator) {
        //write your bussiness logic here otherwise it will give same old JSON response
       throw new HttpResponseException(
            response()->json([
               'status' => 'false',
               'code' => 422,
               'title' => trans('title.warning'),
               'message' => $validator->messages()->first(),
               'data' => null
            ])
        );
    }
}
