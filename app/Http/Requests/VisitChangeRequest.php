<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Foundation\Http\FormRequest;

class VisitChangeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('id');
        return [
            'date'=>'required|date',
            'prayer_name' => 'required|string',
            'user_id' => 'required|unique:users,id,' . $id . ',id,deleted_at,NULL',
            'type_id'=>'nullable|numeric',
            'reason'=>'nullable|string',
            'has_action' => 'nullable',
            'status_id'=>'required|exists:constants,id',
            'reject_reason' => 'nullable|string',
        ];
    }

    public function attributes()
    {
        return [
            'date'=>'تاريخ الزيارة',
            'prayer_name' => 'الصلاة',
            'user_id' => 'المفتش',
            'type_id'=>'نوع الطلب',
            'reason'=>'السبب',
            'has_action' => 'الحدث',
            'status_id'=>'الحالة',
            'reject_reason' => 'سبب الرفض',
        ];
    }

    public function messages()
    {
        return [
            //
        ];
    }

    public function failedValidation(Validator $validator) {
        //write your bussiness logic here otherwise it will give same old JSON response
       throw new HttpResponseException(
            response()->json([
               'status' => 'false',
               'code' => 422,
               'title' => trans('title.warning'),
               'message' => $validator->messages()->first(),
               'data' => null
            ])
        );
    }
}
