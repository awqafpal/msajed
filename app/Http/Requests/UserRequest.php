<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Foundation\Http\FormRequest;


class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('id');
        /** Email is required */
        return [
            'avatar'=>'nullable|image',
            'name' => 'required|min:3,max:100',
            'username' => 'required|digits:9|unique:users,username,' . $id . ',id,deleted_at,NULL',
            'phone_number'=>'required|numeric',
            'email' => 'nullable|unique:users,email,' . $id . ',id,deleted_at,NULL',
            'password' => $this->id == null ? 'required|between:6,16|confirmed' : 'nullable|between:6,16|confirmed',
            'password_confirmation' => $this->id == null ? 'required|between:6,16' : 'nullable|between:6,16',
            'region_id' => 'required|exists:regions,id',
            'status' => 'required|numeric|in:0,1',
            'user_type_id' => 'required|exists:user_types,id',
            'role' => 'required', 
        ];
    }


    public function attributes()
    {
        return [
            'username' => 'رقم الهوية',
            'name' => 'الاسم كامل',
            'email' => 'البريد الالكتروني',
            'role' => 'الصلاحية',
            'password' => 'كلمة المرور',
            'status' => 'حالة',
            'user_type_id' => 'المستوى الاداري',
            'region_id' => 'المنطقة'
        ];
    }

    public function messages()
    {
        return [
            //
        ];
    }

    public function failedValidation(Validator $validator) {
        //write your bussiness logic here otherwise it will give same old JSON response
       throw new HttpResponseException(
            response()->json([
               'status' => 'false',
               'code' => 422,
               'title' => trans('title.warning'),
               'message' => $validator->messages()->first(),
               'data' => null
            ])
        );
    }
}
