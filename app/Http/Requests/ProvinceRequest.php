<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Foundation\Http\FormRequest;

class ProvinceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('id');
        return [
            'name' => 'required|max:60|unique:provinces,name,' . $id,
            'modeer' => 'nullable',
            'irshad' => 'nullable',
            'jawwal' => 'nullable|starts_with:59,56|digits:9',
            'telephone' => 'nullable|digits:7',
            'fax' => 'nullable',
            'address' => 'nullable|max:255',
        ];
    }


    public function attributes()
    {
        return [
            'name' => 'اسم المحافظة',
            'modeer' => 'المدير',
            'irshad' => 'الارشاد',
            'jawwal' => 'رقم الجوال',
            'telephone' => 'رقم الهاتف',
            'fax' => 'الفاكس',
            'address' => 'العنوان',
        ];
    }

    public function messages()
    {
        return [
            //
        ];
    }

    public function failedValidation(Validator $validator) {
        //write your bussiness logic here otherwise it will give same old JSON response
       throw new HttpResponseException(
            response()->json([
               'status' => 'false',
               'code' => 422,
               'title' => trans('title.warning'),
               'message' => $validator->messages()->first(),
               'data' => null
            ])
        );
   }
}
