<?php

namespace App\Exports;

use App\Models\Mosque;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class MosquesExport implements FromView
{
    public function view(): View
    {
        return view('admin.exports.mosques', [
            'mosques' => Mosque::all()
        ]);
    }
}
