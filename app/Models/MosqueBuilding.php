<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MosqueBuilding extends Model
{
    // تفاصيل بناء المسجد والاعمار
    use SoftDeletes;
    protected $fillable = [
        'mosque_id',
        'date_bulid_ad', // تاريخ التشكيل ميلادي
        'date_bulid_hijri', //  تاريخ التشكيل هجري
        'building_cost', //تكلفة الإنشاء
        'creation_reason', // مبررات الانشاء
        'mosque_building_notes', // jتفاصيل اخرى
        // الجهة المتبرعة
        'location', // موقع الجهة المتبرعة == خارجي, محلي
        'country_name', // اسم الدولة
        'user_name', // اسم الشخص
        'date_of_formation', // تاريخ التشكيل
        // المقاول/ المهندس المشرف
        'engineer_full_name', // الاسم رباعي
        'engineer_identity_number', // رقم الهوية
        'engineer_phone_number', // رقم الجوال
        'engineer_address', // عنوان السكن
        'engineer_license_number' // رقم الترخيص
    ];

    public function mosque()
    {
        return $this->belongsTo(Mosque::class);
    }

}
 