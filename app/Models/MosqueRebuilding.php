<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MosqueRebuilding extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'mosque_id',
        'date_rebuilding_ad', // تاريخ التشكيل ميلادي
        'date_rebuilding_hijri', //  تاريخ التشكيل هجري
        'rebuilding_cost', //تكلفة الإنشاء
        'rebuilding_reason', // مبررات الانشاء
        'mosque_rebuilding_notes', // jتفاصيل اخرى
        // الجهة المتبرعة
        'location', // موقع الجهة المتبرعة == خارجي, محلي
        'country_name', // اسم الدولة
        'user_name', // اسم الشخص
        'date_of_formation', // تاريخ التشكيل
        // المقاول/ المهندس المشرف
        'engineer_full_name', // الاسم رباعي
        'engineer_identity_number', // رقم الهوية
        'engineer_phone_number', // رقم الجوال
        'engineer_address', // عنوان السكن
        'engineer_license_number', // رقم الترخيص

        'due_date', // تاريخ الاستحقاق أو تاريخ التوسعة
        'grant', // منحة التوسعة أو الضرر
        'space', // مساحة التوسعة
        'year_of_damage', // سنة الضرر

    ];

    public function mosque()
    {
        return $this->belongsTo(Mosque::class);
    }
}
