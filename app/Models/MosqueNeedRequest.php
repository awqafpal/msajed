<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MosqueNeedRequest extends Model
{
    // 'طلبات احتياج' المساجد
    use SoftDeletes;
    protected $fillable = [
        'request_type', // نوع الطلب
        'request_submit_side', //جهة تقديم الطلب
        'phone_number', //رقم الجوال
        'mosque_id', //إسم المسجد
        'address', //عنوان المسجد
        'request_content', //وصف طلب الاحتياج
        'request_quantity', //الكمية المطلوبة
        'estimated_value', //القيمة التقديرية
        'status', // اعتماد جهة الاختصاص
    ];
    
    public function mosque()
    {
        return $this->belongsTo(Mosque::class);
    }

    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s',
    ];
}
