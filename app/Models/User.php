<?php

namespace App\Models;

use App\Traits\EncryptionTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

/**
 * Class User
 * @package App\Models
 *
 * @property $id
 * @property $username
 * @property $name
 * @property $email
 * @property $online_status
 * @property $email_verified_at
 * @property $password
 * @property $remember_token
 * @property $status
 * @property $created_at
 * @property $updated_at
 * @property $deleted_at
 */
class User extends Authenticatable
{
    use Notifiable, SoftDeletes, HasRoles, EncryptionTrait;

    /**
     * @var string
     */
    protected $guard_name = 'admin';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_type_id',
        'region_id',
        'name',
        'username',
        'phone_number',
        'image',
        'email',
        'password',
        'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * @var array
     */
    protected $appends = [
        'id_hash'
    ];

    /**
     * @param $value
     */
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }

    /**
     * @return string
     */
    function getIdHashAttribute()
    {
        return $this->encrypt($this->id);
    }

    public function region(){
        return $this->belongsTo(Region::class,'region_id','id');
    }

    public function type(){
        return $this->belongsTo(UserType::class,'user_type_id','id');
    }

    public function mosques(){
        return $this->hasMany(Mosque::class,'user_id','id');
    }

    // To make new Functions
    public function scopeWhereInMyUsers($query)
    {
        $auth = auth()->user();
        if ( in_array($auth->user_type_id,[1,2,3,5,6,7]) ) {
            //1:  مدير عام المديريات
            //2: مدير وحدة التخطيط وتطوير الأداء المؤسسي
            //3: مدير المساجد
            //5: رئيس قسم التنسيق والمتابعة بالادارة
            //6: رئيس قسم التفتيش بدائرة متابعة المساجد
            //7: رئيس قسم لجان المساجد بدائرة متابعة المساجد
            $query = $query;
        }
        elseif (in_array($auth->user_type_id,[4,8,9,10,11])) {
            //4: مدير المديرية
            //8: رئيس القسم
            //9: إداري قسم المساجد بالمديرية
            //10: رئيس شعبة التفتيش بقسم المساجد بالمديرية
            //11: رئيس شعبة لجان المساجد بقسم المساجد بالمديرية
            $query = $query->whereHas('region', function ($query) use ($auth) {
                $query->where('province_id', $auth->region->province_id);
            });
        }
        else {
            // 12 : مفتش
            $query = $query->where('id', $auth->id);
        }
        return $query;
    }

    /**
     * Scope a query to only include related users.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeRelated($query)
    {
        $auth = auth()->user();
        switch($auth->user_type_id){
            case '13':
                // مدير النظام
                $query = $query;
                break;
            case '1':
                // مدير عام المديريات
                $query = $query;
                break;
            case '2':
                // مدير وحدة التخطيط وتطوير الأداء المؤسسي
                $query = $query;
                break;
            case '3':
                // مدير المساجد
                $query = $query;
                break;
            case '4':
                // مدير المديرية
                $query = $query->whereHas('region', function ($query) use ($auth) {
                    $query->where('province_id', $auth->region->province_id);
                });
                break;
            case '5':
                // رئيس قسم التنسيق والمتابعة بالإدارة
                $query = $query->whereHas('region', function ($query) use ($auth) {
                    $query->where('province_id', $auth->region->province_id);
                });
                break;
            case '6':
                // رئيس قسم التفتيش بدائرة متابعة المساجد
                $query = $query->whereHas('region', function ($query) use ($auth) {
                    $query->where('province_id', $auth->region->province_id);
                });
                break;
            case '7':
                // رئيس قسم لجان المساجد بدائرة متابعة المساجد
                $query = $query->whereHas('region', function ($query) use ($auth) {
                    $query->where('province_id', $auth->region->province_id);
                });
                break;
            case '8':
                // رئيس قسم
                $query = $query->whereHas('region', function ($query) use ($auth) {
                    $query->where('province_id', $auth->region->province_id);
                });
                break;
            case '9':
                // إداري قسم المساجد بالمديرية
                $query = $query->whereHas('region', function ($query) use ($auth) {
                    $query->where('province_id', $auth->region->province_id);
                });
                break;
            case '10':
                // رئيس شعبة التفتيش بقسم المساجد بالمديرية
                $query = $query->whereHas('region', function ($query) use ($auth) {
                    $query->where('province_id', $auth->region->province_id);
                });
                break;
            case '11':
                // رئيس شعبة لجان المساجد بقسم المساجد بالمديرية
                $query = $query->whereHas('region', function ($query) use ($auth) {
                    $query->where('province_id', $auth->region->province_id);
                });
                break;
            default:
                // مفتش أو غير ذلك
                $query = $query->where('id', $auth->id);
        }
        return $query;
    }

}
