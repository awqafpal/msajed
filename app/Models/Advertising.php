<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Advertising extends Model
{
    use SoftDeletes;

    protected $table = 'advertisings';
    protected $fillable = [
        'title',
        'slug',
        'image',
        'description',
        'status',
        'views'
    ];

    protected $casts = [
        'status' => 'boolean',
    ];
}
