<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
class CleaningComponent extends Model
{

    protected $table = 'cleaning_components';
    protected $fillable = [
        'name', 
        'is_used'
    ];

    public function toggleStatus()
    {
        $this->update([
            'is_used' => DB::raw('NOT is_used')
        ]);
    }
}
