<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MosqueVisit extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'date','mosque_id','visited_by','visit_time','time_to_leave',
        'inspector_recommendations','department_head_recommendations',
        'province_manager_recommendations','is_submitted'
    ];

    protected $dates = ['date','visit_time','time_to_leave'];
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s',
    ];

    public function mosque(){
        return $this->belongsTo(Mosque::class,'mosque_id','id');
    }

    public function visitor(){
        return $this->belongsTo(User::class ,'visited_by');
    }

    public function components(){
        return $this->hasMany(CleaningComponentVisit::class , 'visit_id' ,'id') ;
    }
    public function conditions(){
        return $this->hasMany(EmployeeCondition::class , 'visit_id' ,'id') ;
    }

    public function getApprovedAttribute()
    {
        $auth = auth()->user()->user_type_id ;
        switch ($auth){
            case '13':
                $approving = ($this->province_manager_recommendations && $this->department_head_recommendations && $this->is_submitted) ? true : false ;
                break ;
            case '1':
                $approving = ($this->province_manager_recommendations && $this->department_head_recommendations && $this->is_submitted) ? true : false ;
                break ;
            case '3':
                $approving = ($this->province_manager_recommendations && $this->department_head_recommendations && $this->is_submitted) ? true : false ;
                break ;
            case '4':
                $approving = ($this->province_manager_recommendations && $this->is_submitted) ? true : false ;
                break ;
            case '8':
                $approving = ($this->department_head_recommendations && $this->is_submitted) ? true : false ;
                break ;
            case '12':
                $approving = ($this->is_submitted) ? true : false ;
                break ;
            default:
                $approving = false ;
                //
        }
        return $approving ;
    }

    public function getStatusAttribute()
    {
        if ($this->province_manager_recommendations && $this->is_submitted){
            $status = 'province_approved';
        }else if($this->department_head_recommendations && $this->is_submitted){
            $status = 'head_approved';
        }else if ($this->is_submitted == 1){
            $status = 'inspector_approved';
        }else{
            $status = 'not_approved';
        }
        return $status ;

    }

    /**
     * Scope a query to only include related users.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeRelated($query)
    {
        $auth = auth()->user();
        $province = $auth->region->province_id;
        switch($auth->user_type_id){
            case '13':
                // مدير النظام
                $query = $query;
                break;
            case '1':
                // مدير عام المديريات
                $query = $query;
                break;
            case '2':
                // مدير وحدة التخطيط وتطوير الأداء المؤسسي
                $query = $query;
                break;
            case '3':
                // مدير المساجد
                $query = $query;
                break;
            case '4':
                // مدير المديرية
                $query = $query->whereNotNull('department_head_id')
                            ->whereHas('mosque', function ($query) use ($province) {
                                $query->whereHas('region', function($qry) use($province){
                                    $qry->where('province_id', $province);
                                });
                            });
                break;
            case '5':
                // رئيس قسم التنسيق والمتابعة بالإدارة
                $query = $query->whereNotNull('department_head_id')
                            ->whereHas('mosque', function ($query) use ($province) {
                                $query->whereHas('region', function($qry) use($province){
                                    $qry->where('province_id', $province);
                                });
                            });
                break;
            case '6':
                // رئيس قسم التفتيش بدائرة متابعة المساجد
                $query = $query->whereNotNull('department_head_id')
                            ->whereHas('mosque', function ($query) use ($province) {
                                $query->whereHas('region', function($qry) use($province){
                                    $qry->where('province_id', $province);
                                });
                            });
                break;
            case '7':
                // رئيس قسم لجان المساجد بدائرة متابعة المساجد
                $query = $query->whereNotNull('department_head_id')
                            ->whereHas('mosque', function ($query) use ($province) {
                                $query->whereHas('region', function($qry) use($province){
                                    $qry->where('province_id', $province);
                                });
                            });
                break;
            case '8':
                // رئيس قسم
                $query = $query->where('is_submitted', 1)
                               ->whereHas('visitor', function ($query) use ($province) {
                                    $query->where('user_type_id', 12)
                                        ->whereHas('region', function($qry) use($province){
                                            $qry->where('province_id', $province);
                                        });
                               });
            case '9':
                // إداري قسم المساجد بالمديرية
                $query = $query->whereHas('mosque', function ($query) use ($province) {
                    $query->whereHas('region', function($qry) use($province){
                        $qry->where('province_id', $province);
                    });
                });
                break;
            case '10':
                // رئيس شعبة التفتيش بقسم المساجد بالمديرية
                $query = $query->whereHas('region', function ($query) use ($auth) {
                    $query->where('province_id', $auth->region->province_id);
                });
                break;
            case '11':
                // رئيس شعبة لجان المساجد بقسم المساجد بالمديرية
                $query = $query->whereHas('region', function ($query) use ($auth) {
                    $query->where('province_id', $auth->region->province_id);
                });
                break;
            default:
                // مفتش أو غير ذلك
                $query = $query->where('visited_by', $auth->id);
        }
        return $query;
    }

}
