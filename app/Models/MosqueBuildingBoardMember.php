<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MosqueBuildingBoardMember extends Model
{
    // رئيس وأعضاء مجلس البناء
    protected $fillable = [
        'mosque_id',
        'full_name', // الاسم رباعي
        'identity_number',
        'job',
        'phone_number',
        'address'
    ];

    public function mosque()
    {
        return $this->belongsTo(Mosque::class);
    }


    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s',
    ];
}
 