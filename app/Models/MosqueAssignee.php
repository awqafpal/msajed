<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MosqueAssignee extends Model
{
    use SoftDeletes; 
    protected $fillable = [
        'mosque_id',
        'assignee_user_name', // اسم المتنازل
        'assignee_mobile', //  رقم جوال المتنازل
        'assignee_identity', //رقم هوية المتنازل
        'assignee_date', //  تاريخ الوقف
        'assignee_land_space', // مساحة الأرض الموقوفة
        'assignee_notes', // تفاصيل أخرى
    ];

    public function mosque()
    {
        return $this->belongsTo(Mosque::class);
    }

    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s',
    ];

}
