<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InspectionScheduleTime extends Model
{
    protected $table = 'inspection_schedule_times';
    protected $fillable = [
        'date', 'inspection_schedule_id',
        'fajr', 'dhuhr', 'asr', 'maghrib', 'isha',
    ];

   // protected $dates = ['date'];
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s',
    ];

    public function schedule()
    {
        return $this->belongsTo(InspectionSchedule::class);
    }

}
