<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PermissionGroup extends Model
{
    protected $table = 'permission_group';
    protected $fillable = [
        'name'
    ];

    public function children()
    {
        return $this->hasMany('App\Models\Permission', 'group_id', 'id')->where('show_in_menu',1)->orderBy('sort');
    }
}
