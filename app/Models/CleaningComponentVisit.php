<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CleaningComponentVisit extends Model
{
    protected $fillable = [
        'visit_id','cleaning_component_id' ,'status_id'
    ];


    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s',
    ];

    public function visit(){
        return $this->belongsTo(MosqueVisit::class ,'visit_id');
    }

    public function cleaningComponent(){
        return $this->belongsTo(CleaningComponent::class ,'cleaning_component_id');
    }

    public function status(){
        return $this->belongsTo(Constant::class ,'status_id');
    }
}
