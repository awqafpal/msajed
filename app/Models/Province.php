<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    use SoftDeletes;
    protected $table = 'provinces';
    protected $fillable = [
        'name',
        'modeer',
        'irshad',
        'jawwal',
        'telephone',
        'fax',
        'address',
    ];
    public function regions()
    {
        return $this->hasMany(Region::class);
    }

    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s',
		 ];

}

