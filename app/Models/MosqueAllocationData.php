<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MosqueAllocationData extends Model
{
    //بيانات التخصيص
    use SoftDeletes;
    protected $fillable = [
        'mosque_id',
        'allocation_date',
        'allocation_land_space',
        'allocation_notes',
    ];

    public function mosque()
    {
        return $this->belongsTo(Mosque::class);
    }

    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s',
    ];
} 
