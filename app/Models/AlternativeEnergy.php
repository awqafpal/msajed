<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AlternativeEnergy extends Model
{
    use SoftDeletes;

    protected $table = 'alternative_energies';
    protected $fillable = [
        'name',
    ];
}
