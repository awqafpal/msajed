<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmployeeCondition extends Model
{
    protected $fillable = [
        'visit_id', 'employee_id', 'attendance', 'commitment', 'notes'
    ];

    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s',
    ];

    public function visit()
    {
        return $this->belongsTo(MosqueVisit::class, 'visit_id');
    }
    public function employee()
    {
        return $this->belongsTo(Employee::class, 'employee_id');
    }
}
