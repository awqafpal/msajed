<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Region extends Model
{
  use SoftDeletes;

  protected $fillable = [
      'province_id',
      'name',
  ];

  public $with = ['province'];

  public function province(){
    return $this->belongsTo(Province::class ,'province_id')->withDefault([
        'name' => 'غير مدخل'
    ]);
  }

  /**
  * Get all of the mosques for the Region
  *
  * @return \Illuminate\Database\Eloquent\Relations\HasMany
  */
  public function mosques()
  {
      return $this->hasMany(Mosque::class, 'region_id', 'id');
  }

  /**
  * Get all of the employees for the Region
  *
  * @return \Illuminate\Database\Eloquent\Relations\HasMany
  */
  public function employees()
  {
      return $this->hasMany(Employee::class, 'region_id', 'id');
  }

  protected $casts = [
    'created_at' => 'datetime:Y-m-d H:i:s',
    'updated_at' => 'datetime:Y-m-d H:i:s',
  ];

}
