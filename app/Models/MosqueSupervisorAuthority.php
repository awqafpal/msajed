<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MosqueSupervisorAuthority extends Model
{
    //جهة الإشراف على المسجد
    use SoftDeletes;
    protected $fillable = [
        'mosque_id',
        'type',
        'supervisor_authority_name',
    ];


    public function mosque()
    {
        return $this->belongsTo(Mosque::class);
    }

    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s',
    ];
}
 