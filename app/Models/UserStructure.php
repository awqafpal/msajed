<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserStructure extends Model
{
    use SoftDeletes;

    protected $table = 'user_structures';
    protected $fillable = [
        'name',
    ];

}
