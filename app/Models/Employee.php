<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{

    protected $table = 'employees';
    protected $fillable = [
        'job_type',
        'job_id',
        'mosque_id',
        'image',
        'name',
        'phone_number',
        'identity_number',
        'job_no',
        'hiring_type',
        'hiring_date',
        'qualification',
        'region_id',
        'address',
    ];

    // المنطقة
    public function region()
    {
        return $this->belongsTo(Region::class,'region_id','id')->withDefault([
            'name' => 'غير مدخل'
        ]);
    }

    public function job()
    {
        return $this->belongsTo(Job::class,'job_id','id')->withDefault([
            'name' => 'غير مدخل'
        ]);
    }

    public function mosque()
    {
        return $this->belongsTo(Mosque::class,'mosque_id','id')->withDefault([
            'name' => 'غير مدخل'
        ]);
    }
    public function jobType()
    {
        return $this->belongsTo(Constant::class,'job_type','id')->withDefault([
            'name' => 'غير مدخل'
        ]);
    }
    public function hiringType()
    {
        return $this->belongsTo(Constant::class,'hiring_type','id')->withDefault([
            'name' =>'غير مدخل'
        ]);
    }
}
