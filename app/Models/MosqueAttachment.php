<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MosqueAttachment extends Model
{
    use SoftDeletes;

    protected $table = 'mosque_attachments';
    protected $fillable = [
        'mosque_id',
        'attachment_type_id',
        'file',
    ];
    // نوع المرفق
    public function type()
    {
        return $this->belongsTo(AttachmentType::class);
    }

    // المسجد
    public function mosque()
    {
        return $this->belongsTo(Mosque::class);
    }

}
 