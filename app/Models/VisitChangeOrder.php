<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VisitChangeOrder extends Model
{
    protected $fillable = [
        'schedule_time_id','user_id','type','reason','has_action',
        'status_id','reject_reason','alternative_mosque_id'
    ];

    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s',
    ];


    public function time(){
        return $this->belongsTo(InspectionScheduleTime::class,'schedule_time_id' ,'id');
    }

    public function user(){
        return $this->belongsTo(User::class,'user_id' ,'id');
    }

    public function status(){
        return $this->belongsTo(Constant::class ,'status_id');
    }

    public function getStateAttribute()
    {
        switch($this->status){
            case 1 :
                $status ='accepted';
                break;
            case 0:
                $status ='rejected';
                break;
            default:
                $status ='new';
        }
        return $status ;
    }


}
