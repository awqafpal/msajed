<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WaterSource extends Model
{
    use SoftDeletes;

    protected $table = 'water_sources';
    protected $fillable = [
        'name',
    ];
}
