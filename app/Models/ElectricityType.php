<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ElectricityType extends Model
{
    use SoftDeletes;

    protected $table = 'electricity_types';
    protected $fillable = [
        'name',
    ];
}
