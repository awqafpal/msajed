<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserType extends Model
{
    use SoftDeletes;

    protected $table = 'user_types';
    protected $fillable = [
        'name','user_structure_id'
    ];

    public function structure(){
        return $this->belongsTo(UserStructure::class,'user_structure_id','id');
    }

    public $with=['structure'];

}
