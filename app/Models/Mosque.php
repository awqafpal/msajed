<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Mosque extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'name',
        'slug',
        'description',
        'image',
        // - عنوان المسجد
        'region_id', // المحافظة والمنطقه
        'property_id',
        'user_id', // مفتش المسجد
        'street',  // الشارع
        'piece', // القطعة
        'qasema', // قسيمة
        'mosque_no', // رقم المسجد
        // تاريخ إنشاء المسجد
        'date_bulid_ad', // ميلادي
        'date_bulid_hijri', // هجري
        // تصنيف المسجد:
        'archaeological_status', // أثري، غير أثري
        'central_status', // مركزي، غير مركزي
        'type_status', // كبير، محلي، مصلى
        // وصف المسجد
        'prayer_no', // عدد المصلين
        'mosque_space', // مساحة المسجد
        'floor_no', // عدد الطوابق
        'building_type', // طبيعة البناء
        'bank_account_number', // رقم الحساب البنكي
        'facilities_number', // عدد وقفيات المسجد
        'mosque_date_ministry', // تاريخ تبعية المسجد للوزارة
        'is_approved',
        'lessons',
        'courses',
    ];

    protected $hidden = [
        'region',
        'property'
    ];

    // المنطقة
    public function region()
    {
        return $this->belongsTo(Region::class)->withDefault([
            'name' => 'غير مدخل'
        ]);
    }

    // المفتش
    public function user()
    {
        return $this->belongsTo(User::class)->withDefault([
            'name' => 'غير مدخل'
        ]);
    }

    // بيانات المتنازل/ الواقف
    public function assignees()
    {
        return $this->hasMany(MosqueAssignee::class);
    }

    // مجلس إدارة المسجد -- رئيس المجلس والاعضاء
    public function boardMembers()
    {
        return $this->hasMany(MosqueBoardMember::class);
    }

    public function employees(){
        return $this->hasMany(Employee::class);
    }


    // جهة الاشراف
    public function supervisor()
    {
        return $this->hasOne(MosqueSupervisorAuthority::class)->withDefault([
            'name' => 'غير مدخل'
        ]);
    }

    // بيانات التخصيص
    public function allocationData()
    {
        return $this->hasMany(MosqueAllocationData::class);
    }

    // إعادة الإعمار
    public function mosqueRebuilds()
    {
        return $this->hasMany(MosqueRebuilding::class);
    }


    // مرفقات المسجد
    public function attachments(){
        return $this->hasMany(MosqueAttachment::class);
    }

    // نوع المسجد
    public function type()
    {
        return $this->belongsTo(Constant::class,'type_status')->withDefault([
            'name' => 'غير مدخل'
        ]);
    }
    // ملكية المسجد
    public function property()
    {
        return $this->belongsTo(Property::class,'property_id')->withDefault([
            'name' => 'غير مدخل'
        ]);
    }
    // طبيعة البناء
    public function building()
    {
        return $this->belongsTo(Constant::class,'building_type')->withDefault([
            'name' => 'غير مدخل'
        ]);
    }

    public function visits()
    {
        return $this->hasMany(MosqueVisit::class);
    }


}
