<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InspectionSchedule extends Model
{
    protected $fillable = [
        'month_year', 'user_id',
        'inspector_approval','department_head_approval', 'province_manager_approval',
    ];

    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s',
    ];

    //protected $appends = ['has_approving'];


    public function user()
    {
        return $this->belongsTo(User::class,'user_id','id');
    }

    public function times()
    {
        return $this->hasMany(InspectionScheduleTime::class)->orderBy('id');
    }



    public function getHasApprovingAttribute()
    {
        $auth = auth()->user()->user_type_id ;
        switch ($auth){
            case '4':
                $approving = ($this->province_manager_approval) ? true : false;
                break ;
            case '8':
                $approving = ($this->department_head_approval) ? true : false;
                break ;
            case '9':
                $approving = ($this->department_head_approval) ? true : false;
                break ;
            case '12':
                $approving = ($this->inspector_approval) ? true : false;
                break ;
            default:
                $approving = false ;

            //
        }
        return $approving ;

    }

    public function getStatusAttribute()
    {
        if($this->province_manager_approval == 1){
            $status = 'province_approved';
        }else if($this->department_head_approval == 1){
            $status = 'head_approved';
        }else if ($this->inspector_approval == 1){
            $status = 'inspector_approved';
        }else{
            $status = 'not_approved';
        }
        return $status ;

    }

    // this is a recommended way to declare event handlers
    public static function boot()
    {
        parent::boot();

        static::deleting(function ($schedule) { // before delete() method call this
            $schedule->times()->delete();
            // do the rest of the cleanup...
        });
    }
}
