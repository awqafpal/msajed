<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Generalization extends Model
{
    use SoftDeletes;

    protected $table = 'generalizations';
    protected $fillable = [
        'title',
        'slug',
        'image',
        'description',
        'status',
        'views'
    ];

    protected $casts = [
        'status' => 'boolean',
    ];
}
