<?php


namespace App\Helpers;
use App\Models\PermissionGroup;

class Common
{
    public static function getMenu($user){
        $menu = [];
        $user_permissions = $user->getAllPermissions();
        $permissionsIds = $user_permissions->pluck('id')->toArray();

        $groupsIds = $user_permissions->sortBy('group_id')->unique('group_id')->pluck('group_id')->toArray();
        $groups = PermissionGroup::with(['children'=> function($query) use ($permissionsIds){
                    $query->whereIn('id', $permissionsIds);
        }])->whereIn('id',$groupsIds)->get();

        //dd($groups);
        foreach ($groups as $key => $group){
            $menu[$key]['name'] = $group->name;
            $menu[$key]['children'] = $group->children->all();

        }
       // dd($menu);
        return $menu;
    }
}
