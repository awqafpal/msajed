<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCleaningComponentVisitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cleaning_component_visits', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('visit_id');
            $table->unsignedBigInteger('cleaning_component_id');
            $table->unsignedBigInteger('status_id');

            //foreign keys
            $table->foreign('visit_id')->references('id')->on('mosque_visits')->onDelete('cascade');
            $table->foreign('cleaning_component_id')->references('id')->on('cleaning_components')->onDelete('cascade');
            $table->foreign('status_id')->references('id')->on('constants')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cleaning_component_visits');
    }
}
