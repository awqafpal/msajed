<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMosqueAllocationDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mosque_allocation_data', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('mosque_id')->nullable();
            $table->date('allocation_date')->nullable();
            $table->string('allocation_land_space')->nullable();
            $table->text('allocation_notes')->nullable();
            $table->timestamps();
            $table->softDeletes();

            // FOREIGN KEYS
            $table->foreign('mosque_id')->references('id')->on('mosques')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mosque_allocation_data');
    }
}
