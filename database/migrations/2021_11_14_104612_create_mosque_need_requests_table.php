<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMosqueNeedRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mosque_need_requests', function (Blueprint $table) {
            $table->id(); 
            $table->unsignedBigInteger('mosque_id')->nullable();
            $table->string('request_type')->nullable();
            $table->string('request_submit_side')->nullable();
            $table->string('phone_number')->nullable();
            $table->string('address')->nullable();
            $table->text('request_content')->nullable();
            $table->string('request_quantity')->nullable();
            $table->string('estimated_value')->nullable();
            $table->boolean('status')->default(0);
            $table->timestamps();
            $table->softDeletes();

            // FOREIGN KEYS
            $table->foreign('mosque_id')->references('id')->on('mosques')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mosque_need_requests');
    }
}
