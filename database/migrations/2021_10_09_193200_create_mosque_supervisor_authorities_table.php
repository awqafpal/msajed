<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMosqueSupervisorAuthoritiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mosque_supervisor_authorities', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('type')->nullable();
            $table->unsignedBigInteger('mosque_id')->nullable();
            $table->string('supervisor_authority_name')->nullable();
            $table->timestamps();
            $table->softDeletes();

            // FOREIGN KEYS
            $table->foreign('type')->references('id')->on('constants')->onDelete('cascade');
            $table->foreign('mosque_id')->references('id')->on('mosques')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mosque_supervisor_authorities');
    }
}
