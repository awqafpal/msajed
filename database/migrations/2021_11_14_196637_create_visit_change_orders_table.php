<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVisitChangeOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('visit_change_orders', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('schedule_time_id');
            $table->string('prayer_name');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('type_id');
            $table->text('reason');
            $table->boolean('has_action')->default(0);
            $table->unsignedBigInteger('status_id')->nullable();
            $table->text('reject_reason')->nullable();

            $table->foreign('schedule_time_id')->references('id')->on('inspection_schedule_times')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('status_id')->references('id')->on('constants')->onDelete('cascade');
            $table->foreign('type_id')->references('id')->on('constants')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visit_change_orders');
    }
}
