<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMosqueVisitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mosque_visits', function (Blueprint $table) {
            $table->id();
            $table->date('date');
            $table->unsignedBigInteger('mosque_id');
            $table->unsignedBigInteger('visited_by')->comment('inspector user');
            $table->time('visit_time')->nullable();
            $table->time('time_to_leave')->nullable();
            $table->text('inspector_recommendations')->nullable();
            $table->boolean('is_submitted')->default(0);
            $table->text('department_head_recommendations')->nullable();
            $table->text('province_manager_recommendations')->nullable();

            //foreign keys
            $table->foreign('mosque_id')->references('id')->on('mosques')->onDelete('cascade');
            $table->foreign('visited_by')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mosque_visits');
    }
}
