<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMosqueBuildingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mosque_buildings', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('mosque_id')->nullable();
            $table->date('date_bulid_ad')->nullable(); // تاريخ التشكيل ميلادي
            $table->date('date_bulid_hijri')->nullable(); //  تاريخ التشكيل هجري
            $table->string('building_cost')->nullable(); // تكلفة الإنشاء
            $table->unsignedBigInteger('creation_reason')->nullable();
            $table->text('mosque_building_notes')->nullable(); // تفاصيل أخرى
            //  الجهة المتبرعة
            $table->unsignedBigInteger('location')->nullable();  // موقع الجهة المتبرعة == خارجي, محلي
            $table->string('country_name')->nullable(); //إسم الدولة
            $table->string('user_name')->nullable(); // إسم الشخص
            $table->date('date_of_formation')->nullable(); // تاريخ التشكيل
            // بيانات المقاول / المشرف
            $table->string('engineer_full_name')->nullable(); // الاسم رباعي
            $table->string('engineer_identity_number')->nullable(); //رقم الهوية
            $table->string('engineer_phone_number')->nullable(); // رقم الجوال
            $table->string('engineer_address')->nullable(); // عنوان السكن
            $table->string('engineer_license_number')->nullable(); // رقم الترخيص

            $table->timestamps();
            $table->softDeletes();

            // FOREIGN KEYS
            $table->foreign('mosque_id')->references('id')->on('mosques')->onDelete('cascade');
            $table->foreign('creation_reason')->references('id')->on('constants')->onDelete('cascade');
            $table->foreign('location')->references('id')->on('constants')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mosque_buildings');
    }
}
