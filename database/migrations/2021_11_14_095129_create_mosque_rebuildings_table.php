<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMosqueRebuildingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mosque_rebuildings', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('mosque_id')->nullable();
            $table->date('date_rebuilding_ad')->nullable(); // تاريخ التشكيل ميلادي
            $table->date('date_rebuilding_hijri')->nullable(); //  تاريخ التشكيل هجري
            $table->string('rebuilding_cost')->nullable(); // تكلفة الإنشاء
            $table->unsignedBigInteger('rebuilding_reason')->nullable();
            $table->text('mosque_rebuilding_notes')->nullable(); // تفاصيل أخرى
            //   الجهة المتبرعة بإعادة الاعمار
            $table->unsignedBigInteger('location')->nullable();  // موقع الجهة المتبرعة == خارجي, محلي
            $table->string('country_name')->nullable(); //إسم الدولة
            $table->string('user_name')->nullable(); // إسم الشخص
            $table->date('date_of_formation')->nullable(); // تاريخ التشكيل
            // بيانات المقاول / المشرف
            $table->string('engineer_full_name')->nullable(); // الاسم رباعي
            $table->string('engineer_identity_number')->nullable(); //رقم الهوية
            $table->string('engineer_phone_number')->nullable(); // رقم الجوال
            $table->string('engineer_address')->nullable(); // عنوان السكن
            $table->string('engineer_license_number')->nullable(); // رقم الترخيص

            // بيانات التوسعة / الحروب
            $table->date('due_date')->nullable(); // تاريخ الاستحقاق
            $table->string('grant')->nullable(); // منحة التوسعة أو الضرر
            $table->string('space')->nullable(); // مساحة التوسعة
            $table->date('year_of_damage')->nullable(); // سنة الضرر

            $table->timestamps();
            $table->softDeletes();

            // FOREIGN KEYS
            $table->foreign('mosque_id')->references('id')->on('mosques')->onDelete('cascade');
            $table->foreign('rebuilding_reason')->references('id')->on('constants')->onDelete('cascade');
            $table->foreign('location')->references('id')->on('constants')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mosque_rebuildings');
    }
}
