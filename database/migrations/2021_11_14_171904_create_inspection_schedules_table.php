<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInspectionSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inspection_schedules', function (Blueprint $table) {
            $table->id();
            $table->string('month_year');
            $table->unsignedBigInteger('user_id');
            $table->boolean('inspector_approval')->default(0);
            $table->boolean('department_head_approval')->default(0);
            // $table->unsignedBigInteger('department_head_id')->nullable();
            $table->boolean('province_manager_approval')->default(0);
            // $table->unsignedBigInteger('province_manager_id')->nullable();
            $table->timestamps();

            // FOREIGN KEYS
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            // $table->foreign('department_head_id')->references('id')->on('users')->onDelete('cascade');
            // $table->foreign('province_manager_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inspection_schedules');
    }
}
