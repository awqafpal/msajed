<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMosquesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mosques', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('region_id')->nullable();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->unsignedBigInteger('property_id')->nullable();
            $table->string('name');
            $table->string('slug')->nullable();
            $table->text('description')->nullable();
            $table->string('image')->default('backend/img/default.jpg');
            $table->string('street')->nullable();
            $table->string('piece')->nullable();
            $table->string('qasema')->nullable();
            $table->string('mosque_no')->nullable();
            $table->date('date_bulid_ad')->nullable();
            $table->date('date_bulid_hijri')->nullable();
            $table->date('mosque_date_ministry')->nullable()->comment('تاريخ تبعية المسجد للوزارة'); 
            $table->boolean('archaeological_status')->nullable();
            $table->boolean('central_status')->nullable();
            $table->unsignedBigInteger('type_status')->nullable()->comment('كبير', 'محلي', 'مصلى');
            $table->string('prayer_no')->nullable();
            $table->string('mosque_space')->nullable();
            $table->string('floor_no')->nullable();
            $table->unsignedBigInteger('building_type')->nullable()->comment('باطون', 'زينقو', 'كرميد', 'بركس');
            $table->string('bank_account_number')->nullable();
            $table->string('facilities_number')->nullable()->comment('عدد وقفيات المسجد');
            $table->boolean('is_approved')->default(0)->nullable();
            $table->longText('lessons')->nullable();
            $table->longText('courses')->nullable();
            $table->timestamps();
            $table->softDeletes();


            // FOREIGN KEYS
            $table->foreign('region_id')->references('id')->on('regions')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('property_id')->references('id')->on('properties')->onDelete('cascade');
            $table->foreign('type_status')->references('id')->on('constants')->onDelete('cascade');
            $table->foreign('building_type')->references('id')->on('constants')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mosques');
    }
}
