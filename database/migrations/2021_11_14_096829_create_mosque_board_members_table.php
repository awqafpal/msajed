<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMosqueBoardMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mosque_board_members', function (Blueprint $table) {
            $table->id(); 
            $table->unsignedBigInteger('mosque_id')->nullable();
            $table->string('full_name')->nullable(); // الاسم رباعي
            $table->string('identity_number')->nullable(); //رقم الهوية
            $table->string('phone_number')->nullable(); // رقم الجوال
            $table->string('address')->nullable(); // عنوان السكن
            $table->string('job')->nullable(); // طبيعة العمل
            $table->timestamps();

            // FOREIGN KEYS
            $table->foreign('mosque_id')->references('id')->on('mosques')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mosque_board_members');
    }
}
