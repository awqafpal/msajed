<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('region_id')->nullable();
            $table->unsignedBigInteger('job_type')->nullable();
            $table->unsignedBigInteger('job_id')->nullable();
            $table->unsignedBigInteger('mosque_id')->nullable();
            $table->string('name');
            $table->string('image')->default('backend/img/default.jpg');
            $table->string('phone_number')->nullable();
            $table->string('identity_number')->nullable();
            $table->string('address')->nullable();
            $table->string('job_no')->nullable(); 
            $table->string('qualification')->nullable();
            $table->date('hiring_date')->nullable();
            $table->unsignedBigInteger('hiring_type')->nullable();
            $table->timestamps();
            $table->softDeletes();

            // FOREIGN KEYS
            $table->foreign('region_id')->references('id')->on('regions')->onDelete('cascade');
            $table->foreign('job_id')->references('id')->on('jobs')->onDelete('cascade');
            $table->foreign('mosque_id')->references('id')->on('mosques')->onDelete('cascade');
            $table->foreign('hiring_type')->references('id')->on('constants')->onDelete('cascade');
            $table->foreign('job_type')->references('id')->on('constants')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
