<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInspectionScheduleTimesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inspection_schedule_times', function (Blueprint $table) {
            $table->id();
            $table->date('date');
            $table->unsignedBigInteger('inspection_schedule_id');
            // comment("JSON id,name,status,color,reason ")
            $table->json('fajr')->nullable();
            $table->json('dhuhr')->nullable();
            $table->json('asr')->nullable();
            $table->json('maghrib')->nullable();
            $table->json('isha')->nullable();
            $table->timestamps();

            // FOREIGN KEYS
            $table->foreign('inspection_schedule_id')->references('id')->on('inspection_schedules')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inspection_schedule_times');
    }
}
