<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMosqueAssigneesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mosque_assignees', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('mosque_id')->nullable();
            $table->string('assignee_user_name')->nullable();
            $table->string('assignee_mobile')->nullable();
            $table->string('assignee_identity')->nullable();
            $table->date('assignee_date')->nullable();
            $table->string('assignee_land_space')->nullable();
            $table->text('assignee_notes')->nullable();
            $table->timestamps();
            $table->softDeletes();

            // FOREIGN KEYS
            $table->foreign('mosque_id')->references('id')->on('mosques')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mosque_assignees');
    }
}
