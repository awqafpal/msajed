<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMosqueAttachmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mosque_attachments', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('mosque_id')->nullable();
            $table->unsignedBigInteger('attachment_type_id')->nullable();
            $table->string('file')->nullable();
            $table->timestamps();
            $table->softDeletes();

            // FOREIGN KEYS
            $table->foreign('attachment_type_id')->references('id')->on('attachment_types')->onDelete('cascade');
            $table->foreign('mosque_id')->references('id')->on('mosques')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mosque_attachments');
    }
}
