<?php

use Illuminate\Database\Seeder;

class WaterSourcesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('water_sources')->delete();
        
        \DB::table('water_sources')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'بلدية',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'بئر مياه',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'محطة تحلية',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'بئر مياه ومحطة تحلية',
                'created_at' => '2021-09-11 17:08:33',
                'updated_at' => '2021-09-11 17:08:33',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'بئر ماء',
                'created_at' => '2021-09-11 17:11:41',
                'updated_at' => '2021-09-11 17:11:41',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'بلدية',
                'created_at' => '2021-09-21 00:34:42',
                'updated_at' => '2021-09-21 00:34:42',
                'deleted_at' => NULL,
            ),
        ));
    }
}
