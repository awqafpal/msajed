<?php

use Illuminate\Database\Seeder;

class UserTypesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('user_types')->delete();
        
        \DB::table('user_types')->insert(array (
            0 => 
            array (
                'id' => 1,
                'user_structure_id' => NULL,
                'name' => 'مدير عام المديريات',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'user_structure_id' => NULL,
                'name' => 'مدير وحدة التخطيط وتطوير الأداء المؤسسي',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'user_structure_id' => 1,
                'name' => 'مدير المساجد',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'user_structure_id' => 1,
                'name' => 'مدير المديرية',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'user_structure_id' => 1,
                'name' => 'رئيس قسم التنسيق والمتابعة بالإدارة',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'user_structure_id' => 3,
                'name' => 'رئيس قسم التفتيش بدائرة متابعة المساجد',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'user_structure_id' => 3,
                'name' => 'رئيس قسم لجان المساجد بدائرة متابعة المساجد',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'user_structure_id' => 5,
                'name' => 'رئيس قسم',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'user_structure_id' => 6,
                'name' => 'إداري قسم المساجد بالمديرية',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'user_structure_id' => 6,
                'name' => 'رئيس شعبة التفتيش بقسم المساجد بالمديرية',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'user_structure_id' => 6,
                'name' => 'رئيس شعبة لجان المساجد بقسم المساجد بالمديرية',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'user_structure_id' => 7,
                'name' => 'مفتش',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}