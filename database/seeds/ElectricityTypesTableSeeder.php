<?php

use Illuminate\Database\Seeder;

class ElectricityTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('electricity_types')->delete();
        
        \DB::table('electricity_types')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => '3 فاز',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'فاز ونل',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'دفع مسبق',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'name' => '3 فاز  فاز ونل  دفع مسبق',
                'created_at' => '2021-09-11 17:05:33',
                'updated_at' => '2021-09-11 17:05:33',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'name' => '3فاز',
                'created_at' => '2021-09-13 06:36:55',
                'updated_at' => '2021-09-13 06:36:55',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'name' => '3فاز/فاز ونل/ ودفع مسبق',
                'created_at' => '2021-09-18 22:40:29',
                'updated_at' => '2021-09-18 22:40:29',
                'deleted_at' => NULL,
            ),
        ));
        
    }
}
