<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesTableSeeder::class);
        $this->call(PermissionsTableSeeder::class);
        $this->call(RoleHasPermissionsTableSeeder::class);
        $this->call(ModelHasRolesTableSeeder::class);
       // $this->call(AdminUserSeeder::class);
        $this->call(ConstantsTableSeeder::class);


        /* Mosque System Seeder */
        $this->call(UserStructuresTableSeeder::class);
        $this->call(UserTypesTableSeeder::class);
        $this->call(ProvincesTableSeeder::class);
        $this->call(RegionsTableSeeder::class);
        $this->call(PropertiesTableSeeder::class);
        $this->call(FacilityTableSeeder::class);
        $this->call(ElectricityTypesTableSeeder::class);
        $this->call(AlternativeEnergiesTableSeeder::class);
        $this->call(WaterSourcesTableSeeder::class);
        $this->call(JobsTableSeeder::class);
        $this->call(AttachmentTypesTableSeeder::class);
        $this->call(CleaningComponentsTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(MosquesTableSeeder::class);
        $this->call(EmployeeTableSeeder::class);
        
        $this->call(PermissionGroupTableSeeder::class);
    }
}
