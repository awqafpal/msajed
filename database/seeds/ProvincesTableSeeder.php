<?php


use Illuminate\Database\Seeder;

class ProvincesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('provinces')->delete();
        
        \DB::table('provinces')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'الشمال',
                'modeer' => 'د. مرسي جمال سلمان',
                'irshad' => 'أ. مطيع موسى السيلاوي',
                'jawwal' => '0599634815',
                'telephone' => '2496360',
                'fax' => '2496360',
                'address' => 'بيت لاهيا - الشيخ زايد',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'غزة',
                'modeer' => 'د. محمد كمال سالم',
                'irshad' => 'م. محمد حسن أبو سعدة',
                'jawwal' => '0595911530',
                'telephone' => '2802992',
                'fax' => '2802992',
                'address' => 'غزة - الصبرة - مقابل مسجد قباء الجديد ( السوسي )',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'خانيونس',
                'modeer' => 'د. محمد صالح الغلبان',
                'irshad' => 'أ. أيمن العبد مسلم النجار',
                'jawwal' => '0595343949',
                'telephone' => '2052095',
                'fax' => '2052095',
                'address' => 'صلاح الدين / بجوار مستشفى السلام',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'الوسطى',
                'modeer' => 'أ. سمير إسماعيل مسلم',
                'irshad' => 'أ. مصعب يوسف درويش',
                'jawwal' => '0599406381',
                'telephone' => '2531604',
                'fax' => '2531604',
                'address' => 'دير البلح - الشارع العام - بجوار كلية الدعوة',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'رفح',
                'modeer' => 'أ. طارق أحمد أبو يونس',
                'irshad' => 'د. فايز إبراهيم الزاملي',
                'jawwal' => '0594704072',
                'telephone' => '2136996',
                'fax' => '2136996',
                'address' => 'الشارع العام - بجوار مسجد العودة',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}