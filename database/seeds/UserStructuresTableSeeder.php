<?php


use Illuminate\Database\Seeder;

class UserStructuresTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('user_structures')->delete();
        
        \DB::table('user_structures')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'مدير عام المديريات',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'مدير وحدة التخطيط وتطوير الأداء المؤسسي',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'مدير المساجد',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'إداري',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'مدير المديرية',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'رئيس قسم',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'رئيس شعبة',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'مفتش',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}