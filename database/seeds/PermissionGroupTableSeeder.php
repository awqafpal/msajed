<?php

use Illuminate\Database\Seeder;

class PermissionGroupTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('permission_group')->delete();
        
        \DB::table('permission_group')->insert(array (
            0 => 
            array (
                'id' => 1,
                'prefix' => 'Admin',
                'namespace' => 'Admin',
                'name' => 'لوحة التحكم',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'prefix' => 'Admin',
                'namespace' => 'Admin',
                'name' => 'إعدادات الموقع',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'prefix' => 'Admin',
                'namespace' => 'Admin',
                'name' => 'الثوابت والخدمات',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'prefix' => 'Admin',
                'namespace' => 'Admin',
                'name' => 'المستخدمين والصلاحيات',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'prefix' => 'Admin',
                'namespace' => 'Admin',
                'name' => 'المستويات الادارية',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'prefix' => 'Admin',
                'namespace' => 'Admin',
                'name' => 'المساجد',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'prefix' => 'Admin',
                'namespace' => 'Admin',
                'name' => 'الموظفين',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'prefix' => 'Admin',
                'namespace' => 'Admin',
                'name' => 'النشر والإعلام',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'prefix' => 'Admin',
                'namespace' => 'Admin',
                'name' => 'الشكاوي',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'prefix' => 'Admin',
                'namespace' => 'Admin',
                'name' => 'التفتيش',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'prefix' => 'Admin',
                'namespace' => 'Admin',
                'name' => 'الطلبات',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}