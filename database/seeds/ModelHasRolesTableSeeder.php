<?php

use Illuminate\Database\Seeder;

class ModelHasRolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        \DB::table('model_has_roles')->delete();

        \DB::table('model_has_roles')->insert(array (
            0 => 
            array ( 
                'role_id' => 1,
                'model_id' => 1,
                'model_type' => 'App\\Models\\User',
            ),
            1 => 
            array (
                'role_id' => 12,
                'model_id' => 2,
                'model_type' => 'App\\Models\\User',
            ),
            2 => 
            array (
                'role_id' => 8,
                'model_id' => 3,
                'model_type' => 'App\\Models\\User',
            ),
            3 => 
            array (
                'role_id' => 8,
                'model_id' => 4,
                'model_type' => 'App\\Models\\User',
            ),
            4 => 
            array (
                'role_id' => 8,
                'model_id' => 5,
                'model_type' => 'App\\Models\\User',
            ),
            5 => 
            array (
                'role_id' => 8,
                'model_id' => 6,
                'model_type' => 'App\\Models\\User',
            ),
            6 => 
            array (
                'role_id' => 8,
                'model_id' => 7,
                'model_type' => 'App\\Models\\User',
            ),
            7 => 
            array (
                'role_id' => 3,
                'model_id' => 8,
                'model_type' => 'App\\Models\\User',
            ),
            8 => 
            array (
                'role_id' => 12,
                'model_id' => 9,
                'model_type' => 'App\\Models\\User',
            ),
            9 => 
            array (
                'role_id' => 12,
                'model_id' => 10,
                'model_type' => 'App\\Models\\User',
            ),
            10 => 
            array (
                'role_id' => 12,
                'model_id' => 11,
                'model_type' => 'App\\Models\\User',
            ),
            11 => 
            array (
                'role_id' => 12,
                'model_id' => 12,
                'model_type' => 'App\\Models\\User',
            ),
            12 => 
            array (
                'role_id' => 12,
                'model_id' => 13,
                'model_type' => 'App\\Models\\User',
            ),
            13 => 
            array (
                'role_id' => 12,
                'model_id' => 14,
                'model_type' => 'App\\Models\\User',
            ),
            14 => 
            array (
                'role_id' => 12,
                'model_id' => 15,
                'model_type' => 'App\\Models\\User',
            ),
            15 => 
            array (
                'role_id' => 12,
                'model_id' => 16,
                'model_type' => 'App\\Models\\User',
            ),
            16 => 
            array (
                'role_id' => 12,
                'model_id' => 17,
                'model_type' => 'App\\Models\\User',
            ),
            17 => 
            array (
                'role_id' => 12,
                'model_id' => 18,
                'model_type' => 'App\\Models\\User',
            ),
            18 => 
            array (
                'role_id' => 12,
                'model_id' => 19,
                'model_type' => 'App\\Models\\User',
            ),
            19 => 
            array (
                'role_id' => 12,
                'model_id' => 20,
                'model_type' => 'App\\Models\\User',
            ),
            20 => 
            array (
                'role_id' => 12,
                'model_id' => 21,
                'model_type' => 'App\\Models\\User',
            ),
            21 => 
            array (
                'role_id' => 12,
                'model_id' => 22,
                'model_type' => 'App\\Models\\User',
            ),
            22 => 
            array (
                'role_id' => 12,
                'model_id' => 23,
                'model_type' => 'App\\Models\\User',
            ),
            23 => 
            array (
                'role_id' => 12,
                'model_id' => 24,
                'model_type' => 'App\\Models\\User',
            ),
            24 => 
            array (
                'role_id' => 12,
                'model_id' => 25,
                'model_type' => 'App\\Models\\User',
            ),
            25 => 
            array (
                'role_id' => 12,
                'model_id' => 26,
                'model_type' => 'App\\Models\\User',
            ),
            26 => 
            array (
                'role_id' => 12,
                'model_id' => 27,
                'model_type' => 'App\\Models\\User',
            ),
            27 => 
            array (
                'role_id' => 12,
                'model_id' => 28,
                'model_type' => 'App\\Models\\User',
            ),
            28 => 
            array (
                'role_id' => 12,
                'model_id' => 29,
                'model_type' => 'App\\Models\\User',
            ),
            29 => 
            array (
                'role_id' => 12,
                'model_id' => 30,
                'model_type' => 'App\\Models\\User',
            ),
            30 => 
            array (
                'role_id' => 12,
                'model_id' => 31,
                'model_type' => 'App\\Models\\User',
            ),
            31 => 
            array (
                'role_id' => 12,
                'model_id' => 32,
                'model_type' => 'App\\Models\\User',
            ),
            32 => 
            array (
                'role_id' => 12,
                'model_id' => 33,
                'model_type' => 'App\\Models\\User',
            ),
            33 => 
            array (
                'role_id' => 12,
                'model_id' => 34,
                'model_type' => 'App\\Models\\User',
            ),
            34 => 
            array (
                'role_id' => 12,
                'model_id' => 35,
                'model_type' => 'App\\Models\\User',
            ),
            35 => 
            array (
                'role_id' => 12,
                'model_id' => 36,
                'model_type' => 'App\\Models\\User',
            ),
            36 => 
            array (
                'role_id' => 12,
                'model_id' => 37,
                'model_type' => 'App\\Models\\User',
            ),
            37 => 
            array (
                'role_id' => 12,
                'model_id' => 38,
                'model_type' => 'App\\Models\\User',
            ),
            38 => 
            array (
                'role_id' => 12,
                'model_id' => 39,
                'model_type' => 'App\\Models\\User',
            ),
            39 => 
            array (
                'role_id' => 12,
                'model_id' => 40,
                'model_type' => 'App\\Models\\User',
            ),
            40 => 
            array (
                'role_id' => 12,
                'model_id' => 41,
                'model_type' => 'App\\Models\\User',
            ),
            41 => 
            array (
                'role_id' => 12,
                'model_id' => 42,
                'model_type' => 'App\\Models\\User',
            ),
            42 => 
            array (
                'role_id' => 12,
                'model_id' => 43,
                'model_type' => 'App\\Models\\User',
            ),
            43 => 
            array (
                'role_id' => 12,
                'model_id' => 44,
                'model_type' => 'App\\Models\\User',
            ),
            44 => 
            array (
                'role_id' => 12,
                'model_id' => 45,
                'model_type' => 'App\\Models\\User',
            ),
            45 => 
            array (
                'role_id' => 12,
                'model_id' => 46,
                'model_type' => 'App\\Models\\User',
            ),
            46 => 
            array (
                'role_id' => 12,
                'model_id' => 47,
                'model_type' => 'App\\Models\\User',
            ),
            47 => 
            array (
                'role_id' => 12,
                'model_id' => 48,
                'model_type' => 'App\\Models\\User',
            ),
            48 => 
            array (
                'role_id' => 12,
                'model_id' => 49,
                'model_type' => 'App\\Models\\User',
            ),
            49 => 
            array (
                'role_id' => 12,
                'model_id' => 50,
                'model_type' => 'App\\Models\\User',
            ),
            50 => 
            array (
                'role_id' => 12,
                'model_id' => 51,
                'model_type' => 'App\\Models\\User',
            ),
            51 => 
            array (
                'role_id' => 12,
                'model_id' => 52,
                'model_type' => 'App\\Models\\User',
            ),
            52 => 
            array (
                'role_id' => 12,
                'model_id' => 53,
                'model_type' => 'App\\Models\\User',
            ),
            53 => 
            array (
                'role_id' => 1,
                'model_id' => 54,
                'model_type' => 'App\\Models\\User',
            ),
            54 => 
            array (
                'role_id' => 1,
                'model_id' => 55,
                'model_type' => 'App\\Models\\User',
            ),
            55 => 
            array (
                'role_id' => 5,
                'model_id' => 56,
                'model_type' => 'App\\Models\\User',
            ),
            56 => 
            array (
                'role_id' => 4,
                'model_id' => 57,
                'model_type' => 'App\\Models\\User',
            ),
            57 => 
            array (
                'role_id' => 4,
                'model_id' => 58,
                'model_type' => 'App\\Models\\User',
            ),
            58 => 
            array (
                'role_id' => 4,
                'model_id' => 59,
                'model_type' => 'App\\Models\\User',
            ),
            59 => 
            array (
                'role_id' => 4,
                'model_id' => 60,
                'model_type' => 'App\\Models\\User',
            ),
            60 => 
            array (
                'role_id' => 4,
                'model_id' => 61,
                'model_type' => 'App\\Models\\User',
            ),
            61 => 
            array (
                'role_id' => 6,
                'model_id' => 62,
                'model_type' => 'App\\Models\\User',
            ),
            62 => 
            array (
                'role_id' => 2,
                'model_id' => 63,
                'model_type' => 'App\\Models\\User',
            ),
            63 => 
            array (
                'role_id' => 12,
                'model_id' => 64,
                'model_type' => 'App\\Models\\User',
            ),
        ));
    }
}
