<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('roles')->delete();

        \DB::table('roles')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'مدير عام المديريات',
                'guard_name' => 'admin',
                'status' => 1,
                'created_at' => '2021-08-06 20:15:47',
                'updated_at' => '2021-08-06 20:15:47',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'مدير وحدة التخطيط وتطوير الأداء المؤسسي',
                'guard_name' => 'admin',
                'status' => 1,
                'created_at' => '2021-08-06 20:15:48',
                'updated_at' => '2021-08-06 20:15:48',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'مدير المساجد',
                'guard_name' => 'admin',
                'status' => 1,
                'created_at' => '2021-08-06 20:15:48',
                'updated_at' => '2021-08-06 20:15:48',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'مدير المديرية',
                'guard_name' => 'admin',
                'status' => 1,
                'created_at' => '2021-08-06 20:15:48',
                'updated_at' => '2021-08-06 20:15:48',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'رئيس قسم التنسيق والمتابعة بالإدارة',
                'guard_name' => 'admin',
                'status' => 1,
                'created_at' => '2021-08-06 20:15:49',
                'updated_at' => '2021-08-06 20:15:49',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'رئيس قسم التفتيش بدائرة متابعة المساجد',
                'guard_name' => 'admin',
                'status' => 1,
                'created_at' => '2021-08-06 20:15:49',
                'updated_at' => '2021-08-06 20:15:49',
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'رئيس قسم لجان المساجد بدائرة متابعة المساجد',
                'guard_name' => 'admin',
                'status' => 1,
                'created_at' => '2021-08-06 20:15:49',
                'updated_at' => '2021-08-06 20:15:49',
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'رئيس قسم',
                'guard_name' => 'admin',
                'status' => 1,
                'created_at' => '2021-08-06 20:15:49',
                'updated_at' => '2021-08-06 20:15:49',
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'إداري قسم المساجد بالمديرية',
                'guard_name' => 'admin',
                'status' => 1,
                'created_at' => '2021-08-06 20:15:49',
                'updated_at' => '2021-08-06 20:15:49',
            ),
            9 => 
            array (
                'id' => 10,
                'name' => 'رئيس شعبة التفتيش بقسم المساجد بالمديرية',
                'guard_name' => 'admin',
                'status' => 1,
                'created_at' => '2021-08-06 20:15:49',
                'updated_at' => '2021-08-06 20:15:49',
            ),
            10 => 
            array (
                'id' => 11,
                'name' => 'رئيس شعبة لجان المساجد بقسم المساجد بالمديرية',
                'guard_name' => 'admin',
                'status' => 1,
                'created_at' => '2021-08-06 20:15:49',
                'updated_at' => '2021-08-06 20:15:49',
            ),
            11 => 
            array (
                'id' => 12,
                'name' => 'مفتش',
                'guard_name' => 'admin',
                'status' => 1,
                'created_at' => '2021-08-06 20:15:49',
                'updated_at' => '2021-08-06 20:15:49',
            ),
            
        ));
        
    }
}
