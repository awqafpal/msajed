<?php

use Illuminate\Database\Seeder;

class RoleHasPermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('role_has_permissions')->delete();
        
        $permissions = \DB::table('permissions')->get('id');
        foreach ($permissions as $permission){
            \DB::table('role_has_permissions')->insert(['permission_id' => $permission->id, 'role_id' => 1,]) ;
        }
    }
}
