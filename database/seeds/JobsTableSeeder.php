<?php

use Illuminate\Database\Seeder;

class JobsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('jobs')->delete();
        
        \DB::table('jobs')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'الامام عبدالله سليمان عبدالغفور',
                'created_at' => NULL,
                'updated_at' => '2021-09-01 16:48:10',
                'deleted_at' => '2021-09-01 16:48:10',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'المحفظ',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'الواعظ',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'المؤذن',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'الخادم',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'إمام ومحفظ',
                'created_at' => NULL,
                'updated_at' => '2021-09-12 10:15:23',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'إمام وواعظ',
                'created_at' => NULL,
                'updated_at' => '2021-09-12 10:14:48',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'مؤذن وخادم',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'خدمات مسجدية',
                'created_at' => '2021-07-04 17:57:30',
                'updated_at' => '2021-09-12 10:12:17',
                'deleted_at' => '2021-09-12 10:12:17',
            ),
            9 => 
            array (
                'id' => 10,
                'name' => 'شؤون دينية',
                'created_at' => '2021-07-04 17:57:43',
                'updated_at' => '2021-09-12 10:12:39',
                'deleted_at' => '2021-09-12 10:12:39',
            ),
            10 => 
            array (
                'id' => 11,
                'name' => 'امام وخطيب',
                'created_at' => '2021-07-04 17:58:11',
                'updated_at' => '2021-07-04 17:58:11',
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'name' => 'مدرس دورات احكام',
                'created_at' => '2021-07-04 17:58:55',
                'updated_at' => '2021-07-04 17:58:55',
                'deleted_at' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'name' => 'تمام ومحفظ',
                'created_at' => '2021-09-04 13:42:16',
                'updated_at' => '2021-09-12 10:14:06',
                'deleted_at' => '2021-09-12 10:14:06',
            ),
            13 => 
            array (
                'id' => 14,
                'name' => 'مؤذن',
                'created_at' => '2021-09-04 13:43:01',
                'updated_at' => '2021-09-04 13:43:01',
                'deleted_at' => NULL,
            ),
            14 => 
            array (
                'id' => 15,
                'name' => 'إمام',
                'created_at' => '2021-09-06 13:31:25',
                'updated_at' => '2021-09-06 13:31:25',
                'deleted_at' => NULL,
            ),
            15 => 
            array (
                'id' => 16,
                'name' => 'أمين مكتبة',
                'created_at' => '2021-09-19 13:36:34',
                'updated_at' => '2021-09-19 13:36:34',
                'deleted_at' => NULL,
            ),
        ));
    }
}
