<?php

use Illuminate\Database\Seeder;

class AdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('users')->truncate();
        \App\Models\User::create([
            'username' => 'admin',
            'name' => 'admin',
            'email' => 'admin@palwakf.net',
            'password' => '123456',
            'status' => 1,
        ]);
    }
}
