<?php


use Illuminate\Database\Seeder;

class RegionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('regions')->delete();
        
        \DB::table('regions')->insert(array (
            0 => 
            array (
                'id' => 1,
                'province_id' => 1,
                'name' => 'جباليا البلد والنزلة',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'province_id' => 1,
                'name' => 'بيت حانون والعزبة والقرية البدوية',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'province_id' => 1,
                'name' => 'بيت لاهيا والمشروع',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'province_id' => 1,
                'name' => 'الصفطاوي ومشروع عامر',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'province_id' => 1,
                'name' => 'معسكر جباليا المنطقة الغربية',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'province_id' => 1,
                'name' => 'معسكر جباليا المنطقة الشرقية',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 9,
                'province_id' => 4,
                'name' => 'دير البلح',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 10,
                'province_id' => 4,
                'name' => 'النصيرات',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 11,
                'province_id' => 4,
                'name' => 'البريج',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 12,
                'province_id' => 4,
                'name' => 'المغازي',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id' => 13,
                'province_id' => 4,
                'name' => 'المغراقة',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'id' => 14,
                'province_id' => 4,
                'name' => 'الزوايدة',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            12 => 
            array (
                'id' => 16,
                'province_id' => 2,
                'name' => 'الرمال',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            13 => 
            array (
                'id' => 17,
                'province_id' => 2,
                'name' => 'الشيخ رضوان',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            14 => 
            array (
                'id' => 18,
                'province_id' => 2,
                'name' => 'تل الهوا',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            15 => 
            array (
                'id' => 19,
                'province_id' => 2,
                'name' => 'شرق الزيتون',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            16 => 
            array (
                'id' => 20,
                'province_id' => 2,
                'name' => 'الصبرة',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            17 => 
            array (
                'id' => 21,
                'province_id' => 2,
                'name' => 'الشجاعية',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            18 => 
            array (
                'id' => 22,
                'province_id' => 2,
                'name' => 'الشعف',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            19 => 
            array (
                'id' => 23,
                'province_id' => 2,
                'name' => 'التفاح',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            20 => 
            array (
                'id' => 24,
                'province_id' => 2,
                'name' => 'الدرج',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            21 => 
            array (
                'id' => 25,
                'province_id' => 2,
                'name' => 'الشاطئ الشمالي والنصر',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            22 => 
            array (
                'id' => 26,
                'province_id' => 2,
                'name' => 'الشاطئ الجنوبي',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            23 => 
            array (
                'id' => 30,
                'province_id' => 5,
                'name' => 'البحر',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            24 => 
            array (
                'id' => 31,
                'province_id' => 5,
                'name' => 'تل السلطان',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            25 => 
            array (
                'id' => 32,
                'province_id' => 5,
                'name' => 'الغربية',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            26 => 
            array (
                'id' => 33,
                'province_id' => 5,
                'name' => 'البلد',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            27 => 
            array (
                'id' => 34,
                'province_id' => 5,
            'name' => '(الحشاش ) عريبة',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            28 => 
            array (
                'id' => 35,
                'province_id' => 5,
                'name' => 'مصبح',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            29 => 
            array (
                'id' => 36,
                'province_id' => 5,
                'name' => 'النصر',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            30 => 
            array (
                'id' => 37,
                'province_id' => 5,
                'name' => 'حي الجنينة',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            31 => 
            array (
                'id' => 38,
                'province_id' => 5,
                'name' => 'حي السلام',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            32 => 
            array (
                'id' => 39,
                'province_id' => 5,
                'name' => 'الشوكة',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            33 => 
            array (
                'id' => 40,
                'province_id' => 5,
                'name' => 'الشابورة',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            34 => 
            array (
                'id' => 41,
                'province_id' => 5,
                'name' => 'مشروع عامر',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            35 => 
            array (
                'id' => 43,
                'province_id' => 3,
                'name' => 'ميراج',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            36 => 
            array (
                'id' => 57,
                'province_id' => 3,
                'name' => 'جورت اللوت',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            37 => 
            array (
                'id' => 61,
                'province_id' => 5,
                'name' => 'خربة العدس',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            38 => 
            array (
                'id' => 62,
                'province_id' => 3,
                'name' => 'البلد',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            39 => 
            array (
                'id' => 63,
                'province_id' => 3,
                'name' => 'المعسكر',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            40 => 
            array (
                'id' => 64,
                'province_id' => 3,
                'name' => 'المشروع',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            41 => 
            array (
                'id' => 65,
                'province_id' => 3,
                'name' => 'القرارة',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            42 => 
            array (
                'id' => 66,
                'province_id' => 3,
                'name' => 'السطر الشرقي',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            43 => 
            array (
                'id' => 67,
                'province_id' => 3,
                'name' => 'معن',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            44 => 
            array (
                'id' => 68,
                'province_id' => 3,
                'name' => 'بني سهيلا',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            45 => 
            array (
                'id' => 69,
                'province_id' => 3,
                'name' => 'عبسان الكبيرة',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            46 => 
            array (
                'id' => 70,
                'province_id' => 3,
                'name' => 'عبسان الجديدة',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            47 => 
            array (
                'id' => 71,
                'province_id' => 3,
                'name' => 'خزاعة',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            48 => 
            array (
                'id' => 72,
                'province_id' => 3,
                'name' => 'القيزان',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            49 => 
            array (
                'id' => 73,
                'province_id' => 3,
                'name' => 'البحر',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            50 => 
            array (
                'id' => 74,
                'province_id' => 3,
                'name' => 'الفخاري',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            51 => 
            array (
                'id' => 75,
                'province_id' => 3,
                'name' => 'المنارة',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            52 => 
            array (
                'id' => 76,
                'province_id' => 5,
                'name' => 'البرازيل',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            53 => 
            array (
                'id' => 77,
                'province_id' => 3,
                'name' => 'السطر الغربي',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            54 => 
            array (
                'id' => 81,
                'province_id' => 5,
                'name' => 'الزهور',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            55 => 
            array (
                'id' => 82,
                'province_id' => 5,
                'name' => 'الشعوت',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            56 => 
            array (
                'id' => 83,
                'province_id' => 5,
                'name' => 'حي التنور',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            57 => 
            array (
                'id' => 91,
                'province_id' => 2,
                'name' => 'غرب الزيتون',
                'created_at' => now(),
                'updated_at' => now(),
                'deleted_at' => NULL,
            ),
            58 => 
            array (
                'id' => 92,
                'province_id' => 4,
                'name' => 'جحر الديك',
                'created_at' => '2021-07-14 10:33:35',
                'updated_at' => '2021-07-14 10:33:35',
                'deleted_at' => NULL,
            ),
            59 => 
            array (
                'id' => 93,
                'province_id' => 4,
                'name' => 'مدينة الزهراء',
                'created_at' => '2021-07-14 10:35:40',
                'updated_at' => '2021-07-14 10:35:40',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}