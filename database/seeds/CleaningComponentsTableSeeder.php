<?php

use Illuminate\Database\Seeder;

class CleaningComponentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('cleaning_components')->delete();
        
        \DB::table('cleaning_components')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'صحن المسجد',
                'is_used' => 0,
                'created_at' => NULL,
                'updated_at' => '2021-09-10 23:21:27',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'جهاز الصوت',
                'is_used' => 1,
                'created_at' => NULL,
                'updated_at' => '2021-08-31 10:27:07',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'الساحة والممرات',
                'is_used' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'الإنارة',
                'is_used' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'الأثاث والشبابيك',
                'is_used' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'المراوح والتكييف',
                'is_used' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'المتوضأ',
                'is_used' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'الكهرباء البديلة',
                'is_used' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'دورات المياه',
                'is_used' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'name' => 'ماتور المياه',
                'is_used' => 0,
                'created_at' => NULL,
                'updated_at' => '2021-07-05 14:57:39',
            ),
            10 => 
            array (
                'id' => 11,
                'name' => 'التوحيد',
                'is_used' => 1,
                'created_at' => '2021-07-05 14:57:01',
                'updated_at' => '2021-07-05 14:57:01',
            ),
            11 => 
            array (
                'id' => 12,
                'name' => 'جيد',
                'is_used' => 1,
                'created_at' => '2021-09-21 00:41:10',
                'updated_at' => '2021-09-21 00:41:10',
            ),
            12 => 
            array (
                'id' => 13,
                'name' => 'ممتاز',
                'is_used' => 1,
                'created_at' => '2021-09-21 02:14:25',
                'updated_at' => '2021-09-21 02:14:25',
            ),
        ));
    }
}
