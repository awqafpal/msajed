<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Index Route
Route::get('/', ['as' => 'index', 'uses' => 'HomeController@getIndex']);
Route::get('admin', ['as' => 'dashboard', 'uses' => 'HomeController@getIndex']);
Route::get('clear', ['as' => 'clear', 'uses' => 'HomeController@systemClear']);


//Dictionary Route
Route::group(['prefix' => 'admin'], function ()
{
    Route::get('dictionary.js', ['as' => 'admin.common.general', 'uses' => 'HomeController@dictionary']);
});



Route::name('admin.')->group(function () {
    //LOGIN GROUP
    Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'middleware' => ['guest:admin', 'throttle:100,1']], function () {
        //LOGIN ROUTE
        Route::get('login', ['as' => 'login.view', 'uses' => 'LoginController@getLogin']);
        Route::post('login', ['as' => 'login.view', 'uses' => 'LoginController@postLogin']);
    });

    Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'middleware' => ['web', 'auth:admin', 'throttle:100,1']], function () {
        Route::get('test', ['as' => 'test', 'uses' => 'TestController@getIndex']);

        //Dashboard Route
        Route::get('dashboard', ['as' => 'dashboard.view', 'uses' => 'DashboardController@getIndex']);
        Route::get('profile', ['as' => 'dashboard.profile', 'uses' => 'DashboardController@getProfile']);
        Route::post('profile', ['as' => 'dashboard.profile', 'uses' => 'DashboardController@postProfile']);
        Route::get('password', ['as' => 'dashboard.password', 'uses' => 'DashboardController@getPassword']);
        Route::post('password', ['as' => 'dashboard.password', 'uses' => 'DashboardController@postPassword']);

        //Setting Route
//        Route::get('settings', ['as' => 'settings.view', 'middleware' => ['permission:admin.settings.view'], 'uses' => 'SettingsController@getIndex']);
//        Route::post('settings', ['as' => 'settings.view', 'middleware' => ['permission:admin.settings.view'], 'uses' => 'SettingsController@postIndex']);
//
        //Users Route
        Route::get('users', ['as' => 'users.view', 'middleware' => ['permission:admin.users.view|admin.users.add|admin.users.edit|admin.users.delete|admin.users.password'], 'uses' => 'UsersController@getIndex']);
        Route::get('users/list', ['as' => 'users.list', 'middleware' => ['permission:admin.users.view|admin.users.add|admin.users.edit|admin.users.delete|admin.users.password'], 'uses' => 'UsersController@getList']);
        Route::get('users/add', ['as' => 'users.add', 'middleware' => ['permission:admin.users.add'], 'uses' => 'UsersController@getAdd']);
        Route::post('users/add', ['as' => 'users.add', 'middleware' => ['permission:admin.users.add'], 'uses' => 'UsersController@postAdd']);
        Route::get('users/edit/{id}', ['as' => 'users.edit', 'middleware' => ['permission:admin.users.edit'], 'uses' => 'UsersController@getEdit']);
        Route::post('users/edit/{id}', ['as' => 'users.edit', 'middleware' => ['permission:admin.users.edit'], 'uses' => 'UsersController@postEdit']);
        Route::get('users/password/{id}', ['as' => 'users.password', 'middleware' => ['permission:admin.users.password'], 'uses' => 'UsersController@getPassword']);
        Route::post('users/password/{id}', ['as' => 'users.password', 'middleware' => ['permission:admin.users.password'], 'uses' => 'UsersController@postPassword']);
        Route::get('users/delete/{id}', ['as' => 'users.delete', 'middleware' => ['permission:admin.users.delete'], 'uses' => 'UsersController@getDelete']);
        Route::get('users/permissions/{id}', ['as' => 'users.permissions', 'middleware' => ['permission:admin.users.permissions'], 'uses' =>'UsersController@getPermissions']);
        Route::post('users/permissions/{id}', ['as' => 'users.permissions', 'middleware' => ['permission:admin.users.permissions'], 'uses' =>'UsersController@postPermissions']);




        // ROLES ROUTE
        Route::get('roles', ['as' => 'roles.view', 'middleware' => ['permission:admin.roles.view|admin.roles.add|admin.roles.edit|admin.roles.delete|admin.roles.status|admin.roles.permissions'], 'uses' => 'RolesController@getIndex']);
        Route::get('roles/list', ['as' => 'roles.list', 'middleware' => ['permission:admin.roles.view|admin.roles.add|admin.roles.edit|admin.roles.delete|admin.roles.status|admin.roles.permissions'], 'uses' => 'RolesController@getList']);
        Route::get('roles/add', ['as' => 'roles.add', 'middleware' => ['permission:admin.roles.add'], 'uses' => 'RolesController@getAdd']);
        Route::post('roles/add', ['as' => 'roles.add', 'middleware' => ['permission:admin.roles.add'], 'uses' => 'RolesController@postAdd']);
        Route::get('roles/edit/{id}', ['as' => 'roles.edit', 'middleware' => ['permission:admin.roles.edit'], 'uses' => 'RolesController@getEdit']);
        Route::post('roles/edit/{id}', ['as' => 'roles.edit', 'middleware' => ['permission:admin.roles.edit'], 'uses' => 'RolesController@postEdit']);
        Route::get('roles/delete/{id}', ['as' => 'roles.delete', 'middleware' => ['permission:admin.roles.delete'], 'uses' => 'RolesController@getDelete']);
        Route::post('roles/status', ['as' => 'roles.status', 'middleware' => ['permission:admin.roles.status'], 'uses' => 'RolesController@postStatus']);
        Route::get('roles/permissions/{id}', ['as' => 'roles.permissions', 'middleware' => ['permission:admin.roles.permissions'], 'uses' =>'RolesController@getPermissions']);
        Route::post('roles/permissions/{id}', ['as' => 'roles.permissions', 'middleware' => ['permission:admin.roles.permissions'], 'uses' =>'RolesController@postPermissions']);

        //Logout Route
        Route::get('logout', ['as' => 'dashboard.logout', 'uses' => 'DashboardController@getLogout']);



        /** Mosques System Routes */
        // Provinces ROUTE
        Route::get('provinces', ['as' => 'provinces.view', 'uses' => 'ProvinceController@getIndex']);
        Route::get('provinces/list', ['as' => 'provinces.list', 'uses' => 'ProvinceController@getList']);
        Route::get('provinces/add', ['as' => 'provinces.add', 'uses' => 'ProvinceController@getAdd']);
        Route::post('provinces/store', ['as' => 'provinces.store', 'uses' => 'ProvinceController@postAdd']);
        Route::get('provinces/edit/{id}', ['as' => 'provinces.edit', 'uses' => 'ProvinceController@getEdit']);
        Route::post('provinces/update/{id}', ['as' => 'provinces.update', 'uses' => 'ProvinceController@postEdit']);
        Route::get('provinces/delete/{id}', ['as' => 'provinces.delete', 'uses' => 'ProvinceController@getDelete']);
        Route::get('provinces/{id}/regions/ajax', ['as' => 'provinces.regions.ajax', 'uses' => 'ProvinceController@getRegionsAJAX']);

        // Regions ROUTE
        Route::get('regions', ['as' => 'regions.view', 'uses' => 'RegionController@getIndex']);
        Route::get('regions/list', ['as' => 'regions.list', 'uses' => 'RegionController@getList']);
        Route::get('regions/add', ['as' => 'regions.add', 'uses' => 'RegionController@getAdd']);
        Route::post('regions/store', ['as' => 'regions.store', 'uses' => 'RegionController@postAdd']);
        Route::get('regions/edit/{id}', ['as' => 'regions.edit', 'uses' => 'RegionController@getEdit']);
        Route::post('regions/update/{id}', ['as' => 'regions.update', 'uses' => 'RegionController@postEdit']);
        Route::get('regions/delete/{id}', ['as' => 'regions.delete', 'uses' => 'RegionController@getDelete']);
        Route::get('regions/{id}/mosques/ajax', ['as' => 'regions.mosques.ajax', 'uses' => 'RegionController@getMosquesAJAX']);
        Route::get('regions/{id}/employees/ajax', ['as' => 'regions.employees.ajax', 'uses' => 'RegionController@getEmployeesAJAX']);


        // USER STRUCTURE ROUTE
        Route::get('user-structures', ['as' => 'userStructures.view', 'uses' => 'UserStructureController@getIndex']);
        Route::get('user-structures/list', ['as' => 'userStructures.list', 'uses' => 'UserStructureController@getList']);
        Route::get('user-structures/add', ['as' => 'userStructures.add', 'uses' => 'UserStructureController@getAdd']);
        Route::post('user-structures/store', ['as' => 'userStructures.store', 'uses' => 'UserStructureController@postAdd']);
        Route::get('user-structures/edit/{id}', ['as' => 'userStructures.edit', 'uses' => 'UserStructureController@getEdit']);
        Route::post('user-structures/update/{id}', ['as' => 'userStructures.update', 'uses' => 'UserStructureController@postEdit']);
        Route::get('user-structures/delete/{id}', ['as' => 'userStructures.delete', 'uses' => 'UserStructureController@getDelete']);


        // USER TYPES ROUTE
        Route::get('user-types', ['as' => 'userTypes.view', 'uses' => 'UserTypeController@getIndex']);
        Route::get('user-types/list', ['as' => 'userTypes.list', 'uses' => 'UserTypeController@getList']);
        Route::get('user-types/add', ['as' => 'userTypes.add', 'uses' => 'UserTypeController@getAdd']);
        Route::post('user-types/store', ['as' => 'userTypes.store', 'uses' => 'UserTypeController@postAdd']);
        Route::get('user-types/edit/{id}', ['as' => 'userTypes.edit', 'uses' => 'UserTypeController@getEdit']);
        Route::post('user-types/update/{id}', ['as' => 'userTypes.update', 'uses' => 'UserTypeController@postEdit']);
        Route::get('user-types/delete/{id}', ['as' => 'userTypes.delete', 'uses' => 'UserTypeController@getDelete']);

        // PROPERTIES ROUTE
        Route::get('properties', ['as' => 'properties.view', 'uses' => 'PropertyController@getIndex']);
        Route::get('properties/list', ['as' => 'properties.list', 'uses' => 'PropertyController@getList']);
        Route::get('properties/add', ['as' => 'properties.add', 'uses' => 'PropertyController@getAdd']);
        Route::post('properties/store', ['as' => 'properties.store', 'uses' => 'PropertyController@postAdd']);
        Route::get('properties/edit/{id}', ['as' => 'properties.edit', 'uses' => 'PropertyController@getEdit']);
        Route::post('properties/update/{id}', ['as' => 'properties.update', 'uses' => 'PropertyController@postEdit']);
        Route::get('properties/delete/{id}', ['as' => 'properties.delete', 'uses' => 'PropertyController@getDelete']);

        // FACILITIES ROUTE
        Route::get('facilities', ['as' => 'facilities.view', 'uses' => 'FacilityController@getIndex']);
        Route::get('facilities/list', ['as' => 'facilities.list', 'uses' => 'FacilityController@getList']);
        Route::get('facilities/add', ['as' => 'facilities.add', 'uses' => 'FacilityController@getAdd']);
        Route::post('facilities/store', ['as' => 'facilities.store', 'uses' => 'FacilityController@postAdd']);
        Route::get('facilities/edit/{id}', ['as' => 'facilities.edit', 'uses' => 'FacilityController@getEdit']);
        Route::post('facilities/update/{id}', ['as' => 'facilities.update', 'uses' => 'FacilityController@postEdit']);
        Route::get('facilities/delete/{id}', ['as' => 'facilities.delete', 'uses' => 'FacilityController@getDelete']);

        // ELECTRICITY TYPES ROUTE
        Route::get('electricity-types', ['as' => 'electricityTypes.view', 'uses' => 'ElectricityTypeController@getIndex']);
        Route::get('electricity-types/list', ['as' => 'electricityTypes.list', 'uses' => 'ElectricityTypeController@getList']);
        Route::get('electricity-types/add', ['as' => 'electricityTypes.add', 'uses' => 'ElectricityTypeController@getAdd']);
        Route::post('electricity-types/store', ['as' => 'electricityTypes.store', 'uses' => 'ElectricityTypeController@postAdd']);
        Route::get('electricity-types/edit/{id}', ['as' => 'electricityTypes.edit', 'uses' => 'ElectricityTypeController@getEdit']);
        Route::post('electricity-types/update/{id}', ['as' => 'electricityTypes.update', 'uses' => 'ElectricityTypeController@postEdit']);
        Route::get('electricity-types/delete/{id}', ['as' => 'electricityTypes.delete', 'uses' => 'ElectricityTypeController@getDelete']);


        // ALTERNATIVE ENERGIES ROUTE
        Route::get('alternative-energies', ['as' => 'alternativeEnergies.view', 'uses' => 'AlternativeEnergyController@getIndex']);
        Route::get('alternative-energies/list', ['as' => 'alternativeEnergies.list', 'uses' => 'AlternativeEnergyController@getList']);
        Route::get('alternative-energies/add', ['as' => 'alternativeEnergies.add', 'uses' => 'AlternativeEnergyController@getAdd']);
        Route::post('alternative-energies/store', ['as' => 'alternativeEnergies.store', 'uses' => 'AlternativeEnergyController@postAdd']);
        Route::get('alternative-energies/edit/{id}', ['as' => 'alternativeEnergies.edit', 'uses' => 'AlternativeEnergyController@getEdit']);
        Route::post('alternative-energies/update/{id}', ['as' => 'alternativeEnergies.update', 'uses' => 'AlternativeEnergyController@postEdit']);
        Route::get('alternative-energies/delete/{id}', ['as' => 'alternativeEnergies.delete', 'uses' => 'AlternativeEnergyController@getDelete']);

        // WATER SOURCES ROUTE
        Route::get('water-sources', ['as' => 'waterSources.view', 'uses' => 'WaterSourceController@getIndex']);
        Route::get('water-sources/list', ['as' => 'waterSources.list', 'uses' => 'WaterSourceController@getList']);
        Route::get('water-sources/add', ['as' => 'waterSources.add', 'uses' => 'WaterSourceController@getAdd']);
        Route::post('water-sources/store', ['as' => 'waterSources.store', 'uses' => 'WaterSourceController@postAdd']);
        Route::get('water-sources/edit/{id}', ['as' => 'waterSources.edit', 'uses' => 'WaterSourceController@getEdit']);
        Route::post('water-sources/update/{id}', ['as' => 'waterSources.update', 'uses' => 'WaterSourceController@postEdit']);
        Route::get('water-sources/delete/{id}', ['as' => 'waterSources.delete', 'uses' => 'WaterSourceController@getDelete']);

        // JOBS ROUTE
        Route::get('jobs', ['as' => 'jobs.view', 'uses' => 'JobController@getIndex']);
        Route::get('jobs/list', ['as' => 'jobs.list', 'uses' => 'JobController@getList']);
        Route::get('jobs/add', ['as' => 'jobs.add', 'uses' => 'JobController@getAdd']);
        Route::post('jobs/store', ['as' => 'jobs.store', 'uses' => 'JobController@postAdd']);
        Route::get('jobs/edit/{id}', ['as' => 'jobs.edit', 'uses' => 'JobController@getEdit']);
        Route::post('jobs/update/{id}', ['as' => 'jobs.update', 'uses' => 'JobController@postEdit']);
        Route::get('jobs/delete/{id}', ['as' => 'jobs.delete', 'uses' => 'JobController@getDelete']);

        // ATTACHMENT TYPES ROUTE
        Route::get('attachment-types', ['as' => 'attachmentTypes.view', 'uses' => 'AttachmentTypeController@getIndex']);
        Route::get('attachment-types/list', ['as' => 'attachmentTypes.list', 'uses' => 'AttachmentTypeController@getList']);
        Route::get('attachment-types/add', ['as' => 'attachmentTypes.add', 'uses' => 'AttachmentTypeController@getAdd']);
        Route::post('attachment-types/store', ['as' => 'attachmentTypes.store', 'uses' => 'AttachmentTypeController@postAdd']);
        Route::get('attachment-types/edit/{id}', ['as' => 'attachmentTypes.edit', 'uses' => 'AttachmentTypeController@getEdit']);
        Route::post('attachment-types/update/{id}', ['as' => 'attachmentTypes.update', 'uses' => 'AttachmentTypeController@postEdit']);
        Route::get('attachment-types/delete/{id}', ['as' => 'attachmentTypes.delete', 'uses' => 'AttachmentTypeController@getDelete']);

        // ADVERTISINGS ROUTE
        Route::get('advertisings', ['as' => 'advertisings.view', 'uses' => 'AdvertisingController@getIndex']);
        Route::get('advertisings/list', ['as' => 'advertisings.list', 'uses' => 'AdvertisingController@getList']);
        Route::get('advertisings/add', ['as' => 'advertisings.add', 'uses' => 'AdvertisingController@getAdd']);
        Route::post('advertisings/store', ['as' => 'advertisings.store', 'uses' => 'AdvertisingController@postAdd']);
        Route::get('advertisings/edit/{id}', ['as' => 'advertisings.edit', 'uses' => 'AdvertisingController@getEdit']);
        Route::post('advertisings/update/{id}', ['as' => 'advertisings.update', 'uses' => 'AdvertisingController@postEdit']);
        Route::get('advertisings/delete/{id}', ['as' => 'advertisings.delete', 'uses' => 'AdvertisingController@getDelete']);

        // NEWS ROUTE
        Route::get('news', ['as' => 'news.view', 'uses' => 'NewsController@getIndex']);
        Route::get('news/list', ['as' => 'news.list', 'uses' => 'NewsController@getList']);
        Route::get('news/add', ['as' => 'news.add', 'uses' => 'NewsController@getAdd']);
        Route::post('news/store', ['as' => 'news.store', 'uses' => 'NewsController@postAdd']);
        Route::get('news/edit/{id}', ['as' => 'news.edit', 'uses' => 'NewsController@getEdit']);
        Route::post('news/update/{id}', ['as' => 'news.update', 'uses' => 'NewsController@postEdit']);
        Route::get('news/delete/{id}', ['as' => 'news.delete', 'uses' => 'NewsController@getDelete']);

        // GENERALIZATION ROUTE
        Route::get('generalizations', ['as' => 'generalizations.view', 'uses' => 'GeneralizationController@getIndex']);
        Route::get('generalizations/list', ['as' => 'generalizations.list', 'uses' => 'GeneralizationController@getList']);
        Route::get('generalizations/add', ['as' => 'generalizations.add', 'uses' => 'GeneralizationController@getAdd']);
        Route::post('generalizations/store', ['as' => 'generalizations.store', 'uses' => 'GeneralizationController@postAdd']);
        Route::get('generalizations/edit/{id}', ['as' => 'generalizations.edit', 'uses' => 'GeneralizationController@getEdit']);
        Route::post('generalizations/update/{id}', ['as' => 'generalizations.update', 'uses' => 'GeneralizationController@postEdit']);
        Route::get('generalizations/delete/{id}', ['as' => 'generalizations.delete', 'uses' => 'GeneralizationController@getDelete']);

        // CLEANING COMPONENTS ROUTE
        Route::get('cleaning-components', ['as' => 'cleaningComponents.view', 'uses' => 'CleaningComponentController@getIndex']);
        Route::get('cleaning-components/list', ['as' => 'cleaningComponents.list', 'uses' => 'CleaningComponentController@getList']);
        Route::get('cleaning-components/add', ['as' => 'cleaningComponents.add', 'uses' => 'CleaningComponentController@getAdd']);
        Route::post('cleaning-components/store', ['as' => 'cleaningComponents.store', 'uses' => 'CleaningComponentController@postAdd']);
        Route::get('cleaning-components/edit/{id}', ['as' => 'cleaningComponents.edit', 'uses' => 'CleaningComponentController@getEdit']);
        Route::post('cleaning-components/update/{id}', ['as' => 'cleaningComponents.update', 'uses' => 'CleaningComponentController@postEdit']);
        Route::get('cleaning-components/change-status/{id}', ['as' => 'cleaningComponents.status', 'uses' => 'CleaningComponentController@changeStatus']);
        //Route::get('cleaning-components/delete/{id}', ['as' => 'cleaningComponents.delete', 'uses' => 'CleaningComponentController@getDelete']);

        // SETTINGS ROUTE
        Route::get('settings', ['as' => 'settings.view', 'uses' => 'SettingComponentController@getIndex']);
        Route::get('settings/list', ['as' => 'settings.list', 'uses' => 'SettingComponentController@getList']);
        Route::get('settings/add', ['as' => 'settings.add', 'uses' => 'SettingComponentController@getAdd']);
        Route::post('settings/store', ['as' => 'settings.store', 'uses' => 'SettingComponentController@postAdd']);
        Route::get('settings/edit/{id}', ['as' => 'settings.edit', 'uses' => 'SettingComponentController@getEdit']);
        Route::post('settings/update/{id}', ['as' => 'settings.update', 'uses' => 'SettingComponentController@postEdit']);
        Route::get('settings/delete/{id}', ['as' => 'settings.delete', 'uses' => 'SettingComponentController@getDelete']);

        // EMPLOYEES ROUTE
        Route::get('employees', ['as' => 'employees.view', 'uses' => 'EmployeeController@getIndex']);
        Route::get('employees/list', ['as' => 'employees.list', 'uses' => 'EmployeeController@getList']);
        Route::get('employees/add', ['as' => 'employees.add', 'uses' => 'EmployeeController@getAdd']);
        Route::post('employees/store', ['as' => 'employees.store', 'uses' => 'EmployeeController@postAdd']);
        Route::get('employees/edit/{id}', ['as' => 'employees.edit', 'uses' => 'EmployeeController@getEdit']);
        Route::post('employees/update/{id}', ['as' => 'employees.update', 'uses' => 'EmployeeController@postEdit']);
        Route::get('employees/delete/{id}', ['as' => 'employees.delete', 'uses' => 'EmployeeController@getDelete']);
        Route::get('employees/export/', ['as' => 'employees.export', 'uses' => 'EmployeeController@getExport']);

        // MOSQUES ROUTE
        Route::get('mosques', ['as' => 'mosques.view', 'uses' => 'MosqueController@getIndex']);
        Route::get('mosques/list', ['as' => 'mosques.list', 'uses' => 'MosqueController@getList']);
        Route::get('mosques/add', ['as' => 'mosques.add', 'uses' => 'MosqueController@getAdd']);
        Route::post('mosques/store', ['as' => 'mosques.store', 'uses' => 'MosqueController@postAdd']);
        Route::get('mosques/show/{id}', ['as' => 'mosques.show', 'uses' => 'MosqueController@getShow']);
        Route::get('mosques/print/{id}', ['as' => 'mosques.print', 'uses' => 'MosqueController@getPrint']);
        Route::get('mosques/edit/{id}', ['as' => 'mosques.edit', 'uses' => 'MosqueController@getEdit']);
        Route::post('mosques/update/{id}', ['as' => 'mosques.update', 'uses' => 'MosqueController@postEdit']);
        Route::get('mosques/delete/{id}', ['as' => 'mosques.delete', 'uses' => 'MosqueController@getDelete']);
        Route::get('mosques/export/', ['as' => 'mosques.export', 'uses' => 'MosqueController@getExport']);

        Route::post('mosques/employees', ['as' => 'mosques.employees', 'uses' => 'MosqueController@syncEmployees']);
        Route::post('mosques/attachments', ['as' => 'mosques.attachments', 'uses' => 'MosqueController@uploadAttachments']);
        Route::post('mosques/details', ['as' => 'mosques.details', 'uses' => 'MosqueController@syncDetails']);
        Route::post('mosques/building', ['as' => 'mosques.building', 'uses' => 'MosqueController@syncBuilding']);
        Route::post('mosques/rebuilding', ['as' => 'mosques.rebuilding', 'uses' => 'MosqueController@syncRebuilding']);


        // MOSQUES NEED REQUESTS ROUTE
        Route::get('mosques-need-requests', ['as' => 'needRequests.view', 'uses' => 'NeedRequestController@getIndex']);
        Route::get('mosques-need-requests/list', ['as' => 'needRequests.list', 'uses' => 'NeedRequestController@getList']);
        Route::get('mosques-need-requests/add', ['as' => 'needRequests.add', 'uses' => 'NeedRequestController@getAdd']);
        Route::post('mosques-need-requests/store', ['as' => 'needRequests.store', 'uses' => 'NeedRequestController@postAdd']);
        Route::get('mosques-need-requests/edit/{id}', ['as' => 'needRequests.edit', 'uses' => 'NeedRequestController@getEdit']);
        Route::post('mosques-need-requests/update/{id}', ['as' => 'needRequests.update', 'uses' => 'NeedRequestController@postEdit']);
        Route::get('mosques-need-requests/delete/{id}', ['as' => 'needRequests.delete', 'uses' => 'NeedRequestController@getDelete']);

        // INSPECTORS ROUTE
        Route::get('inspectors', ['as' => 'inspectors.view', 'uses' => 'InspectorController@getIndex']);
        Route::get('inspectors/list', ['as' => 'inspectors.list', 'uses' => 'InspectorController@getList']);
        Route::get('inspectors/mosques/{id}', ['as' => 'inspectors.mosques', 'uses' => 'InspectorController@getMosques']);
        Route::get('inspectors/mosques/list/{id}', ['as' => 'inspectors.mosques.list', 'uses' => 'InspectorController@getMosquesList']);
        Route::get('inspectors/mosques/{id}/edit', ['as' => 'inspectors.mosques.edit', 'uses' => 'InspectorController@getEdit']);
        Route::post('inspectors/mosques/{id}/update', ['as' => 'inspectors.mosques.update', 'uses' => 'InspectorController@postEdit']);
        Route::get('inspectors/mosques/{id}/delete', ['as' => 'inspectors.mosques.delete', 'uses' => 'InspectorController@getDelete']);


        // INSPECTION SCHEDULES ROUTE
        Route::get('inspection-schedules', ['as' => 'inspectionSchedules.view', 'uses' => 'InspectionScheduleController@getIndex']);
        Route::get('inspection-schedules/list', ['as' => 'inspectionSchedules.list', 'uses' => 'InspectionScheduleController@getList']);
        Route::get('inspection-schedules/add', ['as' => 'inspectionSchedules.add', 'uses' => 'InspectionScheduleController@getAdd']);
        Route::post('inspection-schedules/store', ['as' => 'inspectionSchedules.store', 'uses' => 'InspectionScheduleController@postAdd']);
        Route::get('inspection-schedules/show/{id}', ['as' => 'inspectionSchedules.show', 'uses' => 'InspectionScheduleController@getShow']);
        Route::get('inspection-schedules/edit/{id}', ['as' => 'inspectionSchedules.edit', 'uses' => 'InspectionScheduleController@getEdit']);
        Route::post('inspection-schedules/update/{id}', ['as' => 'inspectionSchedules.update', 'uses' => 'InspectionScheduleController@postEdit']);
        Route::get('inspection-schedules/delete/{id}', ['as' => 'inspectionSchedules.delete', 'uses' => 'InspectionScheduleController@getDelete']);
        Route::get('inspection-schedules/times/{id}/ajax', ['as' => 'inspectionScheduleTimes.ajax', 'uses' => 'InspectionScheduleController@getScheduleTimesAjax']);
        Route::post('inspection-schedules/times/store', ['as' => 'inspectionScheduleTimes.store', 'uses' => 'InspectionScheduleController@postAddTime']);
        Route::get('inspection-schedules/show/{id}', ['as' => 'inspectionSchedules.show', 'uses' => 'InspectionScheduleController@getShow']);
        Route::get('inspection-schedules/approval/{id}', ['as' => 'inspectionSchedules.approval', 'uses' => 'InspectionScheduleController@getApproval']);
        Route::get('inspection-schedules/print/{id}', ['as' => 'inspectionSchedules.print', 'uses' => 'InspectionScheduleController@getPrint']);

        Route::get('inspection-schedule-times/{id}', ['as' => 'inspectionScheduleTimes.view', 'uses' => 'InspectionScheduleController@getIndex']);
        Route::get('inspection-schedule-times/list/{id}', ['as' => 'inspectionScheduleTimes.list', 'uses' => 'InspectionScheduleController@getScheduleTimes']);
        // requiredVisitsByDate >>> return JSON
        Route::get('inspection-schedules/required-visits-by-date', ['as' => 'inspectionSchedules.requiredVisitsByDate', 'uses' => 'InspectionScheduleController@getRequiredVisitsByDate']);

        // Mosque Visits ROUTE
        Route::get('visits', ['as' => 'visits.view', 'uses' => 'MosqueVisitController@getIndex']);
        Route::get('visits/list', ['as' => 'visits.list', 'uses' => 'MosqueVisitController@getList']);
        Route::get('visits/add', ['as' => 'visits.add', 'uses' => 'MosqueVisitController@getAdd']);
        // setuop form visit >>> return html
        Route::get('visits/setup/{id}', ['as' => 'visits.setup', 'uses' => 'MosqueVisitController@getSetup']);
        Route::post('visits/store', ['as' => 'visits.store', 'uses' => 'MosqueVisitController@postAdd']);
        Route::get('visits/show/{id}', ['as' => 'visits.show', 'uses' => 'MosqueVisitController@getShow']);
        Route::get('visits/edit/{id}', ['as' => 'visits.edit', 'uses' => 'MosqueVisitController@getEdit']);
        Route::post('visits/update/{id}', ['as' => 'visits.update', 'uses' => 'MosqueVisitController@postEdit']);
        Route::get('visits/delete/{id}', ['as' => 'visits.delete', 'uses' => 'MosqueVisitController@getDelete']);
        Route::post('visits/approve/{id}', ['as' => 'visits.approve', 'uses' => 'MosqueVisitController@postApproval']);


        // VISIT CHANGE ORDERS ROUTE
        Route::get('visit-change-orders', ['as' => 'visitChangeOrders.view', 'uses' => 'VisitChangeOrderController@getIndex']);
        Route::get('visit-change-orders/list', ['as' => 'visitChangeOrders.list', 'uses' => 'VisitChangeOrderController@getList']);
        Route::get('visit-change-orders/add', ['as' => 'visitChangeOrders.add', 'uses' => 'VisitChangeOrderController@getAdd']);
        Route::post('visit-change-orders/store', ['as' => 'visitChangeOrders.store', 'uses' => 'VisitChangeOrderController@postAdd']);
        Route::get('visit-change-orders/edit/{id}', ['as' => 'visitChangeOrders.edit', 'uses' => 'VisitChangeOrderController@getEdit']);
        Route::post('visit-change-orders/update/{id}', ['as' => 'visitChangeOrders.update', 'uses' => 'VisitChangeOrderController@postEdit']);
        Route::get('visit-change-orders/delete/{id}', ['as' => 'visitChangeOrders.delete', 'uses' => 'VisitChangeOrderController@getDelete']);



        /*
        //Mosque السماجد
        Route::get('mosques/{filterBy?}', 'MosqueController@index')->name('mosque.index');
        Route::get('mosque/getMosqueData/{filterBy?}', 'MosqueController@getMosqueData')->name('mosque.getMosqueData');
        Route::get('mosque/show/{mosque}', 'MosqueController@show')->name('mosque.show');
        Route::get('mosque/print/{mosque}', 'MosqueController@print')->name('mosque.print');
        Route::get('mosque/add', 'MosqueController@create')->name('mosque.add');
        Route::post('mosque/store', 'MosqueController@store')->name('mosque.store');
        Route::get('mosque/edit/{id}', 'MosqueController@edit')->name('mosque.edit');
        Route::post('mosque/update/{id}', 'MosqueController@update')->name('mosque.update');
        Route::post('mosque/delete/{id}', 'MosqueController@destroy')->name('mosque.delete');
        Route::get('mosque/approval/{action}/{id}', 'MosqueController@approval')->name('mosque.approval');
        Route::get('mosque/json-by-region/{regionId}', 'MosqueController@getMosquesByRegion')->name('mosque.getMosquesByRegion');
        // البحث المتقدم
        Route::get('mosque/search', 'MosqueController@search')->name('mosque.search');
        Route::get('mosque/customeSearch', 'MosqueController@customeSearch')->name('mosque.customeSearch');

    */
    });
});

