"use strict";

// Class definition
var KTWizard1 = function () {
	// Base elements
	var wizardEl;
	var formEl;
	var formBasicInfo;
	var validator;
	var wizard;

	// Private functions
	var initWizard = function () {
		// Initialize form wizard
		wizard = new KTWizard('kt_wizard_v1', {
			startStep: 1, // initial active step number
			clickableSteps: true  // allow step clicking
		});

		// Validation before going to next page
		wizard.on('beforeNext', function(wizardObj) {
			if (validator.form() !== true) {
				wizardObj.stop();  // don't go to the next step
			}
			
		});

		wizard.on('beforePrev', function(wizardObj) {
			if (validator.form() !== true) {
				wizardObj.stop();  // don't go to the next step
			}
		});

		// Change event
		wizard.on('change', function(wizard) {
			setTimeout(function() {
				KTUtil.scrollTop();
			}, 500);
		});
	}

	var initValidation = function() {
		validator = formBasicInfo.validate({
			// Validate only visible fields
			ignore: ":hidden",

			// Validation rules
			rules: {
				//= Step 1
				// mosque_id: {
				// 	required: false
				// },
				// name: {
				// 	required: true
				// },
				// region_id: {
				// 	required: true
				// },
				// mosque_space: {
				// 	required: true
				// },

				// type_status: {
				// 	required: true
				// },
				// property_id: {
				// 	required: true
				// },
				// building_type: {
				// 	required: true
				// },
				
				//= Step 2
				package: {
					required: true
				},
				weight: {
					required: true
				},
				width: {
					required: true
				},
				height: {
					required: true
				},
				length: {
					required: true
				},

				//= Step 3
				delivery: {
					required: true
				},
				packaging: {
					required: true
				},
				preferreddelivery: {
					required: true
				},

				//= Step 4
				locaddress1: {
					required: true
				},
				locpostcode: {
					required: true
				},
				loccity: {
					required: true
				},
				locstate: {
					required: true
				},
				loccountry: {
					required: true
				},
			},
			messages: {
				name: {
					required: "اسم المسجد مطلوب"
				},
				region_id: {
					required: "يجب اختيار المنطقة"
				},
				mosque_space: {
					required: "حقل مساحة المسجد مطلوب"
				},
			},

			// Display error
			invalidHandler: function(event, validator) {
				KTUtil.scrollTop();
				/*
				swal.fire({
					"title": "",
					"text": "يرجى إدخال البيانات المطلوبة أولاً",
					"type": "error",
					"confirmButtonClass": "btn btn-secondary"
				});
				*/
			},

			// Submit valid form
			submitHandler: function (form) {

			}
		});
	}

	var initSubmit = function() {
		// var btn = formEl.find('[data-ktwizard-type="action-submit"]');
		// btn.on('click', function(e) {
		// 	e.preventDefault();

		// 	if (validator.form()) {
		// 		// See: src\js\framework\base\app.js
		// 		KTApp.progress(btn);
		// 		//KTApp.block(formEl);

		// 		// See: http://malsup.com/jquery/form/#ajaxSubmit
		// 		formEl.ajaxSubmit({
		// 			success: function() {
		// 				KTApp.unprogress(btn);
		// 				//KTApp.unblock(formEl);

		// 				swal.fire({
		// 					"title": "",
		// 					"text": "تم حفظ البيانات بنجاح!",
		// 					"type": "success",
		// 					"confirmButtonClass": "btn btn-secondary"
		// 				});
		// 			}
		// 		});
		// 	}
		// });

		var btnBasicInfo = formBasicInfo.find('[data-ktwizard-type="action-next"]');
		btnBasicInfo.on('click', function(e) {
			console.log('doaa');
			e.preventDefault();
			if (validator.form()) {
				KTApp.progress(btnBasicInfo);

				// See: http://malsup.com/jquery/form/#ajaxSubmit
				formBasicInfo.ajaxSubmit({
					success: function() {
						KTApp.unprogress(btnBasicInfo);
						swal.fire({
							"title": "",
							"text": "تم حفظ البيانات الاساسية بنجاح!",
							"type": "success",
							"confirmButtonClass": "btn btn-secondary"
						});
					}
				});
			}
		});
	}

	return {
		// public functions
		init: function() {
			wizardEl = KTUtil.get('kt_wizard_v1');
			formEl = $('#kt_form');
			formBasicInfo = $('#kt_form_basic_info');
			initWizard();
			initValidation();
			initSubmit();
		}
	};
}();

jQuery(document).ready(function() {
	KTWizard1.init();
});
