"use strict";

// Class definition
var KTUppy = function () {
	const Tus = Uppy.Tus;
	const ProgressBar = Uppy.ProgressBar;
	const Informer = Uppy.Informer;

	// Private functions
	var initUppy3 = function(){
		var id = '.kt_uppy';

		var uppyDrag = Uppy.Core({ 
			autoProceed: true,
			restrictions: {
				maxFileSize: 1000000, // 1mb
				maxNumberOfFiles: 5,
				minNumberOfFiles: 1,
				allowedFileTypes: ['image/*']
			} 
		});

		uppyDrag.use(Uppy.DragDrop, { target: id + ' .kt-uppy__drag' });  
		uppyDrag.use(ProgressBar, { 
			target: id + ' .kt-uppy__progress',
			hideUploadButton: false,
			hideAfterFinish: false 
		});      
		uppyDrag.use(Informer, { target: id + ' .kt-uppy__informer'  });
		//uppyDrag.use(Tus, { endpoint: 'https://master.tus.io/files/' });

		uppyDrag.on('complete', function(file) {
			var imagePreview = "";
			$.each(file.successful, function(index, value){
				var imageType = /image/;
				var thumbnail = "";
				if (imageType.test(value.type)){
					thumbnail = '<div class="kt-uppy__thumbnail"><img src="'+value.uploadURL+'"/></div>';
				}
				var sizeLabel = "bytes";
				var filesize = value.size;
				if (filesize > 1024){
					filesize = filesize / 1024;
					sizeLabel = "kb";
					if(filesize > 1024){
						filesize = filesize / 1024;
						sizeLabel = "MB";
					}
				}					
				imagePreview += '<div class="kt-uppy__thumbnail-container" data-id="'+value.id+'">'+thumbnail+' <span class="kt-uppy__thumbnail-label">'+value.name+' ('+ Math.round(filesize, 2) +' '+sizeLabel+')</span><span data-id="'+value.id+'" class="kt-uppy__remove-thumbnail"><i class="flaticon2-cancel-music"></i></span></div>';
			});

			$(id + ' .kt-uppy__thumbnails').append(imagePreview);
		});

		$(document).on('click', id + ' .kt-uppy__thumbnails .kt-uppy__remove-thumbnail', function(){
			var imageId = $(this).attr('data-id');
			uppyDrag.removeFile(imageId);					
			$(id + ' .kt-uppy__thumbnail-container[data-id="'+imageId+'"').remove();
		});			
	}

	var initUppy4 = function(){
		var id = '#kt_uppy_4';

		var uppyDrag = Uppy.Core({ 
			autoProceed: false,
			restrictions: {
				maxFileSize: 1000000, // 1mb
				maxNumberOfFiles: 5,
				minNumberOfFiles: 1
			} 
		});
		
		uppyDrag.use(Uppy.DragDrop, { target: id + ' .kt-uppy__drag' });  
		uppyDrag.use(ProgressBar, { target: id + ' .kt-uppy__progress' });      
		uppyDrag.use(Informer, { target: id + ' .kt-uppy__informer'  });
		uppyDrag.use(Tus, { endpoint: 'https://master.tus.io/files/' });

		uppyDrag.on('complete', function(file) {
			var imagePreview = "";
			$.each(file.successful, function(index, value){
				var imageType = /image/;
				var thumbnail = "";
				if (imageType.test(value.type)){
					thumbnail = '<div class="kt-uppy__thumbnail"><img src="'+value.uploadURL+'"/></div>';
				}
				var sizeLabel = "bytes";
				var filesize = value.size;
				if (filesize > 1024){
					filesize = filesize / 1024;
					sizeLabel = "kb";
					if(filesize > 1024){
						filesize = filesize / 1024;
						sizeLabel = "MB";
					}
				}					
				imagePreview += '<div class="kt-uppy__thumbnail-container" data-id="'+value.id+'">'+thumbnail+' <span class="kt-uppy__thumbnail-label">'+value.name+' ('+ Math.round(filesize, 2) +' '+sizeLabel+')</span><span data-id="'+value.id+'" class="kt-uppy__remove-thumbnail"><i class="flaticon2-cancel-music"></i></span></div>';
			});

			$(id + ' .kt-uppy__thumbnails').append(imagePreview);
		});

		var uploadBtn = $(id + ' .kt-uppy__btn');
		uploadBtn.click(function () {
			uppyDrag.upload();
		});

		$(document).on('click', id + ' .kt-uppy__thumbnails .kt-uppy__remove-thumbnail', function(){
			var imageId = $(this).attr('data-id');
			uppyDrag.removeFile(imageId);					
			$(id + ' .kt-uppy__thumbnail-container[data-id="'+imageId+'"').remove();
		});	
	}


	return {
		// public functions
		init: function() {
			initUppy3();
			initUppy4();
		}
	};
}();

KTUtil.ready(function() {	
	KTUppy.init();
});