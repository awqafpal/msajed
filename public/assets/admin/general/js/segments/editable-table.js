
const $tableID = $('#table');
const $buildingBoardTable = $('#buildingBoardTable'); 
const $rebuildingBoardTable = $('#rebuildingBoardTable'); 

/** =================================================================================== */

const newTr = `<tr class="hide">
                <td class="pt-3-half" type="text" contenteditable="true" placeholder="أدخل الاسم كاملاً"  data-name="full_name"></td>
                <td class="pt-3-half" type="text" contenteditable="true" placeholder="أدخل رقم الهوية" data-name="identity_number" maxlength="9" onkeypress="return isNumber(event)"></td>
                <td class="pt-3-half" type="text" contenteditable="true" placeholder="أدخل رقم الجوال" data-name="phone_number" maxlength="10" onkeypress="return isNumber(event)"></td>
                <td class="pt-3-half" type="text" contenteditable="true" placeholder="أدخل وظيفة العضو" data-name="job"></td>
                <td class="pt-3-half" type="text" contenteditable="true" placeholder="أدخل العنوان" data-name="address"></td>
                <td data-name="actions">
                    <span class="table-remove">
                            <button type="button" class="btn btn-danger btn-rounded btn-sm my-0">
                            إزالة
                        </button>
                    </span>
                </td>
                </tr>`;
$('.table-add').on('click', 'button', () => { 
    const $clone = $tableID.find('tbody tr').last().clone(true).removeClass('hide'); 
    if ($tableID.find('tbody tr').length === 0) { 
        $('tbody').append(newTr); 
    } 
    $tableID.find('table').append(newTr); 
});
$tableID.on('click', '.table-remove', function () { 
    $(this).parents('tr').detach(); 
});
/** =================================================================================== */
const rebuildingNewTr = `<tr class="hide">
                <td class="pt-3-half" type="text" contenteditable="true" placeholder="أدخل الاسم كاملاً"  data-name="full_name"></td>
                <td class="pt-3-half" type="text" contenteditable="true" placeholder="أدخل رقم الهوية" data-name="identity_number" maxlength="9" onkeypress="return isNumber(event)"></td>
                <td class="pt-3-half" type="text" contenteditable="true" placeholder="أدخل رقم الجوال" data-name="phone_number" maxlength="10" onkeypress="return isNumber(event)"></td>
                <td class="pt-3-half" type="text" contenteditable="true" placeholder="أدخل وظيفة العضو" data-name="job"></td>
                <td class="pt-3-half" type="text" contenteditable="true" placeholder="أدخل العنوان" data-name="address"></td>
                <td data-name="actions">
                    <span class="rebuilding-board-table-remove">
                            <button type="button" class="btn btn-danger btn-rounded btn-sm my-0">
                            إزالة
                        </button>
                    </span>
                </td>
                </tr>`;
$('.rebuilding-board-table-add').on('click', 'button', () => { 
    const $clone1 = $rebuildingBoardTable.find('tbody tr').last().clone(true).removeClass('hide'); 
    if ($rebuildingBoardTable.find('tbody tr').length === 0) { 
        $('tbody').append(rebuildingNewTr); 
    } 
    $rebuildingBoardTable.find('table').append(rebuildingNewTr); 
});
$rebuildingBoardTable.on('click', '.rebuilding-board-table-remove', function () { 
    console.log('rebuildingBoardTable') ; 
    $(this).parents('tr').detach(); 
});

/** =================================================================================== */
const buildingNewTr = `<tr class="hide">
                <td class="pt-3-half" type="text" contenteditable="true" placeholder="أدخل الاسم كاملاً"  data-name="full_name"></td>
                <td class="pt-3-half" type="text" contenteditable="true" placeholder="أدخل رقم الهوية" data-name="identity_number" maxlength="9" onkeypress="return isNumber(event)"></td>
                <td class="pt-3-half" type="text" contenteditable="true" placeholder="أدخل رقم الجوال" data-name="phone_number" maxlength="10" onkeypress="return isNumber(event)"></td>
                <td class="pt-3-half" type="text" contenteditable="true" placeholder="أدخل وظيفة العضو" data-name="job"></td>
                <td class="pt-3-half" type="text" contenteditable="true" placeholder="أدخل العنوان" data-name="address"></td>
                <td data-name="actions">
                    <span class="building-board-table-remove">
                            <button type="button" class="btn btn-danger btn-rounded btn-sm my-0">
                            إزالة
                        </button>
                    </span>
                </td>
                </tr>`;
$('.building-board-table-add').on('click', 'button', () => { 
    const $clone2 = $buildingBoardTable.find('tbody tr').last().clone(true).removeClass('hide'); 
    if ($buildingBoardTable.find('tbody tr').length === 0) { 
        $('tbody').append(buildingNewTr); 
    } 
    $buildingBoardTable.find('table').append(buildingNewTr); 
});
$buildingBoardTable.on('click', '.building-board-table-remove', function () { 
    console.log('buildingBoardTable') ; 
    $(this).parents('tr').detach(); 
});
/** =================================================================================== */

$("td[contenteditable='true'][maxlength]").on('keydown paste', function (event) {
    var cntMaxLength = parseInt($(this).attr('maxlength'));
    if ($(this).text().length === cntMaxLength && event.keyCode != 8) {
        event.preventDefault();
    }
});

// $("td[contenteditable='true']").on('keydown paste', function (event) {
//     var trIndex = $(this).closest('tr').index();
//     console.log(trIndex);
// });