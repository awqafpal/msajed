


var InspectionScheduleTime = function () {
    var view_tbl;
    var view_url = base_url + prefix + '/inspection-schedule-times/'+schedule_id;
    var list_url = base_url + prefix + '/inspection-schedule-times/list/'+schedule_id;
    var table_id = '#inspection_schedule_times_table';
    /////////////////// View //////////////////
    ///////////////////////////////////////////
    var viewTable = function () {
        var link = list_url;
        var columns = [
            {"data": "index", "title": "#", "orderable": false, "searchable": false, "class" : "no-export"},
            {"data": "day", "orderable": false, "searchable": false},
            {"data": "date", "orderable": false, "searchable": false},
            {"data": "dhuhr-edit", "orderable": false, "searchable": false},
            {"data": "asr", "orderable": false, "searchable": false},
            {"data": "maghrib", "orderable": false, "searchable": false},
            {"data": "isha", "orderable": false, "searchable": false},
        ];
        var perPage = 35;
        var order = [];

        var ajaxFilter = function (d) {
        };

        view_tbl = DataTable.init($(table_id), link, columns, order, ajaxFilter, perPage);
    };

    /////////////////// Render //////////////////
    ///////////////////////////////////////////
    var renderSingle = function (response) {
        let element = response.data ;
        if(element.prayer == 'fajr'){
            var fajr = jQuery.parseJSON(element.fajr);
            (fajr) ? $('#fajr-'+ element.date).html(fajr.name) : $('#fajr-'+ element.date).html('') ;
        }
        if(element.prayer == 'dhuhr'){
            var dhuhr = jQuery.parseJSON(element.dhuhr);
            (dhuhr) ? $('#dhuhr-'+ element.date).html(dhuhr.name) : $('#dhuhr-'+ element.date).html('') ;
        }
        if(element.prayer == 'asr'){
            var asr = jQuery.parseJSON(element.asr);
            (asr) ? $('#asr-'+ element.date).html(asr.name) : $('#asr-'+ element.date).html('') ;
        }
        if(element.prayer == 'maghrib'){
            var maghrib = jQuery.parseJSON(element.maghrib);
            (maghrib) ? $('#maghrib-'+ element.date).html(maghrib.name) : $('#maghrib-'+ element.date).html('') ;
        }
        if(element.prayer == 'isha'){
            var isha = jQuery.parseJSON(element.isha);
            (isha) ? $('#isha-'+ element.date).html(isha.name) : $('#isha-'+ element.date).html('') ;
        }
    }
    var renderMultiple = function (response) {
        let data = response.data ;
        //console.log(response.data);
        $(data).each(function( index, element) {
            if(element.fajr){
                var fajr = jQuery.parseJSON(element.fajr);
                $('#fajr-'+ element.date).html(fajr.name);
            }
            if(element.dhuhr){
                var dhuhr = jQuery.parseJSON(element.dhuhr);
                $('#dhuhr-'+ element.date).html(dhuhr.name);
            }
            if(element.asr){
                var asr = jQuery.parseJSON(element.asr);
                $('#asr-'+ element.date).html(asr.name);
            }
            if(element.maghrib){
                var maghrib = jQuery.parseJSON(element.maghrib);
                $('#maghrib-'+ element.date).html(maghrib.name);
            }
            if(element.isha){
                var isha = jQuery.parseJSON(element.isha);
                $('#isha-'+ element.date).html(isha.name);
            }
        });
    }
    /////////////////// Prayers //////////////////
    ///////////////////////////////////////////
    var times = function () {
        var scheduleId = $('#schedule_id').val();
        //console.log(scheduleId);
        var url = base_url + prefix + '/inspection-schedules/times/'+scheduleId+'/ajax'
        $.ajax({
            url: url,
            dataType:"json",
            type: "get",
            success: function(response){
                renderMultiple(response);
            }
        });
    };
    /////////////////// ADD ///////////////////
    ///////////////////////////////////////////
    var add = function () {
        $('#frmAdd').submit(function(e) {
            e.preventDefault();
            var link = $(this).attr('action');
            var formData = new FormData(this);
            var method = $(this).attr('method');

            Forms.doAction(link, formData, method, null, addCallBack);
        });
    };

    var addCallBack = function (obj) {
        console.log(obj);
        if(obj.code === 200) {
            var delay = 1750;
            setTimeout(function () {
                $('.modal').trigger("click");
                //Forms.notify(obj.title, obj.code, obj.message);
                renderSingle(obj);
            }, delay);
        }
    };

    /////////////////// APPROVAL ///////////////////
    ///////////////////////////////////////////
    var approval = function () {
        $(document).on('click', '.approve_schedule', function (e) {
            e.preventDefault();
            var btn = $(this);

            Common.approval(function() {
                var link = btn.data('url');
                var formData = {};
                var method = "GET";

                Forms.doAction(link, formData, method, null, approvalCallBack);
            });
        });
    };
    var approvalCallBack = function (obj) {
        if(obj.code === 200) {
            var delay = 1750;
            setTimeout(function () {
                $('.hideWhenApprove').hide();
            }, delay);
        }
    };
    /////////////////// EDIT //////////////////
    ///////////////////////////////////////////
    var edit = function () {
        $('#frmEdit').submit(function(e) {
            e.preventDefault();
            var link = $(this).attr('action');
            var formData = new FormData(this);
            var method = $(this).attr('method');
            Forms.doAction(link, formData, method, null, editCallBack);
        });
    };

    var editCallBack = function (obj) {
        if(obj.code === 200) {
            var delay = 1750;

            setTimeout(function () {
                window.location = view_url;
            }, delay);
        }
    };

    //////////////// DELETE ///////////////////
    ///////////////////////////////////////////
    var deleteItem = function () {
        $(document).on('click', '.delete_btn', function (e) {
            e.preventDefault();
            var btn = $(this);

            Common.confirm(function() {
                var link = btn.data('url');
                var formData = {};
                var method = "GET";

                Forms.doAction(link, formData, method, view_tbl);
            });
        });
    };

    ///////////////// INITIALIZE //////////////
    ///////////////////////////////////////////
    return {
        init: function () {
            //viewTable();
            times();
            add();
            approval();
            edit();
            deleteItem();
            //search();
        }
    }
}();

$(document).ready(function() {
    InspectionScheduleTime.init();
});
