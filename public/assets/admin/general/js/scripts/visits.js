


var MosqueVisit = function () {
    var view_tbl;
    var view_url = base_url + prefix + '/visits';
    var list_url = base_url + prefix + '/visits/list';
    var table_id = '#visits_table';
    /////////////////// View //////////////////
    ///////////////////////////////////////////
    var viewTable = function () {
        var link = list_url;
        var columns = [
            {"data": "index", "title": "#", "orderable": false, "searchable": false},
            {"data": "date", "orderable": true, "searchable": false},
            {"data": "mosque-name", "orderable": true, "searchable": false},
            {"data": "province-name", "orderable": true, "searchable": false},
            {"data": "visitor-name", "orderable": false, "searchable": false},
            {"data": "status", "orderable": false, "searchable": false},
            {"data": "actions", "orderable": false, "searchable": false, "class" : "text-center"}
        ];
        var perPage = 25;
        var order = [[1, 'desc']];

        var ajaxFilter = function (d) {
            d.name = $('#name').val();
            d.identity_number = $('#identity_number').val();
            d.phone_number = $('#phone_number').val();
        };

        view_tbl = DataTable.init($(table_id), link, columns, order, ajaxFilter, perPage);
    };
    /////////////////// ADD ///////////////////
    ///////////////////////////////////////////
    var add = function () {
        $('#frmAdd').submit(function(e) {
            e.preventDefault();
            var link = $(this).attr('action');
            var formData = new FormData(this);
            var method = $(this).attr('method');

            Forms.doAction(link, formData, method, null, addCallBack);
        });
    };

    var addCallBack = function (obj) {
        if(obj.code === 200) {
            var delay = 1750;

            setTimeout(function () {
                window.location = view_url;
            }, delay);
        }
    };
    /////////////////// EDIT //////////////////
    ///////////////////////////////////////////
    var edit = function () {
        $('#frmEdit').submit(function(e) {
            e.preventDefault();
            var link = $(this).attr('action');
            var formData = new FormData(this);
            var method = $(this).attr('method');
            Forms.doAction(link, formData, method, null, editCallBack);
        });
    };

    var editCallBack = function (obj) {
        if(obj.code === 200) {
            var delay = 1750;

            setTimeout(function () {
                window.location = view_url;
            }, delay);
        }
    };

    //////////////// DELETE ///////////////////
    ///////////////////////////////////////////
    var deleteItem = function () {
        $(document).on('click', '.delete_btn', function (e) {
            e.preventDefault();
            var btn = $(this);

            Common.confirm(function() {
                var link = btn.data('url');
                var formData = {};
                var method = "GET";

                Forms.doAction(link, formData, method, view_tbl);
            });
        });
    };

    //////////////// Get Mosques ///////////////
    ///////////////////////////////////////////
    var getMosques = function () {
        $(document).on('change', '.get-mosques', function (e) {
            e.preventDefault();
            $('#form-elements').html('');
            $('#save-btn').attr('disabled' ,true);

            var date = $('#visit_date').val();
            var user = $('#user').val();
            //alert(user);  // correct line

            if(! user){
                Forms.notify('خطأ !',500, 'يجب اختيار اسم المفتش أولاً');
            }
            else {
                $.ajax({
                    url: base_url + prefix + '/inspection-schedules/required-visits-by-date',
                    type: "GET",
                    data: {
                        date: date,
                        user: user,
                    },
                    success: function(data) {
                        console.log(data);
                        var mosques = data.data ;
                        if(mosques.length == 0){
                            Forms.notify(data.title,data.code, data.message);
                            $('#mosques').empty();
                            $('#mosques').attr('disabled',true);
                        }
                        else{
                            $('#mosques').empty().append('<option selected disabled >اختر المسجد ...</option>');
                            $.each(mosques, function(key, mosque) {
                                $('#mosques')
                                    .append($("<option></option>")
                                        .attr("value", mosque.id)
                                        .text(mosque.name));
                            });
                            $('#mosques').attr('disabled',false).selectpicker("refresh");
                        }
                    },
                    error: function(data) {
                        var response = data.responseJSON ;
                        Forms.notify('خطأ !',500, 'خلل أثناء تنفيذ العمليةً');
                        $('#mosques').attr('disabled',true);
                    },
                });
            }
        });
    };
    //////////////// Search ///////////////////
    ///////////////////////////////////////////
    var search = function () {
        $('.searchable').on('input change', function (e) {
            e.preventDefault();
            view_tbl.draw(false);
        });

        $('.search').on('click', function (e) {
            e.preventDefault();
            view_tbl.draw(false);
        });

        $('#frmSearch').keydown(function (e) {
            if (e.keyCode == 13) {
                e.preventDefault();
                view_tbl.draw(false);
            }
        });
    };
    //////////////// Setup Visit Form ///////////////////
    ///////////////////////////////////////////
    var setup = function () {
        $( "#mosques" ).change(function() {
            var mosque_id = $('#mosques').val();
            var date = $('#visit_date').val();
            // alert(mosque_id);
            var url = base_url + prefix + '/visits/setup/'+mosque_id;
            $.ajax({
                url: url,
                type: "GET",
                success: function(response) {
                    $('#form-elements').html('');
                    $('#form-elements').append(response);
                    $('#save-btn').attr('disabled' ,false);
                },
                error: function(data) {

                },
            });
        });
    };

    //////////////// Show Modal ///////////////////
    ///////////////////////////////////////////
    var modal = function(){
        // Execute something when the modal window is shown.
        $('#approve-visit').on('show.bs.modal', function (event) {
            /* Clear DATA Froml MODAL */
            document.getElementById("ApproveForm").reset();
            $('.approve-btn').html('اعتماد');
            $('#note').html('');

            var button = $(event.relatedTarget); // Button that triggered the modal
            let visitID = button.data('id'); // Extract info from data-* attributes

            console.log(visitID);
            /* SET DATA TO MODAL */
            $('#visit_id').val(visitID);
        });

    }
    /////////////////// Approval ///////////////////
    ///////////////////////////////////////////
    var approval = function () {
        $('#ApproveForm').submit(function(e) {
            e.preventDefault();
            var link = $(this).attr('action');
            var formData = new FormData(this);
            var method = $(this).attr('method');

            Forms.doAction(link, formData, method, null, approvalCallBack);
        });
    };

    var approvalCallBack = function (obj) {
        if(obj.code === 200) {
            var delay = 750;
            setTimeout(function () {
                $('.modal').trigger("click");
                $('#flag').html('');
                    $('#flag').append('<span  class="btn btn-success btn-elevate btn-icon-sm"  aria-haspopup="true" aria-expanded="false" style="cursor: default">' +
                        '<i class="fa fa-check-circle"></i>' +
                        'معتمد' +
                        '</span>');
            }, delay);
        }
    };
    //////////////// Direct Approval ///////////
    ///////////////////////////////////////////
    var directApproval = function(){
        $(document).on('click', '#directApproving', function (e) {
            e.preventDefault();
            var btn = $(this);

            var loader = $('.direct-approve-btn');
            var approving = "اعتماد";
            var sending = "جاري اعتماد الزيارة ... ";
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                }
            });
            $.ajax({
                type: "POST",
                url: btn.data('url'),
                data: {
                    id: btn.data('id')
                },
                beforeSend: function() {
                    btn.attr('disabled', true);
                    loader.html("<i class='fa fa-spinner fa-spin fa-lg'></i> " + sending);
                },
                success: function(response){
                    var data = response ;
                    Forms.notify(data.title,data.code, data.message);
                    loader.html(approving);
                    btn.attr('disabled', false);
                    $('#flag').html('');
                    $('#flag').append('<span  class="btn btn-success btn-elevate btn-icon-sm"  aria-haspopup="true" aria-expanded="false" style="cursor: default">' +
                        '<i class="fa fa-check-circle"></i>' +
                        'معتمد' +
                        '</span>');
                },
                error: function(jqXHR) {
                    Forms.notify('خطأ !',500, 'خلل أثناء اعتماد الزيارة');
                    loader.html(approving);
                    btn.attr('disabled', false);
                },
            });
        });
    }

    ///////////////// INITIALIZE //////////////
    ///////////////////////////////////////////
    return {
        init: function () {
            viewTable();
            add();
            edit();
            deleteItem();
            search();
            getMosques();
            setup();
            modal();
            approval();
            directApproval();
        }
    }
}();

$(document).ready(function() {
    MosqueVisit.init();
});
