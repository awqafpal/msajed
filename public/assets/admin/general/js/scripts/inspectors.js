


var Inspectors = function () {

    var view_tbl;
    var view_url = base_url + prefix + '/inspectors';
    var list_url = base_url + prefix + '/inspectors/list';
    var table_id = '#inspectors_table';
    /////////////////// View //////////////////
    ///////////////////////////////////////////
    var viewTable = function () {
        var link = list_url;
        var columns = [
            {"data": "index", "title": "#", "orderable": false, "searchable": false},
            {"data": "image", "orderable": true, "searchable": true},
            {"data": "username", "orderable": true, "searchable": true},
            {"data": "name", "orderable": true, "searchable": true},
            {"data": "mosques", "orderable": false, "searchable": false},
            {"data": "province-name", "orderable": false, "searchable": true},
            {"data": "status", "orderable": false, "searchable": false},
            {"data": "actions", "orderable": false, "searchable": false, "class" : "text-center"}
        ];
        var perPage = 25;
        var order = [[1, 'desc']];

        var ajaxFilter = function (d) {
            d.username = $('#username').val();
            d.name = $('#name').val();
            d.status = $('#status').val();
        };

        view_tbl = DataTable.init($(table_id), link, columns, order, ajaxFilter, perPage);
    };
    //////////////// Search ///////////////////
    ///////////////////////////////////////////
    var search = function () {
        $('.searchable').on('input change', function (e) {
            e.preventDefault();
            view_tbl.draw(false);
        });

        $('.search').on('click', function (e) {
            e.preventDefault();
            view_tbl.draw(false);
        });

        $('#frmSearch').keydown(function (e) {
            if (e.keyCode == 13) {
                e.preventDefault();
                view_tbl.draw(false);
            }
        });
    };

    ///////////////// INITIALIZE //////////////
    ///////////////////////////////////////////
    return {
        init: function () {
            viewTable();
            search();
        }
    }
}();

$(document).ready(function() {
    Inspectors.init();
});
