


var Mosque = function () {
    var view_tbl;
    var view_url = base_url + prefix + '/mosques';
    var list_url = base_url + prefix + '/mosques/list';
    var table_id = '#mosques_table';
    /////////////////// View //////////////////
    ///////////////////////////////////////////
    var viewTable = function () { 
        var link = list_url;
        var columns = [
            {"data": "index", "title": "#", "orderable": false, "searchable": false},
            {"data": "image", "orderable": false, "searchable": false},
            {"data": "name", "orderable": false, "searchable": false},
            {"data": "province-name", "orderable": false, "searchable": false},
            {"data": "archaeological_status", "orderable": false, "searchable": false},
            {"data": "central_status", "orderable": false, "searchable": false},
            {"data": "actions", "orderable": false, "searchable": false, "class" : "text-center"}
        ];
        var perPage = 25;
        var order = [[1, 'desc']];

        var ajaxFilter = function (d) {
            d.name = $('#name').val();
        };

        view_tbl = DataTable.init($(table_id), link, columns, order, ajaxFilter, perPage);
    };
    /////////////////// ADD ///////////////////
    ///////////////////////////////////////////
    var add = function () {
        $('#kt_form_basic_info').submit(function(e) {
            e.preventDefault();
            var link = $(this).attr('action');
            var formData = new FormData(this);
            var method = $(this).attr('method');

            Forms.doAction(link, formData, method, null, addCallBack);
        });
    };

    var addCallBack = function (obj) {
        if(obj.code === 200) {
            var mosqueId = obj.data.id;
            console.log(obj);
            if(mosqueId){
                $('.mosque_id').val(mosqueId);
            }
            /*
            var delay = 1750;
            setTimeout(function () {
                window.location = view_url;
            }, delay);
            */
        }
    };

    var syncEmployees = function () {
        $('#kt_form_mosque_employees').submit(function(e) {
            e.preventDefault();
            var link = $(this).attr('action');
            var formData = new FormData(this);
            var method = $(this).attr('method');

            Forms.doAction(link, formData, method, null, addCallBack);
        });
    };
    /////////////////// EDIT //////////////////
    ///////////////////////////////////////////
    var edit = function () {
        $('#kt_form_basic_info').submit(function(e) {
            e.preventDefault();
            var link = $(this).attr('action');
            var formData = new FormData(this);
            var method = $(this).attr('method');
            Forms.doAction(link, formData, method, null, editCallBack);
        });
    };

    var editCallBack = function (obj) {
        if(obj.code === 200) {
            var mosqueId = obj.data.id;
            console.log(obj);
            if(mosqueId){
                $('.mosque_id').val(mosqueId);
            }
            // var delay = 1750;

            // setTimeout(function () {
            //     window.location = view_url;
            // }, delay);
        }
    };

    //////////////// DELETE ///////////////////
    ///////////////////////////////////////////
    var deleteItem = function () {
        $(document).on('click', '.delete_btn', function (e) {
            e.preventDefault();
            var btn = $(this);

            Common.confirm(function() {
                var link = btn.data('url');
                var formData = {};
                var method = "GET";

                Forms.doAction(link, formData, method, view_tbl);
            });
        });
    };
    //////////////// Search ///////////////////
    ///////////////////////////////////////////
    var search = function () {
        $('.searchable').on('input change', function (e) {
            e.preventDefault();
            view_tbl.draw(false);
        });

        $('.search').on('click', function (e) {
            e.preventDefault();
            view_tbl.draw(false);
        });

        $('#frmSearch').keydown(function (e) {
            if (e.keyCode == 13) {
                e.preventDefault();
                view_tbl.draw(false);
            }
        });
    };
    //////////////// Get all regions of province ///////////////////
    ///////////////////////////////////////////
    var regions = function () {
        $( "#province" ).change(function() {
            var provinceId = $("#province").val();
            // var selectedRegion = $(".mosque_id").data('region');
            // console.log(selectedRegion);
            var url = base_url + prefix + '/provinces/'+provinceId+'/regions/ajax'
            $.ajax({
                url: url,
                dataType:"html",
                type: "get",
                success: function(data){
                    //console.log(data);
                    $('#regions').empty().append(data).selectpicker("refresh");
                    
                }
            });
        });
    };
    //////////////// Get all employees of region ///////////////////
    ///////////////////////////////////////////
    var employees = function () {
        $("#regions").change(function() {
            var regionId = $("#regions").val();
            var url = base_url + prefix + '/regions/'+regionId+'/employees/ajax'
            $.ajax({
                url: url,
                dataType:"html",
                type: "get",
                success: function(data){
                    //console.log(data);
                    $('#employees').empty().append(data).selectpicker("refresh");
                }
            });
        });
    };

    //////////////// Sync details of mosque ///////////////////
    ///////////////////////////////////////////
    var details = function () {
        $('#kt_form_mosque_details').submit(function(e) {
            e.preventDefault();
            getDataFromTable('table','administrativeBoard');
            var link = $(this).attr('action');
            var formData = new FormData(this);
            var method = $(this).attr('method');
            //console.log($('#administrativeBoard').val());
            Forms.doAction(link, formData, method, null, addCallBack);
        });
    }; 

    //////////////// Sync building of mosque ///////////////////
    ///////////////////////////////////////////
    var building = function () {
        $('#kt_form_mosque_building').submit(function(e) {
            e.preventDefault(); 
            getDataFromTable('buildingBoardTable','buildingBoard');
            var link = $(this).attr('action');
            var formData = new FormData(this);
            var method = $(this).attr('method');
            Forms.doAction(link, formData, method, null, addCallBack);
        });
    };

    //////////////// Sync rebuilding of mosque ///////////////////
    ///////////////////////////////////////////
    var rebuilding = function () {
        $('#kt_form_mosque_rebuilding').submit(function(e) {
            e.preventDefault();
            getDataFromTable('rebuildingBoardTable','rebuildingBoard');
            var link = $(this).attr('action');
            var formData = new FormData(this);
            var method = $(this).attr('method');
            Forms.doAction(link, formData, method, null, addCallBack);
        });
    };

    //////////////// dropzone * attachments ///////////////////
    ///////////////////////////////////////////
    var dropzone = function () {
        var csrf = $('meta[name="csrf-token"]').attr('content') ;
        var origin_url = window.location.origin;
        var url  = origin_url + 'admin/mosques/attachments' ;
        // console.log(url);
        $('.dropzone').dropzone({
            url: url, // Set the url for your upload script location
            method:"post",
            headers:{'X-CSRF-TOKEN': csrf},
            uploadMultiple:true,
            paramName: "file", // The name that will be used to transfer the file
            maxFiles: 10,
            maxFilesize: 5, // MB
            addRemoveLinks: false,
            accept: function(file, done) {
                if (file.name == "justinbieber.jpg") {
                    done("Naha, you don't.");
                } else {
                    done();
                }
            },
            sending: function(file, xhr, formData){
                formData.append('mosque_id', $('.mosque_id').val());
                formData.append('attachment_type_id', $(this)[0].element.dataset.type);
            },
            success: function(file, response){
                // console.log('dastal');
                // console.log(response);
                Forms.notify(response.title,response.code, response.message);
            }
        });
    };

    //////////////// general scripts of mosque ///////////////////
    ///////////////////////////////////////////
    var general = function () {
        $.viewMap = {0: $([]), view2: $(".view2"),  view3: $(".view3"),};
        $("#rebuilding_reason").change(function () {
            $.each($.viewMap, function () {
                this.addClass('d-none');
            }); 
            $.viewMap[$(this).find(':selected').data('name')].removeClass('d-none')
        });
    };


    var getDataFromTable = function (tableId , arrayInputId ) {
        //console.log('Good Night!');
        var data = Array();
        $("#"+tableId+" tbody tr").each(function(i, v){
            data[i] = {};
            $(this).children('td').each(function(ii, vv){
                if($(this).data('name') != 'actions'){
                    data[i][$(this).data('name')] = $(this).text();
                }

            });
        })
        $("#"+arrayInputId).val(JSON.stringify(data));
    }
    
    ///////////////// INITIALIZE //////////////
    ///////////////////////////////////////////
    return {
        init: function () {
            viewTable();
            add();
            //edit();
            deleteItem();
            search();
            regions();
            employees();
            syncEmployees();
            details();
            building();
            rebuilding();
            //dropzone();
            general();
        }
    }
}();

$(document).ready(function() {
    Mosque.init();
});
