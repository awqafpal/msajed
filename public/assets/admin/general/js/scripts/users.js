


var Users = function () {

    var view_tbl;
    var view_url = base_url + prefix + '/users';
    var list_url = base_url + prefix + '/users/list';
    var table_id = '#users_table';
    /////////////////// View //////////////////
    ///////////////////////////////////////////
    var viewTable = function () {
        var link = list_url;
        var columns = [
            {"data": "index", "title": "#", "orderable": false, "searchable": false},
            {"data": "image", "orderable": true, "searchable": true},
            {"data": "username", "orderable": true, "searchable": true},
            {"data": "name", "orderable": true, "searchable": true},
            {"data": "user-type", "orderable": false, "searchable": false},
            {"data": "province-name", "orderable": false, "searchable": true},
            {"data": "status", "orderable": false, "searchable": false},
            {"data": "actions", "orderable": false, "searchable": false, "class" : "text-center"}
        ];
        var perPage = 25;
        var order = [[1, 'desc']];

        var ajaxFilter = function (d) {
            d.username = $('#username').val();
            d.name = $('#name').val();
            d.email = $('#email').val();
            d.status = $('#status').val();
        };

        view_tbl = DataTable.init($(table_id), link, columns, order, ajaxFilter, perPage);
    };
    /////////////////// ADD ///////////////////
    ///////////////////////////////////////////
    var add = function () {
        $('#frmAdd').submit(function(e) {
            e.preventDefault();
            var link = $(this).attr('action');
            var formData = new FormData(this);
            var method = $(this).attr('method'); 

            Forms.doAction(link, formData, method, null, addCallBack);
        });
    };

    var addCallBack = function (obj) {
        if(obj.code === 200) {
            var delay = 1750;

            setTimeout(function () {
                window.location = view_url;
            }, delay);
        }
    };
    /////////////////// EDIT //////////////////
    ///////////////////////////////////////////
    var edit = function () {
        $('#frmEdit').submit(function(e) {
            e.preventDefault();
            var link = $(this).attr('action');
            var formData = new FormData(this);
            var method = $(this).attr('method');
            Forms.doAction(link, formData, method, null, editCallBack);
        });
    };

    var editCallBack = function (obj) {
        if(obj.code === 200) {
            var delay = 1750;

            setTimeout(function () {
                window.location = view_url;
            }, delay);
        }
    };
    /////////// Change Password ///////////////
    ///////////////////////////////////////////
    var changePassword = function () {
        $('#frmChangePassword').submit(function(e) {
            e.preventDefault();
            var link = $(this).attr('action');
            var formData = $(this).serialize();
            var method = $(this).attr('method');
            Forms.doAction(link, formData, method, null, changePasswordCallBack);
        });
    };

    var changePasswordCallBack = function (obj) {
        if(obj.code === 200) {
            var delay = 1750;

            setTimeout(function () {
                window.location = view_url;
            }, delay);
        }
    };
    /////////// Role Permissions ///////////////
    ///////////////////////////////////////////
    var permissions = function () {
        $('#frmPermissions').submit(function(e) {
            e.preventDefault();
            var link = $(this).attr('action');
            var formData = $(this).serialize();
            var method = $(this).attr('method');
            Forms.doAction(link, formData, method, null, permissionsCallBack);
        });
    };

    var permissionsCallBack = function (obj) {
        if(obj.code === 200) {
            var delay = 1750;

            setTimeout(function () {
                window.location = view_url;
            }, delay);
        }
    };
    //////////////// DELETE ///////////////////
    ///////////////////////////////////////////
    var deleteItem = function () {
        $(document).on('click', '.delete_btn', function (e) {
            e.preventDefault();
            var btn = $(this);

            Common.confirm(function() {
                var link = btn.data('url');
                var formData = {};
                var method = "GET";

                Forms.doAction(link, formData, method, view_tbl);
            });
        });
    };
    //////////////// Search ///////////////////
    ///////////////////////////////////////////
    var search = function () {
        $('.searchable').on('input change', function (e) {
            e.preventDefault();
            view_tbl.draw(false);
        });

        $('.search').on('click', function (e) {
            e.preventDefault();
            view_tbl.draw(false);
        });

        $('#frmSearch').keydown(function (e) {
            if (e.keyCode == 13) {
                e.preventDefault();
                view_tbl.draw(false);
            }
        });
    };

    //////////////// Get all regions of province ///////////////////
    ///////////////////////////////////////////
    var regions = function () {
        $( "#province" ).change(function() {
            var provinceId = $("#province").val();
            var url = base_url + prefix + '/provinces/'+provinceId+'/regions/ajax'
            $.ajax({
                url: url,
                dataType:"html",
                type: "get",
                success: function(data){
                    //console.log(data);
                    $('#regions').empty().append(data).selectpicker("refresh");
                    
                }
            });
        });
    };
     
    ///////////////// INITIALIZE //////////////
    ///////////////////////////////////////////
    return {
        init: function () {
            viewTable();
            add();
            edit();
            changePassword();
            permissions();
            deleteItem();
            search();
            regions(); 
        }
    }
}();

$(document).ready(function() {
    Users.init();
});
