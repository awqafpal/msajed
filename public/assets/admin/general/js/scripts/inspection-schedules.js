


var InspectionSchedule = function () {
    var view_tbl;
    var view_url = base_url + prefix + '/inspection-schedules';
    var list_url = base_url + prefix + '/inspection-schedules/list';
    var table_id = '#inspection_schedules_table';
    /////////////////// View //////////////////
    ///////////////////////////////////////////
    var viewTable = function () {
        var link = list_url;
        var columns = [
            {"data": "index", "title": "#", "orderable": false, "searchable": false},
            {"data": "month_year", "orderable": false, "searchable": false},
            {"data": "user", "orderable": false, "searchable": false},
            {"data": "inspector_approval", "orderable": false, "searchable": false},
            {"data": "department_head_approval", "orderable": false, "searchable": false},
            {"data": "province_manager_approval", "orderable": false, "searchable": false},
            {"data": "actions", "orderable": false, "searchable": false, "class" : "text-center"}
        ];
        var perPage = 25;
        var order = [[1, 'desc']];

        var ajaxFilter = function (d) {
            d.title = $('#title').val();
        };

        view_tbl = DataTable.init($(table_id), link, columns, order, ajaxFilter, perPage);
    };
    /////////////////// ADD ///////////////////
    ///////////////////////////////////////////
    var add = function () {
        $('#frmAdd').submit(function(e) {
            e.preventDefault();
            var link = $(this).attr('action');
            var formData = new FormData(this);
            var method = $(this).attr('method');

            Forms.doAction(link, formData, method, null, addCallBack);
        });
    };

    var addCallBack = function (obj) {
        console.log(obj);
        if(obj.code === 200) {
            var delay = 1750;
            setTimeout(function () {
                window.location = base_url + prefix + '/inspection-schedules/edit/' + obj.data.id ;
            }, delay);
        }
    };
    /////////////////// EDIT //////////////////
    ///////////////////////////////////////////
    var edit = function () {
        $('#frmEdit').submit(function(e) {
            e.preventDefault();
            var link = $(this).attr('action');
            var formData = new FormData(this);
            var method = $(this).attr('method');
            Forms.doAction(link, formData, method, null, editCallBack);
        });
    };

    var editCallBack = function (obj) {
        if(obj.code === 200) {
            var delay = 1750;

            setTimeout(function () {
                window.location = view_url;
            }, delay);
        }
    };

    //////////////// DELETE ///////////////////
    ///////////////////////////////////////////
    var deleteItem = function () {
        $(document).on('click', '.delete_btn', function (e) {
            e.preventDefault();
            var btn = $(this);

            Common.confirm(function() {
                var link = btn.data('url');
                var formData = {};
                var method = "GET";

                Forms.doAction(link, formData, method, view_tbl);
            });
        });
    };
    //////////////// Search ///////////////////
    ///////////////////////////////////////////
    var search = function () {
        $('.searchable').on('input change', function (e) {
            e.preventDefault();
            view_tbl.draw(false);
        });

        $('.search').on('click', function (e) {
            e.preventDefault();
            view_tbl.draw(false);
        });

        $('#frmSearch').keydown(function (e) {
            if (e.keyCode == 13) {
                e.preventDefault();
                view_tbl.draw(false);
            }
        });
    };


    //////////////// Schedule Times ////////////
    ///////////////////////////////////////////
    var schedule_url = window.location.pathname;
    var schedule_id = schedule_url.substring(schedule_url.lastIndexOf('/') + 1);

    var view_schedule_tbl;
    var view_schedule_url = base_url + prefix + '/inspection-schedule-times/'+schedule_id;
    var list_schedule_url = base_url + prefix + '/inspection-schedule-times/list/'+schedule_id;
    var table_schedule_id = '#inspection_schedule_times_table';

    var viewScheduleTable = function () {
        var linkURL = list_schedule_url;
        var datatable_columns = [
            {"data": "index", "title": "#", "orderable": false, "searchable": false, "class" : "no-export"},
            {"data": "day", "orderable": false, "searchable": false},
            {"data": "date", "orderable": false, "searchable": false},
            {"data": "dhuhr-edit", "orderable": false, "searchable": false},
            {"data": "asr", "orderable": false, "searchable": false},
            {"data": "maghrib", "orderable": false, "searchable": false},
            {"data": "isha", "orderable": false, "searchable": false},
        ];

        var perPage = 35;
        var order_page = [];
        var ajaxFiltering = function (d) {
           // d.title = $('#title').val();
        };

        view_schedule_tbl = DataTable.init($(table_schedule_id), linkURL, datatable_columns, order_page, ajaxFiltering, perPage);
    };

    ///////////////// INITIALIZE //////////////
    ///////////////////////////////////////////
    return {
        init: function () {
            viewTable();
            add();
            edit();
            deleteItem();
            search();
            viewScheduleTable();
        }
    }
}();

$(document).ready(function() {
    InspectionSchedule.init();
});
