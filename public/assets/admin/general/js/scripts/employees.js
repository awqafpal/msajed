


var Employee = function () {
    var view_tbl;
    var view_url = base_url + prefix + '/employees';
    var list_url = base_url + prefix + '/employees/list';
    var table_id = '#employees_table';
    /////////////////// View //////////////////
    ///////////////////////////////////////////
    var viewTable = function () {
        var link = list_url;
        var columns = [
            {"data": "index", "title": "#", "orderable": false, "searchable": false},
            {"data": "image", "orderable": false, "searchable": false},
            {"data": "name", "orderable": true, "searchable": false},
            {"data": "identity_number", "orderable": true, "searchable": false},
            {"data": "phone_number", "orderable": true, "searchable": false},
            {"data": "province-name", "orderable": true, "searchable": false},
            {"data": "job-name", "orderable": false, "searchable": false},
            {"data": "mosque-name", "orderable": false, "searchable": false},
            {"data": "actions", "orderable": false, "searchable": false, "class" : "text-center"}
        ];
        var perPage = 25;
        var order = [[1, 'desc']];

        var ajaxFilter = function (d) {
            d.name = $('#name').val();
            d.identity_number = $('#identity_number').val();
            d.phone_number = $('#phone_number').val();
        };

        view_tbl = DataTable.init($(table_id), link, columns, order, ajaxFilter, perPage);
    };
    /////////////////// ADD ///////////////////
    ///////////////////////////////////////////
    var add = function () {
        $('#frmAdd').submit(function(e) {
            e.preventDefault();
            var link = $(this).attr('action');
            var formData = new FormData(this);
            var method = $(this).attr('method');

            Forms.doAction(link, formData, method, null, addCallBack);
        });
    };

    var addCallBack = function (obj) {
        if(obj.code === 200) {
            var delay = 1750;

            setTimeout(function () {
                window.location = view_url;
            }, delay);
        }
    };
    /////////////////// EDIT //////////////////
    ///////////////////////////////////////////
    var edit = function () {
        $('#frmEdit').submit(function(e) {
            e.preventDefault();
            var link = $(this).attr('action');
            var formData = new FormData(this);
            var method = $(this).attr('method');
            Forms.doAction(link, formData, method, null, editCallBack);
        });
    };

    var editCallBack = function (obj) {
        if(obj.code === 200) {
            var delay = 1750;

            setTimeout(function () {
                window.location = view_url;
            }, delay);
        }
    };

    //////////////// DELETE ///////////////////
    ///////////////////////////////////////////
    var deleteItem = function () {
        $(document).on('click', '.delete_btn', function (e) {
            e.preventDefault();
            var btn = $(this);

            Common.confirm(function() {
                var link = btn.data('url');
                var formData = {};
                var method = "GET";

                Forms.doAction(link, formData, method, view_tbl);
            });
        });
    };
    //////////////// Search ///////////////////
    ///////////////////////////////////////////
    var search = function () {
        $('.searchable').on('input change', function (e) {
            e.preventDefault();
            view_tbl.draw(false);
        });

        $('.search').on('click', function (e) {
            e.preventDefault();
            view_tbl.draw(false);
        });

        $('#frmSearch').keydown(function (e) {
            if (e.keyCode == 13) {
                e.preventDefault();
                view_tbl.draw(false);
            }
        });
    };
    //////////////// Get all regions of province ///////////////////
    ///////////////////////////////////////////
    var regions = function () {
        $( "#province" ).change(function() {
            var provinceId = $("#province").val();
            var url = base_url + prefix + '/provinces/'+provinceId+'/regions/ajax'
            $.ajax({
                url: url,
                dataType:"html",
                type: "get",
                success: function(data){
                    //console.log(data);
                    $('#regions').empty().append(data).selectpicker("refresh");

                }
            });
        });
    }; 

    //////////////// Get all mosques of region ///////////////////
    ///////////////////////////////////////////
    var mosques = function () {
        $( "#regions" ).change(function() {
            var regionId = $("#regions").val();
            var url = base_url + prefix + '/regions/'+regionId+'/mosques/ajax'
            $.ajax({
                url: url,
                dataType:"html",
                type: "get",
                success: function(data){
                    //console.log(data);
                    $('#mosques').empty().append(data).selectpicker("refresh");

                }
            });
        });
    };
    ///////////////// INITIALIZE //////////////
    ///////////////////////////////////////////
    return {
        init: function () {
            viewTable();
            add();
            edit();
            deleteItem();
            search();
            regions();
            mosques();
        }
    }
}();

$(document).ready(function() {
    Employee.init();
});
