@extends('admin.layout.master')

@section('title')
طلبات تغيير مواعيد زيارات المساجد
@stop

@section('css')

@stop
@section('subheader')
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    وزارة الأوقاف والشؤون الدينية </h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="{{ route('admin.dashboard.view') }}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="{{ route('admin.dashboard.view') }}" class="kt-subheader__breadcrumbs-link">
                        الرئيسية
                    </a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="{{ route('admin.visitChangeOrders.view') }}" class="kt-subheader__breadcrumbs-link">
                        طلبات تغيير مواعيد زيارات المساجد
                    </a>
                </div>
            </div>
        </div>
    </div>
@stop

@section('content')
    <!-- begin:: Content -->
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Portlet-->
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                البحث
                            </h3>
                        </div>
                    </div>
                    <!--begin::Form-->
                    <form class="kt-form kt-form--label-right">
                        <div class="kt-portlet__body">
                            <div class="form-group row">
                                <div class="col-lg-4">
                                    <input type="text" class="form-control searchable" id="name" name="name" placeholder="اسم الموظف">
                                </div>
                                <div class="col-lg-4">
                                    <input type="text" class="form-control searchable" id="identity_number" name="identity_number" placeholder="رقم الهوية">
                                </div>
                                <div class="col-lg-4">
                                    <input type="text" class="form-control searchable" id="phone_number" name="phone_number" placeholder="رقم الموبايل">
                                </div>
                            </div>
                        </div>
                    </form>
                    <!--end::Form-->
                </div>
                <!--end::Portlet-->
            </div>
        </div>
    <!-- End:: Content -->

    <!-- begin:: Content -->
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    طلبات تغيير مواعيد زيارات المساجد
                </h3>
            </div>
            @if(auth()->user()->can('admin.visitChangeOrders.add') || auth()->user()->can('admin.visitChangeOrders.export'))
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        @can('admin.visitChangeOrders.add')
                        <a href="{{ route('admin.visitChangeOrders.add') }}" class="btn btn-brand btn-elevate btn-icon-sm">
                            إضافة جديد
                        </a>
                        @endcan
                        @can('admin.visitChangeOrders.export')
                        <a href="{{ route('admin.visitChangeOrders.export') }}" class="btn btn-info btn-elevate btn-icon-sm">
                            تصدير اكسل
                        </a>
                        @endcan
                    </div>
                </div>
            </div>
            @endif
        </div>
        <div class="kt-portlet__body">
            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover" id="orders_table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>تاريخ الطلب</th>
                    <th>المفتش</th>
                    <th>موعد الصلاة</th>
                    <th>نوع الطلب</th>
                    <th>السبب</th>
                    <th>الحالة</th>
                    <th width="10%">أدوات</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>

            <!--end: Datatable -->
        </div>
    </div>
    <!-- end:: Content -->
@stop

@section('js')
    <script src="assets/admin/general/js/scripts/visit-change-orders.js" type="text/javascript"></script>
@stop
