@if(auth()->user()->can('admin.employees.edit') || auth()->user()->can('admin.employees.delete'))
    @can('admin.employees.edit')
        <a href="{{ route('admin.employees.edit', ['id' => $id]) }}" class="btn btn-outline-success btn-elevate-hover btn-circle btn-icon btn-sm" title="تعديل">
            <i class="la la-edit"></i>
        </a>
    @endcan
    @can('admin.employees.delete')
        <a href="javascript:;" data-url="{{ route('admin.employees.delete', ['id' => $id]) }}" class="btn btn-outline-danger btn-elevate-hover btn-circle btn-icon btn-sm delete_btn" title="حذف">
            <i class="la la-trash"></i>
        </a>
    @endcan
@endif
