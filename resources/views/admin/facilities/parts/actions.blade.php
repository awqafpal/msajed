@if(auth()->user()->can('admin.facilities.edit') || auth()->user()->can('admin.facilities.delete'))
    @can('admin.facilities.edit')
        <a href="{{ route('admin.facilities.edit', ['id' => $id]) }}" class="btn btn-outline-success btn-elevate-hover btn-circle btn-icon btn-sm" title="تعديل">
            <i class="la la-edit"></i>
        </a>
    @endcan
    @can('admin.facilities.delete')
        <a href="javascript:;" data-url="{{ route('admin.facilities.delete', ['id' => $id]) }}" class="btn btn-outline-danger btn-elevate-hover btn-circle btn-icon btn-sm delete_btn" title="حذف">
            <i class="la la-trash"></i>
        </a>
    @endcan
@endif
