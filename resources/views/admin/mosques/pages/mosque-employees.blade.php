<form id="kt_form_mosque_employees" method="post" action="{{ route('admin.mosques.employees') }}" encrypt="multipart/form-data">
    <input type="hidden" name="mosque_id" class="form-control mosque_id" value=""/>
    <div class="form-group row">
        <div class="col-lg-12">
            <label class="">الموظفين العاملين في المسجد:</label>
            <select class="form-control kt-selectpicker searchable" data-live-search="true"  multiple="multiple" 
                    id="employees" name="employees[]" data-title="ابحث عن اسم الموظف" data-size="5">
            </select>
            <span class="form-text text-muted">ملاحظة: يتم فلترة الموظفين (غير المنسبين لمساجد) بناءً على المنطقة المختارة في البيانات الاساسية </span>
        </div>
    </div>
    <!--begin: Form Actions -->
    <div class="kt-portlet__foot">
        <div class="kt-form__actions">
            <a  href="{{ route('admin.mosques.view') }}" class="btn btn-secondary btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u">
                الغاء
            </a>
            <button type="submit" class="btn btn-success btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u save-mosque-employees">
                حفظ
            </button>
        </div> 
    </div>
    <!--end: Form Actions -->
</form>