<form id="kt_form_mosque_building" method="post" action="{{ route('admin.mosques.building') }}" encrypt="multipart/form-data">
    @csrf 
    <input type="hidden" name="mosque_id" class="form-control mosque_id" value="{{$info->id}}"/>
    <div class="kt-notification">
        <a class="kt-notification__item" data-toggle="collapse" href="#buildingData" role="button" aria-expanded="false" aria-controls="buildingData">
            <div class="kt-notification__item-icon">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--brand">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <polygon points="0 0 24 0 24 24 0 24"></polygon>
                        <path d="M18.5,8 C17.1192881,8 16,6.88071187 16,5.5 C16,4.11928813 17.1192881,3 18.5,3 C19.8807119,3 21,4.11928813 21,5.5 C21,6.88071187 19.8807119,8 18.5,8 Z M18.5,21 C17.1192881,21 16,19.8807119 16,18.5 C16,17.1192881 17.1192881,16 18.5,16 C19.8807119,16 21,17.1192881 21,18.5 C21,19.8807119 19.8807119,21 18.5,21 Z M5.5,21 C4.11928813,21 3,19.8807119 3,18.5 C3,17.1192881 4.11928813,16 5.5,16 C6.88071187,16 8,17.1192881 8,18.5 C8,19.8807119 6.88071187,21 5.5,21 Z" fill="#000000" opacity="0.3"></path>
                        <path d="M5.5,8 C4.11928813,8 3,6.88071187 3,5.5 C3,4.11928813 4.11928813,3 5.5,3 C6.88071187,3 8,4.11928813 8,5.5 C8,6.88071187 6.88071187,8 5.5,8 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 C14,5.55228475 13.5522847,6 13,6 L11,6 C10.4477153,6 10,5.55228475 10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,18 L13,18 C13.5522847,18 14,18.4477153 14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 C10,18.4477153 10.4477153,18 11,18 Z M5,10 C5.55228475,10 6,10.4477153 6,11 L6,13 C6,13.5522847 5.55228475,14 5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 C18.4477153,14 18,13.5522847 18,13 L18,11 C18,10.4477153 18.4477153,10 19,10 Z" fill="#000000"></path>
                    </g>
                </svg> </div>
            <div class="kt-notification__item-details">
                <div class="kt-notification__item-title">
                    تفاصيل بناء المسجد:
                </div>
            
            </div>
        </a>
        <div class="kt-section collapse kt-margin-t-50" id="buildingData">
            <div class="kt-section__content">
                <div class="form-group row">
                    <div class="col-lg-4">
                        <label> تاريخ الانشاء (ميلادي) </label>
                        <input type="text" class="form-control date" name="date_bulid_ad" placeholder="{{now()->format('Y-m-d')}}" autocomplete="off"/>
                    </div>
                    <div class="col-lg-4">
                        <label> تاريخ الانشاء (هجري) </label>
                        <input type="text" class="form-control hijri-date-picker" name="date_bulid_hijri"  placeholder="{{now()->format('Y-m-d')}}" autocomplete="off"/>
                    </div>
                    <div class="col-lg-4">
                        <label> تاريخ تشكيل لجنة الاعمار (ميلادي) </label>
                        <input type="text" class="form-control date" name="date_of_formation" placeholder="{{now()->format('Y-m-d')}}" autocomplete="off"/>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-4 form-group-sub">
                        <label class="form-control-label">سبب انشاء المسجد :</label>
                        <select class="form-control kt-selectpicker searchable" data-live-search="true"  name="creation_reason" data-title="اختر سبب الانشاء">                                   
                            @foreach ($creation_reasons as $reason)
                                <option value="{{$reason->id}}">{{ $reason->name}}</option>
                            @endforeach
                        </select>                
                    </div>
                    <div class="col-lg-4">
                        <label> تكلفة الانشاء </label>
                        <input type="text" class="form-control" name="building_cost"  placeholder="تكلفة انشاء المسجد" />
                    </div>
                    <div class="col-lg-4">
                        <label> اسم المتبرع / الواقف </label>
                        <input type="text" class="form-control" name="user_name"  placeholder="اسم المتبرع أو الواقف"/>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-4 form-group-sub">
                        <label class="form-control-label">موقع الجهة المتبرعة :</label>
                        <select class="form-control kt-selectpicker searchable" data-live-search="true"  name="location" data-title="موقع الجهة المتبرعة">                                   
                            @foreach ($locations as $location)
                                <option value="{{$location->id}}">{{ $location->name}}</option>
                            @endforeach
                        </select>                
                    </div>
                    <div class="col-lg-4">
                        <label> اسم الدولة </label>
                        <input type="text" class="form-control" name="country_name"  placeholder="اسم الدولة" />
                    </div>
                    
                </div>
                <div class="form-group form-group-last row">
                    <div class="col-lg-12  form-group-sub">
                        <label class="">تفاصيل أخرى:</label>
                        <textarea class="summernote" name="mosque_building_notes"></textarea>
                    </div>
                </div>
            </div>
        </div>
        <a class="kt-notification__item" data-toggle="collapse" href="#buildingEngineerData" role="button" aria-expanded="false" aria-controls="buildingEngineerData">
            <div class="kt-notification__item-icon">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--brand">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <polygon points="0 0 24 0 24 24 0 24"></polygon>
                        <path d="M18.5,8 C17.1192881,8 16,6.88071187 16,5.5 C16,4.11928813 17.1192881,3 18.5,3 C19.8807119,3 21,4.11928813 21,5.5 C21,6.88071187 19.8807119,8 18.5,8 Z M18.5,21 C17.1192881,21 16,19.8807119 16,18.5 C16,17.1192881 17.1192881,16 18.5,16 C19.8807119,16 21,17.1192881 21,18.5 C21,19.8807119 19.8807119,21 18.5,21 Z M5.5,21 C4.11928813,21 3,19.8807119 3,18.5 C3,17.1192881 4.11928813,16 5.5,16 C6.88071187,16 8,17.1192881 8,18.5 C8,19.8807119 6.88071187,21 5.5,21 Z" fill="#000000" opacity="0.3"></path>
                        <path d="M5.5,8 C4.11928813,8 3,6.88071187 3,5.5 C3,4.11928813 4.11928813,3 5.5,3 C6.88071187,3 8,4.11928813 8,5.5 C8,6.88071187 6.88071187,8 5.5,8 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 C14,5.55228475 13.5522847,6 13,6 L11,6 C10.4477153,6 10,5.55228475 10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,18 L13,18 C13.5522847,18 14,18.4477153 14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 C10,18.4477153 10.4477153,18 11,18 Z M5,10 C5.55228475,10 6,10.4477153 6,11 L6,13 C6,13.5522847 5.55228475,14 5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 C18.4477153,14 18,13.5522847 18,13 L18,11 C18,10.4477153 18.4477153,10 19,10 Z" fill="#000000"></path>
                    </g>
                </svg> </div>
            <div class="kt-notification__item-details">
                <div class="kt-notification__item-title">
                    بيانات المقاول / المهندس المشرف :
                </div>
            </div>
        </a>
        <div class="kt-section collapse kt-margin-t-50" id="buildingEngineerData">
            <div class="kt-section__content">
                <div class="form-group row">
                    <div class="col-lg-4 form-group-sub">
                        <label class="form-control-label">الاسم رباعي:</label>
                        <input type="text" class="form-control" name="engineer_full_name" value="{{ old('engineer_full_name') }}" placeholder="اسم الشخص رباعي" >
                    </div>
                    <div class="col-lg-4 form-group-sub">
                        <label class="form-control-label">رقم الهوية:</label>
                        <input type="text" class="form-control" name="engineer_identity_number" value="{{ old('engineer_identity_number') }}" placeholder="رقم هوية المهندس" onkeypress="return isNumber(event)" maxlength="9">
                    </div>
                    <div class="col-lg-4 form-group-sub">
                        <label class="form-control-label">رقم الجوال:</label>
                        <input type="text" class="form-control" name="engineer_phone_number" value="{{ old('engineer_phone_number') }}" placeholder="رقم جوال المهندس"  onkeypress="return isNumber(event)" maxlength="9">
                    </div>
                </div>
                <div class="form-group form-group-last row">
                    <div class="col-lg-4 form-group-sub">
                        <label class="form-control-label">رقم الترخيص:</label>
                        <input type="text" class="form-control" name="engineer_license_number" value="{{ old('engineer_license_number') }}" placeholder="رقم الترخيص"  onkeypress="return isNumber(event)" >
                    </div>
                    <div class="col-lg-8  form-group-sub">
                        <label class="">عنوان السكن:</label>
                        <input type="text" class="form-control" name="engineer_address" value="{{ old('engineer_address') }}" placeholder="عنوان سكن المهندس" >
                    </div>
                </div>
            </div>
        </div>
        <a class="kt-notification__item" data-toggle="collapse" href="#buildingBoardData" role="button" aria-expanded="false" aria-controls="buildingBoardData">
            <div class="kt-notification__item-icon">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--brand">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <polygon points="0 0 24 0 24 24 0 24"></polygon>
                        <path d="M18.5,8 C17.1192881,8 16,6.88071187 16,5.5 C16,4.11928813 17.1192881,3 18.5,3 C19.8807119,3 21,4.11928813 21,5.5 C21,6.88071187 19.8807119,8 18.5,8 Z M18.5,21 C17.1192881,21 16,19.8807119 16,18.5 C16,17.1192881 17.1192881,16 18.5,16 C19.8807119,16 21,17.1192881 21,18.5 C21,19.8807119 19.8807119,21 18.5,21 Z M5.5,21 C4.11928813,21 3,19.8807119 3,18.5 C3,17.1192881 4.11928813,16 5.5,16 C6.88071187,16 8,17.1192881 8,18.5 C8,19.8807119 6.88071187,21 5.5,21 Z" fill="#000000" opacity="0.3"></path>
                        <path d="M5.5,8 C4.11928813,8 3,6.88071187 3,5.5 C3,4.11928813 4.11928813,3 5.5,3 C6.88071187,3 8,4.11928813 8,5.5 C8,6.88071187 6.88071187,8 5.5,8 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 C14,5.55228475 13.5522847,6 13,6 L11,6 C10.4477153,6 10,5.55228475 10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,18 L13,18 C13.5522847,18 14,18.4477153 14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 C10,18.4477153 10.4477153,18 11,18 Z M5,10 C5.55228475,10 6,10.4477153 6,11 L6,13 C6,13.5522847 5.55228475,14 5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 C18.4477153,14 18,13.5522847 18,13 L18,11 C18,10.4477153 18.4477153,10 19,10 Z" fill="#000000"></path>
                    </g>
                </svg> </div>
            <div class="kt-notification__item-details">
                <div class="kt-notification__item-title">
                    رئيس واعضاء لجنة البناء :
                </div>
            
            </div>
        </a>
        <div class="kt-section collapse kt-margin-t-50" id="buildingBoardData"> 
            <input type="hidden" name="buildingBoard" id="buildingBoard" value=""/>
            <h3 class="kt-section__title text-danger">
                <span class="building-board-table-add">
                    <button type="button" class="btn btn-success btn-sm"><i class="fa fa-plus"></i>اضافة</button>
                </span>
            </h3>
            <div class="kt-section__content">
                <div class="form-group form-group-last row">
                    <div class="col-lg-12 form-group-sub" id="buildingBoardTable">
                        <table class="table table-bordered table-responsive-md table-striped text-center">
                            <thead>
                            <tr>
                                <th class="text-center">الاسم بالكامل</th>
                                <th class="text-center">رقم الهوية</th>
                                <th class="text-center">رقم الجوال</th>
                                <th class="text-center">الوظيفة</th>
                                <th class="text-center">العنوان</th>
                                <th class="text-center">الاجراءات</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td class="pt-3-half" type="text" contenteditable="true" placeholder="أدخل الاسم كاملاً"  data-name="full_name"></td>
                                <td class="pt-3-half" type="text" contenteditable="true" placeholder="أدخل رقم الهوية" data-name="identity_number" maxlength="9" onkeypress="return isNumber(event)"></td>
                                <td class="pt-3-half" type="text" contenteditable="true" placeholder="أدخل رقم الجوال" data-name="phone_number" maxlength="10" onkeypress="return isNumber(event)"></td>
                                <td class="pt-3-half" type="text" contenteditable="true" placeholder="أدخل وظيفة العضو" data-name="job"></td>
                                <td class="pt-3-half" type="text" contenteditable="true" placeholder="أدخل العنوان" data-name="address"></td>
                                <td data-name="actions">
                                <span class="building-board-table-remove">
                                    <button type="button" class="btn btn-danger btn-rounded btn-sm my-0">
                                    إزالة
                                    </button></span>
                                </td>
                            </tr>
                            
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div> 
    <!--begin: Form Actions -->
    <div class="kt-portlet__foot">
        <div class="kt-form__actions">
            <a  href="{{ route('admin.mosques.view') }}" class="btn btn-secondary btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u">
                الغاء
            </a>
            <button type="submit" class="btn btn-success btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u save-mosque-building">
                حفظ
            </button>
        </div> 
    </div>
    <!--end: Form Actions -->
</form>
