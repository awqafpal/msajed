<div class="row">
    <div class="kt-portlet kt-portlet--tabs">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-toolbar">
                <ul class="nav nav-tabs nav-tabs-line nav-tabs-line-danger nav-tabs-line-2x nav-tabs-line-right" role="tablist">
                    @foreach ($attachment_types as $attachment_type)
                    <li class="nav-item">
                        <a class="nav-link @if($loop->first) active @endif" data-toggle="tab" href="#kt_portlet_tab_{{$attachment_type->id}}" role="tab">
                            <i class="fa fa-calendar-check-o" aria-hidden="true"></i>{{$attachment_type->name}}
                        </a>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="kt-portlet__body">
            <div class="tab-content">
                @foreach ($attachment_types as $attachment_type)
                <div class="tab-pane @if($loop->first) active @endif" id="kt_portlet_tab_{{$attachment_type->id}}" role="tabpanel">
                    <form enctype="multipart/form-data" action="{{ route('admin.mosques.attachments') }}" method="POST">
                        <input type="hidden" name="attachment_type_id" class="attachment_type_id" value="{{ $attachment_type->id }}">
                        <input type="hidden" name="mosque_id" class="mosque_id" value=""/>
                        <div class="dropzone dropzone-default dz-clickable" data-type="{{ $attachment_type->id }}" 
                             data-url="{{ route('admin.mosques.attachments') }}" id="kt_dropzone_{{$loop->iteration}}">
                            <div class="dropzone-msg dz-message needsclick filesContainer">
                                <h3 class="dropzone-msg-title">قم بسحب الملفات هنا أو انقر للتحميل.</h3>
                                <input type="file" multiple name="attachments[]" class="hide_file" />
                            </div>
                        </div>
                        <div id="target" class="m--margin-top-10">
                        </div>
                    </form>
                </div>
                @endforeach
            </div>
        </div>
    </div>    
</div>

