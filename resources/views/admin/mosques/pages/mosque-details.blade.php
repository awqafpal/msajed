<form id="kt_form_mosque_details" method="post" action="{{ route('admin.mosques.details') }}" encrypt="multipart/form-data" onsubmit="return false">
    @csrf
    <input type="hidden" name="mosque_id" class="form-control mosque_id" value="{{$info->id}}"/>
    <div class="kt-notification">
        <a class="kt-notification__item" data-toggle="collapse" href="#assigneeData" role="button" aria-expanded="false" aria-controls="assigneeData">
            <div class="kt-notification__item-icon">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--brand">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <polygon points="0 0 24 0 24 24 0 24"></polygon>
                        <path d="M18.5,8 C17.1192881,8 16,6.88071187 16,5.5 C16,4.11928813 17.1192881,3 18.5,3 C19.8807119,3 21,4.11928813 21,5.5 C21,6.88071187 19.8807119,8 18.5,8 Z M18.5,21 C17.1192881,21 16,19.8807119 16,18.5 C16,17.1192881 17.1192881,16 18.5,16 C19.8807119,16 21,17.1192881 21,18.5 C21,19.8807119 19.8807119,21 18.5,21 Z M5.5,21 C4.11928813,21 3,19.8807119 3,18.5 C3,17.1192881 4.11928813,16 5.5,16 C6.88071187,16 8,17.1192881 8,18.5 C8,19.8807119 6.88071187,21 5.5,21 Z" fill="#000000" opacity="0.3"></path>
                        <path d="M5.5,8 C4.11928813,8 3,6.88071187 3,5.5 C3,4.11928813 4.11928813,3 5.5,3 C6.88071187,3 8,4.11928813 8,5.5 C8,6.88071187 6.88071187,8 5.5,8 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 C14,5.55228475 13.5522847,6 13,6 L11,6 C10.4477153,6 10,5.55228475 10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,18 L13,18 C13.5522847,18 14,18.4477153 14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 C10,18.4477153 10.4477153,18 11,18 Z M5,10 C5.55228475,10 6,10.4477153 6,11 L6,13 C6,13.5522847 5.55228475,14 5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 C18.4477153,14 18,13.5522847 18,13 L18,11 C18,10.4477153 18.4477153,10 19,10 Z" fill="#000000"></path>
                    </g>
                </svg> </div>
            <div class="kt-notification__item-details">
                <div class="kt-notification__item-title">
                    بيانات المتنازل/ الواقف:
                </div>
            
            </div>
        </a>
        <div class="kt-section collapse kt-margin-t-50" id="assigneeData">
            <div class="kt-section__content">
                <div class="form-group row">
                    <div class="col-lg-4 form-group-sub">
                        <label class="form-control-label">اسم المتنازل:</label>
                        <input type="text" class="form-control" name="assignee_user_name" value="{{ old('name') }}" placeholder="اسم المتنازل">
                    </div>
                    <div class="col-lg-4 form-group-sub">
                        <label class="form-control-label">رقم هوية المتنازل:</label>
                        <input type="text" class="form-control" name="assignee_identity" value="{{ old('assignee_identity') }}" placeholder="رقم هوية المتنازل" onkeypress="return isNumber(event)" maxlength="9">
                    </div>
                    <div class="col-lg-4 form-group-sub">
                        <label class="form-control-label">رقم موبايل المتنازل:</label>
                        <input type="text" class="form-control" name="assignee_mobile" value="{{ old('assignee_mobile') }}" placeholder="رقم موبايل المتنازل" onkeypress="return isNumber(event)" maxlength="10">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-4 form-group-sub">
                        <label class="form-control-label">تاريخ التنازل:</label>
                        <input type="text" class="form-control date" name="assignee_date" value="{{ old('assignee_date') }}" placeholder="تاريخ المتنازل" >
                    </div>
                    <div class="col-lg-4 form-group-sub">
                        <label class="form-control-label"> مساحة الأرض الموقوفة/ م<sup>2</sup>:</label>
                        <input type="text" class="form-control" name="assignee_land_space" value="{{ old('assignee_land_space') }}" placeholder="مساحة الأرض الموقوفة بالمتر المربع" >
                    </div>
                </div>
                <div class="form-group form-group-last row">
                    <div class="col-lg-12  form-group-sub">
                        <label class="">تفاصيل أخرى:</label>
                        <textarea class="summernote" name="assignee_notes"></textarea>
                    </div>
                </div>
            </div>
        </div>
        <a class="kt-notification__item" data-toggle="collapse" href="#allocationData" role="button" aria-expanded="false" aria-controls="allocationData">
            <div class="kt-notification__item-icon">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--brand">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <polygon points="0 0 24 0 24 24 0 24"></polygon>
                        <path d="M18.5,8 C17.1192881,8 16,6.88071187 16,5.5 C16,4.11928813 17.1192881,3 18.5,3 C19.8807119,3 21,4.11928813 21,5.5 C21,6.88071187 19.8807119,8 18.5,8 Z M18.5,21 C17.1192881,21 16,19.8807119 16,18.5 C16,17.1192881 17.1192881,16 18.5,16 C19.8807119,16 21,17.1192881 21,18.5 C21,19.8807119 19.8807119,21 18.5,21 Z M5.5,21 C4.11928813,21 3,19.8807119 3,18.5 C3,17.1192881 4.11928813,16 5.5,16 C6.88071187,16 8,17.1192881 8,18.5 C8,19.8807119 6.88071187,21 5.5,21 Z" fill="#000000" opacity="0.3"></path>
                        <path d="M5.5,8 C4.11928813,8 3,6.88071187 3,5.5 C3,4.11928813 4.11928813,3 5.5,3 C6.88071187,3 8,4.11928813 8,5.5 C8,6.88071187 6.88071187,8 5.5,8 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 C14,5.55228475 13.5522847,6 13,6 L11,6 C10.4477153,6 10,5.55228475 10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,18 L13,18 C13.5522847,18 14,18.4477153 14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 C10,18.4477153 10.4477153,18 11,18 Z M5,10 C5.55228475,10 6,10.4477153 6,11 L6,13 C6,13.5522847 5.55228475,14 5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 C18.4477153,14 18,13.5522847 18,13 L18,11 C18,10.4477153 18.4477153,10 19,10 Z" fill="#000000"></path>
                    </g>
                </svg> </div>
            <div class="kt-notification__item-details">
                <div class="kt-notification__item-title">
                    بيانات التخصيص:
                </div>
            </div>
        </a>
        <div class="kt-section collapse kt-margin-t-50" id="allocationData">
            <div class="kt-section__content">
                <div class="form-group row">
                    <div class="col-lg-4 form-group-sub">
                        <label class="form-control-label">تاريخ التخصيص:</label>
                        <input type="text" class="form-control date" name="allocation_date" value="{{ old('allocation_date') }}" placeholder="تاريخ التخصيص" >
                    </div>
                    <div class="col-lg-4 form-group-sub">
                        <label class="form-control-label"> مساحة الأرض المخصصة/ م<sup>2</sup>:</label>
                        <input type="text" class="form-control" name="allocation_land_space" value="{{ old('allocation_land_space') }}" placeholder="مساحة الأرض المخصصة بالمتر المربع" >
                    </div>
                </div>
                <div class="form-group form-group-last row">
                    <div class="col-lg-12  form-group-sub">
                        <label class="">تفاصيل أخرى:</label>
                        <textarea class="summernote" name="allocation_notes"></textarea>
                    </div>
                </div>
            </div>
        </div>
        <a class="kt-notification__item" data-toggle="collapse" href="#supervisorData" role="button" aria-expanded="false" aria-controls="supervisorData">
            <div class="kt-notification__item-icon">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--brand">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <polygon points="0 0 24 0 24 24 0 24"></polygon>
                        <path d="M18.5,8 C17.1192881,8 16,6.88071187 16,5.5 C16,4.11928813 17.1192881,3 18.5,3 C19.8807119,3 21,4.11928813 21,5.5 C21,6.88071187 19.8807119,8 18.5,8 Z M18.5,21 C17.1192881,21 16,19.8807119 16,18.5 C16,17.1192881 17.1192881,16 18.5,16 C19.8807119,16 21,17.1192881 21,18.5 C21,19.8807119 19.8807119,21 18.5,21 Z M5.5,21 C4.11928813,21 3,19.8807119 3,18.5 C3,17.1192881 4.11928813,16 5.5,16 C6.88071187,16 8,17.1192881 8,18.5 C8,19.8807119 6.88071187,21 5.5,21 Z" fill="#000000" opacity="0.3"></path>
                        <path d="M5.5,8 C4.11928813,8 3,6.88071187 3,5.5 C3,4.11928813 4.11928813,3 5.5,3 C6.88071187,3 8,4.11928813 8,5.5 C8,6.88071187 6.88071187,8 5.5,8 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 C14,5.55228475 13.5522847,6 13,6 L11,6 C10.4477153,6 10,5.55228475 10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,18 L13,18 C13.5522847,18 14,18.4477153 14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 C10,18.4477153 10.4477153,18 11,18 Z M5,10 C5.55228475,10 6,10.4477153 6,11 L6,13 C6,13.5522847 5.55228475,14 5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 C18.4477153,14 18,13.5522847 18,13 L18,11 C18,10.4477153 18.4477153,10 19,10 Z" fill="#000000"></path>
                    </g>
                </svg> </div>
            <div class="kt-notification__item-details">
                <div class="kt-notification__item-title">
                    جهة الاشراف على المسجد:
                </div>
            
            </div>
        </a>
        <div class="kt-section collapse kt-margin-t-50" id="supervisorData">
            <div class="kt-section__content">
                <div class="form-group row">
                    <div class="col-lg-6 form-group-sub">
                        <label class="form-control-label">جهة الإشراف:</label>
                        <select class="form-control kt-selectpicker searchable" data-live-search="true"  name="supervisor_authority_type" data-title="اختر جهة الاشراف">                                   
                            @foreach ($supervisor_authority_types as $supervisor_authority_type)
                                <option value="{{$supervisor_authority_type->id}}">{{ $supervisor_authority_type->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-lg-6 form-group-sub">
                        <label class="form-control-label">اسم الجهة المشرفة:</label>
                        <input type="text" class="form-control" name="supervisor_authority_name" value="{{ old('supervisor_authority_name') }}" placeholder="اسم الجهة المشرفة">
                    </div>
                </div>
            </div>
        </div>
        <a class="kt-notification__item" data-toggle="collapse" href="#administrativeBoardData" role="button" aria-expanded="false" aria-controls="administrativeBoardData">
            <div class="kt-notification__item-icon">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--brand">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <polygon points="0 0 24 0 24 24 0 24"></polygon>
                        <path d="M18.5,8 C17.1192881,8 16,6.88071187 16,5.5 C16,4.11928813 17.1192881,3 18.5,3 C19.8807119,3 21,4.11928813 21,5.5 C21,6.88071187 19.8807119,8 18.5,8 Z M18.5,21 C17.1192881,21 16,19.8807119 16,18.5 C16,17.1192881 17.1192881,16 18.5,16 C19.8807119,16 21,17.1192881 21,18.5 C21,19.8807119 19.8807119,21 18.5,21 Z M5.5,21 C4.11928813,21 3,19.8807119 3,18.5 C3,17.1192881 4.11928813,16 5.5,16 C6.88071187,16 8,17.1192881 8,18.5 C8,19.8807119 6.88071187,21 5.5,21 Z" fill="#000000" opacity="0.3"></path>
                        <path d="M5.5,8 C4.11928813,8 3,6.88071187 3,5.5 C3,4.11928813 4.11928813,3 5.5,3 C6.88071187,3 8,4.11928813 8,5.5 C8,6.88071187 6.88071187,8 5.5,8 Z M11,4 L13,4 C13.5522847,4 14,4.44771525 14,5 C14,5.55228475 13.5522847,6 13,6 L11,6 C10.4477153,6 10,5.55228475 10,5 C10,4.44771525 10.4477153,4 11,4 Z M11,18 L13,18 C13.5522847,18 14,18.4477153 14,19 C14,19.5522847 13.5522847,20 13,20 L11,20 C10.4477153,20 10,19.5522847 10,19 C10,18.4477153 10.4477153,18 11,18 Z M5,10 C5.55228475,10 6,10.4477153 6,11 L6,13 C6,13.5522847 5.55228475,14 5,14 C4.44771525,14 4,13.5522847 4,13 L4,11 C4,10.4477153 4.44771525,10 5,10 Z M19,10 C19.5522847,10 20,10.4477153 20,11 L20,13 C20,13.5522847 19.5522847,14 19,14 C18.4477153,14 18,13.5522847 18,13 L18,11 C18,10.4477153 18.4477153,10 19,10 Z" fill="#000000"></path>
                    </g>
                </svg> </div>
            <div class="kt-notification__item-details">
                <div class="kt-notification__item-title">
                    مجلس ادارة المسجد:
                </div>
            
            </div>
        </a>
        <div class="kt-section collapse kt-margin-t-50" id="administrativeBoardData">
            <input type="hidden" name="administrativeBoard" id="administrativeBoard" value=""/>
            <h3 class="kt-section__title text-danger">
                <span class="table-add">
                    <button type="button" class="btn btn-success btn-sm"><i class="fa fa-plus"></i>اضافة</button>
                </span>
            </h3>
            <div class="kt-section__content">
                <div class="form-group form-group-last row">
                    <div class="col-lg-12 form-group-sub" id="table">
                        <table class="table table-bordered table-responsive-md table-striped text-center">
                            <thead>
                            <tr>
                                <th class="text-center">الاسم بالكامل</th>
                                <th class="text-center">رقم الهوية</th>
                                <th class="text-center">رقم الجوال</th>
                                <th class="text-center">الوظيفة</th>
                                <th class="text-center">العنوان</th>
                                <th class="text-center">الاجراءات</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td class="pt-3-half" type="text" contenteditable="true" placeholder="أدخل الاسم كاملاً"  data-name="full_name"></td>
                                <td class="pt-3-half" type="text" contenteditable="true" placeholder="أدخل رقم الهوية" data-name="identity_number" maxlength="9" onkeypress="return isNumber(event)"></td>
                                <td class="pt-3-half" type="text" contenteditable="true" placeholder="أدخل رقم الجوال" data-name="phone_number" maxlength="10" onkeypress="return isNumber(event)"></td>
                                <td class="pt-3-half" type="text" contenteditable="true" placeholder="أدخل وظيفة العضو" data-name="job"></td>
                                <td class="pt-3-half" type="text" contenteditable="true" placeholder="أدخل العنوان" data-name="address"></td>
                                <td data-name="actions">
                                <span class="table-remove">
                                    <button type="button" class="btn btn-danger btn-rounded btn-sm my-0">
                                    إزالة
                                    </button></span>
                                </td>
                            </tr>
                            
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div> 
    <!--begin: Form Actions -->
    <div class="kt-portlet__foot">
        <div class="kt-form__actions">
            <a  href="{{ route('admin.mosques.view') }}" class="btn btn-secondary btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u">
                الغاء
            </a>
            <button type="submit" class="btn btn-success btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u save-mosque-details">
                حفظ
            </button>
        </div> 
    </div>
    <!--end: Form Actions -->
</form>