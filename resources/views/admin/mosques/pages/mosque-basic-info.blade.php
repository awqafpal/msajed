<form id="kt_form_basic_info" method="post" action="{{ $action}}" encrypt="multipart/form-data">
    <input type="hidden" name="mosque_id" class="form-control mosque_id" value="{{$info->id}}" data-region="{{ $info->region_id}}"/>
    <div class="form-group row">
        <div class="col-xl-12 text-center">
            <div class="kt-avatar kt-avatar--outline kt-avatar--circle" id="kt_user_avatar_3">
                <div class="kt-avatar__holder" style="background-image: url({{($info->image) ? asset($info->image) : 'backend/img/default.jpg'}})"></div>
                <label class="kt-avatar__upload" data-toggle="kt-tooltip" title="" data-original-title="قم بتغيير الصورة">
                    <i class="fa fa-pen"></i>
                    <input type="file" name="mosque_image" accept="image/*">
                </label>
                <span class="kt-avatar__cancel" data-toggle="kt-tooltip" title="" data-original-title="إلغاء">
                    <i class="fa fa-times"></i>
                </span>
            </div>
            <span class="form-text text-muted">يُسمح بالصيغ التالية: png, jpg, jpeg.</span>
        </div>
    </div>   
    <div class="form-group row">
        <div class="col-lg-4">
            <label> اسم المسجد </label>
            <input type="text" class="form-control" name="name" value="{{ old('name',$info->name) }}" placeholder="اسم المسجد">
        </div>
        <div class="col-lg-4">
            <label class="">المحافظة:</label>
            <select class="form-control kt-selectpicker searchable" data-live-search="true" id="province" name="province_id" data-title="اختر المحافظة">
                @foreach ($provinces as $province)
                    <option value="{{$province->id}}" {{ $province->id == $info->region->province_id ? 'selected' : ''}} >{{ $province->name}}</option>
                @endforeach
            </select>
        </div>
        <div class="col-lg-4">
            <label class="">المنطقة:</label>
            <select class="form-control kt-selectpicker searchable" data-live-search="true" id="regions" name="region_id" data-title="اختر المنطقة">                                   
                @if($info->region_id)
                    @foreach ($regions as $region)
                        <option value="{{$region->id}}" {{ $region->id == $info->region_id ? 'selected' : ''}} >{{ $region->name}}</option>
                    @endforeach
                @endif
            </select>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-lg-4">
            <label class="">نوع المسجد:</label>
            <select class="form-control kt-selectpicker searchable" data-live-search="true"  name="type_status" data-title="اختر نوع المسجد">
                @foreach ($types as $type)
                    <option value="{{$type->id}}" {{ $type->id == $info->type_status ? 'selected' : ''}}>{{ $type->name}}</option>
                @endforeach
            </select>
        </div>
        <div class="col-lg-4">
            <label class="">الملكية:</label>
            <select class="form-control kt-selectpicker searchable" data-live-search="true"  name="property_id" data-title="اختر نوع الملكية">                                   
                @foreach ($properties as $property)
                    <option value="{{$property->id}}" {{ $property->id == $info->property_id ? 'selected' : ''}}>{{ $property->name}}</option>
                @endforeach
            </select>
        </div>
        <div class="col-lg-4">
            <label class="">طبيعة البناء:</label>
            <select class="form-control kt-selectpicker searchable" data-live-search="true"  name="building_type" data-title="اختر طبيعة البناء">
                @foreach ($building_types as $building_type)
                    <option value="{{$building_type->id}}" {{ $building_type->id == $info->building_type ? 'selected' : ''}}>{{ $building_type->name}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-lg-4">
            <label> رقم المسجد </label>
            <input type="text" class="form-control" name="mosque_no" value="{{ old('mosque_no',$info->mosque_no) }}" placeholder="رقم المسجد" onkeypress="return isNumber(event)">
        </div>
        <div class="col-lg-4">
            <label> رقم الحساب البنكي </label>
            <input type="text" class="form-control" name="bank_account_number" value="{{ old('bank_account_number',$info->bank_account_number) }}" placeholder="رقم الحساب البنكي" onkeypress="return isNumber(event)">
        </div>
        <div class="col-lg-4">
            <label class="">المفتش:</label>
            <select class="form-control kt-selectpicker searchable" data-live-search="true"  name="user_id" data-title="اختر مفتش المسجد">                                   
                @foreach ($users as $user)
                    <option value="{{$user->id}}" {{ $user->id == $info->user_id ? 'selected' : ''}}>{{ $user->name}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-lg-4">
            <label> تاريخ الانشاء (ميلادي) </label>
            <input type="text" class="form-control date" name="date_bulid_ad" value="{{ old('date_bulid_ad',$info->date_bulid_ad) }}" placeholder="{{now()->format('Y-m-d')}}" autocomplete="off"/>
        </div>
        <div class="col-lg-4">
            <label> تاريخ الانشاء (هجري) </label>
            <input type="text" class="form-control hijri-date-picker" name="date_bulid_hijri" value="{{ old('date_bulid_hijri',$info->date_bulid_hijri) }}"  placeholder="{{now()->format('Y-m-d')}}" autocomplete="off"/>
        </div>
        <div class="col-lg-4">
            <label> تاريخ تبعية المسجد للوزارة </label>
            <input type="text" class="form-control date" name="mosque_date_ministry"  value="{{ old('mosque_date_ministry',$info->mosque_date_ministry) }}" placeholder="{{now()->format('Y-m-d')}}" autocomplete="off"/>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-lg-4">
            <label> القطعة </label>
            <input type="text" class="form-control" name="piece" value="{{ old('piece',$info->piece) }}" placeholder="رقم القطعة" onkeypress="return isNumber(event)"/>
        </div>
        <div class="col-lg-4">
            <label> القسيمة </label>
            <input type="text" class="form-control" name="qasema" value="{{ old('qasema',$info->qasema) }}"  placeholder="رقم القسيمة" onkeypress="return isNumber(event)"/>
        </div>
        <div class="col-lg-4">
            <label> المساحة(م<sup>2</sup>) </label>
            <input type="text" class="form-control" name="mosque_space" value="{{ old('mosque_space',$info->mosque_space) }}"   placeholder="المساحة بالمتر المربع" onkeypress="return isNumber(event)"/>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-lg-4">
            <label> عدد الوقفيات </label>
            <input type="text" class="form-control" name="facilities_number" value="{{ old('facilities_number',$info->facilities_number) }}" placeholder="عدد الوقفيات" onkeypress="return isNumber(event)"/>
        </div>
        <div class="col-lg-4">
            <label> عدد المصلين </label>
            <input type="text" class="form-control" name="prayer_no" value="{{ old('prayer_no',$info->prayer_no) }}" placeholder="عدد المصلين" onkeypress="return isNumber(event)"/>
        </div>
        <div class="col-lg-4">
            <label> عدد الطوابق </label>
            <input type="text" class="form-control" name="floor_no" value="{{ old('prayer_no',$info->prayer_no) }}"  placeholder="عدد الطوابق" onkeypress="return isNumber(event)"/>
        </div> 
    </div>
    <div class="form-group row">
        <div class="col-lg-8">
            <label> عنوان المسجد (الحي-الشارع) </label>
            <input type="text" class="form-control" name="street" value="{{ old('street',$info->street) }}" placeholder="عنوان المسجد التفصيلي (الحي-الشارع)" />
        </div>
        <div class="col-lg-4">
            <div class="kt-margin-t-25  float-left">
                <label class="col-form-label float-left kt-padding-r-5">مركزي؟ </label>
                <span class="kt-switch kt-switch--outline kt-switch--icon kt-switch--success kt-margin-r-50"  >
                    <label>
                        <input type="checkbox" name="central_status" value="1" {{ old('central_status',$info->central_status) == 1 ? 'checked' : '' }}>
                        <span></span>
                    </label>
                </span>
            </div>
            <div class="kt-margin-t-25">
                <label class="col-form-label float-left kt-padding-r-5">أثري؟ </label>
                <span class="kt-switch kt-switch--outline kt-switch--icon kt-switch--success " >
                    <label>
                        <input type="checkbox" name="archaeological_status" value="1" {{ old('archaeological_status',$info->archaeological_status) == 1 ? 'checked' : '' }}>
                        <span></span>
                    </label>
                </span>
            </div>
        </div>
        
    </div>
    <div class="form-group row">
        <div class="col-lg-12">
            <label class="">وصف المسجد:</label>
            <textarea class="summernote" name="description">{!! old('description',$info->description) !!}</textarea>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-lg-12">
            <label class="">الدروس المنهجية:</label>
            <textarea class="summernote" name="lessons">{!! old('lessons',$info->lessons) !!}</textarea>
        </div>
    </div>    
    <div class="form-group row">
        <div class="col-lg-12">
            <label class="">الدورات العلمية:</label>
            <textarea class="summernote" name="courses">{!! old('courses',$info->courses) !!}</textarea>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-lg-4">
            <label>بحث عن المنطقة</label>
            <input type="text" class="form-control" name="lat" id="searchmap" placeholder="">
        </div>
        <div class="col-lg-4">
            <label>الإحداث السيني</label>
            <input  type="text" readonly value="" class="form-control" name="gis_lat" id="lat">
        </div>
        <div class="col-lg-4">
            <label >الإحداث الصادي</label>
            <input type="text" readonly value="" class="form-control" name="gis_lng" id="lng">
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-12">
            <div id="map-canvas"></div>
        </div>
    </div>
    <!--begin: Form Actions -->
    <div class="kt-portlet__foot">
        <div class="kt-form__actions">
            <a  href="{{ route('admin.mosques.view') }}" class="btn btn-secondary btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u">
                الغاء
            </a>
            <button type="submit" class="btn btn-success btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u  save-basic-info">
                حفظ
            </button>
        </div> 
    </div>
    <!--end: Form Actions -->
</form>