@extends('admin.layout.master')

@section('title')
    إضافة مسجد
@stop

@section('style')
@stop

@section('css')
    <link href="{{asset('assets/admin/css/pages/wizard/wizard-1.rtl.css')}}" rel="stylesheet" type="text/css" />
    <style>
        .kt-wizard-v1 .kt-wizard-v1__wrapper .kt-form {
            width: 90% !important;
        }
        .select2-search__field{
            direction: rtl !important;
        }
        .pt-3-half { padding-top: 1.4rem; }
        .kt-wizard-v1__nav-label{ font-size:12px !important; }
        .d-none { display: none !important; }
        [contenteditable=true]:empty:before{
            content: attr(placeholder);
            pointer-events: none;
            display: block; /* For Firefox */
        }
    </style>
@stop

@section('subheader')
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    وزارة الأوقاف والشؤون الدينية </h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="{{ route('admin.dashboard.view') }}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="{{ route('admin.dashboard.view') }}" class="kt-subheader__breadcrumbs-link">
                        الرئيسية
                    </a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="{{ route('admin.mosques.view') }}" class="kt-subheader__breadcrumbs-link">
                        المساجد
                    </a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="{{ route('admin.mosques.add') }}" class="kt-subheader__breadcrumbs-link">
                        إضافة مسجد
                    </a>
                </div>
            </div>
        </div>
    </div>
@stop

@section('content')
    <!-- begin:: Content -->
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <div class="kt-portlet__body kt-portlet__body--fit">
                <div class="kt-grid kt-wizard-v1 kt-wizard-v1--white" id="kt_wizard_v1" data-ktwizard-state="step-first">
                    <div class="kt-grid__item">

                        <!--begin: Form Wizard Nav -->
                        <div class="kt-wizard-v1__nav">

                            <!--doc: Remove "kt-wizard-v1__nav-items--clickable" class and also set 'clickableSteps: false' in the JS init to disable manually clicking step titles -->
                            <div class="kt-wizard-v1__nav-items kt-wizard-v1__nav-items--clickable">
                                <div class="kt-wizard-v1__nav-item" data-ktwizard-type="step" data-ktwizard-state="current">
                                    <div class="kt-wizard-v1__nav-body">
                                        <div class="kt-wizard-v1__nav-icon">
                                            <i class="flaticon-home-2"></i>
                                        </div>
                                        <div class="kt-wizard-v1__nav-label">
                                            البيانات الأساسية
                                        </div>
                                    </div>
                                </div>
                                <div class="kt-wizard-v1__nav-item" data-ktwizard-type="step">
                                    <div class="kt-wizard-v1__nav-body">
                                        <div class="kt-wizard-v1__nav-icon">
                                            <i class="flaticon-users"></i>
                                        </div>
                                        <div class="kt-wizard-v1__nav-label">
                                            الموظفين
                                        </div>
                                    </div>
                                </div>
                                <div class="kt-wizard-v1__nav-item" data-ktwizard-type="step">
                                    <div class="kt-wizard-v1__nav-body">
                                        <div class="kt-wizard-v1__nav-icon">
                                            <i class="flaticon-upload-1"></i>
                                        </div>
                                        <div class="kt-wizard-v1__nav-label">
                                            المرفقات
                                        </div>
                                    </div>
                                </div>
                                <div class="kt-wizard-v1__nav-item" data-ktwizard-type="step">
                                    <div class="kt-wizard-v1__nav-body">
                                        <div class="kt-wizard-v1__nav-icon">
                                            <i class="flaticon-calendar"></i>
                                        </div>
                                        <div class="kt-wizard-v1__nav-label">
                                            تفاصيل الملكية
                                        </div>
                                    </div>
                                </div>
                                <div class="kt-wizard-v1__nav-item" data-ktwizard-type="step">
                                    <div class="kt-wizard-v1__nav-body">
                                        <div class="kt-wizard-v1__nav-icon">
                                            <i class="flaticon-notes"></i>
                                        </div>
                                        <div class="kt-wizard-v1__nav-label">
                                            تفاصيل البناء
                                        </div>
                                    </div>
                                </div>
                                <div class="kt-wizard-v1__nav-item" data-ktwizard-type="step">
                                    <div class="kt-wizard-v1__nav-body">
                                        <div class="kt-wizard-v1__nav-icon">
                                            <i class="flaticon-notes"></i>
                                        </div>
                                        <div class="kt-wizard-v1__nav-label">
                                           اعادة الاعمار
                                        </div>
                                    </div>
                                </div>
                                <div class="kt-wizard-v1__nav-item" data-ktwizard-type="step">
                                    <div class="kt-wizard-v1__nav-body">
                                        <div class="kt-wizard-v1__nav-icon">
                                            <i class="flaticon-infinity"></i>
                                        </div>
                                        <div class="kt-wizard-v1__nav-label">
                                            الخدمات
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!--end: Form Wizard Nav -->
                    </div>
                    <div class="kt-grid__item kt-grid__item--fluid kt-wizard-v1__wrapper">
 
                        <!--begin: Form Wizard Form-->
                        <div class="kt-form kt-form--label-right" id="kt_form">

                            <!--begin: Form Wizard Step 1-->
                            <div class="kt-wizard-v1__content" data-ktwizard-type="step-content" data-ktwizard-state="current">
                                @include('admin.mosques.pages.mosque-basic-info')
                            </div>

                            <!--end: Form Wizard Step 1-->

                            <!--begin: Form Wizard Step 2-->
                            <div class="kt-wizard-v1__content" data-ktwizard-type="step-content">
                                @include('admin.mosques.pages.mosque-employees')
                            </div>

                            <!--end: Form Wizard Step 2-->

                            <!--begin: Form Wizard Step 3-->
                            <div class="kt-wizard-v1__content" data-ktwizard-type="step-content">
                                @include('admin.mosques.pages.mosque-attachments')
                            </div>

                            <!--end: Form Wizard Step 3-->

                            <!--begin: Form Wizard Step 4-->
                            <div class="kt-wizard-v1__content" data-ktwizard-type="step-content">
                                @include('admin.mosques.pages.mosque-details')
                            </div>

                            <!--end: Form Wizard Step 4-->

                            <!--begin: Form Wizard Step 4-->
                            <div class="kt-wizard-v1__content" data-ktwizard-type="step-content">
                                @include('admin.mosques.pages.mosque-building')
                            </div>
                            <!--end: Form Wizard Step 4-->

                             <!--begin: Form Wizard Step 5-->
                             <div class="kt-wizard-v1__content" data-ktwizard-type="step-content">
                                @include('admin.mosques.pages.mosque-rebuilding')
                            </div>
                            <!--end: Form Wizard Step 4-->

                            <!--begin: Form Wizard Step 5-->
                            <div class="kt-wizard-v1__content" data-ktwizard-type="step-content">
                                @include('admin.mosques.pages.mosque-services')
                            </div>
                            <!--end: Form Wizard Step 5-->
                        </div>

                        <!--end: Form Wizard Form-->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end:: Content -->
@stop

@section('js')
    <!--begin::Page Scripts(used by this page) -->
    <script src="assets/admin/js/pages/crud/file-upload/ktavatar.js" type="text/javascript"></script>
    <script src="assets/admin/js/pages/custom/wizard/wizard-add-mosque.js" type="text/javascript"></script>
    <script src="assets/admin/js/pages/crud/forms/editors/summernote.js" type="text/javascript"></script>
    <script src="assets/admin/general/js/segments/editable-table.js" type="text/javascript"></script>    
    <script src="assets/admin/general/js/scripts/mosques.js" type="text/javascript"></script>
@stop

@section('scripts')
<script>
    var csrf = $('meta[name="csrf-token"]').attr('content') ;
    $('.dropzone').dropzone({
        url: "{{ route('admin.mosques.attachments') }}", // Set the url for your upload script location
        method:"post",
        headers:{'X-CSRF-TOKEN': csrf},
        uploadMultiple:true,
        paramName: "file", // The name that will be used to transfer the file
        maxFiles: 10,
        maxFilesize: 5, // MB
        addRemoveLinks: false,
        accept: function(file, done) {
            if (file.name == "justinbieber.jpg") {
                done("Naha, you don't.");
            } else {
                done();
            }
        },
        sending: function(file, xhr, formData){
            formData.append('mosque_id', $('.mosque_id').val());
            formData.append('attachment_type_id', $(this)[0].element.dataset.type);
        },
        success: function(file, response){
            // console.log('dastal');
            // console.log(response);
            Forms.notify(response.title,response.code, response.message);
        }
    });
</script>
@stop