@if(auth()->user()->can('admin.mosques.show') || auth()->user()->can('admin.mosques.edit') || auth()->user()->can('admin.mosques.delete'))
    @can('admin.mosques.show')
        <a href="{{ route('admin.mosques.show', ['id' => $id]) }}" class="btn btn-outline-info btn-elevate-hover btn-circle btn-icon btn-sm" title="عرض التفاصيل">
            <i class="la la-eye"></i>
        </a>
    @endcan
    @can('admin.mosques.edit')
        <a href="{{ route('admin.mosques.edit', ['id' => $id]) }}" class="btn btn-outline-success btn-elevate-hover btn-circle btn-icon btn-sm" title="تعديل">
            <i class="la la-edit"></i>
        </a>
    @endcan
    @can('admin.mosques.delete')
        <a href="javascript:;" data-url="{{ route('admin.mosques.delete', ['id' => $id]) }}" class="btn btn-outline-danger btn-elevate-hover btn-circle btn-icon btn-sm delete_btn" title="حذف">
            <i class="la la-trash"></i>
        </a>
    @endcan
@endif
