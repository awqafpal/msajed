@if($archaeological_status == 0)
    <span class="btn-sm btn btn-label-danger">غير أثري</span>
@else
    <span class="btn-sm btn btn-label-success">أثري</span>
@endif