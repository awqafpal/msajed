@if(auth()->user()->can('admin.advertisings.edit') || auth()->user()->can('admin.advertisings.delete'))
    @can('admin.advertisings.edit')
        <a href="{{ route('admin.advertisings.edit', ['id' => $id]) }}" class="btn btn-outline-success btn-elevate-hover btn-circle btn-icon btn-sm" title="تعديل">
            <i class="la la-edit"></i>
        </a>
    @endcan
    @can('admin.advertisings.delete')
        <a href="javascript:;" data-url="{{ route('admin.advertisings.delete', ['id' => $id]) }}" class="btn btn-outline-danger btn-elevate-hover btn-circle btn-icon btn-sm delete_btn" title="حذف">
            <i class="la la-trash"></i>
        </a>
    @endcan
@endif
