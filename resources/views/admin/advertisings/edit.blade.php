@extends('admin.layout.master')

@section('title')
    تعديل اعلان
@stop

@section('css')

@stop

@section('subheader')
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    وزارة الأوقاف والشؤون الدينية </h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="{{ route('admin.dashboard.view') }}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="{{ route('admin.dashboard.view') }}" class="kt-subheader__breadcrumbs-link">
                        الرئيسية
                    </a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="{{ route('admin.advertisings.view') }}" class="kt-subheader__breadcrumbs-link">
                        الاعلانات
                    </a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="{{ route('admin.advertisings.edit', ['id' => $info->id]) }}" class="kt-subheader__breadcrumbs-link">
                        تعديل اعلان
                    </a>
                </div>
            </div>
        </div>
    </div>
@stop

@section('content')
    <!-- begin:: Content -->
    <div class="row">
        <div class="col-lg-12">
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            تعديل اعلان
                        </h3>
                    </div>
                </div>
                <!--begin::Form-->
                <form class="kt-form kt-form--label-right" id="frmEdit" method="post" action="{{ route('admin.advertisings.update', ['id' => $info->id]) }}" encrypt="multipart/form-data">
                    <div class="kt-portlet__body">
                        <div class="form-group row">
                            <div class="col-lg-8">
                                <label class="">عنوان الاعلان:</label>
                                <input type="text" class="form-control" name="title" value="{{ old('title',$info->title) }}" placeholder="عنوان الاعلان">
                            </div>
                            <div class="col-lg-4">
                                <div class="kt-margin-t-25  float-left">
                                    <label class="col-form-label float-left kt-padding-r-5">حالة النشر: </label>
                                    <span class="kt-switch kt-switch--outline kt-switch--icon kt-switch--success kt-margin-r-50"  >
                                        <label>
                                            <input type="checkbox" name="status" value="1" {{ old('status',$info->status)== 1 ? 'checked' : '' }}>
                                            <span></span>
                                        </label>
                                    </span>
                                </div>
                            </div>
                        </div>   
                        <div class="form-group row">
                            <div class="col-lg-12">
                                <label class="">ارفق بوستر الاعلان:</label>
                                <input type="file" class="form-control" name="poster" value="{{ old('poster',$info->image) }}" placeholder="بوستر الاعلان">
                            </div>
                        </div> 
                        <div class="form-group row">
                            <div class="col-lg-12">
                                <label class="">وصف الاعلان:</label>
                                <textarea class="summernote" name="description" id="kt_summernote_1">{!! old('description',$info->description) !!}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <div class="row">
                                <div class="col-lg-6 kt-align-right">
                                    <button type="submit" class="btn btn-brand save">حفظ</button>
                                    <a href="{{ route('admin.advertisings.view') }}" class="btn btn-secondary">الغاء</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <!--end::Form-->
            </div>
            <!--end::Portlet-->
        </div>
    </div>
    <!-- End:: Content -->
@stop

@section('js')
    <!--begin::Page Scripts(used by this page) -->
    <script src="assets/admin/js/pages/crud/forms/editors/summernote.js" type="text/javascript"></script>
    <script src="assets/admin/general/js/scripts/advertisings.js" type="text/javascript"></script>
@stop
