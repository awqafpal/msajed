@if(auth()->user()->can('admin.properties.edit') || auth()->user()->can('admin.properties.delete'))
    @can('admin.properties.edit')
        <a href="{{ route('admin.properties.edit', ['id' => $id]) }}" class="btn btn-outline-success btn-elevate-hover btn-circle btn-icon btn-sm" title="تعديل">
            <i class="la la-edit"></i>
        </a>
    @endcan
    @can('admin.properties.delete')
        <a href="javascript:;" data-url="{{ route('admin.properties.delete', ['id' => $id]) }}" class="btn btn-outline-danger btn-elevate-hover btn-circle btn-icon btn-sm delete_btn" title="حذف">
            <i class="la la-trash"></i>
        </a>
    @endcan
@endif
