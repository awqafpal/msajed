@if($status == 'province_approved')
    <span  class="btn btn-success btn-elevate btn-icon-sm"  aria-haspopup="true" aria-expanded="false" style="cursor: default">
        <i class="fa fa-check-circle"></i>
        معتمد - مدير مديرية
    </span>
@elseif($status == 'head_approved')
    <span  class="btn btn-success btn-elevate btn-icon-sm"  aria-haspopup="true" aria-expanded="false" style="cursor: default">
        <i class="fa fa-check-circle"></i>
        معتمد - رئيس قسم
    </span>
@elseif($status == 'inspector_approved')
    <span  class="btn btn-success btn-elevate btn-icon-sm"  aria-haspopup="true" aria-expanded="false" style="cursor: default">
        <i class="fa fa-check-circle"></i>
        معتمد - مفتش
    </span>
@else
    <span  class="btn btn-warning btn-elevate btn-icon-sm"  aria-haspopup="true" aria-expanded="false" style="cursor: default">
        <i class="fa fa-exclamation-circle"></i>
        بانتظار الاعتماد
    </span>
@endif
