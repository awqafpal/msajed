@if(auth()->user()->can('admin.inspectionSchedules.show') || auth()->user()->can('admin.inspectionSchedules.edit') || auth()->user()->can('admin.inspectionSchedules.delete'))
    @can('admin.inspectionSchedules.show')
        <a href="{{ route('admin.inspectionSchedules.show', ['id' => $id]) }}" class="btn btn-outline-info btn-elevate-hover btn-circle btn-icon btn-sm" title="عرض وطباعة">
            <i class="la la-eye"></i>
        </a>
    @endcan
@can('admin.inspectionSchedules.edit')
        <a href="{{ route('admin.inspectionSchedules.edit', ['id' => $id]) }}" class="btn btn-outline-success btn-elevate-hover btn-circle btn-icon btn-sm" title="تعديل واعتماد">
            <i class="la la-edit"></i>
        </a>
    @endcan
    @can('admin.inspectionSchedules.delete')
        <a href="javascript:;" data-url="{{ route('admin.inspectionSchedules.delete', ['id' => $id]) }}" class="btn btn-outline-danger btn-elevate-hover btn-circle btn-icon btn-sm delete_btn" title="حذف">
            <i class="la la-trash"></i>
        </a>
    @endcan
@endif
