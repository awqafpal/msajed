<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>طباعة جدول تفتيش</title>

    <!-- Normalize or reset CSS with your favorite library -->
    <link rel="stylesheet" href="{{ asset('assets/admin/print/normalize.css') }}">

    <!-- Load paper.css for happy printing -->
    <link rel="stylesheet" href="{{ asset('assets/admin/print/paper/paper.css') }}">

    <!-- Load custom.css for happy printing -->
    <link rel="stylesheet" href="{{ asset('assets/admin/print/custom.css') }}">

    <!-- Set page size here: A5, A4 or A3 -->
    <!-- Set also "landscape" if you need -->
    <style>@page {
            size: A4
        }

        body * {
            font-family: 'DroidArabicNaskhRegular';
            font-weight: normal;
            font-style: normal;
        }

        h3, h4 {
            margin: 0;
            font-size: 14px;
        }

        p {
            margin: 0
        }

        .table tr td {
            padding: 2px 0;
            font-size: 13px;
        }

        .conditions p {
            font-size: 18.12px
        }

        .contract span {
            font-size: 18px
        }

        .contract tr td {
            padding: 0.25em 0;
        }
    </style>
</head>

<!-- Set "A5", "A4" or "A3" for class name -->
<!-- Set also "landscape" if you need -->
<body class="A4">

<!-- Each sheet element should have the class "sheet" -->
<!-- "padding-**mm" is optional: you can set 10, 15, 20 or 25 -->

<article>
    <section class="sheet padding-15mm">
        <table style="width: 100%;float: right;" class="table">
            <tr>
                <td>
                    <header>
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 40%;vertical-align: top">
                                    <h3 style="margin: 0px;font-size: 13px;">دولــــــة فلســــطين</h3>
                                    <h3 style="margin: 0px;font-size: 13px;">وزارة الأوقاف والشؤون الدينية</h3>
                                 <h3 style="margin: 0px;font-size: 13px;">الإدارة العامة للمديريات</h3>
                                    <h3 style="margin: 0px;font-size: 13px;">
                                        مديرية أوقاف : {{ $info->user->region->province->name }} </h3>
                                </td>
                                <td style="width: 20%;text-align:center;">
                                    <img src="{{ asset('assets/admin/print/paper/logo.bmp') }}" width="50%">
                                </td>
                                <td style="width: 40%;vertical-align: top">
                                    <div style="float: right;margin-right:65px;">
                                        <h4 style="margin:0;font-size: 13px;text-align:right"></h4>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </header>
                    <hr style="background-color: rgb(0, 0, 0);height: 3px;margin-bottom: 0px"/>
                    <hr style="margin-top: 0px"/>
                    <table style="width: 100%; float:right">
                        <tr>
                            <td class="gray">
                                <h4 style="margin:0;font-size: 12px;text-align:center">جدول تفتيش المساجد </h4>
                            </td>
                        </tr>
                    </table>
                    <br>
                    <table style="width: 100%; float:right">
                        <tr>
                            <td style="width: 35%; float:right">
                                <input type="hidden" id="inspection_id" value="{{$info->id}}"/>
                                <h4 style="margin:0;font-size: 12px;text-align:right">المفتش : {{$info->user->name}}</h4>
                            </td>
                            <td style="width: 30%; float:right">
                                <h4 style="margin:0;font-size: 12px;text-align:center">البريد الالكتروني : {{$info->user->email ?? 'غير مدخل'}}</h4>
                            </td>
                            <td style="width: 35%; float:right">
                                <h4 style="margin:0;font-size: 12px;text-align:left">رقم الجوال : {{$info->user->phone_number ?? 'غير مدخل'}}</h4>
                            </td>
                        </tr>
                    </table>
                    <br>
                    <table style="width:100%" class="custom-table1" id="inspection_schedule_times_table">
                        <thead>
                            <tr>
                                <th rowspan="2" class="text-center width-5-percent">#</th>
                                <th rowspan="2" class="text-center width-10-percent">اليوم</th>
                                <th rowspan="2" class="text-center width-10-percent">التاريخ</th>
                                <th colspan="5" class="text-center">أسماء المساجد المقرر زيارتها خلال اليوم</th>
                            </tr>
                            <tr>
                                <th class="text-center width-15-percent" >فجراً</th>
                                <th class="text-center width-15-percent" >ظهراً</th>
                                <th class="text-center width-15-percent" >عصراً</th>
                                <th class="text-center width-15-percent">مغرباً</th>
                                <th class="text-center width-15-percent">عشاءً</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($times as $time)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$time->translatedFormat('l')}}</td>
                                    <td>{{$time->format('Y-m-d')}}</td>
                                    <td class="text-center width-15-percent"  id="fajr-{{$time->format('Y-m-d')}}"></td>
                                    <td class="text-center width-15-percent"  id="dhuhr-{{$time->format('Y-m-d')}}"></td>
                                    <td class="text-center width-15-percent"  id="asr-{{$time->format('Y-m-d')}}"></td>
                                    <td class="text-center width-15-percent"  id="maghrib-{{$time->format('Y-m-d')}}"></td>
                                    <td class="text-center width-15-percent"  id="isha-{{$time->format('Y-m-d')}}"></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </td>
            </tr>
        </table>
    </section>
</article>
<div class="pba"></div>


</body>

<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script>
    $(document).ready(function () {
        var id = $('#inspection_id').val();
        var url = '{{ route("admin.inspectionScheduleTimes.ajax", ":id") }}';
        url = url.replace(':id', id);
        console.log(url);
        $.ajax({
            url: url,
            dataType:"json",
            type: "get",
            success: function(response){
                let data = response.data ;
                console.log(response.data);
                $(data).each(function( index, element) {
                    if(element.fajr){
                        var fajr = jQuery.parseJSON(element.fajr);
                        $('#fajr-'+ element.date).html(fajr.name);
                    }
                    if(element.dhuhr){
                        var dhuhr = jQuery.parseJSON(element.dhuhr);
                        $('#dhuhr-'+ element.date).html(dhuhr.name);
                    }
                    if(element.asr){
                        var asr = jQuery.parseJSON(element.asr);
                        $('#asr-'+ element.date).html(asr.name);
                    }
                    if(element.maghrib){
                        var maghrib = jQuery.parseJSON(element.maghrib);
                        $('#maghrib-'+ element.date).html(maghrib.name);
                    }
                    if(element.isha){
                        var isha = jQuery.parseJSON(element.isha);
                        $('#isha-'+ element.date).html(isha.name);
                    }
                });
            }
        });
    });
</script>
</html>
