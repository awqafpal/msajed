@extends('admin.layout.master')

@section('title')
    عرض مواعيد جدول التفتيش
@stop

@section('css')
<style>
    .width-5-percent{
        width: 5%;
    }
    .width-13-percent{
        width: 13%;
    }
    .width-20-percent{
        width: 20%;
    }
    .width-15-percent{
        width: 15%;
    }
    .width-10-percent{
        width: 10%;
    }
    .width-7-percent{
        width: 7%;
    }
</style>
@stop

@section('subheader')
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    وزارة الأوقاف والشؤون الدينية </h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="{{ route('admin.dashboard.view') }}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="{{ route('admin.dashboard.view') }}" class="kt-subheader__breadcrumbs-link">
                        الرئيسية
                    </a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="{{ route('admin.inspectionSchedules.view') }}" class="kt-subheader__breadcrumbs-link">
                        جداول التفتيش
                    </a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="{{ route('admin.inspectionSchedules.show', ['id' => $info->id]) }}" class="kt-subheader__breadcrumbs-link">
                        عرض مواعيد جدول التفتيش
                    </a>
                </div>
            </div>
        </div>
    </div>
@stop

@section('content')
    <!-- begin:: Content -->
    <div class="row">
        <div class="col-lg-12">
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            عرض مواعيد جدول التفتيش
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-wrapper">
                            <div class="kt-portlet__head-actions">
                                @can('admin.inspectionSchedules.print')
                                <a href="{{ route('admin.inspectionSchedules.print', ['id' => $info->id]) }}" target="_blank" class="btn btn-info btn-elevate btn-icon-sm">
                                    <i class="fa fa-print"></i>
                                    طباعة
                                </a>
                                @endcan
                            </div>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div class="form-group row">
                        <input value="{{ $info->id }}" type="hidden" id="schedule_id" >
                        <div class="col-lg-4">
                            <label class="">السنة-الشهر:</label>
                            <input value="{{  $info->month_year }}" type="text" class="form-control" disabled >
                        </div>
                        <div class="col-lg-4">
                            <label class="">المفتش:</label>
                            <input type="text" class="form-control"  value="{{$info->user->name}}"  disabled>
                        </div>
                    </div>

                    <!--begin: Datatable -->
                    @include('admin.inspection-schedules.includes.schedule-times-content')
                    <!--end: Datatable -->
                </div>
            </div>
            <!--end::Portlet-->
        </div>
    </div>
    <!-- End:: Content -->
@stop

@section('js')
    <!--begin::Page Scripts(used by this page) -->
    <script src="assets/admin/general/js/scripts/inspection-schedule-times.js" type="text/javascript"></script>
@stop

@section('scripts')

@stop
