<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>طباعة جدول تفتيش</title>

    <!-- Normalize or reset CSS with your favorite library -->
    <link rel="stylesheet" href="{{ asset('assets/admin/print/normalize.css') }}">

    <!-- Load paper.css for happy printing -->
    <link rel="stylesheet" href="{{ asset('assets/admin/print/paper/paper.css') }}">

    <!-- Load custom.css for happy printing -->
    <link rel="stylesheet" href="{{ asset('assets/admin/print/custom.css') }}">

    <!-- Set page size here: A5, A4 or A3 -->
    <!-- Set also "landscape" if you need -->
    <style>@page {
            size: A4
        }

        body * {
            font-family: 'DroidArabicNaskhRegular';
            font-weight: normal;
            font-style: normal;
        }

        h3, h4 {
            margin: 0;
            font-size: 14px;
        }

        p {
            margin: 0
        }

        .table tr td {
            padding: 2px 0;
            font-size: 13px;
        }

        .conditions p {
            font-size: 18.12px
        }

        .contract span {
            font-size: 18px
        }

        .contract tr td {
            padding: 0.25em 0;
        }
    </style>
</head>

<!-- Set "A5", "A4" or "A3" for class name -->
<!-- Set also "landscape" if you need -->
<body class="A4">

<!-- Each sheet element should have the class "sheet" -->
<!-- "padding-**mm" is optional: you can set 10, 15, 20 or 25 -->

<article>
    <section class="sheet padding-15mm">
        <table style="width: 100%;float: right;" class="table">
            <tr>
                <td>
                    <header>
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 40%;vertical-align: top">
                                    <h3 style="margin: 0px;font-size: 13px;">دولــــــة فلســــطين</h3>
                                    <h3 style="margin: 0px;font-size: 13px;">وزارة الأوقاف والشؤون الدينية</h3>
                                 <h3 style="margin: 0px;font-size: 13px;">الإدارة العامة للمديريات</h3>
                                    <h3 style="margin: 0px;font-size: 13px;">
                                        مديرية أوقاف : </h3>
                                </td>
                                <td style="width: 20%;text-align:center;">
                                    <img src="{{ asset('assets/admin/print/paper/logo.bmp') }}" width="50%">
                                </td>
                                <td style="width: 40%;vertical-align: top">
                                    <div style="float: right;margin-right:65px;">
                                        <h4 style="margin:0;font-size: 13px;text-align:right"></h4>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </header>
                    <hr style="background-color: rgb(0, 0, 0);height: 3px;margin-bottom: 0px"/>
                    <hr style="margin-top: 0px"/>
                    <table style="width: 75%; float:right">
                        <tr>
                            <td>
                                <h4 style="margin:0;font-size: 13px;text-align:right">موضوع الطلب : </h4>
                            </td>
                        </tr>
                        <tr>
                            <td class="gray">
                                <h4 style="margin:0;font-size: 13px;text-align:right">بيانات الطلب : </h4>
                            </td>
                        </tr>
                    </table>
                    <table style="width: 20%; float:left; margin-top:5px">
                        <tr>
                            <td class="gray">
                                <h4 style="margin:0;font-size: 13px;text-align:center">تاريخ تحرير الطلب </h4>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <h4 style="margin:0;font-size: 13px;text-align:center">  </h4>
                            </td>
                        </tr>
                    </table>
                    <br>
                    <table style="width:100%" class="custom-table" >
                        <tr>
                            <th width="15%" colspan="1">مقدم الطلب</th>
                            <td width="35%" colspan="3"></td>
                            <th width="15%" colspan="1">رقم الطلب</th>
                            <td width="35%" colspan="3"></td>
                        </tr>
                        <tr>
                            <th width="15%" colspan="1">نوع العقار</th>
                            <td width="35%" colspan="3"></td>
                            <th width="15%" colspan="1">المؤجر للسيد</th>
                            <td width="35%" colspan="3"></td>
                        </tr>

                    </table>
                    <table style="width: 100%; float:right; margin-top:5px">
                        <tr>
                            <td class="gray">
                                <h4 style="margin:0;font-size: 13px;text-align:right">مشروحات رئيس القسم :</h4>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </section>
</article>
<div class="pba"></div>


</body>

<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script>
    $(document).ready(function () {
        window.print();
    });
</script>
</html>
