@extends('admin.layout.master')

@section('title')
    ضبط مواعيد جدول التفتيش
@stop

@section('css')
<style>
    .width-5-percent{
        width: 5%;
    }
    .width-13-percent{
        width: 13%;
    }
    .width-20-percent{
        width: 20%;
    }
    .width-15-percent{
        width: 15%;
    }
    .width-10-percent{
        width: 10%;
    }
    .width-7-percent{
        width: 7%;
    }
</style>
@stop

@section('subheader')
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    وزارة الأوقاف والشؤون الدينية </h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="{{ route('admin.dashboard.view') }}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="{{ route('admin.dashboard.view') }}" class="kt-subheader__breadcrumbs-link">
                        الرئيسية
                    </a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="{{ route('admin.inspectionSchedules.view') }}" class="kt-subheader__breadcrumbs-link">
                        جداول التفتيش
                    </a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="{{ route('admin.inspectionSchedules.edit', ['id' => $info->id]) }}" class="kt-subheader__breadcrumbs-link">
                        ضبط مواعيد جدول التفتيش
                    </a>
                </div>
            </div>
        </div>
    </div>
@stop

@section('content')
    <!-- begin:: Content -->
    <div class="row">
        <div class="col-lg-12">
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            ضبط مواعيد جدول التفتيش
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-wrapper">
                            <div class="kt-portlet__head-actions">
                                @if(in_array(auth()->user()->user_type_id , [1,3,13] ))
                                    @if($info->status == 'not_approved')
                                        <button type="button" class="btn btn-brand btn-elevate btn-icon-sm hideWhenApprove" data-toggle="modal" data-target="#kt_modal_mosque_visit">إضافة زيارة</button>
                                        <a href="#" class="btn btn-info btn-elevate btn-icon-sm approve_schedule hideWhenApprove" data-url="{{route('admin.inspectionSchedules.approval', ['id' => $info->id])}}">
                                            اعتماد جدول التفتيش
                                        </a>
                                    @else
                                        <span  class="btn btn-success btn-elevate btn-icon-sm"  aria-haspopup="true" aria-expanded="false" style="cursor: default">
                                            <i class="fa fa-check-circle"></i>
                                            @if($info->status == 'province_approved') معتمد - مدير مديرية
                                            @elseif($info->status == 'head_approved')  معتمد - رئيس قسم
                                            @elseif($info->status == 'inspector_approved') معتمد - مفتش
                                            @endif
                                        </span>
                                    @endif
                                @elseif(in_array(auth()->user()->user_type_id , [4,8,9,12,13] ))
                                    @if($info->has_approving)
                                        <span  class="btn btn-success btn-elevate btn-icon-sm "  aria-haspopup="true" aria-expanded="false" style="cursor: default">
                                            <i class="fa fa-check-circle"></i>
                                                معتمد
                                        </span>
                                    @else
                                        <button type="button" class="btn btn-brand btn-elevate btn-icon-sm hideWhenApprove" data-toggle="modal" data-target="#kt_modal_mosque_visit">إضافة زيارة</button>
                                        <a href="#" class="btn btn-info btn-elevate btn-icon-sm approve_schedule hideWhenApprove" data-url="{{route('admin.inspectionSchedules.approval', ['id' => $info->id])}}">
                                            اعتماد جدول التفتيش
                                        </a>
                                    @endif
                                @else
                                    @include('admin.inspection-schedules.parts.approval-status',['status' => $info->status])
                                @endif
                                @can('admin.inspectionSchedules.print')
                                <a href="{{ route('admin.inspectionSchedules.print', ['id' => $info->id]) }}" target="_blank" class="btn btn-info btn-elevate btn-icon-sm">
                                    <i class="fa fa-print"></i>
                                    طباعة
                                </a>
                                @endcan
                            </div>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div class="form-group row">
                        <input value="{{ $info->id }}" type="hidden" id="schedule_id" >
                        <div class="col-lg-4">
                            <label class="">السنة-الشهر:</label>
                            <input value="{{  $info->month_year }}" type="text" class="form-control" disabled >
                        </div>
                        <div class="col-lg-4">
                            <label class="">المفتش:</label>
                            <input type="text" class="form-control"  value="{{$info->user->name}}"  disabled>
                        </div>
                    </div>

                    <!--begin: Datatable -->
                    @include('admin.inspection-schedules.includes.schedule-times-content')
                    <!--end: Datatable -->
                </div>
            </div>
            <!--end::Portlet-->
        </div>
    </div>
    <!-- End:: Content -->
@stop
@section('modal')
    @include('admin.inspection-schedules.includes.add-inspection-time')
@stop
@section('js')
    <!--begin::Page Scripts(used by this page) -->
    <script src="assets/admin/general/js/scripts/inspection-schedule-times.js" type="text/javascript"></script>
@stop

@section('scripts')
    <!--begin::Page Scripts(used by this page) -->
    <script>
    $('.date2').datepicker({
        language: 'ar',
        todayHighlight: true,
        clearBtn: true,
        todayBtn: 'linked',
        orientation: 'top',
        // orientation: 'bottom',
        autoclose: true,
        format: 'yyyy-mm-dd',
        minDate: $(this).data('min'),
        maxDate: $(this).data('max'),
    });
    </script>
@stop
