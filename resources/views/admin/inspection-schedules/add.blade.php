@extends('admin.layout.master')

@section('title')
    إضافة جدول تفتيش
@stop

@section('css')

@stop

@section('subheader')
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    وزارة الأوقاف والشؤون الدينية </h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="{{ route('admin.dashboard.view') }}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="{{ route('admin.dashboard.view') }}" class="kt-subheader__breadcrumbs-link">
                        الرئيسية
                    </a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="{{ route('admin.inspectionSchedules.view') }}" class="kt-subheader__breadcrumbs-link">
                        جداول التفتيش
                    </a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="{{ route('admin.inspectionSchedules.add') }}" class="kt-subheader__breadcrumbs-link">
                        إضافة جدول تفتيش
                    </a>
                </div>
            </div>
        </div>
    </div>
@stop

@section('content')
    <!-- begin:: Content -->
    <div class="row">
        <div class="col-lg-12">
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            إضافة جدول تفتيش
                        </h3>
                    </div>
                </div>
                <!--begin::Form-->
                <form class="kt-form kt-form--label-right" id="frmAdd" method="post" action="{{ route('admin.inspectionSchedules.store') }}" encrypt="multipart/form-data">
                    <div class="kt-portlet__body">
                        <div class="form-group row">
                            <div class="col-lg-4">
                                <label class="">السنة-الشهر:</label>
                                <input type="month" class="form-control" name="month_year" value="{{ old('month_year') }}" placeholder="السنة-الشهر">
                            </div> 
                            <div class="col-lg-4">
                                <label class="">المفتش:</label>
                                <select class="form-control kt-selectpicker" name="user_id" data-live-search="true" data-title="اختر قيمة">
                                    @foreach ($users as $user)
                                        <option value="{{$user->id}}">{{ $user->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>   
                        
                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <div class="row">
                                <div class="col-lg-6 kt-align-right">
                                    <button type="submit" class="btn btn-brand save">تهيئة مواقيت جدول التفتيش</button>
                                    <a href="{{ route('admin.inspectionSchedules.view') }}" class="btn btn-secondary">الغاء</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <!--end::Form-->
            </div>
            <!--end::Portlet-->
        </div>
    </div>
    <!-- End:: Content -->
@stop

@section('js')
    <script src="assets/admin/general/js/scripts/inspection-schedules.js" type="text/javascript"></script>
@stop
  
