<!-- Modal -->
<div class="modal fade" id="kt_modal_mosque_visit" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">تحديد زيارة مسجد</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <!--begin::Form-->
            <form class="kt-form kt-form--label-right" id="frmAdd" method="post" action="{{ route('admin.inspectionScheduleTimes.store') }}">
                @csrf
                <input value="{{ $info->id }}" type="hidden" name="inspection_schedule_id" >
                <div class="modal-body">
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <label class="">ميقات الصلاة : </label>
                            <div class="kt-radio-inline">
                                <label class="kt-radio kt-radio kt-radio--success">
                                    <input type="radio" name="prayer" value="fajr" checked>الفجر
                                    <span></span>
                                </label>
                                <label class="kt-radio kt-radio kt-radio--success">
                                    <input type="radio" name="prayer" value="dhuhr">الظهر
                                    <span></span>
                                </label>
                                <label class="kt-radio kt-radio kt-radio--success">
                                    <input type="radio" name="prayer" value="asr">العصر
                                    <span></span>
                                </label>
                                <label class="kt-radio kt-radio kt-radio--success">
                                    <input type="radio" name="prayer" value="maghrib">المغرب
                                    <span></span>
                                </label>
                                <label class="kt-radio kt-radio kt-radio--success">
                                    <input type="radio" name="prayer" value="isha">العشاء
                                    <span></span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-4">
                            <label class="">التاريخ:</label>
                            <input type="text" class="form-control date" name="date" value="{{ old('date') }}" placeholder="التاريخ" readonly data-min="{{ $firstDayofMonth }}" data-max="{{ $lastDayofMonth }}">
                        </div>
                        <div class="col-lg-8">
                            <label class="">المسجد:</label>
                            <select class="form-control kt-selectpicker searchable" data-live-search="true" id="mosque_id" name="mosque_id">
                                <option value="" selected> اختر المسجد </option>
                                @foreach($mosques as $mosque)
                                    <option value="{{$mosque->id}}"> {{$mosque->name}} </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">الغاء</button>
                    <button type="submit" class="btn btn-brand save">حفظ</button>
                </div>
            </form>
        </div>
    </div>
</div>
