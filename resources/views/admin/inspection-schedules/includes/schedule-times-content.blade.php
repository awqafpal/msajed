<table class="table table-striped- table-bordered table-hover" id="inspection_schedule_times_table">
    <thead>
        <tr>
            <th rowspan="2" class="text-center width-5-percent">#</th>
            <th rowspan="2" class="text-center width-10-percent">اليوم</th>
            <th rowspan="2" class="text-center width-10-percent">التاريخ</th>
            <th colspan="5" class="text-center">أسماء المساجد المقرر زيارتها خلال اليوم</th>
        </tr>
        <tr>
            <th class="text-center width-15-percent" >فجراً</th>
            <th class="text-center width-15-percent" >ظهراً</th>
            <th class="text-center width-15-percent" >عصراً</th>
            <th class="text-center width-15-percent">مغرباً</th>
            <th class="text-center width-15-percent">عشاءً</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($times as $time)
            <tr>
                <td>{{$loop->iteration}}</td>
                <td>{{$time->translatedFormat('l')}}</td>
                <td>{{$time->format('Y-m-d')}}</td>
                <td class="text-center width-15-percent"  id="fajr-{{$time->format('Y-m-d')}}"></td>
                <td class="text-center width-15-percent"  id="dhuhr-{{$time->format('Y-m-d')}}"></td>
                <td class="text-center width-15-percent"  id="asr-{{$time->format('Y-m-d')}}"></td>
                <td class="text-center width-15-percent"  id="maghrib-{{$time->format('Y-m-d')}}"></td>
                <td class="text-center width-15-percent"  id="isha-{{$time->format('Y-m-d')}}"></td>
            </tr>
        @endforeach
    </tbody>
</table>
