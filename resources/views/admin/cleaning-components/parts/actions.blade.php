@if(auth()->user()->can('admin.cleaningComponents.edit') || auth()->user()->can('admin.cleaningComponents.delete'))
    @can('admin.cleaningComponents.edit')
        <a href="{{ route('admin.cleaningComponents.edit', ['id' => $id]) }}" class="btn btn-outline-success btn-elevate-hover btn-circle btn-icon btn-sm" title="تعديل">
            <i class="la la-edit"></i>
        </a>
    @endcan
    @can('admin.cleaningComponents.delete')
        <a href="javascript:;" data-url="{{ route('admin.cleaningComponents.status', ['id' => $id]) }}" class="btn btn-outline-danger btn-elevate-hover btn-circle btn-icon btn-sm toggle-status" title="تغيير">
            <i class="la {{$status == 0 ? 'la-lock' : 'la-unlock' }} "></i>
        </a>
    @endcan
@endif
