@if(auth()->user()->can('admin.jobs.edit') || auth()->user()->can('admin.jobs.delete'))
    @can('admin.jobs.edit')
        <a href="{{ route('admin.jobs.edit', ['id' => $id]) }}" class="btn btn-outline-success btn-elevate-hover btn-circle btn-icon btn-sm" title="تعديل">
            <i class="la la-edit"></i>
        </a>
    @endcan
    @can('admin.jobs.delete')
        <a href="javascript:;" data-url="{{ route('admin.jobs.delete', ['id' => $id]) }}" class="btn btn-outline-danger btn-elevate-hover btn-circle btn-icon btn-sm delete_btn" title="حذف">
            <i class="la la-trash"></i>
        </a>
    @endcan
@endif
