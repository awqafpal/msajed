@if(auth()->user()->can('admin.news.edit') || auth()->user()->can('admin.news.delete'))
    @can('admin.news.edit')
        <a href="{{ route('admin.news.edit', ['id' => $id]) }}" class="btn btn-outline-success btn-elevate-hover btn-circle btn-icon btn-sm" title="تعديل">
            <i class="la la-edit"></i>
        </a>
    @endcan
    @can('admin.news.delete')
        <a href="javascript:;" data-url="{{ route('admin.news.delete', ['id' => $id]) }}" class="btn btn-outline-danger btn-elevate-hover btn-circle btn-icon btn-sm delete_btn" title="حذف">
            <i class="la la-trash"></i>
        </a>
    @endcan
@endif
