@extends('admin.layout.master')

@section('title')
    بروفايل مستخدم
@stop

@section('css')

@stop

@section('subheader')
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    وزارة الأوقاف والشؤون الدينية </h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="{{ route('admin.dashboard.view') }}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="{{ route('admin.dashboard.view') }}" class="kt-subheader__breadcrumbs-link">
                        الرئيسية
                    </a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="{{ route('admin.users.view') }}" class="kt-subheader__breadcrumbs-link">
                        المستخدمين
                    </a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="{{ route('admin.users.show', ['id' => $info->id]) }}" class="kt-subheader__breadcrumbs-link">
                        بروفايل مستخدم
                    </a>
                </div>
            </div>
        </div>
    </div>
@stop


@section('content')
<div class="kt-grid kt-grid--desktop kt-grid--ver kt-grid--ver-desktop kt-app">

    <!--Begin:: App Aside Mobile Toggle-->
    <button class="kt-app__aside-close" id="kt_user_profile_aside_close">
        <i class="la la-close"></i>
    </button>

    <!--End:: App Aside Mobile Toggle-->

    <!--Begin:: App Aside-->
    <div class="kt-grid__item kt-app__toggle kt-app__aside" id="kt_user_profile_aside">

        <!--begin:: Widgets/Applications/User/Profile1-->
        <div class="kt-portlet ">
            <div class="kt-portlet__head  kt-portlet__head--noborder">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                    </h3>
                </div>
            </div>
            <div class="kt-portlet__body kt-portlet__body--fit-y">

                <!--begin::Widget -->
                <div class="kt-widget kt-widget--user-profile-1">
                    <div class="kt-widget__head">
                        <div class="kt-widget__media">
                            <img src="{{asset('assets/admin/media/users/default.jpg')}}" alt="image">
                        </div>
                        <div class="kt-widget__content">
                            <div class="kt-widget__section">
                                <a href="javascript:void(0)" class="kt-widget__username">
                                    {{$info->name}}
                                    <i class="flaticon2-correct {{$info->status ? 'kt-font-success':'kt-font-danger' }} "></i>
                                </a>
                                <span class="kt-widget__subtitle">
                                    {{$info->type_name}}
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="kt-widget__body">
                        <div class="kt-widget__content">
                            <div class="kt-widget__info">
                                <span class="kt-widget__label">البريد الالكتروني:</span>
                                <a href="#" class="kt-widget__data">{{$info->email ?? 'غير مدخل'}}</a>
                            </div>
                            <div class="kt-widget__info">
                                <span class="kt-widget__label">رقم الموبايل:</span>
                                <a href="#" class="kt-widget__data">{{$info->mobile ?? 'غير مدخل'}}</a>
                            </div>
                            <div class="kt-widget__info">
                                <span class="kt-widget__label">رقم الواتس:</span>
                                <span class="kt-widget__data">{{$info->whatsapp_number ?? 'غير مدخل'}}</span>
                            </div>
                        </div>
                        <div class="kt-widget__items">
                            
                            <a href="javascript:void(0)" class="kt-widget__item kt-widget__item--active">
                                <span class="kt-widget__section">
                                    <span class="kt-widget__icon">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                                <path d="M12,11 C9.790861,11 8,9.209139 8,7 C8,4.790861 9.790861,3 12,3 C14.209139,3 16,4.790861 16,7 C16,9.209139 14.209139,11 12,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
                                                <path d="M3.00065168,20.1992055 C3.38825852,15.4265159 7.26191235,13 11.9833413,13 C16.7712164,13 20.7048837,15.2931929 20.9979143,20.2 C21.0095879,20.3954741 20.9979143,21 20.2466999,21 C16.541124,21 11.0347247,21 3.72750223,21 C3.47671215,21 2.97953825,20.45918 3.00065168,20.1992055 Z" fill="#000000" fill-rule="nonzero"></path>
                                            </g>
                                        </svg> </span>
                                    <span class="kt-widget__desc">
                                        بيانات الموظف
                                    </span>
                                </span>
                            </a>
                            
                        </div>
                    </div>
                </div>

                <!--end::Widget -->
            </div>
        </div>

        <!--end:: Widgets/Applications/User/Profile1-->
    </div>

    <!--End:: App Aside-->

    <!--Begin:: App Content-->
    <div class="kt-grid__item kt-grid__item--fluid kt-app__content">
        <div class="row">
            <div class="col-xl-12">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">بيانات الموظف <small></small></h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <div class="kt-section kt-section--first">
                            <div class="kt-section__body">
                                <div class="row">
                                    <label class="col-xl-3"></label>
                                    <div class="col-lg-9 col-xl-6">
                                        <h3 class="kt-section__title kt-section__title-sm">البيانات الشخصية:</h3>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">اسم المستخدم</label>
                                    <div class="col-lg-9 col-xl-6">
                                        <input class="form-control" readonly type="text" value="{{$info->username}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">اسم الموظف</label>
                                    <div class="col-lg-9 col-xl-6">
                                        <input class="form-control" readonly type="text" value="{{$info->name}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">رقم الهوية</label>
                                    <div class="col-lg-9 col-xl-6">
                                        <input class="form-control" readonly type="text" value="{{$info->identity_number}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">المنطقة</label>
                                    <div class="col-lg-9 col-xl-6">
                                        <input class="form-control" readonly type="text" value="{{$info->region_name}}">
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-xl-3"></label>
                                    <div class="col-lg-9 col-xl-6">
                                        <h3 class="kt-section__title kt-section__title-sm">بيانات التواصل:</h3>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">البريد الإلكتروني</label>
                                    <div class="col-lg-9 col-xl-6">
                                        <div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text"><i class="la la-at"></i></span></div>
                                            <input class="form-control" readonly type="text" value="{{$info->email}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">رقم الموبايل</label>
                                    <div class="col-lg-9 col-xl-6">
                                        <div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text"><i class="la la-phone"></i></span></div>
                                            <input class="form-control" readonly type="text" value="{{$info->mobile}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">رقم الواتساب</label>
                                    <div class="col-lg-9 col-xl-6">
                                        <div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text"><i class="la la-phone"></i></span></div>
                                            <input class="form-control" readonly type="text" value="{{$info->whatsapp_number}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-xl-3"></label>
                                    <div class="col-lg-9 col-xl-6">
                                        <h3 class="kt-section__title kt-section__title-sm">البيانات الوظيفة:</h3>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">الوظيفة</label>
                                    <div class="col-lg-9 col-xl-6">
                                        <input class="form-control" readonly type="text" value="{{$info->type_name}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">حالة الموظف</label>
                                    <div class="col-lg-9 col-xl-6">
                                        <input class="form-control" readonly type="text" value="{{$info->employee_status_name}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">الرقم الوظيفي</label>
                                    <div class="col-lg-9 col-xl-6">
                                        <input class="form-control" readonly type="text" value="{{$info->job_no}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">المسمى الوظيفي</label>
                                    <div class="col-lg-9 col-xl-6">
                                        <input class="form-control" readonly type="text" value="{{$info->job_title}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">تاريخ التعيين</label>
                                    <div class="col-lg-9 col-xl-6">
                                        <input class="form-control" readonly type="text" value="{{$info->hiring_date}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">بند التعيين</label>
                                    <div class="col-lg-9 col-xl-6">
                                        <input class="form-control" readonly type="text" value="{{$info->hiring_type_name}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">المؤهل العلمي</label>
                                    <div class="col-lg-9 col-xl-6">
                                        <input class="form-control" readonly type="text" value="{{$info->qualification_name}}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--End:: App Content-->
</div>
@stop

@section('js')
    <script src="assets/admin/general/js/scripts/users.js" type="text/javascript"></script>
@stop
