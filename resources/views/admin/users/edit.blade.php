@extends('admin.layout.master')

@section('title')
    تعديل مستخدم
@stop

@section('css')

@stop

@section('subheader')
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    وزارة الأوقاف والشؤون الدينية </h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="{{ route('admin.dashboard.view') }}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="{{ route('admin.dashboard.view') }}" class="kt-subheader__breadcrumbs-link">
                        الرئيسية
                    </a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="{{ route('admin.users.view') }}" class="kt-subheader__breadcrumbs-link">
                        المستخدمين
                    </a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="{{ route('admin.users.edit', ['id' => $info->id_hash]) }}" class="kt-subheader__breadcrumbs-link">
                        تعديل مستخدم
                    </a>
                </div>
            </div>
        </div>
    </div>
@stop


@section('content')
    <!-- begin:: Content -->
    <div class="row">
        <div class="col-lg-12">
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            تعديل مستخدم
                        </h3>
                    </div>
                </div>
                <!--begin::Form-->
                <form class="kt-form kt-form--label-right" id="frmEdit" method="post" action="{{ route('admin.users.edit', ['id' => $info->id_hash]) }}">
                    <div class="kt-portlet__body">
                        <div class="form-group row">
                            <div class="col-xl-12 text-center">
                                <div class="kt-avatar kt-avatar--outline kt-avatar--circle" id="kt_user_avatar_3">
                                    <div class="kt-avatar__holder" style="background-image: url({{asset($info->image)}})"></div>
                                    <label class="kt-avatar__upload" data-toggle="kt-tooltip" title="" data-original-title="قم بتغيير الصورة">
                                        <i class="fa fa-pen"></i>
                                        <input type="file" name="avatar" value="{{old('avatar')}}"  accept=".png, .jpg, .jpeg">
                                    </label>
                                    <span class="kt-avatar__cancel" data-toggle="kt-tooltip" title="" data-original-title="إلغاء">
                                        <i class="fa fa-times"></i>
                                    </span>
                                </div>
                                <span class="form-text text-muted">يُسمح بالصيغ التالية: png, jpg, jpeg.</span>
                            </div>
                        </div>  
                        <div class="form-group row">
                            <div class="col-lg-4">
                                <label class="">الاسم الكامل:</label>
                                <input type="text" class="form-control" name="name" value="{{ old('name',$info->name) }}"
                                       placeholder="الاسم الكامل">
                            </div>
                            <div class="col-lg-4">
                                <label class="">رقم الهوية:</label>
                                <input type="text" class="form-control" name="username" value="{{ old('username',$info->username) }}"
                                       placeholder="رقم الهوية" maxlength="9" onkeypress="return isNumber(event)">
                            </div>
                            <div class="col-lg-4">
                                <label>رقم الجوال:</label>
                                <input type="text" class="form-control" name="phone_number" value="{{ old('phone_number',$info->phone_number) }}"
                                       placeholder="رقم الجوال" onkeypress="return isNumber(event)" maxlength="9">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-4">
                                <label>البريد الإلكتروني:</label>
                                <input type="email" class="form-control" name="email" value="{{ old('email',$info->email) }}"
                                       placeholder="البريد الإلكتروني">
                            </div>
                            <div class="col-lg-4">
                                <label>كلمة المرور:</label>
                                <input type="password" class="form-control" autocomplete="new-password" name="password"
                                       placeholder="كلمة المرور">
                            </div>
                            <div class="col-lg-4">
                                <label class="">تأكيد كلمة المرور:</label>
                                <input type="password" class="form-control" name="password_confirmation"
                                       placeholder="تأكيد كلمة المرور">
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <div class="col-lg-4">
                                <label class="">المحافظة:</label>
                                <select class="form-control kt-selectpicker searchable" data-live-search="true" id="province" name="province_id" data-title="اختر المحافظة">
                                    @foreach ($provinces as $province)
                                        <option value="{{$province->id}}" {{$info->region->province->id == $province->id ? 'selected':''}} >{{ $province->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-lg-4">
                                <label class="">المنطقة:</label>
                                <select class="form-control kt-selectpicker searchable" data-live-search="true" id="regions" name="region_id" data-title="اختر المنطقة">                                   
                                    @foreach ($regions as $region)
                                        <option value="{{$region->id}}" {{$info->region_id == $region->id ? 'selected':''}} >{{ $region->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-lg-4">
                                <label class="">الحالة:</label>
                                <select class="form-control kt-selectpicker" name="status" data-live-search="true">
                                    <option value="1" {{$info->status == 1 ? 'selected':''}}>فعال</option>
                                    <option value="0" {{$info->status == 0 ? 'selected':''}}>معطل</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-4">
                                <label class="">نوع المستخدم:</label>
                                <select class="form-control kt-selectpicker" name="user_type_id" data-live-search="true" data-title="اختر قيمة">
                                    @foreach ($types as $type)
                                        <option value="{{$type->id}}" {{$info->user_type_id == $type->id ? 'selected':''}}>{{ $type->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-lg-4">
                                <label class="">الصلاحية:</label>
                                <select class="form-control kt-selectpicker" name="role" data-live-search="true" data-title="اختر قيمة">
                                    @foreach ($roles as $role)
                                        <option value="{{$role->id}}" {{$info->roles->first()->id == $role->id ? 'selected':''}}>{{ $role->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            
                        </div>
                        
                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <div class="row">
                                <div class="col-lg-6 kt-align-right">
                                    <button type="submit" class="btn btn-brand save">حفظ التغييرات</button>
                                    <a href="{{ route('admin.users.view') }}" class="btn btn-secondary">الغاء</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <!--end::Form-->
            </div>
            <!--end::Portlet-->
        </div>
    </div>
    <!-- End:: Content -->
@stop 

@section('js')
    <script src="assets/admin/js/pages/crud/file-upload/ktavatar.js" type="text/javascript"></script>
    <script src="assets/admin/general/js/scripts/users.js" type="text/javascript"></script>
    <script>
        function isNumber(evt) {
            // $('.filter').html('');
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }
    </script>
@stop
