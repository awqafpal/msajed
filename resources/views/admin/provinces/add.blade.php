@extends('admin.layout.master')

@section('title')
    إضافة محافظة
@stop

@section('css')

@stop

@section('subheader')
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    وزارة الأوقاف والشؤون الدينية </h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="{{ route('admin.dashboard.view') }}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="{{ route('admin.dashboard.view') }}" class="kt-subheader__breadcrumbs-link">
                        الرئيسية
                    </a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="{{ route('admin.provinces.view') }}" class="kt-subheader__breadcrumbs-link">
                    المحافظات
                    </a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="{{ route('admin.provinces.add') }}" class="kt-subheader__breadcrumbs-link">
                        إضافة محافظة
                    </a>
                </div>
            </div>
        </div>
    </div>
@stop

@section('content')
    <!-- begin:: Content -->
    <div class="row">
        <div class="col-lg-12">
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            إضافة محافظة
                        </h3>
                    </div>
                </div>
                <!--begin::Form-->
                <form class="kt-form kt-form--label-right" id="frmAdd" method="post" action="{{ route('admin.provinces.store') }}">
                    <div class="kt-portlet__body">
                        <div class="form-group row">
                            <div class="col-lg-4">
                                <label class="">اسم المحافظة:</label>
                                <input type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="اسم المحافظة">
                            </div>
                            <div class="col-lg-4">
                                <label class="">الارشاد:</label>
                                <input type="text" class="form-control" name="irshad" value="{{ old('irshad') }}" placeholder="الارشاد">
                            </div>
                            <div class="col-lg-4">
                                <label class="">رقم الهاتف:</label>
                                <input type="text" class="form-control" name="telephone" value="{{ old('telephone') }}" placeholder="رقم الهاتف" onkeypress="return isNumber(event)" maxlength="9">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-4">
                                <label class="">المدير:</label>
                                <input type="text" class="form-control" name="modeer" value="{{ old('modeer') }}" placeholder="المدير">
                            </div>
                            <div class="col-lg-4">
                                <label class="">رقم الجوال:</label>
                                <input type="text" class="form-control" name="jawwal" value="{{ old('jawwal') }}" placeholder="رقم الجوال">
                            </div>
                            <div class="col-lg-4">
                                <label class="">الفاكس:</label>
                                <input type="text" class="form-control" name="fax" value="{{ old('fax') }}" placeholder="الفاكس">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-12">
                                <label class="">العنوان:</label>
                                <input type="text" class="form-control" name="address" value="{{ old('address') }}" placeholder="العنوان">
                            </div>
                        </div>
                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <div class="row">
                                <div class="col-lg-6 kt-align-right">
                                    <button type="submit" class="btn btn-brand save">حفظ</button>
                                    <a href="{{ route('admin.provinces.view') }}" class="btn btn-secondary">الغاء</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <!--end::Form-->
            </div>
            <!--end::Portlet-->
        </div>
    </div>
    <!-- End:: Content -->
@stop

@section('js')
    <script src="assets/admin/general/js/scripts/provinces.js" type="text/javascript"></script>
    <script>
        /*  onkeypress="return isNumber(event)" */
        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }
    </script>
@stop
