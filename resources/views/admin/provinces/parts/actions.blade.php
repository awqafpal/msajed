@if(auth()->user()->can('admin.provinces.edit') || auth()->user()->can('admin.provinces.delete'))
    @can('admin.provinces.edit')
        <a href="{{ route('admin.provinces.edit', ['id' => $id]) }}" class="btn btn-outline-success btn-elevate-hover btn-circle btn-icon btn-sm" title="تعديل">
            <i class="la la-edit"></i>
        </a>
    @endcan
    @can('admin.provinces.delete')
        <a href="javascript:;" data-url="{{ route('admin.provinces.delete', ['id' => $id]) }}" class="btn btn-outline-danger btn-elevate-hover btn-circle btn-icon btn-sm delete_btn" title="حذف">
            <i class="la la-trash"></i>
        </a>
    @endcan
@endif
