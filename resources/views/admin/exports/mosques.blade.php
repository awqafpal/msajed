<table>
    <thead>
    <tr>
        <th>اسم المسجد</th>
        <th>الكود التعريفي</th>
        <th>رقم المسجد</th>
        <th>المنطقة-المحافظة</th>
        <th>المفتش</th>
        <th>أثرية المسجد</th>
        <th>مركزية المسجد</th>
        <th>نوع المسجد</th>
        <th>ملكية المسجد</th>
        <th>طبيعة البناء</th>
        <th>رقم الحساب البنكي</th>
        <th>تاريخ الانشاء-هجري</th>
        <th>تاريخ الانشاء-ميلادي</th>
        <th>القطعة</th>
        <th>القسيمة</th>
        <th>المساحة/متر مربع</th>
        <th>عدد الوقفيات</th>
        <th>عدد الطوابق</th>
        <th>عدد المصلين</th>
        <th>عنوان المسجد</th>
        <th>الوصف</th>
        {{-- <th>خط الطول</th>
        <th>خط العرض</th> --}}
    </tr>
    </thead>
    <tbody>
    @foreach($mosques as $mosque)
        <tr>
            <td>{{ $mosque->name }}</td>
            <td>{{ $mosque->mosque_code }}</td>
            <td>{{ $mosque->mosque_no }}</td>
            <td>{{ $mosque->region->province->name}}-{{$mosque->region->name}}</td>
            <td>{{ $mosque->user->name }}</td>
            <td>{{ $mosque->archaeological_status ? 'أثري' : 'غير أثري' }}</td>
            <td>{{ $mosque->central_status ? 'مركزي' : 'غير مركزي' }}</td>
            <td>{{ $mosque->type->name }}</td>
            <td>{{ $mosque->property->name }}</td>
            <td>{{ $mosque->building->name }}</td>
            <td>{{ $mosque->bank_account_number }}</td>
            <td>{{ $mosque->date_bulid_hijri }}</td>
            <td>{{ $mosque->date_bulid_ad }}</td>
            <td>{{ $mosque->piece }}</td>
            <td>{{ $mosque->qasema }}</td>
            <td>{{ $mosque->mosque_space }}</td>
            <td>{{ $mosque->facilities_number }}</td>
            <td>{{ $mosque->floor_no }}</td>
            <td>{{ $mosque->prayer_no }}</td>
            <td>{{ $mosque->street }}</td>
            <td>{{ $mosque->description }}</td>
            {{-- <td>{{ $mosque->latitude  }}</td>
            <td>{{ $mosque->longitude }}</td> --}}
        </tr>
    @endforeach
    </tbody>
</table>
