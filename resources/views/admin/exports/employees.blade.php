<table>
    <thead>
    <tr>
        <th>اسم الموظف</th>
        <th>رقم الهوية</th>
        <th>رقم الجوال</th>
        <th>التصنيف الوظيفي</th>
        <th>الوظيفة</th>
        <th>بند التعيين</th>
        <th>تاريخ التعيين</th>
        <th>الرقم الوظيفي</th>
        <th>المؤهل العلمي</th>
        <th>المسجد</th>
        <th>المحافظة - المنطقة</th>
        <th>العنوان بالتفصيل</th>
    </tr>
    </thead>
    <tbody>
    @foreach($employees as $employee)
        <tr>
            <td>{{ $employee->name }}</td>
            <td>{{ $employee->identity_number }}</td>
            <td>{{ $employee->phone_number }}</td>
            <td>{{ $employee->jobType->name }}</td>
            <td>{{ $employee->job->name }}</td>
            <td>{{ $employee->hiringType->name }}</td>
            <td>{{ $employee->hiring_date }}</td>
            <td>{{ $employee->job_no }}</td>
            <td>{{ $employee->qualification }}</td>
            <td>{{ $employee->mosque->name }}</td>
            <td>{{ $employee->region->province->name}}-{{$employee->region->name}}</td>
            <td>{{ $employee->address }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
