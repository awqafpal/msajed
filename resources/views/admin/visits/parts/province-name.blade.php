@if ($row->visitor->region)
    <span class="btn-sm btn btn-label-success">{{$row->visitor->region->province->name}}-{{$row->visitor->region->name}}</span>
@else
    <span class="btn-sm btn btn-label-danger">غير مدخل</span>
@endif
