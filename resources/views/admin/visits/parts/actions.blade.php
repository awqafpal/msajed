@if(auth()->user()->can('admin.visits.show') || auth()->user()->can('admin.visits.edit') || auth()->user()->can('admin.visits.delete'))
    @can('admin.visits.show')
        <a href="{{ route('admin.visits.show', ['id' => $id]) }}" class="btn btn-outline-info btn-elevate-hover btn-circle btn-icon btn-sm" title="تعديل">
            <i class="la la-eye"></i>
        </a>
    @endcan
    @can('admin.visits.edit')
        <a href="{{ route('admin.visits.edit', ['id' => $id]) }}" class="btn btn-outline-success btn-elevate-hover btn-circle btn-icon btn-sm" title="تعديل">
            <i class="la la-edit"></i>
        </a>
    @endcan
    @can('admin.visits.delete')
        <a href="javascript:;" data-url="{{ route('admin.visits.delete', ['id' => $id]) }}" class="btn btn-outline-danger btn-elevate-hover btn-circle btn-icon btn-sm delete_btn" title="حذف">
            <i class="la la-trash"></i>
        </a>
    @endcan
@endif
