@if($status == 'province_approved')
    <span  class="badge badge-success"  aria-haspopup="true" aria-expanded="false" style="cursor: default">
        <i class="fa fa-check-circle"></i>
        معتمد - مدير مديرية
    </span>
@elseif($status == 'head_approved')
    <span  class="badge badge-success"  aria-haspopup="true" aria-expanded="false" style="cursor: default">
        <i class="fa fa-check-circle"></i>
        معتمد - رئيس قسم
    </span>
@elseif($status == 'inspector_approved')
    <span  class="badge badge-success"  aria-haspopup="true" aria-expanded="false" style="cursor: default">
        <i class="fa fa-check-circle"></i>
        معتمد - مفتش
    </span>
@else
    <span  class="badge badge-warning"  aria-haspopup="true" aria-expanded="false" style="cursor: default">
        <i class="fa fa-exclamation-circle"></i>
        بانتظار الاعتماد
    </span>
@endif
