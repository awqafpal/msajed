<!-- Modal -->
<div class="modal fade" id="approve-visit" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle"> اعتماد زيارة مسجد</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <!--begin::Form-->
            <form class="kt-form kt-form--label-right approve_form" id="ApproveForm" method="post" action="{{route('admin.visits.approve',$info->id)}}">
                @csrf
                <input value="" type="hidden" name="visit_id" id="visit_id">
                <div class="modal-body">
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <label> الملاحظات والتوصيات </label>
                            <textarea class="form-control" rows="7" name="{{auth()->user()->user_type_id == 4 ? 'province_manager_recommendations' : 'department_head_recommendations' }}" id="note" required></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">الغاء</button>
                    <button type="submit" class="btn btn-brand save" name="approve-btn" id="approve-btn">اعتماد</button>
                </div>
            </form>
        </div>
    </div>
</div>
