@extends('admin.layout.master')

@section('title')
    تعديل تقرير زيارة
@stop

@section('css')

@stop

@section('subheader')
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    وزارة الأوقاف والشؤون الدينية </h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="{{ route('admin.dashboard.view') }}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="{{ route('admin.dashboard.view') }}" class="kt-subheader__breadcrumbs-link">
                        الرئيسية
                    </a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="{{ route('admin.visits.view') }}" class="kt-subheader__breadcrumbs-link">
                        زيارات المساجد
                    </a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="{{ route('admin.visits.edit', ['id' => $info->id]) }}" class="kt-subheader__breadcrumbs-link">
                        تعديل زيارة مسجد
                    </a>
                </div>
            </div>
        </div>
    </div>
@stop

@section('content')
    <!-- begin:: Content -->
    <div class="row">
        <div class="col-lg-12">
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            تعديل زيارة مسجد
                        </h3>
                    </div>
                </div>
                <!--begin::Form-->
                <form class="kt-form kt-form--label-right" id="frmEdit" method="post" action="{{ route('admin.visits.update', ['id' => $info->id]) }}" encrypt="multipart/form-data">
                    <input type="hidden" name="date" value="{{$info->date}}">
                    <input type="hidden" name="visited_by" value="{{$info->visited_by}}">
                    <input type="hidden" name="mosque_id" value="{{$info->mosque_id}}">
                    <input type="hidden" name="visit_date" value="{{$info->visit_date}}">
                    <div class="kt-portlet__body">
                        <div class="form-group row">
                            <div class="col-lg-4">
                                <label class="">المفتش:</label>
                                <input type="text" class="form-control" value="{{$info->visitor->name}}" disabled>
                            </div>
                            <div class="col-lg-4">
                                <label class="">التاريخ:</label>
                                <input type="text" class="form-control" value="{{ Carbon\Carbon::parse($info->date)->translatedFormat('l') }} / {{$info->date->format('d-m-Y')}} م" disabled >
                            </div>
                            <div class="col-lg-4">
                                <label class="">المسجد:</label>
                                <input type="text" class="form-control"  value="{{$info->mosque->name}}" disabled />
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-4">
                                <label>العنوان</label>
                                    <input type="text" class="form-control"  value="{{$info->mosque->region->province->name}}-{{$info->mosque->region->name}}" disabled />
                            </div>
                            <div class="col-lg-4">
                                <label>وقت الزيارة</label>
                                <input type="time" name="visit_time" class="form-control"  value="{{$info->visit_time->format('H:i')}}"  />
                            </div>
                            <div class="col-lg-4">
                                <label>وقت المغادرة</label>
                                <input type="time" name="time_to_leave" class="form-control"  value="{{$info->time_to_leave->format('H:i')}}"  />
                            </div>
                        </div>
                        <div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>
                            <div class="row">
                                <div class="col-md-12">
                                    <h3 class="mb-4">مستوى نظافة المسجد</h3>
                                </div>
                                <div class="col-md-6">
                                    <div class="table-responsive">
                                        <table class="datatable table table-bordered table-striped text-center ">
                                            <thead>
                                            <tr>
                                                <th>الوصف</th>
                                                <th>الحالة ( ممتاز - جيد - سيء )</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($components as $component)
                                                @if ($loop->odd)
                                                    <tr>
                                                        <td>{{$component->name}}</td>
                                                        <td style="display: flex">
                                                            <input type="radio" id="excellent_{{$component->id}}" name="components[{{$component->id}}]" value='36' {{ (in_array($component->id ,$info->components->where('status_id','36')->pluck('cleaning_component_id')->toArray() )) ? 'checked' : '' }} >
                                                            <label for="excellent_{{$component->id}}" class="kt-padding-r-35">ممتاز</label><br>
                                                            <input type="radio" id="good_{{$component->id}}" name="components[{{$component->id}}]" value='37' {{ (in_array($component->id ,$info->components->where('status_id','37')->pluck('cleaning_component_id')->toArray() )) ? 'checked' : '' }}>
                                                            <label for="good_{{$component->id}}" class="kt-padding-r-35">جيد</label><br>
                                                            <input type="radio" id="bad_{{$component->id}}" name="components[{{$component->id}}]" value='38' {{ (in_array($component->id ,$info->components->where('status_id','38')->pluck('cleaning_component_id')->toArray() )) ? 'checked' : '' }}>
                                                            <label for="bad_{{$component->id}}" class="kt-padding-r-25">سيء</label>
                                                        </td>
                                                    </tr>
                                                @endif
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="table-responsive">
                                        <table class="datatable table table-bordered table-striped text-center ">
                                            <thead>
                                            <tr>
                                                <th>الوصف</th>
                                                <th>الحالة ( ممتاز - جيد - سيء )</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($components as $component)
                                                @if ($loop->even)
                                                    <tr>
                                                        <td>{{$component->name}}</td>
                                                        <td style="display: flex">
                                                            <input type="radio" id="excellent_{{$component->id}}" name="components[{{$component->id}}]" value='36' {{ (in_array($component->id ,$info->components->where('status_id','36')->pluck('cleaning_component_id')->toArray() )) ? 'checked' : '' }} >
                                                            <label for="excellent_{{$component->id}}" class="kt-padding-r-35">ممتاز</label><br>
                                                            <input type="radio" id="good_{{$component->id}}" name="components[{{$component->id}}]" value='37' {{ (in_array($component->id ,$info->components->where('status_id','37')->pluck('cleaning_component_id')->toArray() )) ? 'checked' : '' }}>
                                                            <label for="good_{{$component->id}}" class="kt-padding-r-35">جيد</label><br>
                                                            <input type="radio" id="bad_{{$component->id}}" name="components[{{$component->id}}]" value='38' {{ (in_array($component->id ,$info->components->where('status_id','38')->pluck('cleaning_component_id')->toArray() )) ? 'checked' : '' }}>
                                                            <label for="bad_{{$component->id}}" class="kt-padding-r-25">سيء</label>
                                                        </td>
                                                    </tr>
                                                @endif
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>
                            <div class="row">
                                <div class="col-md-12">
                                    <h3 class="mb-4">أحوال العاملين :</h3>
                                </div>
                                <div class="col-md-12">
                                    <div class="table-responsive">
                                        <table class="datatable table table-bordered table-striped text-center ">
                                            <thead>
                                            <tr>
                                                <th>المسمى الوظيفي</th>
                                                <th>اسم الموظف</th>
                                                <th>بند التعيين</th>
                                                <th>
                                                    الدوام
                                                    <br>
                                                    ( موجود -غير موجود)
                                                </th>
                                                <th>
                                                    الالتزام
                                                    <br>
                                                    ( ملتزم -غير ملتزم)
                                                </th>
                                                <th>ملاحظات</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($employees as $employee)
                                                <tr>
                                                    <td>{{$employee->job->name}}</td>
                                                    <td>{{$employee->name}}</td>
                                                    <td>{{$employee->hiringType->name}}</td>
                                                    <td>
                                                        <input type="radio" id="employee_attendance_{{$employee->id}}" name="employees[{{$employee->id}}][attendance]" value='1' {{ (in_array($employee->id ,$info->conditions->where('attendance',1)->pluck('employee_id')->toArray() )) ? 'checked' : '' }}>
                                                        <label for="employee_attendance_{{$employee->id}}" class="">موجود</label><br>
                                                        <input type="radio" id="employee_not_attendance_{{$employee->id}}" name="employees[{{$employee->id}}][attendance]" value='0' {{ (in_array($employee->id ,$info->conditions->where('attendance',0)->pluck('employee_id')->toArray() )) ? 'checked' : '' }}>
                                                        <label for="employee_not_attendance_{{$employee->id}}" class=" ">غير موجود</label><br>
                                                    </td>
                                                    <td>
                                                        <input type="radio" id="employee_commitment_{{$employee->id}}" name="employees[{{$employee->id}}][commitment]" value='1' {{ (in_array($employee->id ,$info->conditions->where('commitment',1)->pluck('employee_id')->toArray() )) ? 'checked' : '' }}>
                                                        <label for="employee_commitment_{{$employee->id}}" class="">ملتزم</label><br>
                                                        <input type="radio" id="employee_not_commitment_{{$employee->id}}" name="employees[{{$employee->id}}][commitment]" value='0' {{ (in_array($employee->id ,$info->conditions->where('commitment',0)->pluck('employee_id')->toArray() )) ? 'checked' : '' }}>
                                                        <label for="employee_not_commitment_{{$employee->id}}" class=" ">غير ملتزم</label><br>
                                                    </td>
                                                    <td>
                                                        <textarea class="form-control" rows="4" name="employees[{{$employee->id}}][notes]">{{@$info->conditions->where('employee_id',$employee->id)->first()->notes}}</textarea>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>
                            <div class="row">
                                <div class="col-md-12">
                                    <label>مشروحات وتوصيات المفتش</label>
                                    <textarea class="form-control" rows="7" name="inspector_recommendations">{{$info->inspector_recommendations}}</textarea>
                                </div>
                            </div>
                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <div class="row">
                                <div class="col-lg-6 kt-align-right">
                                    <button type="submit" class="btn btn-brand save">حفظ</button>
                                    <a href="{{ route('admin.visits.view') }}" class="btn btn-secondary">الغاء</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <!--end::Form-->
            </div>
            <!--end::Portlet-->
        </div>
    </div>
    <!-- End:: Content -->
@stop

@section('js')
    <!--begin::Page Scripts(used by this page) -->
    <script src="assets/admin/js/pages/crud/file-upload/ktavatar.js" type="text/javascript"></script>
    <script src="assets/admin/general/js/scripts/visits.js" type="text/javascript"></script>
@stop
