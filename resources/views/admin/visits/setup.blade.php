<div class="row">
    <div class="col-md-6 kt-padding-t-25">
        <div class="form-group">
            <label>وقت الزيارة</label>
            <input type="time" name="visit_time" class="form-control"  value="{{old('visit_time', date('H:i'))}}"  onkeydown="return false"  />
        </div>
    </div>
    <div class="col-md-6 kt-padding-t-25">
        <div class="form-group">
            <label>وقت المغادرة</label>
            <input type="time" name="time_to_leave" class="form-control"  value="{{old('time_to_leave', date('H:i'))}}" onkeydown="return false"  />
        </div>
    </div>
</div>
<div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>
<div class="row">
    <div class="col-md-12">
        <h3 class="mb-4">مستوى نظافة المسجد</h3>
    </div>
    <div class="col-md-6">
        <div class="table-responsive">
            <table class="datatable table table-bordered table-striped text-center ">
                <thead>
                <tr>
                    <th>الوصف</th>
                    <th>الحالة ( ممتاز - جيد - سيء )</th>
                </tr>
                </thead>
                <tbody>
                @foreach($components as $component)
                    @if ($loop->odd)
                        <tr>
                            <td>{{$component->name}}</td>
                            <td style="display: flex">
                                <input type="radio" id="excellent_{{$component->id}}" name="components[{{$component->id}}]" value='36'>
                                <label for="excellent_{{$component->id}}" class="kt-padding-r-35">ممتاز</label><br>
                                <input type="radio" id="good_{{$component->id}}" name="components[{{$component->id}}]" value='37' checked>
                                <label for="good_{{$component->id}}" class="kt-padding-r-35">جيد</label><br>
                                <input type="radio" id="bad_{{$component->id}}" name="components[{{$component->id}}]" value='38'>
                                <label for="bad_{{$component->id}}" class="kt-padding-r-25">سيء</label>
                            </td>
                        </tr>
                    @endif
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-6">
        <div class="table-responsive">
            <table class="datatable table table-bordered table-striped text-center ">
                <thead>
                <tr>
                    <th>الوصف</th>
                    <th>الحالة ( ممتاز - جيد - سيء )</th>
                </tr>
                </thead>
                <tbody>
                @foreach($components as $component)
                    @if ($loop->even)
                        <tr>
                            <td>{{$component->name}}</td>
                            <td style="display: flex">
                                <input type="radio" id="excellent_{{$component->id}}" name="components[{{$component->id}}]" value='36'>
                                <label for="excellent_{{$component->id}}" class="kt-padding-r-35">ممتاز</label><br>
                                <input type="radio" id="good_{{$component->id}}" name="components[{{$component->id}}]" value='37' checked>
                                <label for="good_{{$component->id}}" class="kt-padding-r-35">جيد</label><br>
                                <input type="radio" id="bad_{{$component->id}}" name="components[{{$component->id}}]" value='38'>
                                <label for="bad_{{$component->id}}" class="kt-padding-r-25">سيء</label>
                            </td>
                        </tr>
                    @endif
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@if(count($employees) > 0 )
<div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>
<div class="row">
    <div class="col-md-12">
        <h3 class="mb-4">أحوال العاملين :</h3>
    </div>
    <div class="col-md-12">
        <div class="table-responsive">
            <table class="datatable table table-bordered table-striped text-center ">
                <thead>
                <tr>
                    <th>المسمى الوظيفي</th>
                    <th>اسم الموظف</th>
                    <th>بند التعيين</th>
                    <th>
                        الدوام
                        <br>
                        ( موجود -غير موجود)
                    </th>
                    <th>
                        الالتزام
                        <br>
                        ( ملتزم -غير ملتزم)
                    </th>
                    <th>ملاحظات</th>
                </tr>
                </thead>
                <tbody>
                @foreach($employees as $employee)
                    <tr>
                        <td>{{$employee->job->name}}</td>
                        <td>{{$employee->name}}</td>
                        <td>{{$employee->hiringType->name}}</td>
                        <td>
                            <input type="radio" id="employee_attendance_{{$employee->id}}" name="employees[{{$employee->id}}][attendance]" value='1' checked>
                            <label for="employee_attendance_{{$employee->id}}" class="">موجود</label><br>
                            <input type="radio" id="employee_not_attendance_{{$employee->id}}" name="employees[{{$employee->id}}][attendance]" value='0'>
                            <label for="employee_not_attendance_{{$employee->id}}" class=" ">غير موجود</label><br>
                        </td>
                        <td>
                            <input type="radio" id="employee_commitment_{{$employee->id}}" name="employees[{{$employee->id}}][commitment]" value='1' checked>
                            <label for="employee_commitment_{{$employee->id}}" class="">ملتزم</label><br>
                            <input type="radio" id="employee_not_commitment_{{$employee->id}}" name="employees[{{$employee->id}}][commitment]" value='0'>
                            <label for="employee_not_commitment_{{$employee->id}}" class=" ">غير ملتزم</label><br>
                        </td>
                        <td>
                            <textarea class="form-control" rows="4" name="employees[{{$employee->id}}][notes]"></textarea>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endif
<div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>
<div class="row">

    <div class="col-md-12">
        <label>المشروحات والتوصيات</label>
        <textarea class="form-control" rows="7" name="inspector_recommendations"></textarea>
    </div>

</div>
