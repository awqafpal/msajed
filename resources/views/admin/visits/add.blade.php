@extends('admin.layout.master')

@section('title')
    إضافة تقرير زيارة مسجد
@stop

@section('css')

@stop

@section('subheader')
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    وزارة الأوقاف والشؤون الدينية </h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="{{ route('admin.dashboard.view') }}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="{{ route('admin.dashboard.view') }}" class="kt-subheader__breadcrumbs-link">
                        الرئيسية
                    </a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="{{ route('admin.visits.view') }}" class="kt-subheader__breadcrumbs-link">
                        زيارات المساجد
                    </a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="{{ route('admin.visits.add') }}" class="kt-subheader__breadcrumbs-link">
                        إضافة تقرير زيارة مسجد
                    </a>
                </div>
            </div>
        </div>
    </div>
@stop

@section('content')
    <!-- begin:: Content -->
    <div class="row">
        <div class="col-lg-12">
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            إضافة تقرير زيارة مسجد
                        </h3>
                    </div>
                </div>
                <!--begin::Form-->
                <form class="kt-form kt-form--label-right" id="frmAdd" method="post" action="{{ route('admin.visits.store') }}" encrypt="multipart/form-data">
                    <div class="kt-portlet__body">
                        <div class="form-group row">
                            <div class="col-lg-4"> 
                                <label class="">المفتش:</label>
                                <select class="form-control kt-selectpicker searchable" data-live-search="true" id="user" name="visited_by" data-title="اختر المفتش">
                                    @foreach ($users as $user)
                                        <option value="{{$user->id}}">{{ $user->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-lg-4">
                                <label class="">التاريخ:</label>
                                <input type="text" class="form-control date get-mosques" name="date" id="visit_date" max="{{date('Y-m-d')}}" onkeydown="return false">
                            </div>
                            <div class="col-lg-4">
                                <label class="">المسجد:</label>
                                <select class="form-control kt-selectpicker searchable" data-live-search="true" name="mosque_id" id="mosques" disabled data-title="اختر المسجد">

                                </select>
                                <span class="text-danger" id="mosques_notes"></span>
                            </div>
                        </div>
                        <div id="form-elements">

                        </div>
                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <div class="row">
                                <div class="col-lg-6 kt-align-right">
                                    <button type="submit" class="btn btn-brand save" id="save-btn">حفظ</button>
                                    <a href="{{ route('admin.visits.view') }}" class="btn btn-secondary">الغاء</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <!--end::Form-->
            </div>
            <!--end::Portlet-->
        </div>
    </div>
    <!-- End:: Content -->
@stop

@section('js')
    <!--begin::Page Scripts(used by this page) -->
    <script src="assets/admin/general/js/scripts/visits.js" type="text/javascript"></script>
@stop
