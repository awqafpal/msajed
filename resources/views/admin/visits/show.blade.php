@extends('admin.layout.master')

@section('title')
    تقرير زيارة مسجد
@stop

@section('css')

@stop

@section('subheader')
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    وزارة الأوقاف والشؤون الدينية </h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="{{ route('admin.dashboard.view') }}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="{{ route('admin.dashboard.view') }}" class="kt-subheader__breadcrumbs-link">
                        الرئيسية
                    </a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="{{ route('admin.visits.view') }}" class="kt-subheader__breadcrumbs-link">
                        زيارات المساجد
                    </a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="{{ route('admin.visits.show', ['id' => $info->id]) }}" class="kt-subheader__breadcrumbs-link">
                       تقرير زيارة مسجد
                    </a>
                </div>
            </div>
        </div>
    </div>
@stop

@section('content')
    <!-- begin:: Content -->
    <div class="row">
        <div class="col-lg-12">
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            تقرير زيارة مسجد
                        </h3>
                    </div>
                    @if(auth()->user()->can('admin.visits.approve'))
                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-wrapper">
                            <div class="kt-portlet__head-actions" id="flag">
                                @if($info->approved)
                                    <span  class="btn btn-success btn-elevate btn-icon-sm"  aria-haspopup="true" aria-expanded="false" style="cursor: default">
                                        <i class="fa fa-check-circle"></i>
                                            معتمد
                                    </span>
                                @elseif(!$info->approved && (auth()->user()->user_type_id == 4 || auth()->user()->user_type_id == 8))
                                    <button type="button" class="btn btn-brand btn-elevate btn-icon-sm hideWhenApprove"
                                            data-toggle="modal" data-target="#approve-visit" id="approving" data-id="{{$info->id}}"
                                            data-url="{{route('admin.visits.approve', ['id' => $info->id])}}">
                                            <i class="fa fa-check-circle"></i>اعتماد</button>
                                @else
                                    <button type="button" class="btn btn-brand btn-elevate direct-approve-btn btn-icon-sm btn-index-add"
                                            aria-haspopup="true" aria-expanded="false"
                                            id="directApproving" data-id="{{$info->id}}"
                                            data-url="{{route('admin.visits.approve', ['id' => $info->id])}}">
                                        <i class="fa fa-check-circle"></i>   اعتماد
                                    </button>
                                @endif
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
                <!--begin::Form-->
                <div class="kt-portlet__body">
                    <table class="table table-striped- table-bordered table-hover" id='show_visit'>
                        <tr>
                            <th colspan="4" class="text-center">
                                البيانات الأساسية
                            </th>
                        </tr>
                        <tr>
                            <th width="15%">المسجد</th>
                            <td width="35%"> {{ $info->mosque->name }}</td>
                            <th width="15%">العنوان</th>
                            <td width="35%"> {{ $info->mosque->region->province->name }} - {{ $info->mosque->region->name }}</td>
                        </tr>
                        <tr>
                            <th width="15%">المفتش</th>
                            <td width="35%"> {{ $info->visitor->name }} </td>
                            <th width="15%">تاريخ الزيارة</th>
                            <td width="20%"> {{ Carbon\Carbon::parse($info->date)->translatedFormat('l') }} / {{$info->date->format('d-m-Y')}} م  </td>
                        </tr>
                        <tr>
                            <th width="15%">وقت الزيارة</th>
                            <td width="35%"> {{$info->visit_time->translatedFormat('H:m A')}}</td>
                            <th width="15%">وقت المغادرة</th>
                            <td width="35%"> {{$info->time_to_leave->translatedFormat('H:m A')}} </td>
                        </tr>
                        <tr>
                            <th width="15%">الحالة</th>
                            <td width="85%" colspan="3"></td>
                        </tr>
                    </table>
                    <div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>
                    <div class="row">
                        <div  class="col-md-12">
                            <table class="table table-striped- table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th width="100%" class="text-center">مستوى نظافة المسجد</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <table class="table table-striped- table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>الوصف</th>
                                    <th>الحالة ( ممتاز - جيد - سيء )</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($info->components as $component)
                                    @if ($loop->odd)
                                        <tr>
                                            <td>{{$component->cleaningComponent->name}}</td>
                                            <td>{{$component->status->name}} </td>
                                        </tr>
                                    @endif
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-6">
                            <table class="table table-striped- table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>الوصف</th>
                                    <th>الحالة ( ممتاز - جيد - سيء )</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($info->components as $component)
                                    @if ($loop->even)
                                        <tr>
                                            <td>{{$component->cleaningComponent->name}}</td>
                                            <td>{{$component->status->name}} </td>
                                        </tr>
                                    @endif
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    @if(count($info->conditions) > 0 )
                    <div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>
                    <div class="row">
                        <div  class="col-md-12">
                            <table class="table table-striped- table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th width="100%" class="text-center">أحوال العاملين</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-striped- table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>المسمى الوظيفي</th>
                                        <th>اسم الموظف</th>
                                        <th>بند التعيين</th>
                                        <th>
                                            الدوام
                                            <br>
                                            ( موجود -غير موجود)
                                        </th>
                                        <th>
                                            الالتزام
                                            <br>
                                            ( ملتزم -غير ملتزم)
                                        </th>
                                        <th>ملاحظات</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($info->conditions as $condition)
                                    <tr>
                                        <td>{{@$condition->employee->job->name}}</td>
                                        <td>{{@$condition->employee->name}}</td>
                                        <td>{{@$condition->employee->hiringType->name}}</td>
                                        <td>{{$condition->attendance == 1 ? 'موجود' : 'غير موجود'}}</td>
                                        <td>{{$condition->commitment == 1 ? 'ملتزم' : 'غير ملتزم'}}</td>
                                        <td>{{$condition->notes ?? ''}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    @endif
                    <div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>
                    <div class="row">
                        <div class="col-md-12">
                            <b>مشروحات وتوصيات المفتش : </b>
                        </div>
                        <div class="col-md-12">
                            <p>{{$info->inspector_recommendations ?? '---'}}</p>
                        </div>
                    </div>
                    <div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>
                    <div class="row">
                        <div class="col-md-12">
                            <b>مشروحات وتوصيات رئيس القسم : </b>
                        </div>
                        <div class="col-md-12">
                            <p>{{$info->department_head_recommendations ?? '---'}}</p>
                        </div>
                    </div>
                    <div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>
                    <div class="row">
                        <div class="col-md-12">
                            <b>مشروحات وتوصيات مدير المديرية : </b>
                        </div>
                        <div class="col-md-12">
                            <p>{{$info->province_manager_recommendations ?? '---'}}</p>
                        </div>
                    </div>
                </div>
                <!--end::Form-->
            </div>
            <!--end::Portlet-->
        </div>
    </div>
    <!-- End:: Content -->
@stop
@section('modal')
    @include('admin.visits.includes.approve-mosque')
@stop
@section('js')
    <!--begin::Page Scripts(used by this page) -->
    <script src="assets/admin/general/js/scripts/visits.js" type="text/javascript"></script>
@stop
