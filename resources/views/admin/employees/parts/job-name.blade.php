@if ($row->job)
    <span class="btn-sm btn btn-label-info">{{$row->job->name}}</span>
@else
    <span class="btn-sm btn btn-label-danger">غير مدخل</span>
@endif
