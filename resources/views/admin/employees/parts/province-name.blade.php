@if ($row->region)
    <span class="btn-sm btn btn-label-success">{{$row->region->province->name}}-{{$row->region->name}}</span>
@else
    <span class="btn-sm btn btn-label-danger">غير مدخل</span>
@endif
