@extends('admin.layout.master')

@section('title')
الموظفين
@stop

@section('css')

@stop
@section('subheader')
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    وزارة الأوقاف والشؤون الدينية </h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="{{ route('admin.dashboard.view') }}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="{{ route('admin.dashboard.view') }}" class="kt-subheader__breadcrumbs-link">
                        الرئيسية
                    </a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="{{ route('admin.employees.view') }}" class="kt-subheader__breadcrumbs-link">
                        الموظفين
                    </a>
                </div>
            </div>
        </div>
    </div>
@stop

@section('content')
    <!-- begin:: Content -->
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Portlet-->
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                البحث
                            </h3>
                        </div>
                    </div>
                    <!--begin::Form-->
                    <form class="kt-form kt-form--label-right">
                        <div class="kt-portlet__body">
                            <div class="form-group row">
                                <div class="col-lg-4">
                                    <input type="text" class="form-control searchable" id="name" name="name" placeholder="اسم الموظف">
                                </div>
                                <div class="col-lg-4">
                                    <input type="text" class="form-control searchable" id="identity_number" name="identity_number" placeholder="رقم الهوية">
                                </div>
                                <div class="col-lg-4">
                                    <input type="text" class="form-control searchable" id="phone_number" name="phone_number" placeholder="رقم الموبايل">
                                </div>
                            </div>
                        </div>
                    </form>
                    <!--end::Form-->
                </div>
                <!--end::Portlet-->
            </div>
        </div>
    <!-- End:: Content -->

    <!-- begin:: Content -->
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    الموظفين
                </h3>
            </div>
            @if(auth()->user()->can('admin.employees.add') || auth()->user()->can('admin.employees.export'))
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        @can('admin.employees.add')
                        <a href="{{ route('admin.employees.add') }}" class="btn btn-brand btn-elevate btn-icon-sm">
                            إضافة جديد
                        </a>
                        @endcan
                        @can('admin.employees.export')
                        <a href="{{ route('admin.employees.export') }}" class="btn btn-info btn-elevate btn-icon-sm">
                            تصدير اكسل
                        </a>
                        @endcan
                    </div>
                </div>
            </div>
            @endif
        </div>
        <div class="kt-portlet__body">
            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover" id="employees_table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>الصورة</th>
                    <th>الاسم</th>
                    <th>رقم الهوية</th>
                    <th>رقم الموبايل</th>
                    <th>المحافظة-المنطقة</th>
                    <th>الوظيفة</th>
                    <th>المسجد</th>
                    <th width="10%">أدوات</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>

            <!--end: Datatable -->
        </div>
    </div>
    <!-- end:: Content -->
@stop

@section('js')
    <script src="assets/admin/general/js/scripts/employees.js" type="text/javascript"></script>
@stop
