@extends('admin.layout.master')

@section('title')
    إضافة موظف
@stop

@section('css')

@stop

@section('subheader')
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    وزارة الأوقاف والشؤون الدينية </h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="{{ route('admin.dashboard.view') }}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="{{ route('admin.dashboard.view') }}" class="kt-subheader__breadcrumbs-link">
                        الرئيسية
                    </a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="{{ route('admin.employees.view') }}" class="kt-subheader__breadcrumbs-link">
                        الموظفين
                    </a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="{{ route('admin.employees.add') }}" class="kt-subheader__breadcrumbs-link">
                        إضافة موظف
                    </a>
                </div>
            </div>
        </div>
    </div>
@stop

@section('content')
    <!-- begin:: Content -->
    <div class="row">
        <div class="col-lg-12">
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            إضافة موظف
                        </h3>
                    </div>
                </div>
                <!--begin::Form-->
                <form class="kt-form kt-form--label-right" id="frmAdd" method="post" action="{{ route('admin.employees.store') }}" encrypt="multipart/form-data">
                    <div class="kt-portlet__body">
                        <div class="form-group row">
                            <div class="col-xl-12 text-center">
                                <div class="kt-avatar kt-avatar--outline kt-avatar--circle" id="kt_user_avatar_3">
                                    <div class="kt-avatar__holder" style="background-image: url(backend/img/default.jpg)"></div>
                                    <label class="kt-avatar__upload" data-toggle="kt-tooltip" title="" data-original-title="قم بتغيير الصورة">
                                        <i class="fa fa-pen"></i>
                                        <input type="file" name="avatar" accept=".png, .jpg, .jpeg">
                                    </label>
                                    <span class="kt-avatar__cancel" data-toggle="kt-tooltip" title="" data-original-title="إلغاء">
                                        <i class="fa fa-times"></i>
                                    </span>
                                </div>
                                <span class="form-text text-muted">يُسمح بالصيغ التالية: png, jpg, jpeg.</span>
                            </div>
                        </div> 
                        <div class="form-group row">
                            <div class="col-lg-4">
                                <label class="">الاسم رباعي:</label>
                                <input type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="اسم الموظف رباعي">
                            </div>
                            <div class="col-lg-4">
                                <label class="">رقم الهوية:</label>
                                <input type="text" class="form-control" name="identity_number" value="{{ old('identity_number') }}" placeholder="رقم هوية الموظف" onkeypress="return isNumber(event)">
                            </div>
                            <div class="col-lg-4">
                                <label class="">رقم الموبايل:</label>
                                <input type="text" class="form-control" name="phone_number" value="{{ old('phone_number') }}" placeholder="رقم جوال الموظف" onkeypress="return isNumber(event)">
                            </div>
                        </div>   
                        <div class="form-group row">
                            <div class="col-lg-4">
                                <label class="">الرقم الوظيفي:</label>
                                <input type="text" class="form-control" name="job_no" value="{{ old('job_no') }}" placeholder="رقم الهوية في حال كان غير متوفر">
                            </div>
                            <div class="col-lg-4">
                                <label class="">التصنيف الوظيفي:</label>
                                <select class="form-control kt-selectpicker" name="job_type" data-live-search="true" data-title="اختر قيمة">
                                    @foreach ($job_types as $job_type)
                                        <option value="{{$job_type->id}}">{{ $job_type->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-lg-4">
                                <label class="">الوظيفة:</label>
                                <select class="form-control kt-selectpicker" name="job_id" data-live-search="true" data-title="اختر قيمة">
                                    @foreach ($jobs as $job)
                                        <option value="{{$job->id}}">{{ $job->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>  
                        <div class="form-group row">
                            <div class="col-lg-4">
                                <label class="">بند التعيين:</label>
                                <select class="form-control kt-selectpicker" name="hiring_type" data-live-search="true" data-title="اختر قيمة">
                                    @foreach ($hiring_types as $hiring_type)
                                        <option value="{{$hiring_type->id}}">{{ $hiring_type->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-lg-4">
                                <label class="">تاريخ التعيين:</label>
                                <input type="text" class="form-control date" name="hiring_date" value="{{ old('hiring_date') }}" placeholder="{{now()->format('Y-m-d')}}" autocomplete="off">
                            </div>
                            <div class="col-lg-4">
                                <label class="">المؤهل العلمي:</label>
                                <input type="text" class="form-control" name="qualification" value="{{ old('qualification') }}" placeholder="آخر مؤهل علمي حصل عليه الموظف">
                            </div>
                        </div>  
                        <div class="form-group row">
                            <div class="col-lg-4">
                                <label class="">المحافظة:</label>
                                <select class="form-control kt-selectpicker searchable" data-live-search="true" id="province" name="province_id" data-title="اختر المحافظة">
                                    @foreach ($provinces as $province)
                                        <option value="{{$province->id}}">{{ $province->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-lg-4">
                                <label class="">المنطقة:</label>
                                <select class="form-control kt-selectpicker searchable" data-live-search="true" id="regions" name="region_id" data-title="اختر المنطقة">                                   
                                </select>
                            </div>
                            <div class="col-lg-4">
                                <label class="">المسجد:</label>
                                <select class="form-control kt-selectpicker searchable" data-live-search="true" id="mosques" name="mosque_id" data-title="اختر المسجد">                                   
                                </select>
                            </div>
                        </div>  
                        <div class="form-group row">
                            <div class="col-lg-12">
                                <label class="">عنوان الموظف كاملاً:</label>
                                <input type="text" class="form-control" name="address" value="{{ old('address') }}" placeholder="عنوان الموظف كاملاً">
                            </div>
                        </div>
                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <div class="row">
                                <div class="col-lg-6 kt-align-right">
                                    <button type="submit" class="btn btn-brand save">حفظ</button>
                                    <a href="{{ route('admin.employees.view') }}" class="btn btn-secondary">الغاء</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <!--end::Form-->
            </div>
            <!--end::Portlet-->
        </div>
    </div>
    <!-- End:: Content -->
@stop

@section('js')
    <!--begin::Page Scripts(used by this page) -->
    <script src="assets/admin/js/pages/crud/file-upload/ktavatar.js" type="text/javascript"></script>
    <script src="assets/admin/general/js/scripts/employees.js" type="text/javascript"></script>
@stop
