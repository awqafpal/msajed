@if(auth()->user()->can('admin.needRequests.edit') || auth()->user()->can('admin.needRequests.delete'))
    @can('admin.needRequests.edit')
        <a href="{{ route('admin.needRequests.edit', ['id' => $id]) }}" class="btn btn-outline-success btn-elevate-hover btn-circle btn-icon btn-sm" title="تعديل">
            <i class="la la-edit"></i>
        </a>
    @endcan
    @can('admin.needRequests.delete')
        <a href="javascript:;" data-url="{{ route('admin.needRequests.delete', ['id' => $id]) }}" class="btn btn-outline-danger btn-elevate-hover btn-circle btn-icon btn-sm delete_btn" title="حذف">
            <i class="la la-trash"></i>
        </a>
    @endcan
@endif
