@if ($row->mosque)
    <span class="btn-sm btn btn-label-success">{{$row->mosque->name}}</span>
@else
    <span class="btn-sm btn btn-label-danger">غير مدخل</span>
@endif
