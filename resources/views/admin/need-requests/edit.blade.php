@extends('admin.layout.master')

@section('title')
    تعديل طلب احتياج
@stop

@section('css')

@stop

@section('subheader')
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    وزارة الأوقاف والشؤون الدينية </h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="{{ route('admin.dashboard.view') }}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="{{ route('admin.dashboard.view') }}" class="kt-subheader__breadcrumbs-link">
                        الرئيسية
                    </a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="{{ route('admin.needRequests.view') }}" class="kt-subheader__breadcrumbs-link">
                        طلبات احتياج المساجد
                    </a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="{{ route('admin.needRequests.add' , $info->id) }}" class="kt-subheader__breadcrumbs-link">
                        تعديل طلب احتياج
                    </a>
                </div>
            </div>
        </div>
    </div>
@stop

@section('content')
    <!-- begin:: Content -->
    <div class="row">
        <div class="col-lg-12">
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            تعديل طلب احتياج
                        </h3>
                    </div>
                </div>
                <!--begin::Form-->
                <form class="kt-form kt-form--label-right" id="frmAdd" method="post" action="{{ route('admin.needRequests.update' , $info->id) }}">
                    <div class="kt-portlet__body">
                        <div class="form-group row">
                            <div class="col-lg-4"> 
                                <label class="">المسجد:</label>
                                <select class="form-control kt-selectpicker searchable" data-live-search="true" id="mosques" name="mosque_id" data-title="اختر المسجد">                                   
                                    @foreach ($mosques as $mosque)
                                        <option value="{{$mosque->id}}" {{ $mosque->id == $info->mosque_id ? 'selected' : ''}}>{{ $mosque->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-lg-4">
                                <label class="">نوع الطلب:</label>
                                <input type="text" class="form-control" name="request_type" value="{{ old('request_type', $info->request_type) }}" placeholder="نوع طلب الاحتياج">
                            </div>
                            <div class="col-lg-4">
                                <label class="">جهة تقديم الطلب:</label>
                                <input type="text" class="form-control" name="request_submit_side" value="{{ old('request_submit_side', $info->request_submit_side) }}" placeholder="جهة تقديم الطلب">
                            </div>
                        </div>  
                        <div class="form-group row">
                            <div class="col-lg-4"> 
                                <label class="">رقم الجوال:</label>
                                <input type="text" class="form-control" name="phone_number" value="{{ old('phone_number', $info->phone_number) }}" placeholder="رقم الجوال" onkeypress="return isNumber(event)">
                            </div>
                            <div class="col-lg-4">
                                <label class="">عنوان جهة تقديم الطلب:</label>
                                <input type="text" class="form-control" name="address" value="{{ old('address', $info->address) }}" placeholder="عنوان جهة تقديم الطلب">
                            </div>
                            <div class="col-lg-4">
                                <label class="">الكمية:</label>
                                <input type="text" class="form-control" name="request_quantity" value="{{ old('request_quantity', $info->request_quantity) }}" placeholder="الكمية المطلوبة">
                            </div>
                        </div>  
                        <div class="form-group row">
                            <div class="col-lg-4"> 
                                <label class="">القيمة التقديرية:</label>
                                <input type="text" class="form-control" name="estimated_value" value="{{ old('estimated_value', $info->estimated_value) }}" placeholder="القيمة التقديرية" >
                            </div>
                            <div class="col-lg-4"> 
                                <div class="kt-margin-t-25  float-left">
                                    <label class="col-form-label float-left kt-padding-r-5"> إعتماد الجهات المختصة </label>
                                    <span class="kt-switch kt-switch--outline kt-switch--icon kt-switch--success kt-margin-r-50"  >
                                        <label>
                                            <input type="checkbox" name="req_status" value="1" {{ old('req_status',$info->status) == 1 ? 'checked' : '' }}>
                                            <span></span>
                                        </label>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-12">
                                <label class="">وصف طلب الاحتياج:</label>
                                <textarea class="summernote" name="request_content" id="kt_summernote_1">{{old('request_content', $info->request_content)}}</textarea>
                            </div>
                        </div>    
                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <div class="row">
                                <div class="col-lg-6 kt-align-right">
                                    <button type="submit" class="btn btn-brand save">حفظ</button>
                                    <a href="{{ route('admin.needRequests.view') }}" class="btn btn-secondary">الغاء</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <!--end::Form-->
            </div>
            <!--end::Portlet-->
        </div>
    </div>
    <!-- End:: Content -->
@stop

@section('js')
    <script src="assets/admin/js/pages/crud/forms/editors/summernote.js" type="text/javascript"></script>
    <script src="assets/admin/general/js/scripts/need-requests.js" type="text/javascript"></script>
@stop
