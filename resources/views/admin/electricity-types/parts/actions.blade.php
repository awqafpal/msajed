@if(auth()->user()->can('admin.electricityTypes.edit') || auth()->user()->can('admin.electricityTypes.delete'))
    @can('admin.electricityTypes.edit')
        <a href="{{ route('admin.electricityTypes.edit', ['id' => $id]) }}" class="btn btn-outline-success btn-elevate-hover btn-circle btn-icon btn-sm" title="تعديل">
            <i class="la la-edit"></i>
        </a>
    @endcan
    @can('admin.electricityTypes.delete')
        <a href="javascript:;" data-url="{{ route('admin.electricityTypes.delete', ['id' => $id]) }}" class="btn btn-outline-danger btn-elevate-hover btn-circle btn-icon btn-sm delete_btn" title="حذف">
            <i class="la la-trash"></i>
        </a>
    @endcan
@endif
