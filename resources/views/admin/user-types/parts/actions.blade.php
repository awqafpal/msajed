@if(auth()->user()->can('admin.userTypes.edit') || auth()->user()->can('admin.userTypes.delete'))
    @can('admin.userTypes.edit')
        <a href="{{ route('admin.userTypes.edit', ['id' => $id]) }}" class="btn btn-outline-success btn-elevate-hover btn-circle btn-icon btn-sm" title="تعديل">
            <i class="la la-edit"></i>
        </a>
    @endcan
    @can('admin.userTypes.delete')
        <a href="javascript:;" data-url="{{ route('admin.userTypes.delete', ['id' => $id]) }}" class="btn btn-outline-danger btn-elevate-hover btn-circle btn-icon btn-sm delete_btn" title="حذف">
            <i class="la la-trash"></i>
        </a>
    @endcan
@endif
