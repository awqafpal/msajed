
@if(auth()->user()->can('admin.inspectors.mosques') || auth()->user()->can('admin.inspectors.mosques.edit'))
    @can('admin.inspectors.mosques')
        <a href="{{ route('admin.inspectors.mosques', ['id' => $id_hash]) }}" class="btn btn-outline-info btn-elevate-hover btn-circle btn-icon btn-sm" title="عرض مساجد المفتش">
            <i class="la la-eye"></i>
        </a>
    @endcan
    &nbsp;
    @can('admin.inspectors.mosques.edit')
        <a href="{{ route('admin.inspectors.mosques.edit', ['id' => $id_hash]) }}" class="btn btn-outline-success btn-elevate-hover btn-circle btn-icon btn-sm" title="تعديل مساجد المفتش">
            <i class="la la-edit"></i>
        </a>
    @endcan
@endif
