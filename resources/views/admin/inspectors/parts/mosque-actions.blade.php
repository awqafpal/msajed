@if(auth()->user()->can('admin.inspectors.mosques.delete'))
    @can('admin.inspectors.mosques.delete')
        <a href="javascript:;" data-url="{{ route('admin.inspectors.mosques.delete', ['id' => $id]) }}" class="btn btn-outline-danger btn-elevate-hover btn-circle btn-icon btn-sm delete_btn" title="حذف">
            <i class="la la-trash"></i>
        </a>
    @endcan
@endif
