@extends('admin.layout.master')

@section('title')
    الرئيسية
@stop

@section('css')

@stop

@section('subheader')
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    وزارة الأوقاف والشؤون الدينية </h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="{{ route('admin.dashboard.view') }}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="{{ route('admin.dashboard.view') }}" class="kt-subheader__breadcrumbs-link">
                        الرئيسية </a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="{{ route('admin.inspectors.mosques',$info->id) }}" class="kt-subheader__breadcrumbs-link">
                        تعديل مساجد المفتش </a>
                </div>
            </div>
        </div>
    </div>
@stop

@section('content')
    <!-- begin:: Content -->
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    تعديل مساجد المفتش- {{$info->name}}
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body">
            <!--begin::Form-->
            <form class="kt-form kt-form--label-right" id="frmEdit" method="post" action="{{ route('admin.inspectors.mosques.update', ['id' => $info->id]) }}" encrypt="multipart/form-data">
                @csrf
                <input type="hidden" id="inspector_id" value="{{$info->id}}" />
                <div class="kt-portlet__body">
                    <div class="form-group row">
                        <div class="col-lg-12 kt-mb-20">
                            <span class="badge badge-danger">
                                 منطقة المفتش :{{$info->region->name}}
                            </span>
                        </div>
                        @foreach($inspector_region_mosques as $mosque)
                            <div class="col-lg-3">
                                <label class="kt-checkbox kt-checkbox--solid">
                                    <input class="checkboxInput"   name="mosques[]"
                                        value="{{ $mosque->id }}"
                                        {{ in_array($mosque->id ,$info->mosques->pluck('id')->toArray()) ? 'checked' : '' }}
                                        type="checkbox"  id="inlineCheckbox{{ $mosque->id }}">
                                    <span> </span>
                                </label>
                                {{$mosque->name}}
                            </div>
                        @endforeach
                    </div>
                    <div class="kt-separator kt-separator--border-dashed kt-separator--space-lg kt-separator--portlet-fit"></div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <span class="badge badge-danger">مناطق المديرية الأخرى</span>
                        </div>
                    </div>

                    @foreach($other_regions_mosques as $region=>$other_mosque)
                        <div class="form-group row">
                            <div class="col-lg-12 kt-mb-20">
                                <span class="badge badge-info">{{ $loop->iteration}}. {{$region}} </span>
                            </div>
                            @if (count($other_mosque) > 0)
                                @foreach($other_mosque as $msq)
                                <div class="col-lg-3">
                                    <label class="kt-checkbox kt-checkbox--solid">
                                        <input class="checkboxInput" name="mosques[]"
                                            value="{{ $msq->id }}"
                                            {{ in_array($msq->id ,$info->mosques->pluck('id')->toArray()) ? 'checked' : '' }}
                                            type="checkbox"  id="inlineCheckbox{{ $msq->id }}">
                                        <span> </span>
                                    </label>
                                    {{$msq->name}}
                                </div>
                                @endforeach
                            @else
                                <div class="col-lg-12">
                                    <span class="badge badge-success col-sm-12">
                                        <i class="fa fa-check-circle" style="margin-left:10px"></i>
                                        <strong>تنبيه :  جميع مساجد المنطقة منسبة لمفتشي مساجد أخرين</strong>
                                    </span>
                                </div>
                            @endif
                        </div>
                        <div class="kt-separator kt-separator--border-dashed kt-separator--space-lg kt-separator--portlet-fit"></div>
                    @endforeach
                </div>
                <div class="kt-portlet__foot">
                    <div class="kt-form__actions">
                        <div class="row">
                            <div class="col-lg-6 kt-align-right">
                                <button type="submit" class="btn btn-brand save">حفظ</button>
                                <a href="{{ route('admin.inspectors.mosques', ['id' => $info->id]) }}" class="btn btn-secondary">الغاء</a>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <!--end::Form-->

            <div class="row">

            </div>
        </div>
    </div>
    <!-- end:: Content -->
@stop

@section('js')
    <script src="assets/admin/general/js/scripts/inspector-mosques.js" type="text/javascript"></script>
@stop
