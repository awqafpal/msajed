@if(auth()->user()->can('admin.alternativeEnergies.edit') || auth()->user()->can('admin.alternativeEnergies.delete'))
    @can('admin.alternativeEnergies.edit')
        <a href="{{ route('admin.alternativeEnergies.edit', ['id' => $id]) }}" class="btn btn-outline-success btn-elevate-hover btn-circle btn-icon btn-sm" title="تعديل">
            <i class="la la-edit"></i>
        </a>
    @endcan
    @can('admin.alternativeEnergies.delete')
        <a href="javascript:;" data-url="{{ route('admin.alternativeEnergies.delete', ['id' => $id]) }}" class="btn btn-outline-danger btn-elevate-hover btn-circle btn-icon btn-sm delete_btn" title="حذف">
            <i class="la la-trash"></i>
        </a>
    @endcan
@endif
