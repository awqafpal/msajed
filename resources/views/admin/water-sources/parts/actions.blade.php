@if(auth()->user()->can('admin.waterSources.edit') || auth()->user()->can('admin.waterSources.delete'))
    @can('admin.waterSources.edit')
        <a href="{{ route('admin.waterSources.edit', ['id' => $id]) }}" class="btn btn-outline-success btn-elevate-hover btn-circle btn-icon btn-sm" title="تعديل">
            <i class="la la-edit"></i>
        </a>
    @endcan
    @can('admin.waterSources.delete')
        <a href="javascript:;" data-url="{{ route('admin.waterSources.delete', ['id' => $id]) }}" class="btn btn-outline-danger btn-elevate-hover btn-circle btn-icon btn-sm delete_btn" title="حذف">
            <i class="la la-trash"></i>
        </a>
    @endcan
@endif
