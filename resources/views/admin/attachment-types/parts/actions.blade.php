@if(auth()->user()->can('admin.attachmentTypes.edit') || auth()->user()->can('admin.attachmentTypes.delete'))
    @can('admin.attachmentTypes.edit')
        <a href="{{ route('admin.attachmentTypes.edit', ['id' => $id]) }}" class="btn btn-outline-success btn-elevate-hover btn-circle btn-icon btn-sm" title="تعديل">
            <i class="la la-edit"></i>
        </a>
    @endcan
    @can('admin.attachmentTypes.delete')
        <a href="javascript:;" data-url="{{ route('admin.attachmentTypes.delete', ['id' => $id]) }}" class="btn btn-outline-danger btn-elevate-hover btn-circle btn-icon btn-sm delete_btn" title="حذف">
            <i class="la la-trash"></i>
        </a>
    @endcan
@endif
